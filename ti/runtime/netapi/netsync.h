/*********************************************
 * File: netsync.h
 * Purpose: NETAPI Synchronization primitives
 **************************************************************
 * FILE: netsync.h
 * 
 * DESCRIPTION:  netapi synch utilities header file for user space transport
 *               library
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 **********************************************/
#ifndef NETAPI_SYNC_H
#define NETAPI_SYNCH_H

#ifdef __cplusplus
extern "C" {
#endif
#include "hplib_sync.h"

/*--------------------------*/
/*----------spinLock--------*/
/*--------------------------*/

#define  NETAPI_spinLock_T hplib_spinLock_T

#define NETAPI_spinLock_LOCKVAL hplib_spinLock_LOCKVAL
#define NETAPI_spinLock_UNLOCKVAL hplib_spinLock_UNLOCKVAL
#define NETAPI_spinLock_UNLOCKED_INITIALIZER (NETAPI_spinLock_UNLOCKVAL)

/* init a lock */
#define netapi_spinLock_init hplib_mSpinLockInit

/* lock a spinLock */
#define netapi_spinLock_lock hplib_mSpinLockLock


/* try to get lock 1 time. Return 1 if ok, 0 if un-successful */
#define netapi_spinLock_try_lock hplib_mSpinLockTryLock

/* unlock a spinLock.   */
#define netapi_spinLock_unlock hplib_mSpinLockUnlock

/* poll a lock, return 0 if unlocked, NETAPI_spinLock_LOCKVAL if locked */
#define netapi_spinLock_is_locked hplib_mSpinLockIsLocked


/*--------------------------*/
/*----------rwLock--------*/
/*--------------------------*/

/* a rw lock strucuture */
#define  NETAPI_rwLock_T hplib_rwLock_T

//initialize a rw lock 
#define netapi_rwLock_init hplib_mRWLockInit

// lock a write lock.  
#define netapi_rwLock_write_lock hplib_mRWLockWriteLock


//unlock a writer part of rwLock */
#define netapi_rwLock_write_unlock hplib_mRWLockWriteUnlock

//grab a read lock
//=> can be other readers, but no writer
#define netapi_rwLock_read_lock hplib_mRWLockReadLock

//rw_lock reader unlock
#define netapi_rwLock_read_unlock hplib_mRWLockReadUnlock


/*--------------------------*/
/*----------atomic32--------*/
/*--------------------------*/
#define NETAPI_ATOMIC32_T hplib_atomic32_T



#define NETAPI_ATOMIC_INIT32 hplib_mAtomic32Init

#define netapi_atomic_read32 hplib_mAtomic32Read

#define netapi_atomic_set32 hplib_mAtomic32Set

#define netapi_atomic_add32 hplib_mAtomic32Add

#define netapi_atomic_sub32 hplib_mAtomic32Sub

#define NETAPI_atomic_inc32 hplib_mAtomic32Inc

#define NETAPI_atomic_dec32(p) hplib_mAtomic32Dec

#define netapi_atomic_add_return32 hplib_mAtomic32AddReturn

#define netapi_atomic_sub_return32 hplib_mAtomic32SubReturn


#define netapi_atomic_inc_and_test32 hplib_mAtomic32IncAndTest

#define netapi_atomic_dec_and_test32 hplib_mAtomic32DecAndTest

#define netapi_atomic_test_and_set32 hplib_mAtomic32TestSetReturn

#define netapi_atomic_clear32(p) hplib_mAtomic32Clear

/*--------------------------*/
/*----------atomic64--------*/
/*--------------------------*/
#define NETAPI_ATOMIC64_T hplib_atomic64_T


#define NETAPI_ATOMIC_INIT64(x) hplib_mAtomic64Init

#define netapi_atomic_read64 hplib_mAtomic64Read

#define netapi_atomic_set64 hplib_mAtomic64Set

#define netapi_atomic_add64 hplib_mAtomic64Add

/*******************************************************
 ****************memory barrier************************
******************************************************/
#define netapi_mb hplib_mMemBarrier
#define netapi_rmb hplib_mReadMemBarrier
#define netapi_wmb hplib_mWriteMemBarrier


#ifdef __cplusplus
}
#endif
#endif
