/******************************************************
 *  File: netapi_util.h
 *  Purpose:  misc utilites
 **************************************************************
 * FILE: netapi_util.h
 * 
 * DESCRIPTION:  netapi utility header file for user space transport
 *               library
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************************/
#ifndef __NETAPI_UTIL__H
#define __NETAPI_UTIL__H


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "./src/netapi_loc.h"


//#define NETAPI_DEBUG
#ifdef NETAPI_DEBUG
#define netapi_Log printf
#else
#define netapi_Log
#endif

extern NETAPI_SHM_T* pnetapiShm;

/* Wrapper functions around HPLIB APIs for QMSS , *utility to convert virt2phy, phy2virt */
#define _Osal_qmssVirtToPhy hplib_mVMVirtToPhy

#define _Osal_qmssPhyToVirt hplib_mVMPhyToVirt

//static hplib_spinLock_T netapi_util_lock = hplib_spinLock_UNLOCKED_INITIALIZER;

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_registerHeap  API is used to register a heap that is created by application so that
 *                              it's garbage queue can be polled automatically by @ref netapi_pollHeapGarbage.
 *
 *  @details This API registers an application-created heap with the netapi instance
 *           so that it can add that heap's garbage queue to the garbage poll function. 
 *           NOTE: netapi internal heap is automatically registered
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @param[in]  h   Handle of pklib heap to register
 *  @retval     1 if OK, <0 on error
 *  @pre        @ref netapi_init
 */
static inline int netapi_registerHeap(NETAPI_T p,
                                      Pktlib_HeapHandle h)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    int i;
    pp->spinLock.lock(&pnetapiShm->netapi_util_lock);
    for(i=0;i<TUNE_NETAPI_MAX_HEAPS;i++)
    {
        if (!pp->createdHeaps[i])
        {
            pp->createdHeaps[i]=h;
            pp->spinLock.unlock(&pnetapiShm->netapi_util_lock);
            return 1;
        }
    }
    pp->spinLock.unlock(&pnetapiShm->netapi_util_lock);
    return -1;
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_unregisterHeap  API is used to un-register a heap that was created by application 
 *                                and previously registered so that it's garbage queue could  be polled 
 *                                automatically by @ref netapi_pollHeapGarbage. 
 *
 *  @details This API un-registers an application-created heap with the netapi instance. 
 *           NOTE: netapi internal heap is automatically registered
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @param[in]  h   The pklib handle to heap 
 *  @retval     <0 if err, 1 if OK
 *  @pre        @ref netapi_init
 */
static inline int netapi_unregisterHeap(NETAPI_T p,
                                        Pktlib_HeapHandle h)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    int i;
    pp->spinLock.lock(&pnetapiShm->netapi_util_lock);
    for(i=0;i<TUNE_NETAPI_MAX_HEAPS;i++)
    {
            if (pp->createdHeaps[i] == h)
            {
                pp->createdHeaps[i]=NULL;
                pp->spinLock.unlock(&pnetapiShm->netapi_util_lock);
                return 1;
            }
    }
    pp->spinLock.unlock(&pnetapiShm->netapi_util_lock);
    return -1;
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_closeHeap  API is used to remove a created pktlib heap
 *
 *  @details This API removes an application-created heap with the netapi instance
 *           NOTE: descriptors are zapped and cannot be reused]
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @param[in]  h :  handle to pklib heap 
 *  @retval     <0 if err, 1 if OK
 *  @pre        @ref netapi_init  @ref netapi_registerHeap
 */
static inline int netapi_closeHeap(NETAPI_T p,
                                   Pktlib_HeapHandle h)
{
    int err = 0;

    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    Qmss_QueueHnd q;
    Pktlib_garbageCollection(h);

    Pktlib_deleteHeap(h, &err);

    return 1;
}
/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getCookie  API is used to return a piece of application-provided opaque data that has been
 *                           stored in the netapi instance.
 *
 *  @details The application can save a pointer to opaque data in the @ref NETAPI_T instance.
 *           This APi lets this data be returned to the application.
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     Data provided in @ref netapi_setCookie
 *  @pre        @ref netapi_init, @ref netapi_setCookie
 */

static inline void * netapi_getCookie(NETAPI_T p)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    return pp->cookie;
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_setCookie  API is used to set a piece of application-provided opaque data t in the netapi instance.
 *
 *  @details The applicaiton can save a pointer to opaque data in the @ref NETAPI_T instance.
 *           Pointer to a opaque data can be returned later to the application via @ref netapi_getCookie
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @param[in]  cookie Opaque data to be saved
 *  @retval     none
 *  @pre        @ref netapi_init
 */
static inline void netapi_setCookie(NETAPI_T p,
                                    void * cookie)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    pp->cookie= cookie;
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getBufMemRemainder  API is used to return the amount of free memory available for
 *         allocating buffers for additonal Pktlib heaps
 *
 *  @details The application can use this API to determine how much free memory is
 *           available for heap buffers if it decides to create its own.
 *  @retval     Amount of memory available for heap buffer storage (in bytes)
 *  @pre        @ref netapi_init 
 */
static inline int  netapi_getBufMemRemainder(void)
{
     return hplib_vmGetMemPoolRemainder(0);
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getDefaultFlow:  API is used to return the default NETCP flow that is to
 *                                 be used for incoming packets.
 *
 *  @details   The application can use this API to return the default NETCP flow that is used
 *             for received packets. A NETCP flow is a list of PacketLib Heaps that are to be
 *             used to supply free packets to the receive DMA function.
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     NETCP_CFG_FLOW_HANDLE_T     The handle to default flow
 *  @pre        @ref netapi_init 
 */
static inline NETCP_CFG_FLOW_HANDLE_T netapi_getDefaultFlow(NETAPI_T p)
{
    return NETCP_DEFAULT_FLOW;
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getDefaultRoute:  API is used to return the default NETCP route handle.
 *
 *  @details This API returns the default NETCP route created by @ref netapi_init.
 *           A netcp route consists of a NETCP flow plus a destination pktio channel 
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     The handle of the default route, NETCP_CFG_ROUTE_HANDLE_T
 *  @pre        @ref netapi_init  
 */
static inline NETCP_CFG_ROUTE_HANDLE_T netapi_getDefaultRoute(NETAPI_T p)
{
    return NETCP_DEFAULT_ROUTE;
}

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgGetPolicyUserData    API to retrieve user mode data associated with 
 *                                             Policy APPID.
 *
 *  @details This api is used to retrieve user mode data associated with an Policy APPID
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  app_id  application id whose user mode data is to be retrieved
 *  @retval void*   pointer to user mode data.
 *  @pre       @ref netapi_init 
 */
static inline void*  netapi_netcpCfgGetPolicyUserData(NETAPI_T h,
                                                      NETCP_CFG_SA_T app_id)
{
    NETAPI_NWAL_GLOBAL_CONTEXT_T *p = &netapi_get_global()->nwal_context;
    int slot = netapi_cfgGetMatchId(app_id);
    if ((slot <0 ) || (slot >= TUNE_NETAPI_MAX_POLICY))
    {
        return NULL;
    }
    return (p->policy[slot].user_data);
}

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgGetIpSecUserData    API to retrieve user mode data associated with IPSEC APPID.
 *
 *  @details This api is used to retrieve user mode data associated with an IPSEC APPID
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  app_id  application id whose user mode data is to be retrieved
 *  @retval void*   pointer to user mode data.
 *  @pre       @ref netapi_init 
 */
static inline void*  netapi_netcpCfgGetIpSecUserData(NETAPI_T h,
                                                     NETCP_CFG_SA_T app_id)
{
    NETAPI_NWAL_GLOBAL_CONTEXT_T *p = &netapi_get_global()->nwal_context;
    int slot = netapi_cfgGetMatchId(app_id);
    if ((slot <0 ) || (slot >= TUNE_NETAPI_MAX_SA))
    {
        return NULL;
    }
    return (p->tunnel[slot].user_data);
}


/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgGetIpUserData    API to retrieve user mode data associated with Generic IP APPID.
 *
 *  @details This api is used to retrieve user mode data associated with a Generic IP APPID
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  app_id  application id whose user mode data is to be retrieved
 *  @retval void*   pointer to user mode data.
 *  @pre       @ref netapi_init 
 */
static inline void*  netapi_netcpCfgGetIpUserData(NETAPI_T h,
                                                  NETCP_CFG_SA_T app_id)
{
    NETAPI_NWAL_GLOBAL_CONTEXT_T *p = &netapi_get_global()->nwal_context;
    int slot = netapi_cfgGetMatchId(app_id);
    if ((slot <0 ) || (slot >= TUNE_NETAPI_MAX_NUM_IP))
    {
        return NULL;
    }
    return (p->ips[slot].user_data);
}

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgGetClassiferUserData  API to retrieve user mode data associated with Classifer APPID.
 *
 *  @details This api is used to retrieve user mode data associated with a flassifier APPID
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  app_id  application id whose user mode data is to be retrieved
 *  @retval void*   pointer to user mode data.
 *  @pre       @ref netapi_init 
 */
static inline void*  netapi_netcpCfgGetClassiferUserData(NETAPI_T h,
                                                         NETCP_CFG_SA_T app_id)
{
    NETAPI_NWAL_GLOBAL_CONTEXT_T *p = &netapi_get_global()->nwal_context;
    int slot = netapi_cfgGetMatchId(app_id);
    if ((slot <0 ) || (slot >= TUNE_NETAPI_MAX_CLASSIFIERS))
    {
        return NULL;
    }
    return (p->classi[slot].user_data);
}


/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgGetUserData    API to retrieve user mode data associated with APPID.
 *
 *  @details This api is used to retrieve user mode data associated with an APPID
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  app_id  application id whose user mode data is to be retrieved
 *  @retval void*   pointer to user mode data.
 *  @pre       @ref netapi_init 
 */
static inline void*  netapi_netcpCfgGetUserData(NETAPI_T h,
                                                NETCP_CFG_SA_T app_id)
{
    NETCP_CFG_SA_T appIdType;
    NETAPI_NWAL_GLOBAL_CONTEXT_T *p = &netapi_get_global()->nwal_context;

    appIdType = app_id & 0xff000000;
    switch(appIdType)
    {
        case(NETAPI_NETCP_MATCH_IPSEC):
            return (netapi_netcpCfgGetIpSecUserData(h, app_id));
            break;
        case(NETAPI_NETCP_MATCH_IPSEC_POLICY):
            return (netapi_netcpCfgGetPolicyUserData(h, app_id));
            break;
        case(NETAPI_NETCP_MATCH_GENERIC_IP):
            return (netapi_netcpCfgGetIpUserData(h, app_id));
            break;
        case(NETAPI_NETCP_MATCH_CLASS):
            return (netapi_netcpCfgGetClassiferUserData(h, app_id));
            break;
        default:
            return NULL;
            break;
    }
}

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgUpdateUserData    API to update user mode data associated with APPID.
 *
 *  @details This api is used to update user mode data associated with an APPID
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  app_id  application id whose user mode data is to updated
 *  @retval void*   pointer to user mode data.
 *  @pre       @ref netapi_init 
 */
static inline netapi_RetValue netapi_netcpCfgUpdateUserData(NETAPI_T h,
                                                            NETCP_CFG_SA_T app_id,
                                                            void * user_data)
{
    NETCP_CFG_SA_T appIdType;
    NETAPI_NWAL_GLOBAL_CONTEXT_T *p = &netapi_get_global()->nwal_context;

    int slot = netapi_cfgGetMatchId(app_id);
    if ((slot <0 ) || (slot >= TUNE_NETAPI_MAX_CLASSIFIERS))
    {
        return NETAPI_ERR_BAD_INPUT;
    }
    appIdType = app_id & 0xff000000;
    switch(appIdType)
    {
        case(NETAPI_NETCP_MATCH_IPSEC):
            p->tunnel[slot].user_data = user_data;
            return NETAPI_ERR_OK;
            break;
        case(NETAPI_NETCP_MATCH_IPSEC_POLICY):
            p->policy[slot].user_data = user_data;
            return NETAPI_ERR_OK;
            break;
        case(NETAPI_NETCP_MATCH_GENERIC_IP):
            p->ips[slot].user_data = user_data;
            return NETAPI_ERR_OK;
            break;
        case(NETAPI_NETCP_MATCH_CLASS):
            p->classi[slot].user_data = user_data;
            return NETAPI_ERR_OK;
            break;
        default:
            return NETAPI_ERR_BAD_INPUT;
            break;
    }
}

#ifdef __cplusplus
}
#endif


#endif
