#add local ips
ifconfig eth0:1 10.0.0.10
ifconfig eth0:2 192.168.1.10

#install netapi evm ip addresses into arp cache
arp -v -n -s  10.0.0.100 00:01:02:03:05:05
arp -v -n -s 192.168.1.100 00:01:02:03:05:05
