if [ $# -ne 2 ]; then
    echo "Usage ./sectest.sh <dest udp port> <dest ip address>"
    exit
fi
sudo setkey -f setkeycleanup.conf
sudo setkey -f setkey_hmac-sha1_aes-cbc.conf
echo "Sending packet with hmac-sha1_aes-cbc"
../udpif S hmac-sha1_aes-cbc.txt $1 $2 
sleep 1
#
echo "Cleaning up SAD entries"
sudo setkey -f setkeycleanup.conf
echo "Sending packet with aes_gcm"
./aes_gcm.sh
../udpif S aes_gcm.txt $1 $2 
sleep 1
#
echo "Cleaning up SAD entries"
sudo setkey -f setkeycleanup.conf
echo "Sending packet with aes_ccm"
./aes_ccm.sh
../udpif S aes_ccm.txt $1 $2
sleep 1
#
echo "Cleaning up SAD entries"
sudo setkey -f setkeycleanup.conf
echo "Sending packet with aes_xcbc"
sudo setkey -f setkey_aes_xcbc.conf
../udpif S aes_xcbc.txt $1 $2
sleep 1
#
echo "Cleaning up SAD entries"
sudo setkey -f setkeycleanup.conf
echo "Sending packet with hmac-sha256_aes-ctr"
sudo setkey -f hmac-sha256_aes-ctr.conf 
../udpif S hmac-sha256_aes_ctr.txt $1 $2
sleep 1
#
echo "Cleaning up SAD entries"
sudo setkey -f setkeycleanup.conf
echo "Sending packet with hmac-md5 AH mode"
sudo setkey -f setkey_hmac-md5_ah.conf 
../udpif S hmac_md5_ah.txt $1 $2
sleep 1

sudo setkey -f setkeycleanup.conf

#sudo setkey -f setkeycleanup.conf
#sudo setkey -f setkey_3des_cbc.conf 
#./udpif S 3des_cbc.txt $1   $2
