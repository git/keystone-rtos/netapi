
sudo ip xfrm state add src 192.168.1.10 dst 192.168.1.100 proto esp spi 0x55555555 mode tunnel reqid 100 replay-window 64 aead "rfc4106(gcm(aes))" 0x00112233445566778899aabbccddeeff00112233 128;

sudo ip xfrm policy add dir out src 192.168.1.10 dst 192.168.1.100 proto udp tmpl src 192.168.1.10 dst 192.168.1.100 proto esp mode tunnel reqid 100;

sudo ip xfrm state add src 192.168.1.100 dst 192.168.1.10 proto esp spi 0x55555555 mode tunnel reqid 100 replay-window 64 aead "rfc4106(gcm(aes))" 0x00112233445566778899aabbccddeeff00112233 128;

sudo ip xfrm policy add dir in src 192.168.1.100 dst 192.168.1.10 proto udp tmpl src 192.168.1.100 dst 192.168.1.10 proto esp mode tunnel reqid 100;
