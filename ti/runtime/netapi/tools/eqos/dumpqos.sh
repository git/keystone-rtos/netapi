#!/bin/sh
#dump L2 Shaper Config and Stats
#
echo "**************eth0 L2 QOS:*********************"
echo " "
echo "HP Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-hp/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-hp/packets_discarded`
echo "  - Bytes Sent      = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-hp/bytes_forwarded`
echo "  - Bytes Dropped   = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-hp/bytes_discarded`
echo " "

echo "WRR0 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr0/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr0/packets_discarded`
echo "  - Bytes Sent      = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr0/bytes_forwarded`
echo "  - Bytes Dropped   = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr0/bytes_discarded`
echo " "

echo "WRR1 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr1/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr1/packets_discarded`
echo "  - Bytes Sent      = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr1/bytes_forwarded`
echo "  - Bytes Dropped   = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr1/bytes_discarded`
echo " "

echo "WRR2 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr2/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr2/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr2/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr2/bytes_discarded`
echo " "

echo "WRR3 Queue:"
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr3/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr3/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr3/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr3/bytes_discarded`
echo " "

echo "WRR4 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr4/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr4/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr4/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr4/bytes_discarded`
echo " "

echo "WRR5 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr5/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr5/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr5/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-wrr5/bytes_discarded`
echo " "

echo "BE Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-be/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-be/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-be/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l21-be/bytes_discarded`
echo " "

echo "***********eth1 L2 QOS:**********************"
echo " "
echo "HP Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-hp/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-hp/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-hp/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-hp/bytes_discarded`
echo " "

echo "WRR0 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr0/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr0/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr0/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr0/bytes_discarded`
echo " "

echo "WRR1 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr1/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr1/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr1/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr1/bytes_discarded`
echo " "

echo "WRR2 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr2/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr2/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr2/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr2/bytes_discarded`
echo " "

echo "WRR3 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr3/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr3/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr3/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr3/bytes_discarded`
echo " "

echo "WRR4 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr4/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr4/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr4/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr4/bytes_discarded`
echo " "

echo "WRR5 Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr5/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr5/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr5/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-wrr5/bytes_discarded`
echo " "

echo "BE Queue: "
echo "  - Packets Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-be/packets_forwarded`
echo "  - Packets Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-be/packets_discarded`
echo "  - Bytes   Sent    = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-be/bytes_forwarded`
echo "  - Bytes   Dropped = " `cat /sys/devices/soc.0/hwqueue.6/qos-inputs-2//statistics/l22-be/bytes_discarded`
echo " "
