/*
 *
 * udpIf.c - provides udp 
 *
 */
//#define BENCHMARK
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <sys/time.h>

int nSent=0;

#ifdef BENCHMARK
struct timeval stime,mtime,etime;
#endif

#define SENDSZ 1396
int SendSize=SENDSZ;

#define MAIN_NEEDED
#ifdef MAIN_NEEDED
/******************************************************************************
    main
*******************************************************************************/
/* argv[1] = S for Send, R for Receive
   argv[2] = fileName
   argv[3] = port
   argv[4] = ipAddress
   argv[5] = send size
*/
main(argc,argv)
int argc;
char **argv;
{
int  sending = 0;
char fileName[256];
int  port;
char ipAddress[64];
    if (argc < 4) 
        {
        printf ("usage:udpIf S|R fileName port [ipAddress] [send size]\n");
        exit(-1);
        }
    sending = (*argv[1] == 'S') ? 1 : 0;
    strcpy(fileName,argv[2]);
    port = atoi(argv[3]);
    if (argc >= 5)
        strcpy(ipAddress,argv[4]);
    else
        strcpy(ipAddress,"");
    if (argc ==6) SendSize=atoi(argv[5]);
    if (SendSize > SENDSZ) SendSize=SENDSZ;
    udpif(sending,fileName,port,ipAddress);
}
#endif
char startstring[]="start";
char endstring[]="done";
/******************************************************************************
    udpif
*******************************************************************************/
udpif(sending,fileName,port,ipAddress)
int  sending;
char *fileName;
int  port;
char *ipAddress;
{
struct sockaddr_in addr,fraddr;
int    addrlen, fd, cnt;
socklen_t fraddrlen;
char message[SENDSZ+3];
int  numTries = 0;
#define MAX_TRIES 5
int rc;
int sstate=0;
int total, rtotal;
total=rtotal=0; 
    if (sending)
        {    
        int inFile;
        inFile = open(fileName,O_RDONLY);
        if (inFile < 0)
            {
            printf ("Can't read %s in tcpIf\n",fileName);
            exit(-1);
            }
        fd = socket(AF_INET, SOCK_DGRAM, 0);
        printf ("socket is %d\n",fd);
        if (fd < 0) 
            {
            perror("socket");
            exit(1);
            }
        bzero(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = INADDR_ANY;
        addr.sin_port = 40000;
        addrlen = sizeof(addr);
        rc = bind(fd, &addr, sizeof(addr));
        printf ("Sender:bind rc is %d\n",rc);
        if (rc < 0) 
            {
            perror("bind");
            exit(1);
            }
        addr.sin_addr.s_addr = inet_addr(ipAddress);
        addr.sin_port = htons(port);
        printf("dbg: %x %x\n", addr.sin_addr.s_addr, addr.sin_port);
#ifdef BENCHMARK
               sendto(fd,
                     startstring,
                     strlen(startstring)+1,
                     0,&addr,sizeof(addr));
#endif
        while(1)
            {
            char *mPtr;
            int len;
            int count = read(inFile,message,/*4096*/ /*SENDSZ*/SendSize);
#ifndef BENCHMARK
            //printf ("Sender:in count is %d\n",count);
#endif
            if (count == 0) 
                {
                close(inFile);
#ifdef BENCHMARK
               sendto(fd,
                     endstring,
                     strlen(endstring)+1,
                     0,&addr,sizeof(addr));
                                     
 sendto(fd,
                     endstring,
                     strlen(endstring)+1,
                     0,&addr,sizeof(addr));
 sendto(fd,
                     endstring,
                     strlen(endstring)+1,
                     0,&addr,sizeof(addr));
               printf("sending done %d\n",nSent);
               sleep(1);
#endif
                printf("sending done %d\n",nSent);

                break;
                }
            mPtr = message;
            len = count==-1 ? 5: count;
            do {
                cnt = sendto(fd, 
                     mPtr,
                     len,
                     0,&addr,sizeof(addr));
#ifndef BENCHMARK
               // printf ("Sender:sendto count is %d\n",cnt);
#endif
                if (cnt < 0) 
                    {
                    close(inFile);
                    perror("sendto");
                    exit(1);
                    }
                nSent+=1;
                mPtr += cnt;
                len -= cnt;
                } while(len > 0);
            }
        } 
    else 
        { 
        /* receive */
        int outFile;
        int newSocket;
        int rc;
        fd = socket(AF_INET, SOCK_DGRAM, 0);
        printf ("socket is %d\n",fd);
        if (fd < 0) 
            {
            perror("socket");
            exit(1);
            }
        bzero(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        addr.sin_port = htons(port);
        addrlen = sizeof(addr);
        rc = bind(fd, &addr, sizeof(addr));
        printf ("Receiver:bind is %d\n",rc);
        if (rc < 0) 
            {
            perror("bind");
            exit(1);
            }
        /*outFile = open(fileName,O_WRONLY | O_CREAT);
        if (outFile < 0)
            {
            printf ("Can't write to %s in tcpIf\n",fileName);
            exit(-1);
            }*/
       memset(&fraddr,0,sizeof(fraddr));
       fraddr.sin_family= AF_INET;
       fraddrlen = sizeof(fraddr); 
#ifdef BENCHMARK
        gettimeofday(&stime, 0                  );
        printf("rcv start @ %d: %d\n",stime.tv_sec, stime.tv_usec);

#endif
       while(1)
            {
#ifdef BENCHMARK

        gettimeofday(&mtime, 0                  );
        if(rtotal >= 10000000) 
        {
        printf("rcv mtime @ %d: %d %d %d\n",mtime.tv_sec, mtime.tv_usec,total, rtotal);
        rtotal=0;
        }
#endif
            cnt = recvfrom(fd, 
                       message, 
                       sizeof(message), 
                       0,
                       NULL,
                       &fraddrlen);
            //printf ("Receiver:recv count = %d\n",cnt);
            if (cnt == 0) 
                {
                /*close(outFile);*/
                continue;
                }
            if (cnt < 0) 
                {
                /*close(outFile);*/
                perror("recvfrom");
                exit(1);
                }
           rtotal+=cnt;
           total+=cnt;
 
           message[cnt+1] = '\0';
#ifndef BENCHMARK
            printf("rcvd %s\n",message);
#else
            if (strcmp("done",message)==0)
            {
                time_t blah1;
                suseconds_t blah2;
                gettimeofday(&etime, 0                  );
                blah1= (etime.tv_usec>stime.tv_usec) ?
                               etime.tv_sec-stime.tv_sec :
                               etime.tv_sec-stime.tv_sec-1;
                blah2 = (etime.tv_usec>stime.tv_usec) ?
                                     etime.tv_usec-stime.tv_usec :
                                     1000000-stime.tv_usec+etime.tv_usec;
                printf("DONE @ %d: %d {%d %d}\n",etime.tv_sec, etime.tv_usec,
                                                 blah1, blah2 );
                gettimeofday(&etime, 0                  );
            }
            if (strcmp("start",message)==0)
            {
                gettimeofday(&stime, 0                  );
                printf("START @ %d: %d\n",stime.tv_sec, stime.tv_usec);
            }

#endif
            }
        }
}
