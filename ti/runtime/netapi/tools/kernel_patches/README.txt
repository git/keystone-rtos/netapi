This directory contains patches to be applied to SC-MCSDK 2.0.0.8 Linux kernel distribution.

dma_patch_2.0.0.8.txt: patch to enable allocation of cacheable physically contiguous memory from CMA.

