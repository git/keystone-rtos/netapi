#!/bin/bash
# set smp_affinity of IRQs to core slow path core 1
DEFAULT_IRQ_MASK=1
for I in /proc/irq/*/smp_affinity
do
/bin/echo $DEFAULT_IRQ_MASK > $I 2> /dev/null
done
