#!/bin/sh

if [ -z "$1"]
  then
    echo "Need to supply device type, either k2h, k2k, k2l, k2e"
    exit 1
fi

# start up RM server
/usr/bin/rmServer.out /usr/bin/device/$1/global-resource-list.dtb /usr/bin/device/$1/policy_dsp_arm.dtb
export KERNEL_VER=$(uname -r)
#install kernel module
echo $KERNEL_VER
insmod /lib/modules/$KERNEL_VER/extra/hplibmod.ko
#
ifconfig eth1 up
# any ip address ok here
ifconfig eth1 10.0.2.100
# disable vlan aware mode in switch
devmem2 0x2090804 w 0x4
#
# run net_test_router applicaton, default location of config file is in /etc/transportnetlib/test,
# default config file name is net_test_config.txt
/usr/bin/net_test_router_$1 /etc/transportnetlib/test/net_test_config.txt
