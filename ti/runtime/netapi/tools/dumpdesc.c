/*
 * devmem2.c: Simple program to read/write from/to any location in memory.
 *
 *  Copyright (C) 2000, Jan-Derk Bakker (J.D.Bakker@its.tudelft.nl)
 *
 *
 * This software has been developed for the LART computing board
 * (http://www.lart.tudelft.nl/). The development has been sponsored by
 * the Mobile MultiMedia Communications (http://www.mmc.tudelft.nl/)
 * and Ubiquitous Communications (http://www.ubicom.tudelft.nl/)
 * projects.
 *
 * The author can be reached at:
 *
 *  Jan-Derk Bakker
 *  Information and Communication Theory Group
 *  Faculty of Information Technology and Systems
 *  Delft University of Technology
 *  P.O. Box 5031
 *  2600 GA Delft
 *  The Netherlands
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>

//netTest_utilDumpDescr
void netTest_utilDumpDescr(unsigned long *p, int n)
{
   printf("--------dump of descriptor %d %x\n", n, (int) p);
   printf("> %x %x %x %x %x %x %x %x\n",p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]);
   printf("> %x %x %x %x %x %x %x %x\n",p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]);
   printf("-----------------------------\n");
}

static inline unsigned long netapi_timing_start(void)
{
        volatile int vval;
        //read clock
        asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(vval));
        return vval;
}
static inline unsigned long netapi_timing_stop(void)
{
        volatile int vval2;
        //read clock
        asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(vval2));
        return vval2;
}

  
#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", \
  __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)
 
#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)


int cached = 0;
int test_loc = 1;
int sum=0;

int main(int argc, char **argv) {
    int fd;
    void *map_base, *virt_addr; 
#define N 50
	unsigned long read_result[N], writeval;
	off_t target;
	int access_type = 'w';
volatile unsigned long t1;
volatile unsigned long t2;
int i;
int num2dump=32;

	
	if(argc < 2) {
		fprintf(stderr, "\nUsage:\t%s { address } nun ] ]\n"
			"\taddress : memory address to act upon\n"
			"\tnum    : #descriptors to be dumped (def 32)\n\n",
			argv[0]);
		exit(1);
	}
	target = strtoul(argv[1], 0, 0);
        if (argc ==3) num2dump=atoi(argv[2]);


    if((fd = open("/dev/mem", ((cached) ? (O_RDWR) : (O_RDWR | O_SYNC)))) == -1) FATAL;
    netapi_Log("/dev/mem opened.\n"); 
    fflush(stdout);
    
    /* Map one page */
    map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, target & ~MAP_MASK);
    if(map_base == (void *) -1) FATAL;
    netapi_Log("Memory mapped at address %p. (cached=%d)\n", map_base,cached); 
    fflush(stdout);
    
    virt_addr = map_base + (target & MAP_MASK);
for(i=0;i<num2dump;i++)
{
   long * tip= &((long *)virt_addr)[32*i];
   netTest_utilDumpDescr(tip, i);
}
    if(munmap(map_base, MAP_SIZE) == -1) FATAL;
    close(fd);
    return 0;
}

