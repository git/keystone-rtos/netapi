#!/bin/sh

if [ -z "$1"]
  then
    echo "Need to supply device type, either k2h, k2k"
    exit 1
fi

# start up RM server
/usr/bin/rmServer.out /usr/bin/device/$1/global-resource-list.dtb /usr/bin/device/$1/policy_dsp_arm.dtb
export KERNEL_VER=$(uname -r)
#install kernel module
echo $KERNEL_VER
insmod /lib/modules/$KERNEL_VER/extra/hplibmod.ko
#
#reprogram qos tree shaper rates
echo "312500000" > /sys/devices/soc.0/hwqueue.6/qos-inputs-2/qos-tree-2/output_rate
echo "312500000" > /sys/devices/soc.0/hwqueue.6/qos-inputs-2/qos-tree-3/output_rate

# run nt_bridge applicaton
/usr/bin/nt_bridge_$1 eqos_config1.txt eqos_config2.txt
