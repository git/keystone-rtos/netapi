/**************************************************************
 * FILE PURPOSE :  NETAPI SECURITY CONFIGURATION-
 *         user space access to security transport resources on SOC
 **************************************************************
 * @file netapi_sec.c
 * 
 * @brief DESCRIPTION:  netapi security cfg file for user space transport
 *               library
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

#include "netapi.h"

NETAPI_SA_INFO_LOCAL_T netapi_sa_db[TUNE_NETAPI_MAX_SA];
/********************************************************************
 * FUNCTION PURPOSE:  API to add an IPSEC SA
 ********************************************************************
 * DESCRIPTION:  API to add an IPSEC SA
 ********************************************************************/
NETCP_CFG_SA_T netapi_secAddSAInternal(NETAPI_T h,
                                       int iface_no,
                                       NETAPI_SEC_SA_INFO_T *sa_info,
                                       nwalSecKeyParams_t * key_params,
                                       int inflow_mode,
                                       NETCP_CFG_ROUTE_HANDLE_T  route,
                                       void **p_data_mode_handle,
                                       void **p_inflow_mode_handle,
                                       void * p_user_data,
                                       NETCP_CFG_IP_T ip_rule,
                                       int * perr)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    nwal_RetValue       retValue;
    NetapiNwalTransInfo_t *pTransInfo;
    nwal_TransID_t     trans_id;
    unsigned int appId = NETAPI_NETCP_MATCH_IPSEC | iface_no;
    int tunnelId;
    nwalSaIpSecId_t  saInfo;
    int have_to_wait=1;
    nwalTxDmPSCmdInfo_t     dmPSCmdInfo;
    nwalSaIpSecId_t nwalSaIpSecId;
    uint32_t    swInfo0 = 0;
    uint32_t    swInfo1 = 0;
    int sa_db_slot;
    int free_sa_db_slot = 0;
    int ip_slot = 0;
    void * handle;
    nwalCreateSAParams_t    createParam =
    {
        /* mac handle */
        NULL,  //to be filled in
        4,
        /*nwalSaIpSecParam_t */
            {
                0,/* validParams */
                nwal_SA_MODE_TUNNEL,  //update from input
                64,/* replayWindow */\
                NWAL_SA_DIR_INBOUND,
                0,
                0,
                NWAL_SA_AALG_HMAC_SHA1,/* update from input */
                NWAL_SA_EALG_AES_CTR,  /* update from input */
                { 0x00},      /* remMacAddr:  NA */
                12,             /* update from input, mac size */
                NWAL_MATCH_ACTION_CONTINUE_NEXT_ROUTE,/* Continue parsing to next route for match */
                NWAL_NEXT_ROUTE_FAIL_ACTION_HOST,/* For next route fail action by default is route to host */
                CPPI_PARAM_NOT_SPECIFIED,         /* Use default flow configured to NWAL  if packet is routed to host */
                QMSS_PARAM_NOT_SPECIFIED,         /* Use default queue configured to NWAL if packet is routed to host */
                0                                 /* route type */
           },
           /* nwalSaIpSecKeyParams_t */
           {0}
    };

    /* Get IP slot for IP rule. */
    if (ip_rule)
    {
        ip_slot = netapi_cfgGetMatchId(ip_rule);
        handle = netapip_netcpCfgGetIpHandle(&netapi_get_global()->nwal_context,iface_no,ip_slot);
    }
    else
    {
        handle = netapip_netcpCfgGetMacHandle(&netapi_get_global()->nwal_context,iface_no);
    }

    *perr =NETAPI_ERR_OK;
    if ((!n) || (!sa_info)  ||  (!p_data_mode_handle))
    {
        *perr = NETAPI_ERR_BAD_INPUT;
        return -1;
    }

    /* prior to building table, will need to store SA entry in local database,
     * and make sure this SA has not been already offloaded, if already off-loaded,
     * just return app_id associated with the SA */

    for(sa_db_slot=0; sa_db_slot < TUNE_NETAPI_MAX_SA; sa_db_slot++)
    {
        if (netapi_sa_db[sa_db_slot].inUse)
        {
            if (sa_info->ipType == nwal_IPV4)
            {
                if (netapi_sa_db[sa_db_slot].spi != sa_info->spi)
                    continue;
                if(memcmp(&netapi_sa_db[sa_db_slot].dst.ipv4,
                          &sa_info->dst.ipv4, sizeof(sa_info->dst.ipv4)))
                    continue;

                netapi_sa_db[sa_db_slot].ref_count++;
                return netapi_sa_db[sa_db_slot].app_id;
            }
            else if (sa_info->ipType == nwal_IPV6)
            {
                if (netapi_sa_db[sa_db_slot].spi != sa_info->spi)
                    continue;
                if(memcmp(&netapi_sa_db[sa_db_slot].dst.ipv6,
                          &sa_info->dst.ipv6, sizeof(sa_info->dst.ipv6)))
                    continue;

                netapi_sa_db[sa_db_slot].ref_count++;
                return netapi_sa_db[sa_db_slot].app_id;
            }
            else
            {
                *perr = NETAPI_ERR_BAD_INPUT;
                return -1;
            }
        }
        else
        {
            /* found free slot */
            free_sa_db_slot = sa_db_slot;
        }
    }
    saInfo.spi = sa_info->spi;

    memcpy(&saInfo.dst, &sa_info->dst, sizeof( nwalIpAddr_t));
    
    /* reserve a slot */
    tunnelId = netapip_netcpCfgFindSaSlot(n,
                                          &netapi_get_global()->nwal_context,
                                          iface_no);

    if (tunnelId <0)
    {
        *perr= NETAPI_ERR_NOMEM;
        return -1;
    }
    appId |=  (tunnelId << NETAPI_NETCP_MATCH_ID_SHIFT);

    *p_data_mode_handle= NULL;  
    *p_inflow_mode_handle= NULL;



    if (inflow_mode & NETAPI_SEC_SA_INFLOW)
    {
        pTransInfo = netapip_getFreeTransInfo(n,
                                              (NETAPI_PROC_GLOBAL_T *) n->proc_global,
                                               &trans_id);
        if (!pTransInfo)
        {
            *perr =  NETAPI_ERR_BUSY;
            netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context,tunnelId);
            return -1;
        }
        pTransInfo->transType = NETAPI_NWAL_HANDLE_TRANS_SA;
        pTransInfo->state = NETAPI_NWAL_HANDLE_STATE_OPEN_PENDING;
        pTransInfo->inUse = nwal_TRUE;
        pTransInfo->netapi_handle = h;

        /* build SA parameters */
        saInfo.spi = sa_info->spi;
        memcpy(&saInfo.dst, &sa_info->dst, sizeof( nwalIpAddr_t));
        memcpy(&saInfo.src, &sa_info->src, sizeof( nwalIpAddr_t));
        saInfo.proto = sa_info->proto;
        createParam.h.handle = handle;
        createParam.ipType = sa_info->ipType;
        createParam.saIpSecParam.dir = sa_info->dir;
        createParam.saIpSecParam.saMode = sa_info->saMode;
        createParam.saIpSecParam.replayWindow = sa_info->replayWindow;
        createParam.saIpSecParam.authMode = sa_info->authMode;
        createParam.saIpSecParam.cipherMode = sa_info->cipherMode;
        createParam.saIpSecParam.esnLo = sa_info->esnLo;
        createParam.saIpSecParam.esnHi = sa_info->esnHi;
        if ((sa_info->cipherMode == NWAL_SA_EALG_AES_GCM) ||
            (sa_info->cipherMode == NWAL_SA_EALG_AES_CCM) ||
            (sa_info->authMode == NWAL_SA_AALG_GMAC)      ||
            (sa_info->authMode == NWAL_SA_AALG_HMAC_SHA2_256)      ||
            (sa_info->authMode == NWAL_SA_AALG_HMAC_SHA2_256_RFC4868))
        {
            createParam.saIpSecParam.macSize = 16;
        }
        if  ((sa_info->authMode == NWAL_SA_AALG_NULL) && 
            (!((sa_info->cipherMode == NWAL_SA_EALG_AES_GCM) || 
               (sa_info->cipherMode == NWAL_SA_EALG_AES_CCM))))
        {
            createParam.saIpSecParam.replayWindow = 0;
            createParam.saIpSecParam.macSize = 0;
        }
        memcpy(&createParam.keyParam,key_params,sizeof(nwalSecKeyParams_t));

        if (route != NULL)
        {
            if((route->valid_params & NETCP_CFG_VALID_PARAM_ROUTE_TYPE) ==
                NETCP_CFG_VALID_PARAM_ROUTE_TYPE)
            {
               createParam.saIpSecParam.validParams |= 
                        NWAL_SA_INFO_VALID_PARAM_ROUTE_TYPE;
            }
            netapip_netcpCfgBuildRoute(route,
                                       &createParam.saIpSecParam.appRxPktFlowId,
                                       &createParam.saIpSecParam.appRxPktQueue,
                                       &createParam.saIpSecParam.routeType);
        }

        /* fire off config message */
        retValue = nwal_setSecAssoc (((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                                        trans_id,
                                        (nwal_AppId) appId,
                                        &saInfo,
                                        &createParam,
                                        &pTransInfo->handle);
        if(retValue == nwal_TRANS_COMPLETE)
        {
            have_to_wait=0;
        }
        else if(retValue !=  nwal_OK)
        {
            *perr = NETAPI_ERR_NWAL_ERR0;
            netapip_freeTransInfo(pTransInfo);
            netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context,tunnelId);
            return -1;
        }

        if((trans_id != NWAL_TRANSID_SPIN_WAIT)&&(have_to_wait))
        {
            n->nwal_local.numPendingCfg++;
            while ((pTransInfo->state  !=NETAPI_NWAL_HANDLE_STATE_ERR) &&
                    (pTransInfo->state !=NETAPI_NWAL_HANDLE_STATE_OPEN))
            {
                nwal_pollCtl(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,NULL,NULL);
            }
            if (pTransInfo->state == NETAPI_NWAL_HANDLE_STATE_ERR)
            {
                pTransInfo->state = NETAPI_NWAL_HANDLE_STATE_IDLE;
                pTransInfo->inUse = nwal_FALSE;
                *perr = NETAPI_ERR_PA_FW;
                netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context,tunnelId);
                return -1;
            }
        }

        *p_inflow_mode_handle=pTransInfo->handle;
        netapip_freeTransInfo(pTransInfo);

        if (sa_info->dir == NWAL_SA_DIR_OUTBOUND)
        {
            memset(&nwalSaIpSecId, 0, sizeof(nwalSaIpSecId_t));
            nwalSaIpSecId.spi = sa_info->spi;
            memcpy(&(nwalSaIpSecId.src), &sa_info->src,sizeof( nwalIpAddr_t));
            memcpy(&(nwalSaIpSecId.dst), &sa_info->dst,sizeof( nwalIpAddr_t));
            nwalSaIpSecId.proto = sa_info->proto;
            if (nwal_getSecAssoc(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                                     &nwalSaIpSecId, 
                                     NWAL_SA_DIR_OUTBOUND,
                                     p_inflow_mode_handle,
                                     &swInfo0,
                                     &swInfo1) != nwal_TRUE)
                netapi_Log("netapisecAddSA: call to nwal_getSecAssoc() returned error\n");
        }
    }

    /* sideband mode */
    if (inflow_mode &NETAPI_SEC_SA_SIDEBAND)
    {
        nwalCreateDmSAParams_t  dmSaParam;
        void * dm_handle;
        memset(&dmSaParam,0,sizeof(nwalCreateDmSAParams_t));
        dmSaParam.dmSaParam.dmChnType= (sa_info->dir==NWAL_SA_DIR_INBOUND)?  NWAL_DM_CHAN_DECRYPT: NWAL_DM_CHAN_ENCRYPT; /**direction*/ 
        dmSaParam.dmSaParam.replayWindow=sa_info->replayWindow;
        dmSaParam.dmSaParam.authMode=sa_info->authMode;
        dmSaParam.dmSaParam.cipherMode=sa_info->cipherMode;
        dmSaParam.dmSaParam.macSize=12;
        dmSaParam.dmSaParam.aadSize=0;
        dmSaParam.dmSaParam.enc1st =  (sa_info->dir ==NWAL_SA_DIR_OUTBOUND) ? nwal_TRUE : nwal_FALSE;  //encypt 1st for outbound
        if ((sa_info->cipherMode == NWAL_SA_EALG_AES_GCM) ||
            (sa_info->cipherMode == NWAL_SA_EALG_AES_CCM) ||
            (sa_info->authMode == NWAL_SA_AALG_GMAC))
        {
            dmSaParam.dmSaParam.macSize = 16;
            dmSaParam.dmSaParam.aadSize=8;
            /* Enc1st needs to always be true for combined mode algorithms */
            dmSaParam.dmSaParam.enc1st = nwal_TRUE;
        }
       else
        {
            dmSaParam.dmSaParam.macSize=12;
            dmSaParam.dmSaParam.aadSize=0;
        }

       if  (sa_info->authMode == NWAL_SA_AALG_NULL)
       {
            dmSaParam.dmSaParam.enc1st = nwal_TRUE;
       }
        /* todo; allow app q for Sideband return */
        memcpy(&dmSaParam.keyParam,key_params,sizeof(nwalSecKeyParams_t));
        retValue = nwal_setDMSecAssoc(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                                  (nwal_AppId)appId,
                                  &dmSaParam,
                                  &dm_handle);
        if(retValue != nwal_OK)
        {
            *perr = NETAPI_ERR_NWAL_ERR0;
            netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context,tunnelId);
            return -1;
        }

        *p_data_mode_handle = dm_handle;
        memset(&dmPSCmdInfo, 0, sizeof(nwalTxDmPSCmdInfo_t));
        retValue =  nwal_initDMPSCmdInfo(netapip_returnNwalInstanceHandle(h),
                                         *p_data_mode_handle,
                                         &dmPSCmdInfo); 
    }

    netapip_netcpCfgInsertSa(&netapi_get_global()->nwal_context,
                          tunnelId,
                          (sa_info->dir ==   NWAL_SA_DIR_INBOUND) ? NETAPI_TRUE: NETAPI_FALSE,
                          inflow_mode,
                          &saInfo,
			  &createParam,
                          *p_inflow_mode_handle,
                          *p_data_mode_handle,
                          &dmPSCmdInfo,
                          swInfo0,
                          swInfo1,
                          p_user_data);
    netapi_sa_db[free_sa_db_slot].app_id = appId;
    netapi_sa_db[free_sa_db_slot].inUse = 1;
    netapi_sa_db[free_sa_db_slot].spi = sa_info->spi;
    netapi_sa_db[free_sa_db_slot].ref_count++;
    memcpy(&(netapi_sa_db[free_sa_db_slot].dst), &sa_info->dst,sizeof( nwalIpAddr_t));
    return  (appId);
}

/********************************************************************
 * FUNCTION PURPOSE:  API to add an IPSEC SA
 ********************************************************************
 * DESCRIPTION:  API to add an IPSEC SA
 ********************************************************************/
NETCP_CFG_SA_T netapi_secAddSA(NETAPI_T h,
                                 int iface_no,
                                 NETAPI_SEC_SA_INFO_T *sa_info,
                                 nwalSecKeyParams_t * key_params,
                                 int inflow_mode,
                                 NETCP_CFG_ROUTE_HANDLE_T  route,
                                 void **p_data_mode_handle,
                                 void **p_inflow_mode_handle,
                                 void * p_user_data,
                                 int * perr)
{
	*perr = 0;
	return netapi_secAddSAInternal(h,
                                       iface_no,
                                       sa_info,
                                       key_params,
                                       inflow_mode,
                                       route,
                                       p_data_mode_handle,
                                       p_inflow_mode_handle,
                                       p_user_data,
                                       0,
                                       perr);
}

/********************************************************************
 * FUNCTION PURPOSE:  API IP handle to add an IPSEC SA
 ********************************************************************
 * DESCRIPTION:  API to add an IPSEC SA.
 *		 Piggy back off perr for IP handle flag and IP slot.
 ********************************************************************/
NETCP_CFG_SA_T netapi_secAddSAIP(NETAPI_T h,
                                 int iface_no,
                                 NETAPI_SEC_SA_INFO_T *sa_info,
                                 nwalSecKeyParams_t * key_params,
                                 int inflow_mode,
                                 NETCP_CFG_ROUTE_HANDLE_T  route,
                                 void **p_data_mode_handle,
                                 void **p_inflow_mode_handle,
                                 void * p_user_data,
                                 NETCP_CFG_IP_T ip_rule,
                                 int * perr)
{
	*perr = 0;
	return netapi_secAddSAInternal(h,
                                       iface_no,
                                       sa_info,
                                       key_params,
                                       inflow_mode,
                                       route,
                                       p_data_mode_handle,
                                       p_inflow_mode_handle,
                                       p_user_data,
                                       ip_rule,
                                       perr);
}

/********************************************************************
 * FUNCTION PURPOSE:  Internal function  to dynamically switch between inflow
 *                                  and sideband mode
 ********************************************************************
 * DESCRIPTION:  Internal function  to dynamically switch between inflow
 *                                  and sideband mode
 ********************************************************************/
void netapi_secInflowMode(int iface,
                          NETCP_CFG_SA_T sa,
                          int on)
{
    /* NOT_IMPLEMENTED */
}

/********************************************************************
 * FUNCTION PURPOSE:  Internal function  to delete an IPSEC SA
 ********************************************************************
 * DESCRIPTION:  Internal function to delete an IPSEC SA
 ********************************************************************/
static void netapi_secDelSA_internal(NETAPI_T h,
                                     int iface_no,
                                     NETCP_CFG_SA_T sa_app_id,
                                     int flags,
                                     int *perr)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    nwal_RetValue       retValue;
    NetapiNwalTransInfo_t *pTransInfo;
    nwal_TransID_t     trans_id;
    int tunnelId = (sa_app_id >> NETAPI_NETCP_MATCH_ID_SHIFT) &NETAPI_NETCP_MATCH_ID_MASK;
    void * handle_inflow;
    void * handle_sideband;
    int have_to_wait = 1;
    int sa_db_slot;

    *perr =0;
    for(sa_db_slot=0; sa_db_slot < TUNE_NETAPI_MAX_SA; sa_db_slot++)
    {
            if((netapi_sa_db[sa_db_slot].inUse) &&
            (netapi_sa_db[sa_db_slot].app_id == sa_app_id))
            {
                if(!netapi_sa_db[sa_db_slot].ref_count)
                {
                    *perr = NETAPI_ERR_NOTFOUND;
                    return;
                }
                netapi_sa_db[sa_db_slot].ref_count--;
                if(netapi_sa_db[sa_db_slot].ref_count)
                {
                    return;
                }
                else
                {
                    netapi_sa_db[sa_db_slot].inUse = 0;
                    break;
                }
            }
    }
    handle_inflow = netapip_netcpCfgGetSaHandles(&netapi_get_global()->nwal_context,
                                          tunnelId, &handle_sideband);


    if(handle_inflow)
    {
    /* get a transaction id */
        pTransInfo = netapip_getFreeTransInfo(n,
                                              (NETAPI_PROC_GLOBAL_T *) n->proc_global,
                                              &trans_id);
        if (!pTransInfo)
        {
            *perr =  NETAPI_ERR_BUSY;
            return;
        }
        pTransInfo->transType = NETAPI_NWAL_HANDLE_TRANS_SA;
        pTransInfo->state = NETAPI_NWAL_HANDLE_STATE_CLOSE_PENDING;
        pTransInfo->inUse = nwal_TRUE;
        pTransInfo->netapi_handle = h;

        retValue = nwal_delSecAssoc(
                ((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                trans_id,
                handle_inflow);
        if(retValue == nwal_TRANS_COMPLETE)
        {
           have_to_wait=0;
        }
        else if(retValue !=  nwal_OK)
        {
                *perr = NETAPI_ERR_NWAL_ERR0;
                netapip_freeTransInfo(pTransInfo);
                netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context,tunnelId);
        }
        if((trans_id != NWAL_TRANSID_SPIN_WAIT)&&(have_to_wait))
        {
            n->nwal_local.numPendingCfg++;

            while ((pTransInfo->state  !=NETAPI_NWAL_HANDLE_STATE_ERR) &&
                    (pTransInfo->state !=NETAPI_NWAL_HANDLE_STATE_IDLE))
            {
                nwal_pollCtl(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,NULL,NULL);
            }
            if (pTransInfo->state == NETAPI_NWAL_HANDLE_STATE_ERR)
            {
                netapip_freeTransInfo(pTransInfo);
                *perr = NETAPI_ERR_PA_FW;
                 if (!flags) 
                    netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context, tunnelId);
                return;
           }
        }
        netapip_freeTransInfo(pTransInfo);
    }
    if (handle_sideband)
    {
        retValue=nwal_delDMSecAssoc( ((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                                    handle_sideband);
        if(retValue !=  nwal_OK)
        {
            *perr = NETAPI_ERR_NWAL_ERR0;
        }
    }

    /* zap the entry */
    if (!flags)
        netapip_netcpCfgDeleteSa(&netapi_get_global()->nwal_context, tunnelId);
}

/********************************************************************
 * FUNCTION PURPOSE:  API to delete an IPSEC SA
 ********************************************************************
 * DESCRIPTION:  API to delete an IPSEC SA
 ********************************************************************/
void netapi_secDelSA(NETAPI_T h,
                     int iface_no,
                     NETCP_CFG_SA_T sa_app_id,
                     int *perr)
{
    netapi_secDelSA_internal( h, iface_no,  sa_app_id, 0x00, perr);
}


/********************************************************************
 * FUNCTION PURPOSE:  API to add a receive security policy
 ********************************************************************
 * DESCRIPTION:  API to add a receive security policy
 ********************************************************************/
NETCP_CFG_IPSEC_POLICY_T netapi_secAddRxPolicy(NETAPI_T h, 
                                               NETCP_CFG_SA_T sa,
                                               nwal_IpType ipType,
                                               nwalIpAddr_t * src_ip_addr,
                                               nwalIpAddr_t * dst_ip_addr,
                                               nwalIpOpt_t * ip_qualifiers,
                                               NETCP_CFG_ROUTE_HANDLE_T  route,
                                               void * user_data,
                                               int * perr)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    nwal_RetValue       retValue;
    NetapiNwalTransInfo_t *pTransInfo;
    nwal_TransID_t     trans_id;
    unsigned int appId = NETAPI_NETCP_MATCH_IPSEC_POLICY;
    int policyId;
    int tunnelId= netapi_cfgGetMatchId(sa);
    void * blah;
    int iface_no = netapi_cfgGetMatchLogicalMacIface(sa);

    nwalSecPolParams_t createParam =
    {
        0,  /* handle */
        0,  /* valid params */
        NWAL_SA_DIR_INBOUND,
        4,      /* IP Type */
        {0},  /* dst */
        {0},  /* src */
        {0 },/* IP Options */
        NWAL_MATCH_ACTION_CONTINUE_NEXT_ROUTE,       /* Continue parsing to next route for match */
        NWAL_NEXT_ROUTE_FAIL_ACTION_HOST,            /* For next route fail action by default is route to host */
        CPPI_PARAM_NOT_SPECIFIED,                    /* Use default flow configured to NWAL  if packet is routed to host */
        QMSS_PARAM_NOT_SPECIFIED,                    /* Use default queue configured to NWAL if packet is routed to host */
        0                                            /* Optional: route type */
    };

    void * sa_handle = NULL;
    *perr =0;
    
    if ((!n) )
    {
        *perr = NETAPI_ERR_BAD_INPUT;
        return -1;
    }

    sa_handle = netapip_netcpCfgGetSaHandles(&netapi_get_global()->nwal_context,tunnelId,&blah);
    if (!sa_handle)
    {
        *perr = NETAPI_ERR_BAD_INPUT;
        return -1;
    }

    /* get a transaction id */
    pTransInfo = netapip_getFreeTransInfo(n,
                                          (NETAPI_PROC_GLOBAL_T *) n->proc_global,
                                          &trans_id);
    if (!pTransInfo)
    {
        *perr =  NETAPI_ERR_BUSY;
        return -1;
    }
    pTransInfo->transType = NETAPI_NWAL_HANDLE_TRANS_SA_POLICY;
    pTransInfo->state = NETAPI_NWAL_HANDLE_STATE_OPEN_PENDING;
    pTransInfo->inUse = nwal_TRUE;
    pTransInfo->netapi_handle = h;
    createParam.handle = sa_handle;
    createParam.ipType = ipType;
    if (dst_ip_addr) memcpy(&createParam.dst, dst_ip_addr, sizeof(nwalIpAddr_t)); 
    if (src_ip_addr) memcpy(&createParam.src, src_ip_addr, sizeof(nwalIpAddr_t)); 
    if (ip_qualifiers) memcpy(&createParam.ipOpt,ip_qualifiers ,sizeof(nwalIpOpt_t));
    if (route != NULL)
    {
        if((route->valid_params & NETCP_CFG_VALID_PARAM_ROUTE_TYPE) ==
            NETCP_CFG_VALID_PARAM_ROUTE_TYPE)
        {
           createParam.validParams |= 
                    NWAL_SET_SEC_POLICY_VALID_PARAM_ROUTE_TYPE;
        }
        netapip_netcpCfgBuildRoute(route,
                                   &createParam.appRxPktFlowId,
                                   &createParam.appRxPktQueue,
                                   &createParam.routeType);
    }
    /* reserve a slot */
    policyId = netapip_netcpCfgFindPolicySlot(n,
                                              &netapi_get_global()->nwal_context,
                                              tunnelId);
    if (policyId <0) 
    {
        *perr= NETAPI_ERR_NOMEM;
        netapip_freeTransInfo(pTransInfo);
        return -1;
    }
    appId |=  (policyId <<8);

    /* fire off config message */
    retValue = nwal_setSecPolicy (((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                                  trans_id,
                                  (nwal_AppId) appId,
                                  &createParam,
                                  &pTransInfo->handle);
    if(retValue !=  nwal_OK)
    {
        *perr = NETAPI_ERR_NWAL_ERR0;
        netapip_freeTransInfo(pTransInfo);
        
        netapip_netcpCfgDeletePolicy(&netapi_get_global()->nwal_context,policyId);
        return -1;
    }

    if(trans_id != NWAL_TRANSID_SPIN_WAIT)
    {
        n->nwal_local.numPendingCfg++;
        while ((pTransInfo->state  !=NETAPI_NWAL_HANDLE_STATE_ERR) &&
                (pTransInfo->state !=NETAPI_NWAL_HANDLE_STATE_OPEN))
        {
            nwal_pollCtl(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,NULL,NULL);
        }
        if (pTransInfo->state == NETAPI_NWAL_HANDLE_STATE_ERR)
        {
            netapip_freeTransInfo(pTransInfo);
            *perr = NETAPI_ERR_PA_FW;
            netapip_netcpCfgDeletePolicy(&netapi_get_global()->nwal_context,policyId);
            return -1;
        }
    }

    /* save stuff */
    netapip_netcpCfgInsertPolicy(&netapi_get_global()->nwal_context,
                           policyId,
                          (void *) pTransInfo->handle,
                          user_data);
    netapip_freeTransInfo(pTransInfo);
    return  (appId);
}

/********************************************************************
 * FUNCTION PURPOSE:  Internal function to delete a receive security policy
 ********************************************************************
 * DESCRIPTION:  Internal function to delete a receive security policy
 ********************************************************************/
static void netapi_secDelRxPolicy_internal(NETAPI_T h,
                                           NETCP_CFG_IPSEC_POLICY_T policy_app_id,
                                           int flags,
                                           int *perr)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    nwal_RetValue       retValue;
    NetapiNwalTransInfo_t *pTransInfo;
    nwal_TransID_t     trans_id;
    int policyId = netapi_cfgGetMatchId(policy_app_id);
    void * handle_policy=NULL;

    handle_policy = netapip_netcpCfgGetPolicy(&netapi_get_global()->nwal_context,policyId);
                                          ;
    if (!handle_policy)
    {
        *perr = NETAPI_ERR_BAD_INPUT;
        goto ERR_netapi_secDelRxPolicy_internal;
     }
    *perr =0;

    /* get a transaction id */
    pTransInfo = netapip_getFreeTransInfo(n,
                                         (NETAPI_PROC_GLOBAL_T *) n->proc_global,
                                         &trans_id);
    if (!pTransInfo)
    {
        *perr =  NETAPI_ERR_BUSY;
        goto ERR_netapi_secDelRxPolicy_internal;
    }
    pTransInfo->transType = NETAPI_NWAL_HANDLE_TRANS_SA_POLICY;
    pTransInfo->state = NETAPI_NWAL_HANDLE_STATE_CLOSE_PENDING;
    pTransInfo->inUse = nwal_TRUE;
    pTransInfo->netapi_handle = h;

    /* issue request */
    retValue = nwal_delSecPolicy(
                ((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,
                trans_id,
                handle_policy);
    if(retValue !=  nwal_OK)
    {
        *perr = NETAPI_ERR_NWAL_ERR0;
        netapip_freeTransInfo(pTransInfo);
        goto ERR_netapi_secDelRxPolicy_internal;
    }

    if(trans_id != NWAL_TRANSID_SPIN_WAIT)
    {
        n->nwal_local.numPendingCfg++;
        while ((pTransInfo->state  !=NETAPI_NWAL_HANDLE_STATE_ERR) &&
                    (pTransInfo->state !=NETAPI_NWAL_HANDLE_STATE_IDLE))
        {
            nwal_pollCtl(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle,NULL,NULL);
        }
        if (pTransInfo->state == NETAPI_NWAL_HANDLE_STATE_ERR)
        {
            netapip_freeTransInfo(pTransInfo);
            *perr = NETAPI_ERR_PA_FW;
            //zap the entry
            if (!flags)
                netapip_netcpCfgDeletePolicy(&netapi_get_global()->nwal_context, policyId);
            goto ERR_netapi_secDelRxPolicy_internal;
            
        }
    }
    netapip_freeTransInfo(pTransInfo);
    /* zap the entry */
    if (!flags)
    {
        netapip_netcpCfgDeletePolicy(&netapi_get_global()->nwal_context, policyId);
    }
ERR_netapi_secDelRxPolicy_internal:
    return;
}

/********************************************************************
 * FUNCTION PURPOSE:  API to delete a receive security policy
 ********************************************************************
 * DESCRIPTION:  API to delete a receive security policy
 ********************************************************************/
void netapi_secDelRxPolicy(NETAPI_T h,
                           NETCP_CFG_IPSEC_POLICY_T policy_app_id,
                           int *perr)
{
    netapi_secDelRxPolicy_internal(h,  policy_app_id, 0, perr);
}

/********************************************************************
 * FUNCTION PURPOSE:  API to retrieve SA statistics via NWAL
 ********************************************************************
 * DESCRIPTION:  API to retrieve SA statistics via NWAL
 ********************************************************************/
void  netapi_getSaStats (NETAPI_T               h, 
                         NETCP_CFG_SA_T         handle,
                         NETAPI_SA_STATS_T*     pSaStats)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    void * handle_inflow;
    void * handle_sideband;
    int tunnelId = (handle >> NETAPI_NETCP_MATCH_ID_SHIFT) &0xffff;
    int have_to_wait = 1;
    handle_inflow = netapip_netcpCfgGetSaHandles(&netapi_get_global()->nwal_context,
                                          tunnelId, &handle_sideband);
    if(handle_inflow)
    {
        nwal_getSecAssocStats(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle, 
                                                handle_inflow, &(pSaStats->saIpsecStats));
        pSaStats->validParams |= NETAPI_IPSEC_STAT_VALID;
    }
    if(handle_sideband)
    {
        nwal_getDataModeStats(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle, 
                                                handle_sideband, &(pSaStats->dataModeStats));
        pSaStats->validParams |= NETAPI_SIDEBAND_DATA_MODE_STAT_VALID;
    }
}


/**********************************************************************************
 * FUNCTION PURPOSE:  API to  API to retrieve local channel context information
 **********************************************************************************
 * DESCRIPTION:  API to retrieve API to retrieve local channel context information
 *********************************************************************************/
void netapi_secGetChanCtxInfo(NETAPI_T h,
                           NETCP_CFG_APP_ID_T appId,
                           nwalChanCxtInfo_t* pInfo)
{

    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    void * handle_inflow;
    void * handle_sideband = NULL;
    void * handle_policy=NULL;
    nwalChanCxtInfo_t info;
    uint32_t stage = 0;
    int policyId;
    int tunnelId;

    if(!pInfo)
        return;
    memset(pInfo, 0, sizeof(nwalChanCxtInfo_t));

    stage = netapi_cfgGetMatchStage(appId);

    switch (stage)
    {
        case 1:
            /* this is for SA, need SA and OUTER IP handle */
            tunnelId = netapi_cfgGetMatchId(appId);
            handle_inflow = netapip_netcpCfgGetSaHandles(&netapi_get_global()->nwal_context,
                                          tunnelId, &handle_sideband);
            if(handle_inflow)
            {
                nwal_getChanCxtInfo(&netapi_get_global()->nwal_context,
                            handle_inflow,
                            pInfo);
            }
            break;
        case 2:
            /* this is for policy, need SA inner IP */
            policyId = netapi_cfgGetMatchId(appId);
            handle_policy = netapip_netcpCfgGetPolicy(&netapi_get_global()->nwal_context,
                                                          policyId);
            if (handle_policy)
            {
                nwal_getChanCxtInfo(&netapi_get_global()->nwal_context,
                            handle_policy,
                            pInfo);
            }
            break;
        default:
            break;
    }
    return;
}
