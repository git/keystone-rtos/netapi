/************************************************
*  FILE:  netapi_device.c
 *  Device specific initialization for NETAPI
 *
 * DESCRIPTION: Functions to initialize multicore navigator related global
 *              resources
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************/

#include "netapi.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>

#include <ti/csl/cslr_device.h>
#include <ti/drv/qmss/qmss_qm.h>

//pull in device config for qmss, cppi
#include "qmss_device.c"
#include "cppi_device.c"

/********************************************************************
 * FUNCTION PURPOSE:  Internal NETAPI macro to convert to IP Register
 *                    Virtual Address from a mapped base Virtual
 *                    Address.
 ********************************************************************
 * DESCRIPTION:  Arguments:
 *      virtBaseAddr: Virtual base address mapped using mmap for IP
 *       phyBaseAddr: Physical base address for the IP
 *       phyRegAddr:  Physical register address
 ********************************************************************/


static inline void* NETAPI_GET_REG_VADDR (void *    virtBaseAddr,
                                          uint32_t  phyBaseAddr,
                                          uint32_t  phyRegAddr)
{
    return((void *)((uint8_t *)virtBaseAddr + (phyRegAddr - phyBaseAddr)));
}


/********************************************************************
 * FUNCTION PURPOSE:  Internal NETAI function ti initialize CPPI. 
 ********************************************************************
 * DESCRIPTION:  Internal NETAI function ti initialize CPPI
 ********************************************************************/
int netapip_initCppi(void* rmClientServiceHandle)
{
    int32_t result, i;
    Cppi_GlobalConfigParams     netapi_cppiGblCfgParams[CPPI_MAX_CPDMA];
    Cppi_StartCfg               netapi_cppiStartCfg;

    memset(&netapi_cppiStartCfg, 0, sizeof(Cppi_StartCfg));

    for (i=0; i<CPPI_MAX_CPDMA; i++)
        netapi_cppiGblCfgParams[i] = cppiGblCfgParams[i];

    /* SRIO CPDMA regs */
    netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].gblCfgRegs =
     NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->srioCfgVaddr,
                        CSL_SRIO_CONFIG_REGS,
                        (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].gblCfgRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].txChRegs =
     NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->srioCfgVaddr,
                          CSL_SRIO_CONFIG_REGS,
                          (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].txChRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].rxChRegs =
     NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->srioCfgVaddr,
                          CSL_SRIO_CONFIG_REGS,
                          (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].rxChRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].txSchedRegs =
     NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->srioCfgVaddr,
                          CSL_SRIO_CONFIG_REGS,
                          (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].txSchedRegs);

   netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].rxFlowRegs =
     NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->srioCfgVaddr,
                          CSL_SRIO_CONFIG_REGS,
                          (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_SRIO_CPDMA].rxFlowRegs);

    /* PASS CPDMA regs */
    netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].gblCfgRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                           CSL_PA_SS_CFG_REGS,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].gblCfgRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].txChRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                           CSL_PA_SS_CFG_REGS,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].txChRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].rxChRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                           CSL_PA_SS_CFG_REGS,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].rxChRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].txSchedRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                           CSL_PA_SS_CFG_REGS,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].txSchedRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].rxFlowRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                           CSL_PA_SS_CFG_REGS,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_PASS_CPDMA].rxFlowRegs);

   /* QMSS CPDMA regs */
    netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].gblCfgRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].gblCfgRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].txChRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].txChRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].rxChRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].rxChRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].txSchedRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].txSchedRegs);

    netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].rxFlowRegs =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_cppiGblCfgParams[Cppi_CpDma_QMSS_CPDMA].rxFlowRegs);

    result = Cppi_init (netapi_cppiGblCfgParams);
    if (result != CPPI_SOK)
    {
      return (-1);
    }
    if (rmClientServiceHandle)
    {
      netapi_cppiStartCfg.rmServiceHandle = rmClientServiceHandle;
      Cppi_startCfg(&netapi_cppiStartCfg);
    }
     return 1;
}

/********************************************************************
 * FUNCTION PURPOSE:  Internal NETAI function ti initialize QM. 
 ********************************************************************
 * DESCRIPTION:  Internal NETAI function ti initialize QM (once per SOC)
 ********************************************************************/
int netapip_initQm(int max_descriptors, void* rmClientServiceHandle))
{
    Qmss_InitCfg     qmssInitConfig;
    int32_t          result;
    Qmss_GlobalConfigParams netapi_qmssGblCfgParams;

    memset (&qmssInitConfig, 0, sizeof (Qmss_InitCfg));

    /* Use Internal Linking RAM for optimal performance */
    qmssInitConfig.linkingRAM0Base = 0;
    qmssInitConfig.linkingRAM0Size = 0;
    qmssInitConfig.linkingRAM1Base = 0;
    qmssInitConfig.maxDescNum      = max_descriptors;
    qmssInitConfig.qmssHwStatus =QMSS_HW_INIT_COMPLETE; //bypass some of the hw init
    netapi_qmssGblCfgParams = qmssGblCfgParams[0];

    netapi_qmssGblCfgParams.qmConfigReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmConfigReg);

    netapi_qmssGblCfgParams.qmDescReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmDescReg);

    netapi_qmssGblCfgParams.qmQueMgmtReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmQueMgmtReg);

    netapi_qmssGblCfgParams.qmQueMgmtProxyReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmQueMgmtProxyReg);

    netapi_qmssGblCfgParams.qmQueStatReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmQueStatReg);

    netapi_qmssGblCfgParams.qmQueIntdReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmQueIntdReg);

    netapi_qmssGblCfgParams.qmPdspCmdReg[0] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmPdspCmdReg[0]);

    netapi_qmssGblCfgParams.qmPdspCmdReg[1] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmPdspCmdReg[1]);

    netapi_qmssGblCfgParams.qmPdspCtrlReg[0] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmPdspCtrlReg[0]);

    netapi_qmssGblCfgParams.qmPdspCtrlReg[1] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmPdspCtrlReg[1]);

    netapi_qmssGblCfgParams.qmPdspIRamReg[0] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmPdspIRamReg[0]);

    netapi_qmssGblCfgParams.qmPdspIRamReg[1] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmPdspIRamReg[1]);

    netapi_qmssGblCfgParams.qmStatusRAM =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmStatusRAM);

    netapi_qmssGblCfgParams.qmLinkingRAMReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmLinkingRAMReg);

    netapi_qmssGblCfgParams.qmMcDMAReg =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmMcDMAReg);

    netapi_qmssGblCfgParams.qmTimer16Reg[0] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmTimer16Reg[0]);

    netapi_qmssGblCfgParams.qmTimer16Reg[1] =
      NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                           QMSS_CFG_BASE_ADDR,
                           (uint32_t)netapi_qmssGblCfgParams.qmTimer16Reg[1]);

    netapi_qmssGblCfgParams.qmQueMgmtDataReg = 
                                                      (void *)((uint32_t)netapi_VM_VirtAddr->qmssDataVaddr);


    if (rmClientServiceHandle)
        netapi_qmssGblCfgParams.qmRmServiceHandle = rmClientServiceHandle;

    netapi_qmssGblCfgParams.qmQueMgmtProxyDataReg = NULL;
    result = Qmss_init (&qmssInitConfig, &netapi_qmssGblCfgParams);
    if (result != QMSS_SOK)  {
        return (nwal_FALSE);
    }
    return 1;
}
