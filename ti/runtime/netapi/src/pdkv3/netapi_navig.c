/************************************************
*  FILE:  netapi_device.c
 *  Device specific initialization for NETAPI
 *
 * DESCRIPTION: Functions to initialize multicore navigator related global
 *              resources
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***********************************************/

#include "netapi.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>

#include <ti/csl/cslr_device.h>
#include <ti/drv/qmss/qmss_qm.h>

//pull in device config for qmss, cppi


extern Qmss_GlobalConfigParams qmssGblCfgParams;
extern Cppi_GlobalConfigParams cppiGblCfgParams;

/********************************************************************
 * FUNCTION PURPOSE:  Internal NETAPI macro to convert to IP Register
 *                    Virtual Address from a mapped base Virtual
 *                    Address.
 ********************************************************************
 * DESCRIPTION:  Arguments:
 *      virtBaseAddr: Virtual base address mapped using mmap for IP
 *       phyBaseAddr: Physical base address for the IP
 *       phyRegAddr:  Physical register address
 ********************************************************************/
static inline void* NETAPI_GET_REG_VADDR (void *    virtBaseAddr,
                                          uint32_t  phyBaseAddr,
                                          uint32_t  phyRegAddr)
{
    return((void *)((uint8_t *)virtBaseAddr + (phyRegAddr - phyBaseAddr)));
}


/*****************************************************************************
 * FUNCTION PURPOSE: Global Initialization of CPPI. Once Per System
 *****************************************************************************
 * DESCRIPTION: The function will initialize the CPPI
 *****************************************************************************/
int netapip_initCppi(void* rmClientServiceHandle)
{

    int32_t                         result;
    Cppi_GlobalConfigParams         netapi_cppiGblCfgParams;
    Cppi_StartCfg               netapi_cppiStartCfg;

    memset(&netapi_cppiStartCfg, 0, sizeof(Cppi_StartCfg));
    netapi_cppiGblCfgParams = cppiGblCfgParams;

    /* PASS CPDMA regs */
    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].gblCfgRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                             CSL_NETCP_CFG_REGS,
                             (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].gblCfgRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].txChRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                                     CSL_NETCP_CFG_REGS,
                             (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].txChRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].rxChRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                                     CSL_NETCP_CFG_REGS,
                             (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].rxChRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].txSchedRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                                     CSL_NETCP_CFG_REGS,
                             (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].txSchedRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].rxFlowRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->passCfgVaddr,
                                     CSL_NETCP_CFG_REGS,
                             (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_PASS_CPDMA].rxFlowRegs);

    /* QMSS CPDMA regs */
    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].gblCfgRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
                             CSL_QMSS_CFG_BASE,
                             (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].gblCfgRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txChRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
        CSL_QMSS_CFG_BASE,
        (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txChRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxChRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
        CSL_QMSS_CFG_BASE,
        (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxChRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txSchedRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
        CSL_QMSS_CFG_BASE,
        (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txSchedRegs);

    netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxFlowRegs =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
        CSL_QMSS_CFG_BASE,
        (uint32_t)netapi_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxFlowRegs);

    result = Cppi_init (&netapi_cppiGblCfgParams);
    if (result != CPPI_SOK)  {
        return (-1);
    }

    if (rmClientServiceHandle)
    {
      netapi_cppiStartCfg.rmServiceHandle = rmClientServiceHandle;
      Cppi_startCfg(&netapi_cppiStartCfg);
    }

    return (1);
}

/*****************************************************************************
 * FUNCTION PURPOSE: Global Initialization of Queue Manager. Once Per System
 *****************************************************************************
 * DESCRIPTION: The function will initialize the Queue Manager
 *****************************************************************************/
int netapip_initQm(int max_descriptors, void* rmClientServiceHandle)
{
    Qmss_InitCfg              qmssInitConfig;
    int32_t                   result;
    Qmss_GlobalConfigParams   netapi_qmssGblCfgParams;
    uint32_t                  count;

    memset (&qmssInitConfig, 0, sizeof (Qmss_InitCfg));

    /* Use Internal Linking RAM for optimal performance */
    qmssInitConfig.linkingRAM0Base = 0;
    qmssInitConfig.linkingRAM0Size = 0;
    qmssInitConfig.linkingRAM1Base = 0;
    qmssInitConfig.maxDescNum      = max_descriptors;
    qmssInitConfig.qmssHwStatus = QMSS_HW_INIT_COMPLETE;

    netapi_qmssGblCfgParams = qmssGblCfgParams;

    /* Convert address to Virtual address */
    for(count=0;count < netapi_qmssGblCfgParams.maxQueMgrGroups;count++)
    {
        netapi_qmssGblCfgParams.groupRegs[count].qmConfigReg =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmConfigReg);

        netapi_qmssGblCfgParams.groupRegs[count].qmDescReg =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmDescReg);

        netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtReg =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtReg);

        netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtProxyReg =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtProxyReg);

        netapi_qmssGblCfgParams.groupRegs[count].qmQueStatReg =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmQueStatReg);

        netapi_qmssGblCfgParams.groupRegs[count].qmStatusRAM =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmStatusRAM);

        netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtDataReg =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssDataVaddr,
            CSL_QMSS_DATA_BASE,
           (uint32_t)netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtDataReg);

       netapi_qmssGblCfgParams.groupRegs[count].qmQueMgmtProxyDataReg = NULL;
    }

    for(count=0;count < QMSS_MAX_INTD;count++)
    {
        netapi_qmssGblCfgParams.regs.qmQueIntdReg[count] =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.regs.qmQueIntdReg[count]);
    }

    for(count=0;count < QMSS_MAX_PDSP;count++)
    {
        netapi_qmssGblCfgParams.regs.qmPdspCmdReg[count] =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.regs.qmPdspCmdReg[count]);

        netapi_qmssGblCfgParams.regs.qmPdspCtrlReg[count] =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.regs.qmPdspCtrlReg[count]);

        netapi_qmssGblCfgParams.regs.qmPdspIRamReg[count] =
            NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
            CSL_QMSS_CFG_BASE,
           (uint32_t)netapi_qmssGblCfgParams.regs.qmPdspIRamReg[count]);
    }

    netapi_qmssGblCfgParams.regs.qmLinkingRAMReg =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
        CSL_QMSS_CFG_BASE,
       (uint32_t)netapi_qmssGblCfgParams.regs.qmLinkingRAMReg);

    netapi_qmssGblCfgParams.regs.qmBaseAddr =
        NETAPI_GET_REG_VADDR(netapi_VM_VirtAddr->qmssCfgVaddr,
        CSL_QMSS_CFG_BASE,
       (uint32_t)netapi_qmssGblCfgParams.regs.qmBaseAddr);

    if (rmClientServiceHandle)
        netapi_qmssGblCfgParams.qmRmServiceHandle = rmClientServiceHandle;

    result = Qmss_init (&qmssInitConfig, &netapi_qmssGblCfgParams);
    if (result != QMSS_SOK)
    {
        return (nwal_FALSE);
    }
    return 1;
}
