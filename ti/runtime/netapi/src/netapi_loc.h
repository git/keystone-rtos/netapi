/*****************************************
 * file: netapi_loc.h
 * purpose:  internal netapi stuff
 ****************************************/

#ifndef __NETAPI_LOC__H
#define __NETAPI_LOC__H

#ifdef __cplusplus
extern "C" {
#endif

#include "ti/drv/nwal/nwal_util.h"
#include "ti/drv/nwal/nwal.h"
#include "ti/runtime/hplib/hplib.h"
#include "ti/runtime/netapi/pktio.h"
#include "ti/runtime/netapi/netcp_cfg.h"
#include "ti/runtime/netapi/netapi_tune.h"


extern hplib_virtualAddrInfo_T netapi_VM_VirtAddr[HPLIB_MAX_MEM_POOLS];
extern unsigned char *netapi_VM_SaContextVaddr;
//extern NETAPI_SHM_T* pnetapiShm;

#define     NETAPI_FALSE 0
#define     NETAPI_TRUE 1
/***********************************************
 *  GLOBAL AREA
 *   short term:  this is global to process
 *         (multi-process not supported)
 *   long term:  this structure gets put in shared memory 
 ***********************************************/

/* list of global pktio channels that have been created
   (NETCP_TX, RX are intrinsic so won't be here) */
typedef struct PKTIO_ENTRY_tag
{
   char name[PKTIO_MAX_NAME+1];
   Qmss_Queue qn;         // -1 => slot is free
} PKTIO_ENTRY_T;

/* to hold an IP on an interface */
typedef struct NETCP_INTERFACE_IP_Tag
{
        int in_use;
        void * nwal_handle;
        nwal_IpType  ip_type;
        nwalIpAddr_t  ip_addr;
        nwalIpOpt_t ip_qualifiers;
        void * user_data;
} NETCP_INTERFACE_IP_T;

/* to hold a classifier */
typedef struct NETCP_INTERFACE_CLASSIFIER_Tag
{
    int  in_use;
    int  class_type;   //see netcp_cfg.h
    void * nwal_L2_handle;
    void * nwal_L3_handle;
    void * nwal_L4_handle;
    void * user_data;
} NETCP_INTERFACE_CLASSIFIER_T;

/* to hold an ipsec rx policy */
typedef struct NETCP_IPSEC_POLICY_Tag
{
    int in_use;
    int tunnel; //associated tunnel
    void * nwal_handle;  //handle associated with this RX Policy
    void * user_data; // user data associtaed with this RX Policy
} NETCP_IPSEC_POLICY_T;

/* to hold a tunnel */
typedef struct NETCP_IPSEC_SA_Tag
{
    int in_use;
    int inbound;  //true if inbound
       
    int  sa_mode;  //mode we are going to use 
#define NETCP_IPSEC_SA_MODE_INFLOW 0
#define NETCP_IPSEC_SA_MODE_SIDEBAND 1
 
    void * sa_handle_inflow;   //for inflow mode
    void * sa_handle_sideband; //for sideband mode
    int iface;     //associated interface
    nwalTxDmPSCmdInfo_t     dmPSCmdInfo;
    uint32_t    swInfo0;
    uint32_t    swInfo1;
    void*   user_data;
} NETCP_IPSEC_SA_T;

/* to hold a netcp 'interface' */
typedef struct NETCP_INTERFACE_Tag
{
   int in_use;  /* 1 for valid */
   int state;  /* 0=down, 1=up,  future.. */
   void * nwal_handle;  //handle associated with this interface
   unsigned char mac[6];  // mac address
   unsigned int vlan;   //future
} NETCP_INTERFACE_T;

/*to keep track of netcp config transactions */
typedef struct {
    nwal_Bool_t             inUse;
    uint16_t                transType;
#define NETAPI_NWAL_HANDLE_TRANS_NONE             0
#define NETAPI_NWAL_HANDLE_TRANS_MAC              1
#define NETAPI_NWAL_HANDLE_TRANS_IP               2
#define NETAPI_NWAL_HANDLE_TRANS_PORT             3
#define NETAPI_NWAL_HANDLE_TRANS_SA               4
#define NETAPI_NWAL_HANDLE_TRANS_SA_POLICY        5
#define NETAPI_NWAL_HANDLE_STAT_REQUEST           6

    uint16_t                state;
#define NETAPI_NWAL_HANDLE_STATE_IDLE             0
#define NETAPI_NWAL_HANDLE_STATE_OPEN_PENDING     1
#define NETAPI_NWAL_HANDLE_STATE_OPEN             2
#define NETAPI_NWAL_HANDLE_STATE_CLOSE_PENDING    3
#define NETAPI_NWAL_HANDLE_STATE_ERR                        4
    nwal_Handle             handle;
    uint64_t                transId;
    NETAPI_T                netapi_handle; //the thread making the transaction
}  NetapiNwalTransInfo_t;

/* to hold user defined flows */
typedef struct NETCP_REGISTERED_FLOWS_Tag
{
    int                 in_use;  //1=> in use
    void*               handle; //cppi handle to resource (internal)
    NETCP_CFG_FLOW_T    flow;
} NETCP_REGISTERED_FLOWS_T;

/******************************
 *    nwal global context : for SOC
 *  (shared over all process instances)
 *******************************/
typedef struct
{
    int                             state;
#define NETAPI_NW_CXT_GLOB_INACTIVE               0x0
#define NETAPI__CXT_GLOB_ACTIVE                   0x1
#define NETAPI_NW_CXT_GLOB_RES_ALLOC_COMPLETE     0x3

    nwal_Handle                     nwalInstHandle;  //created instance 
    //internal heaps used just by netcp (sa<->pa). SW doesn't touch these
    Pktlib_HeapHandle               sa2pa_heap;
    Pktlib_HeapHandle               pa2sa_heap;
    //stats
    paSysStats_t                    paStats;
    NETCP_INTERFACE_T               interfaces[TUNE_NETAPI_MAX_NUM_MAC];        //interfaces
    NETCP_INTERFACE_CLASSIFIER_T    classi[TUNE_NETAPI_MAX_CLASSIFIERS]; //classifiers
    NETCP_REGISTERED_FLOWS_T        flows[TUNE_NETAPI_MAX_FLOWS]; //flows
    NETCP_IPSEC_SA_T                tunnel[TUNE_NETAPI_MAX_SA];           //tunnels
    NETCP_IPSEC_POLICY_T            policy[TUNE_NETAPI_MAX_POLICY];  //policies
    NETCP_INTERFACE_IP_T            ips[TUNE_NETAPI_MAX_NUM_IP];           //ips
} NETAPI_NWAL_GLOBAL_CONTEXT_T;
/************************************/
/* global instance for this process */
/************************************/
typedef struct
{
    int                             state; /* see above*/
    int                             numCmdPass;
    int                             numCmdFail;
    int                             numBogusTransIds;
    NetapiNwalTransInfo_t           transInfos[TUNE_NETAPI_MAX_NUM_TRANS]; 
} NETAPI_NWAL_GLOBAL_PROC_CONTEXT_T;

/* NWAL Local context (per core/thread)  */
typedef struct
{
#define NETAPI_NW_CXT_LOC_INACTIVE     0x0
#define NETAPI_NW_CXT_LOC_ACTIVE       0x2
    int                     state;

    int                     numPendingCfg;
    NETCP_CFG_STATS_CB      stats_cb;

/* stats */
    int                     numL2PktsRecvd;
    int                     numL3PktsRecvd;
    int                     numL4PktsRecvd;
    int                     numL4PktsSent;
    int                     TxErrDrop;

    /* local config */
    nwalLocCfg_t           nwalLocCfg;
} NETAPI_NWAL_LOCAL_CONTEXT_T;

/* the global: for SOC, accross all processes */
typedef struct NETAPI_GLOBAL_tag
{
#define NETAPI_MAX_PKTIO (TUNE_NETAPI_MAX_PKTIO) 
    PKTIO_ENTRY_T                   pktios[NETAPI_MAX_PKTIO];
    /* configuration */
    NETAPI_CFG_T                    cfg;
    /* nwal context */
    NETAPI_NWAL_GLOBAL_CONTEXT_T    nwal_context;
    //NETAPI_T                        p_master;  // save p'masters handle 
} NETAPI_GLOBAL_T;

/* the process global: one per process */
typedef struct NETAPI_PROC_GLOBAL_tag
{
    /* nwal context */
    NETAPI_NWAL_GLOBAL_PROC_CONTEXT_T    nwal_context;

    //NETAPI_T                        p_master;  // save p'masters handle 
} NETAPI_PROC_GLOBAL_T;
/************************************
 * this is a per thread structure.
 * It contains stuff local to thread
 *  and pointer to global stuff
 *  that is shared over all threads,
 **************************************/
typedef struct NETAPI_HANDLE_Tag
{
    int                         master; //master type

    void*                       global;  /* pointer to the soc global area */
    void*                       proc_global;  /* pointer to the proc global area */

    /* heap  handles */
    Pktlib_HeapHandle           netcp_heap;    /* internal default */
    Pktlib_HeapHandle           netcp_control_rx_heap; /* for control messages */
    Pktlib_HeapHandle           netcp_control_tx_heap; /* for control messages */
    Pktlib_HeapHandle           createdHeaps[TUNE_NETAPI_MAX_HEAPS];  /* created by app and registered */

    /* pktios defined */
    int                         n_pktios; /* #of pktios that are active for this instance */
    void*                       pktios[NETAPI_MAX_PKTIO]; /* the list of pktios */

    /* scheduler stuff.  unallocated if NETAPI_INCLUDE_SCHED not set */
    void*                       p_sched;

    /* nwal local context */
    NETAPI_NWAL_LOCAL_CONTEXT_T nwal_local;

    /* thread cookie */
    void*                      cookie;  /*set by calling thread */
    HPLIB_SPINLOCK_IF_T        spinLock;
    int                         memRegion;
} NETAPI_HANDLE_T;

typedef struct NETAPI_SHM_Tag
{
    NETAPI_GLOBAL_T     netapi_global;
    hplib_spinLock_T    netapi_pktio_lock;
    hplib_spinLock_T    netapi_netcp_cfg_lock;
    hplib_spinLock_T    netapi_util_lock;
    hplib_spinLock_T    netapi_netcp_cfg_l3_classi_lock;
} NETAPI_SHM_T;

//internal initialization routines */
int netapip_initQm(int max_descriptors, void* rmClientServiceHandle);

int netapip_initCppi(void* rmClientServiceHandle);

int netapip_startQm(void* rmClientServiceHandle);
int netapip_initNwal(int region2use,
                     Pktlib_HeapIfTable * p_table,
                     NETAPI_NWAL_GLOBAL_CONTEXT_T * p_nwal_context,
                     NETAPI_CFG_T *p_cfg);
int netapip_startNwal(Pktlib_HeapHandle             pkt_heap,
                      Pktlib_HeapHandle             cmd_heapRx,
                      Pktlib_HeapHandle             cmd_heapTx,
                      NETAPI_NWAL_LOCAL_CONTEXT_T*  p,
                      NETAPI_CFG_T*                 p_cfg,
                      NETAPI_NWAL_GLOBAL_CONTEXT_T* p_nwal_glob_context,
                      int                          master);

int netapip_initTimer(void);
int netapip_qmSetupMemRegion(unsigned int          numDesc,
                             unsigned int          descSize,
                             unsigned int*         pDescMemBase,
                             int                   memRegion,
                             int                   start_index);


//nwal callbacks
void netapip_pktioNWALRxPktCallback(uint32_t          appCookie,
                                    uint16_t          numPkts,
                                    nwalRxPktInfo_t*  pPktInfo,
                                    uint64_t          timestamp,
                                    nwal_Bool_t*      pFreePkt);

void netapip_netcpCfgNWALCmdCallBack (nwal_AppId        appHandle,
                                      uint16_t          trans_id,
                                      nwal_RetValue     ret);

void netapip_netcpCfgNWALCmdPaStatsReply (nwal_AppId        appHandle,
                                          nwal_TransID_t    trans_id,
                                          paSysStats_t      *stats);

void netapip_pktioNWALSBPktCallback     (uint32_t                   appCookie,
                                         uint16_t                   numPkts,
                                         nwalDmRxPayloadInfo_t*     pDmRxPktInfo,
                                        nwal_Bool_t*                pFreePkt);


//***********************************
//internal utilities
//*************************************

//return the list of pktios for this instance
static inline void ** netapi_get_pktio_list(NETAPI_T p)
{
NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
return &pp->pktios[0];
}

//get scheduler block handle
static inline void * netapi_get_scheduler(NETAPI_T p)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    return pp->p_sched;
}

/* return pointer to global area */
NETAPI_GLOBAL_T * netapi_get_global(void);
NETAPI_PROC_GLOBAL_T * netapi_get_proc_global(void);

//add a pktio name (and queue) to global list
static inline int  netapip_addGlobalPktio(NETAPI_T p, char *name, Qmss_Queue * qn)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    PKTIO_ENTRY_T *pe;
    int i;
    /* find a free slot */
     pe = &((NETAPI_GLOBAL_T *)(pp->global))->pktios[0];

    for(i=0;i<NETAPI_MAX_PKTIO; i++)
    {
        pe = &((NETAPI_GLOBAL_T *)(pp->global))->pktios[i];
        if (pe->qn.qNum ==  -1)
        {
            pe->qn.qNum=qn->qNum;
            pe->qn.qMgr=qn->qMgr;
            strncpy(pe->name, name, PKTIO_MAX_NAME);
            return 1;
        }
    }
    return 0;  //no room
}

//delete a pktio name (and queue) to global list
static inline int  netapi_del_global_pktio(NETAPI_T p, char *name)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    PKTIO_ENTRY_T *pe;
    int i;

    /* find slot */


    for(i=0;i<NETAPI_MAX_PKTIO; i++)
    {
        pe = &((NETAPI_GLOBAL_T *)(pp->global))->pktios[i];
        if (pe->qn.qNum == -1)
            continue;
        if (!strncmp(name, pe->name, PKTIO_MAX_NAME))
        {
            pe->qn.qNum=-1;
            pe->name[0]='\0';
            return 1;
        }
    }
     return 0;  //no room
}


/* get list of global pktios that have been created */
static inline  Qmss_Queue*  netapip_findGlobalPktio(NETAPI_T p, char *name)
{
    NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
    PKTIO_ENTRY_T *pe;
    int i;

    /* find slot */
    for(i=0;i<NETAPI_MAX_PKTIO; i++)
    {
        pe = &((NETAPI_GLOBAL_T *)(pp->global))->pktios[i];
        if (pe->qn.qNum  == -1) continue;
        if (!strncmp(name, pe->name, PKTIO_MAX_NAME))
        {
            return &pe->qn;
        }
    }
    return NULL;  //not found
}

/* return the nwal global instance handle */
static inline nwal_Handle netapip_returnNwalInstanceHandle(NETAPI_T p)
{

NETAPI_HANDLE_T *pp = (NETAPI_HANDLE_T *) p;
return ((NETAPI_GLOBAL_T *)(pp->global))->nwal_context.nwalInstHandle;
}



//utility to clear out a queue
void netapip_zapQ(int queueNum);
void netapip_netcpCfgBuildRoute(NETCP_CFG_ROUTE_T * p_route,
                                int16_t * p_flow,
                                Qmss_QueueHnd * p_q,
                                nwalRouteType_t *p_type);

//database utilities
void netapip_netcpCfgDeleteSa(NETAPI_NWAL_GLOBAL_CONTEXT_T *p,int sa_slot);

void netapip_netcpCfgInsertSa(NETAPI_NWAL_GLOBAL_CONTEXT_T *p,
                          int sa_slot,  //we 'reserved it already'
                          int dir,
                          int mode,
                          void * temp1,
                          void * temp2,
                          void * handle_inflow,
                          void * handle_sideband,
                          nwalTxDmPSCmdInfo_t *dmPSCmdInfo,
                          uint32_t swInfo0,
                          uint32_t swInfo1,
                          void* user_data);
void *netapip_netcpCfgGetSaHandles( NETAPI_NWAL_GLOBAL_CONTEXT_T *p,
                          int sa_slot, void ** p_sideband);
void* netapip_netcpCfgGetMacHandle(NETAPI_NWAL_GLOBAL_CONTEXT_T *p,int iface_no);
void *netapip_netcpCfgGetIpHandle(NETAPI_NWAL_GLOBAL_CONTEXT_T *p,int iface_no,int ip_slot);
NetapiNwalTransInfo_t *  netapip_getFreeTransInfo(NETAPI_HANDLE_T *p_handle,
                                                  NETAPI_PROC_GLOBAL_T *p_global,
                                                  nwal_TransID_t *pTransId);
void netapip_freeTransInfo(NetapiNwalTransInfo_t* pTransInfo);
void *netapip_netcpCfgGetPolicy( NETAPI_NWAL_GLOBAL_CONTEXT_T *p,
                          int policy_slot);
nwalTxDmPSCmdInfo_t* netapip_netcpCfgGetSaSBInfo(NETAPI_NWAL_GLOBAL_CONTEXT_T *p,
                                      NETCP_CFG_SA_T    sa_app_id);
int netapip_netcpCfgGetSaInflowInfo(NETAPI_NWAL_GLOBAL_CONTEXT_T *p,
                                      NETCP_CFG_SA_T    sa_app_id,
                                      uint32_t *swInfo0,
                                      uint32_t *swInfo1);

int netapip_systemInit(NETAPI_HANDLE_T  *handle,
                       Bool             global_master_process);


void netapip_cleanupAtStart(void);




#ifdef __cplusplus
}
#endif

#endif
