/***********************************************************************
 * FILE: netapi.c
 * Purpose:  Main initialization and shutdown routines of NETAPI 
 *           user space transport library.
 ***********************************************************************
 * FILE: netapi.c
 * 
 * DESCRIPTION:  netapi main source file for user space transport
 *               library
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 * *****************************/

#include <unistd.h>
#include "ti/runtime/netapi/netapi.h"
#include "netapi_loc.h"

void* pBase;
NETAPI_SHM_T* pnetapiShm;
void *pRmClientHandle = NULL;

static NETAPI_CFG_T netapi_default_cfg=
{
    TUNE_NETAPI_PERM_MEM_SZ,
    0,  //start of packet offset for hw to place data on rx for default flow
    TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM, //max number of descriptors in system
    TUNE_NETAPI_NUM_GLOBAL_DESC,        //total we will use
    TUNE_NETAPI_DEFAULT_NUM_BUFFERS,   //#descriptors+buffers in default heap
    TUNE_NETAPI_DEFAULT_NUM_SOLO_DESCRIPTORS, //#descriptors w/o buffers in default heap
    TUNE_NETAPI_DEFAULT_BUFFER_SIZE,   //size of buffers in default heap
    0,
    0,
    0,
    NULL                                   /* Resource manager handle to use RM server */
};

HPLIB_SPINLOCK_IF_T spinlock_lol =
{
    hplib_mSpinLockInit,
    hplib_mSpinLockTryLock,
    hplib_mSpinLockIsLocked,
    hplib_mSpinLockLock,
    hplib_mSpinLockUnlock,
    hplib_mRWLockInit,
    hplib_mRWLockWriteLock,
    hplib_mRWLockWriteUnlock,
    hplib_mRWLockReadLock,
    hplib_mRWLockReadUnlock
};


HPLIB_SPINLOCK_IF_T spinlock_mp =
{
    hplib_mSpinLockInit,
    hplib_mSpinLockTryLock,
    hplib_mSpinLockIsLocked,
    hplib_mSpinLockLockMP,
    hplib_mSpinLockUnlockMP,
    hplib_mRWLockInit,
    hplib_mRWLockWriteLockMP,
    hplib_mRWLockWriteUnlockMP,
    hplib_mRWLockReadLockMP,
    hplib_mRWLockReadUnlockMP
};

/* Global variablesto hold virtual address of various subsystems */
hplib_virtualAddrInfo_T netapi_VM_VirtAddr[HPLIB_MAX_MEM_POOLS];

/* Global variables which needs to be populated with memory pool attributes
   which is passed to HPLIB for memory pool initialization*/
hplib_memPoolAttr_T netapi_VM_MempoolAttr[HPLIB_MAX_MEM_POOLS];
unsigned char *netapi_VM_QMemLocalDescRam;
unsigned char *netapi_VM_QMemGlobalDescRam;
unsigned char *netapi_VM_SaContextVaddr;

//global for this process that points to the 
// master thread for this process (either the SYS_MASTER, or PROC_MASTER)
//...
NETAPI_HANDLE_T * netapi_proc_master = NULL;
Pktlib_HeapIfTable  netapi_pktlib_ifTable;
NETAPI_PROC_GLOBAL_T netapi_proc_global;
//NETAPI_GLOBAL_T * netapi_get_global(){ return &netapi_global;}
NETAPI_GLOBAL_T * netapi_get_global(){ return &pnetapiShm->netapi_global;}
NETAPI_PROC_GLOBAL_T * netapi_get_proc_global(){ return &netapi_proc_global;}

/* utility API for NETAPI user to get pktlib if table to use if he creates his own heap */
Pktlib_HeapIfTable *netapi_getPktlibIfTable(void) {return &netapi_pktlib_ifTable;}


/********************************************************************
* FUNCTION PURPOSE:  API instantiates the NETAPI and allocated global resources.
 ********************************************************************
 * DESCRIPTION:  API instantiates the NETAPI and allocated global resources.
 ********************************************************************/
NETAPI_T netapi_init(int                master,
                     NETAPI_CFG_T *     p_cfg)
{
    int i;
    int err;
    int exception_id = 7;
    void* pBase = NULL;

    hplib_shmInfo_T* pshmBase = NULL;
    NETAPI_HANDLE_T * p = (NETAPI_HANDLE_T *) calloc(1,sizeof(NETAPI_HANDLE_T));
    NETAPI_HANDLE_T * p_master;
    if (!p) return NULL;
    p->master = master;

    /*  SYS_MASTER: The following segment should be done 1 Time per system boot by global master process's main core/thread */
    if (master == NETAPI_SYS_MASTER)
    {
        pBase = hplib_shmCreate(HPLIB_SHM_SIZE);

        if (pBase == NULL)
        {
            free(p);
            return NULL;
        }

        if (hplib_shmAddEntry(pBase, sizeof(NETAPI_SHM_T), NETAPI_ENTRY) !=
            hplib_OK)
        {
            free(p);
            return NULL;
        }
        hplib_utilOsalCreate();
    }
    /*ALL others:  we are not the system master; assume SYS_MASTER has
      created SHM area for us already, so just open it */
    else
    {
        pBase = hplib_shmOpen();
        if (pBase)
            Osal_start(pBase);
    }

    /*ALL:  create space for our local pktios */
    for(i=0;i<TUNE_NETAPI_MAX_PKTIO; i++)
    {
        p->pktios[i] = calloc(1,sizeof(PKTIO_HANDLE_T));
        if (!p->pktios[i])
        {
            free(p);
            return NULL;
        }
        else
        {
            PKTIO_ENTRY_T *pe = (PKTIO_ENTRY_T*)p->pktios[i];
            pe->qn.qNum = -1;
        }
    }

#ifdef NETAPI_INCLUDE_SCHED
    /* ALL: create space for scheduler */
    p->p_sched = calloc(1,sizeof(NETAPI_SCHED_HANDLE_T));
    if (!p->p_sched)
    {
        goto ERR_netapi_init;
    }
#endif

    /* SYS_MASTER:  The following segment should be done 1 Time per system boot by global master process */
    if (master == NETAPI_SYS_MASTER)
    {
        pnetapiShm = (NETAPI_SHM_T*)hplib_shmGetEntry(pBase, NETAPI_ENTRY);
        pnetapiShm->netapi_pktio_lock= hplib_spinLock_UNLOCKED_INITIALIZER;
        pnetapiShm->netapi_netcp_cfg_lock = hplib_spinLock_UNLOCKED_INITIALIZER;
        pnetapiShm->netapi_netcp_cfg_l3_classi_lock = hplib_spinLock_UNLOCKED_INITIALIZER;
        pnetapiShm->netapi_util_lock = hplib_spinLock_UNLOCKED_INITIALIZER;

        if (p_cfg)
        {
            memcpy(&pnetapiShm->netapi_global.cfg,p_cfg, sizeof(NETAPI_CFG_T));
        }
        else
        {
            memcpy(&pnetapiShm->netapi_global.cfg,&netapi_default_cfg, sizeof(NETAPI_CFG_T));
        }
        for(i=0;i<TUNE_NETAPI_MAX_PKTIO;i++) 
        {
            pnetapiShm->netapi_global.pktios[i].qn.qNum=-1;
            pnetapiShm->netapi_global.pktios[i].name[0]='\0';
        }
    }
    else
    {
        /* other 'masters' will just get a pointer to the netapi SHM area*/
        pnetapiShm = (NETAPI_SHM_T*)hplib_shmGetEntry(pBase, NETAPI_ENTRY);
    }

    /* all for convenience set a back pointer to the SOC global area
       (in SHM) and process global in netapi_proc_global */
    p->global = (void *) &pnetapiShm->netapi_global;
    p->proc_global = (void *)&netapi_proc_global;
    /* Update spinLock to point to either MP spinlock or fast spinlocks, this is 
       for all callers of netapi_init*/
    if(p_cfg)
    {
        if(p_cfg->def_multi_process)
            p->spinLock = spinlock_mp;
        else
            p->spinLock= spinlock_lol;
    }
    else
        p->spinLock = spinlock_lol;

    /* SYS_MASTER, PROC_MASTER: save a pointer to its netapi structure
       globably for other threads/core of process to use */
    if ((master == NETAPI_SYS_MASTER)||(master == NETAPI_PROC_MASTER))
    {
        p_master=netapi_proc_master = p;
        if (p_cfg->rmHandle)
            pRmClientHandle = p_cfg->rmHandle;
    }
    else
    {
        /* CORE_MASTER/NO_MASTER: get system master/proc master's handle */
        p_master=(NETAPI_HANDLE_T *) netapi_proc_master;
    }


    /* SYS_MASTER: The following segment should be done 1 Time per system boot by global master process */
    /* system init */
    if(master == NETAPI_SYS_MASTER)
    {
        err = netapip_systemInit(p, NETAPI_TRUE);
        if (err < 0)
        {
            goto ERR_netapi_init;
        }
    }
    /* PROC_MASTER case */
    else if (master==NETAPI_PROC_MASTER)
    {
        /* More limited initialization */
        err = netapip_systemInit(p,
                                 NETAPI_FALSE);
        if (err<0)
        {
            goto ERR_netapi_init;
        }
    }
    /* NO_MASTER:  case */
    else if (master == NETAPI_NO_MASTER)
    {
        /*Just copy master's pktio list for now */
        p->n_pktios = p_master->n_pktios;
        memcpy(&p->pktios[0],&p_master->pktios[0],TUNE_NETAPI_MAX_PKTIO*sizeof(PKTIO_HANDLE_T));
        p->nwal_local=p_master->nwal_local;
    }
    /* this is the NETAPI_CORE_MASTER case */
    else if (master == NETAPI_CORE_MASTER)
    {
        /* Start the QMSS. */
        if (netapip_startQm(pRmClientHandle) != 1)
        {
            goto ERR_netapi_init;
        }
        netapip_startNwal(p_master->netcp_heap, 
                          p_master->netcp_control_rx_heap,
                          p_master->netcp_control_tx_heap, 
                          &p->nwal_local,
                          &pnetapiShm->netapi_global.cfg,
                          &pnetapiShm->netapi_global.nwal_context,
                          master);
    }
    else
    {
        goto ERR_netapi_init;
    }

    return (NETAPI_T) p;

/* error handling */
ERR_netapi_init:
    for(i=0;i<TUNE_NETAPI_MAX_PKTIO; i++)
    {
        if (p->pktios[i])
        {
            free(p->pktios[i]);
        }
    }
#ifdef NETAPI_INCLUDE_SCHED
    if (p->p_sched)
    {
        free(p->p_sched);
    }
#endif
    free(p);
    return NULL;
}

/********************************************************************
* FUNCTION PURPOSE:  API de-allocates all global resources allocated as part 
*                                   of ref netapi_init
 ********************************************************************
 * DESCRIPTION:   API de-allocates all global resources allocated as part 
*                                   of ref netapi_init
 ********************************************************************/
void netapi_shutdown(NETAPI_T h)
{
    int i;
    hplib_shmInfo_T* pshmBase;
    NETAPI_HANDLE_T * p = (NETAPI_HANDLE_T *) h;
    if (!p) return;
    void * map_base;
    hplib_VirtMemPoolheader_T *poolHdr;

    if (p->master == NETAPI_SYS_MASTER)
    {
    
        /* Un-configure rules for execption packet handling */

        /* close nwal master (per soc) context */
        nwal_delete(pnetapiShm->netapi_global.nwal_context.nwalInstHandle);

        /* close heaps */
        netapi_closeHeap(h, p->netcp_heap);
        netapi_closeHeap(h, p->netcp_control_rx_heap);
        netapi_closeHeap(h, p->netcp_control_tx_heap);
        netapi_closeHeap(h, netapi_get_global()->nwal_context.pa2sa_heap); 
        netapi_closeHeap(h, netapi_get_global()->nwal_context.sa2pa_heap); 

        //loop over registered heaps
        for(i=0;i<TUNE_NETAPI_MAX_HEAPS;i++)
        {
            if (p->createdHeaps[i])
            {
                netapi_closeHeap(h,p->createdHeaps[i]);
                p->createdHeaps[i]=NULL;
            }
        }
        netapip_cleanupAtStart();  //clear 1st 50 not-specified queues
        //netapip_cleanupAtStart();  //clear 1st 50 not-specified queues
        //reset DDR malloc area
        hplib_resetMallocArea(0);
        Qmss_removeMemoryRegion(p->memRegion,0);
        hplib_vmTeardown();
        hplib_shmDelete();
    }
    else if (p->master == NETAPI_PROC_MASTER)
    {
        //we can zap these because they are local to our process
        netapi_closeHeap(h, p->netcp_control_rx_heap);
        netapi_closeHeap(h, p->netcp_control_tx_heap);
        //loop over registered heaps (again local to our process)
        for(i=0;i<TUNE_NETAPI_MAX_HEAPS;i++)
        {
            if (p->createdHeaps[i])
            {
                netapi_closeHeap(h,p->createdHeaps[i]);
                p->createdHeaps[i]=NULL;
            }
        }
        Qmss_removeMemoryRegion(p->memRegion,0);
        hplib_vmTeardown();
    }
    //TODO other master types
    /* error handling */
    
    for(i=0;i<TUNE_NETAPI_MAX_PKTIO; i++)
    {
        if (p->pktios[i])
        {
            free(p->pktios[i]);
            p->pktios[i] = NULL;
        }
    }
#ifdef NETAPI_INCLUDE_SCHED
    if (p->p_sched)
    {
        free(p->p_sched);
        p->p_sched = NULL;
    }
#endif

    free(p);
    return;
}

/***********************************************************************
* FUNCTION PURPOSE:  API is used to poll for NETCP configuration response messages.
************************************************************************
* DESCRIPTION:  This API is used to poll the netapi internal heaps and any 
 *              application-created heaps that have been registered with 
 *              the netapi instance. The poll function checks the garbage collection 
 *              queue associated with the heap and returns descriptors and buffers 
 *              when appropriate to the main free queue.
***********************************************************************/
void netapi_pollHeapGarbage(NETAPI_T h)
{
    int i;
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;
    Pktlib_garbageCollection(n->netcp_heap);
    //no need to do garbage collection on other internal heaps
    for(i=0;i<TUNE_NETAPI_MAX_HEAPS;i++)
    {
        if (n->createdHeaps[i])
        {
            Pktlib_garbageCollection(n->createdHeaps[i]);
        }
    }
}

/****************************************************************************
* FUNCTION PURPOSE:  API is used to poll for NETCP configuration response messages.
****************************************************************************
* DESCRIPTION:  This API is used to poll the netapi internal heaps and any 
 *              application-created heaps    that have been registered with the 
 *              netapi instance. The poll function checks the garbage collection 
 *              queue associated with the heap and returns descriptors and buffers
 *              when appropriate to the main free queue.
***************************************************************************/
void netapi_netcpPoll(NETAPI_T  p)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) p;
    nwal_pollCtl( ((NETAPI_GLOBAL_T *) (n->global))->nwal_context.nwalInstHandle,NULL,NULL);
}
/**************************************************************************************
* FUNCTION PURPOSE:  API is used to return configured descriptor memory region Id.
***************************************************************************************
* DESCRIPTION:  This API is used to return the configured descriptor memory region Id.
**************************************************************************************/
int netapi_getMemoryRegionId(NETAPI_T  p)
{
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) p;
    if(netapi_proc_master)
        return (netapi_proc_master->memRegion);
    else
        return -1;
}
