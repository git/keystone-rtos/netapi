/******************************************************************************
 * FILE PURPOSE:  Netapi Security configuration header file
 ******************************************************************************
 * FILE NAME:   netapi_sec.h
 *
 * DESCRIPTION: netapi security  header file for user space transport library
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* ============================================================= */


/**
 *   @file netapi_sec.h
 *   @brief netapi security header file for user space transport library
 */

#ifndef __NETAPI_SEC__H
#define __NETAPI_SEC__H

#ifdef __cplusplus
extern "C" {
#endif


#include "netcp_cfg.h"
#include "ti/drv/nwal/nwal.h"
#include <ti/drv/sa/salld.h>




/**
 *  @ingroup cfg_security_structures
 *  @brief NETAPI SA Statistics
 *
 *  @details Pointer to this structure is passed in the call to netapi_getSaStats API. It will be popluated with the requested statistics.
 */
typedef struct NETAPI_SA_STATS_Tag
{
/**
 * Bit map indicating the IPSec SA Inflow/Side band data mode stats validity
 * <br>
 * The validParams field indicates to the caller which stats being returned by calling the @ref netapi_getSaStats are valid.
 *      @ref NETAPI_IPSEC_STAT_VALID, @ref NETAPI_SIDEBAND_DATA_MODE_STAT_VALID
 */
    uint16_t            validParams;            /**< Bit map indicating the IPSec SA Inflow/Side band data mode stats validity */

/**
 * @def NETAPI_IPSEC_STAT_VALID
 * @ingroup security_constants
 *      Indicates to user application that IPSEC stats are valid for INFLOW mode
 */
#define NETAPI_IPSEC_STAT_VALID                 0x0001 

/**
 * @def NETAPI_SIDEBAND_DATA_MODE_STAT_VALID
 * @ingroup security_constants
 *      Indicates to user application that IPSEC stats are valid for SIDEBAND mode
 */
#define NETAPI_SIDEBAND_DATA_MODE_STAT_VALID    0x0002

    Sa_IpsecStats_t     saIpsecStats;           /**<  Structure containing IPSEC stats for INFLOW MODE*/
    Sa_DataModeStats_t  dataModeStats;          /**<  Structure containing Data Mode stats for SIDEBAND MODE */
} NETAPI_SA_STATS_T;


/**
 *  @ingroup cfg_security_structures
 *  @brief NETAPI security SA information
 *
 *  @details This structure contains the information necessary to create a security association (for either for inflow mode or sideband mode)
 */
typedef struct NETAPI_SEC_SA_INFO_tag
{
    nwal_SaDir          dir;            /**< Direction for the channel. Inbound or Outbound */
    uint32_t            spi;            /**< IPSec Security Parameter index */
    nwal_IpSecProto     proto;          /**< IpSec Proto (ESP/AH) */
    nwal_saMode         saMode;         /**< Tunnel/ Transport mode (sideband RX/TX) */
    nwal_IpType         ipType;         /**< IPV4 or V6 (sideband RX/TX) */
    nwalIpAddr_t        src;            /**< Source IP Address (remote) (sideband RX) */
    nwalIpAddr_t        dst;            /**< DST IP Address (local) (sideband RX) */
    uint32_t            replayWindow;   /**< Replay Window Size (sideband RX) */
    nwal_saAALG         authMode;       /**< Authentication Algorithm */
    nwal_saEALG         cipherMode;     /**< Encryption Algorithm */
    uint32_t            esnLo;          /**< Initial Value of Extended Sequence Number LSB (sideband TX) */
    uint32_t            esnHi;          /**< Initial Value of Extended Sequence Number MSB (sideband TX) */
} NETAPI_SEC_SA_INFO_T;



/**
 * @brief This defines the SA mode of operation to be INFLOW. This means that IPSEC will be applied 
 *        as the packet is being received or just before it is transmitted. This is a more efficient 
 *        way to use SA and saves host cycles.
 * @ingroup security_constants
 */
#define NETAPI_SEC_SA_INFLOW   0x2

/**
 * @brief This defines the SA mode of operation to be SIDEBAND. This means that Security Acclerator is
 *        to be used a traditional accelerator where for RX, the packet will first be received on host 
 *        and then sent back to SA for crypto.  For TX the packet will first be sent to SA for crypto,
 *        returned to host and then sent to NETCP for transmit.
 * @ingroup security_constants
 */
#define NETAPI_SEC_SA_SIDEBAND 0x1
/**
 *  @ingroup cfg_security_functions
 *  @brief netapi_secAddSA  API to add an IPSEC SA.
 *
 *  @details API to add an IPSec SA. SAs are IPSec security contexts and define a uni-directional
 *           secure path (tunnel or transport). SAs are attached to MAC interfaces that have already
 *           been created. API allows SA to be configured as either inflow or sideband mode. This API is used for both receive and transmit SAs.
 *  @param[in]  h            The NETAPI handle, @ref NETAPI_T
 *  @param[in]  iface_no     Interface to attach SA to.
 *  @param[in]  sa_info      Information on the SA being added, @ref NETAPI_SEC_SA_INFO_T
 *  @param[in]  key_params   Security key information for the SA.
 *  @param[in]  mode         SA implementation mode @ref NETAPI_SEC_SA_SIDEBAND or @ref NETAPI_SEC_SA_INFLOW
 *  @param[in]  route        Optional: @ref NETCP_CFG_ROUTE_HANDLE_T
 *  @param[in]  data_mode_handle     Returned data mode handle for PKTIO (in the case of sideband SAs)
 *  @param[in]  inflow_mode_handle   Returned inflow mode handle for PKTIO (in the case of TX inflow SAs)
 *  @param[in]  user_data     Optional: pointer to user provided data associated with SA, optional
 *  @param[out]  perr        Pointer to error code.
 *  @retval     Application id associated with created SA @ref NETCP_CFG_SA_T. 
 *              This ID is used when referencing this SA in subsequent APIs (eg. to delete it).
 *              Also in the case of Receive Inflow,  packets will be tagged with this ID so that s/w will know 
 *              that the packet has already been decrypted, authenticated and window-replay checked.
 *              (note: if a RX policy is matched also then the ID associated with the policy will be tagged instead).
 *  @pre        @ref netapi_init
 */
NETCP_CFG_SA_T netapi_secAddSA(NETAPI_T                     h,
                                int iface_no,
                                NETAPI_SEC_SA_INFO_T*       sa_info,
                                nwalSecKeyParams_t*         key_params,
                                int                         mode,
                                NETCP_CFG_ROUTE_HANDLE_T    route,
                                void**                      data_mode_handle,
                                void**                      inflow_mode_handle,
                                void*                       user_data,
                                int*                        perr);

/**
 *  @ingroup cfg_security_functions
 *  @brief netapi_secAddSAIP  API to add an IPSEC SA with IP handle.
 *
 *  @details API to add an IPSec SA. SAs are IPSec security contexts and define a uni-directional
 *           secure path (tunnel or transport). SAs are attached to MAC interfaces that have already
 *           been created. API allows SA to be configured as either inflow or sideband mode. This API is used for both receive and transmit SAs.
 *  @param[in]  h            The NETAPI handle, @ref NETAPI_T
 *  @param[in]  iface_no     Interface to attach SA to.
 *  @param[in]  sa_info      Information on the SA being added, @ref NETAPI_SEC_SA_INFO_T
 *  @param[in]  key_params   Security key information for the SA.
 *  @param[in]  mode         SA implementation mode @ref NETAPI_SEC_SA_SIDEBAND or @ref NETAPI_SEC_SA_INFLOW
 *  @param[in]  route        Optional: @ref NETCP_CFG_ROUTE_HANDLE_T
 *  @param[in]  data_mode_handle     Returned data mode handle for PKTIO (in the case of sideband SAs)
 *  @param[in]  inflow_mode_handle   Returned inflow mode handle for PKTIO (in the case of TX inflow SAs)
 *  @param[in]  user_data     Optional: pointer to user provided data associated with SA, optional
 *  @param[in]  ip_rule      IP rule @ref NETCP_CFG_IP_T
 *  @param[out]  perr        Pointer to error code.
 *  @retval     Application id associated with created SA @ref NETCP_CFG_SA_T. 
 *              This ID is used when referencing this SA in subsequent APIs (eg. to delete it).
 *              Also in the case of Receive Inflow,  packets will be tagged with this ID so that s/w will know 
 *              that the packet has already been decrypted, authenticated and window-replay checked.
 *              (note: if a RX policy is matched also then the ID associated with the policy will be tagged instead).
 *  @pre        @ref netapi_init
 */
NETCP_CFG_SA_T netapi_secAddSAIP(NETAPI_T h,
                                 int iface_no,
                                 NETAPI_SEC_SA_INFO_T *sa_info,
                                 nwalSecKeyParams_t * key_params,
                                 int inflow_mode,
                                 NETCP_CFG_ROUTE_HANDLE_T  route,
                                 void **p_data_mode_handle,
                                 void **p_inflow_mode_handle,
                                 void * p_user_data,
                                 NETCP_CFG_IP_T ip_rule,
                                 int * perr);

/**
 *  @ingroup cfg_security_functions
 *  @brief  netapi_secDelSA: API to delete an IPSEC SA. 
 *
 *  @details API to delete an IPSEC SA
 *  @param[in]  h       The NETAPI handle, @ref NETAPI_T
 *  @param[in] iface_no Interface to attach SA to.
 *  @param[in] sa_app_id Application id returned from call to @ref netapi_secAddSA
 *  @param[out] perr Pointer to error code.
 *  @retval     none
 *  @pre        @ref netapi_init @ref netapi_secAddSA
*/
void netapi_secDelSA(NETAPI_T       h,
                     int            iface_no, 
                     NETCP_CFG_SA_T sa_app_id,
                     int*           perr);

/**
 *  @ingroup cfg_security_functions
 *  @brief netapi_secAddRxPolicy: API to add a receive security policy
 *
 *  @details API to add a receive security policy
 *  @param[in]  h       The NETAPI handle @ref NETAPI_T
 *  @param[in] sa       Application id returned from call to @ref netapi_secAddSA.  
 *                      This SA must have beeen configured for (a) RX and (b) Inflow mode.
 *  @param[in] ipType   IPV4 or IPV6
 *  @param[in] src_ip_addr  source IP for policy check
 *  @param[in] dst_ip_addr  destination IP for policy check
 *  @param[in] ip_qualifiers  IP qualifiers for policy check
 *  @param[in] route    Optional: @ref NETCP_CFG_ROUTE_HANDLE_T
 *  @param[in]  user_data   Optional: pointer to user provided data associated with policy
 *  @param[out] perr    Pointer to error code.
 *  @retval    Aplication id associated with created receive security policy @ref NETCP_CFG_IPSEC_POLICY_T.  This is used to refer to the policy in the @ref netapi_secDelRxPolicy call.  Also, packets that match this policy but do not pass any further lookups in NETCP will be tagged with this ID
 *  @pre        @ref netapi_init @ref netapi_secAddSA
*/
NETCP_CFG_IPSEC_POLICY_T netapi_secAddRxPolicy(NETAPI_T                     h,
                                               NETCP_CFG_SA_T               sa,
                                               nwal_IpType                  ipType,
                                               nwalIpAddr_t*                src_ip_addr,
                                               nwalIpAddr_t*                dst_ip_addr,
                                               nwalIpOpt_t*                 ip_qualifiers,
                                               NETCP_CFG_ROUTE_HANDLE_T     route,
                                               void*                        user_data,
                                               int*                         perr);

/**
 *  @ingroup cfg_security_functions
 *  @brief netapi_secDelRxPolicy: API to add a receive security policy
 *
 *  @details API to add a receive security policy
 *  @param[in]  h               The NETAPI handle, @ref NETAPI_T
 *  @param[in] policy_app_id    Application id returned from call to @ref netapi_secAddRxPolicy
 *  @param[out] perr            Pointer to error code.
 *  @retval     none
 *  @pre        @ref netapi_init, @ref netapi_secAddRxPolicy
*/
void netapi_secDelRxPolicy(NETAPI_T                 h,
                           NETCP_CFG_IPSEC_POLICY_T policy_app_id, 
                           int*                     perr);


/**
 *  @ingroup cfg_security_functions
 *  @brief netapi_getSaStats: API to retrieve SA statistics via NWAL.
 *
 *  @details API to retrieve SA statistics via NWAL.
 *  @param[in]  h   The NETAPI handle, @ref NETAPI_T
 *  @param[in]  handle @ref NETCP_CFG_SA_T
 *  @param[out] pSaStats Pointer to SA stats which will get populated by this API call @ref NETAPI_SA_STATS_T
 *  @retval     none
 *  @pre        @ref netapi_init, @ref netapi_secAddSA
*/
void  netapi_getSaStats (NETAPI_T                   h,
                         NETCP_CFG_SA_T             handle,
                         NETAPI_SA_STATS_T*         pSaStats);



/**
 *  @ingroup cfg_security_functions
 *  @brief  API to retrieve internal context information from channel resources
 *          maintained by NWAL. Selective NetCP PA/SA channel handles are exposed
 *          to handle the case of multiple owner use case for PA/SA LLD
 *  @details API to retrieve internal context information from channel resources
 *          maintained by NWAL. Selective NetCP PA/SA channel handles are exposed
 *          to handle the case of multiple owner use case for PA/SA LLD
 *  @param[in]   h      The NETAPI handle, @ref NETAPI_T
 *  @param[in]   appId  Application Id returned from any config APIs
 *  @param[out]  pInfo  NWAL Channel context information
 *  @retval     none
 *  @pre        @ref netapi_init, @ref netapi_secAddSA
 */
void netapi_secGetChanCtxInfo(NETAPI_T h,
                           NETCP_CFG_APP_ID_T appId,
                           nwalChanCxtInfo_t* pInfo);

typedef struct  {
    uint32_t            spi;            /**< IPSec Security Parameter index */
    nwalIpAddr_t        src;            /**< Source IP Address */
    nwalIpAddr_t        dst;            /**< Destination Address */
    nwal_IpSecProto     proto;          /**< IpSec Proto */
    uint8_t             inUse;
    uint8_t             ref_count;
    NETCP_CFG_SA_T      app_id;
} NETAPI_SA_INFO_LOCAL_T;

#ifdef __cplusplus
}
#endif
#endif
