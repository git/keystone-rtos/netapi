/******************************************************************************
 * FILE PURPOSE:  Top level interface file for NWAL Module
 ******************************************************************************
 * FILE NAME:   pktio.h
 *
 * DESCRIPTION: netapi PKTIO module header file
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *   @file pktio.h
 *   @brief pktio module main header file for user space transport library
 *   @details:  pktio provides an abstraction to H/W queues that are used to transmit and receive packets, 
 *              IPC messages, etc.  Pktio channels can be created by user but there are also several canned 
 *              channels available  for NETCP transmit, receive, SA sideband crypto transmit and receive 
 */



#ifndef __PKTIO__H
#define __PKTIO__H

#ifdef __cplusplus
extern "C" {
#endif


//#include "netapi.h"
#include "ti/drv/nwal/nwal.h"
#include "ti/drv/nwal/nwal_util.h"

/**
 * @def NETCP_TX
 * @ingroup pktio_constants
 *      This defines the default PKTIO NETCP transmit channel name
 */
#define NETCP_TX "NETCP_TX"

/**
 * @def NETCP_RX
 * @ingroup pktio_constants
 * @brief This defines the default PKTIO NETCP receive channel name
 */
#define NETCP_RX "NETCP_RX"

/**
 * @def NETCP_SB_RX
 * @ingroup pktio_constants
 * @brief This defines the PKTIO NETCP-SA receive SIDEBAND channel name.
          This channel is used to receive the results from sideband crypto operations
 */
#define NETCP_SB_RX "NETCP_SB_RX"

/**
 * @def NETCP_SB_TX
 * @ingroup pktio_constants
 * @brief This defines the PKTIO NETCP-SA transmit SIDEBAND channel name.
          This channel is used to send packets for sideband crypto operations
 */
#define NETCP_SB_TX "NETCP_SB_TX"

/**
 * @def PKTIO_MAX_NAME
 * @ingroup pktio_constants
 *      This defines the maximum length of a pktio channel name
 */
#define PKTIO_MAX_NAME 20

/**
 *  @ingroup pktio_structures
 *  @brief PKTIO meta data information .
 *
 *  @details PKTIO meta data information. meta data is used to convey the results of NETCP pre-processing on receive packets and to tell NETCP what functions to perform on transmitted packets. It is a union of sub-structures (one for TX, one for RX, one for sideband rx, one for sideband tx).
 */
typedef struct PKTIO_METADATA_Tag
{

    int flags1;     /**< Configuration flags for PKTIO channel, @ref PKTIO_META_RX,
                                                                @ref PKTIO_META_TX,
                                                                @ref PKTIO_META_SB_RX,
                                                                @ref PKTIO_META_SB_TX*/

/**
 * @def PKTIO_META_RX
 * @ingroup pktio_constants
 *      This defines the PKTIO NETCP receive INFLOW channel
 */
#define PKTIO_META_RX 0x01

/**
 * @def PKTIO_META_TX
 * @ingroup pktio_constants
 *      This defines the PKTIO NETCP transmit INFLOW channel
 */
#define PKTIO_META_TX 0x02

/**
 * @def PKTIO_META_SB_RX
 * @ingroup pktio_constants
 *      This defines the PKTIO NETCP SIDEBAND channel channel
 */
#define PKTIO_META_SB_RX 0x4

/**
 * @def PKTIO_META_SB_TX
 * @ingroup pktio_constants
 *      This defines the PKTIO NETCP transmit SIDEBAND channel
 */
#define PKTIO_META_SB_TX 0x8


/**
 * @def PKTIO_META_IFDMA_TX
 * @ingroup pktio_constants
 *      This defines the PKTIO NETCP infrastructure DMA transfer channel
 */
#define PKTIO_META_IFDMA_TX 0x10

/**
 * @def PKTIO_META_APP_DEF
 * @ingroup pktio_constants
 *      This allows user space application to define custom packet types.
 */
#define PKTIO_META_APP_DEF 0x80000000

/**
 * @brief NWAL Packet meta data information
 */
    union
    {
        nwalRxPktInfo_t*        rx_meta;          /**< NWAL Packet meta data information for incoming packet */
        nwalTxPktInfo_t*        tx_meta;          /**< NWAL Packet meta data information for outgoing packet */
        nwalDmRxPayloadInfo_t*  rx_sb_meta;       /**<NWAL Data mode meta data payload information from NetCP */
        nwalDmTxPayloadInfo_t*  tx_sb_meta;       /**< NWAL Data Mode Payload information for packet to SA */
        unsigned int            tx_ifdma_dest;    /**< infrastructure dma destination flow */
    } u;                                          /**< union NWAL Packet meta data information  */

    void * sa_handle;                       /**< This contains the appplication id associated with created SA.
                                            details This is used when crypto is to be performed on the 
                                            egress packet */
} PKTIO_METADATA_T;

/**
 *  @ingroup pktio_structures
 *  @brief PKTIO fast path configuration structure.
 *
 *  @details This strucuture allows user space applications to specify the PKTIO packet send function.
 *
 *  @note This configuration is not expected at time of @ref netapi_pktioOpen but can be updated via
 *        @ref netapi_pktioControl API.
 */
typedef struct PKTIO_FAST_PATH_CONFIG_Tag
{
    int               fp_send_option;  /**< PKTIO_FP_NONE, @ref PKTIO_FP_NO_CRYPTO_NO_CKSUM_PORT,
                                        PKTIO_FP_L4CKSUM_PORT, @ref PKTIO_FP_ESP_L4CKSUM_PORT,
                                        @ref PKTIO_FP_AH_L4CKSUM_PORT, @ref PKTIO_FP_ESP_PORT, 
                                        @ref PKTIO_FP_AH_PORT */

/**
 * @def PKTIO_FP_NONE
 * @ingroup pktio_constants
 *      Use this define to reconfigure the PKTIO channel send function to not use any PKTIO fast path
        send functions.
 */
#define  PKTIO_FP_NONE 0

/**
 * @def PKTIO_FP_NO_CRYPTO_NO_CKSUM_PORT
 * @ingroup pktio_constants
 *      Use this define to send packet with updates to ENET port, no crypto operation,
 *      no L4 checksum to be performed.
 */
#define  PKTIO_FP_NO_CRYPTO_NO_CKSUM_PORT 1

/**
 * @def PKTIO_FP_L4CKSUM_PORT
 * @ingroup pktio_constants
 *      Use this define to send packet with updates to L4 checksum, ENET port, no crypto operation
 *      to be performed.
 */
#define  PKTIO_FP_L4CKSUM_PORT 2

/**
 * @def PKTIO_FP_ESP_L4CKSUM_PORT
 * @ingroup pktio_constants
 *      Use this define to send packet with Crypto ESP, UDP checksum, updates to ENET port
 */
#define  PKTIO_FP_ESP_L4CKSUM_PORT 3

/**
 * @def PKTIO_FP_AH_L4CKSUM_PORT
  * @ingroup pktio_constants
 *      Use this define to send packet with Cypto AH, UDP checksum, updates to ENET port
 */
#define  PKTIO_FP_AH_L4CKSUM_PORT 4

/**
 * @def PKTIO_FP_ESP_PORT
 * @ingroup pktio_constants
 *      Use this define to send packet with Crypto ESP packet, updates to ENET port
 */
#define  PKTIO_FP_ESP_PORT 5

/**
 * @def PKTIO_FP_AH_PORT
 * @ingroup pktio_constants
 *      Use this define to send packet with AH packet
 */
#define  PKTIO_FP_AH_PORT 6

/**
 * @def PKTIO_FP_AH_PORT
 * @ingroup pktio_constants
 *      Use this define to send packet with Crypto ESP, IP checksum, updates to ENET port
 */
#define  PKTIO_FP_ESP_L3CKSUM_PORT 7

    nwalTxPktInfo_t   *txPktInfo;  /** <The parameters in this structure are used to 
                                         provide additional details for the outgoing packet*/
} PKTIO_FAST_PATH_CONFIG_T;



struct PKTIO_HANDLE_tag;
/**
 *  @ingroup pktio_structures
 *  @brief PKTIO configuration information
 *
 *  @details PKTIO :q information
 */
typedef struct PKTIO_CFG_Tag
{
/**
 * @def PKTIO_RX
 * @ingroup pktio_constants
 *      This defines the pktio channel as type read, i.e., for ingress
 */
#define PKTIO_RX 0x1

/**
 * @def PKTIO_TX
 * @ingroup pktio_constants
 *      This defines the pktio channel as type write, i.e. for egress
 */
#define PKTIO_TX 0x2

/**
 * @def PKTIO_RX_TX
 * @ingroup pktio_constants
 *      This defines the pktio channel as type read/write 
 */
#define PKTIO_RX_TX (PKTIO_RX | PKTIO_TX)

/**
 * Flags for PKTIO channel configuration
 * <br>
 * The following are flags used to configure the pktio channel:
 *      @ref PKTIO_RX , @ref PKTIO_TX, @ref PKTIO_RX_TX
 */
    int flags1; 

/**
 * @def PKTIO_GLOBAL
 * @ingroup pktio_constants
 *      This defines the pktio channel as type global. 
        Type global means that this channel can be used by all worker threads/cores
 */
#define PKTIO_GLOBAL 0x1

/**
 * @def PKTIO_LOCAL
 * @ingroup pktio_constants
 *      This defines the pktio channel as type local.
        Type local means that thi s channel can only be used by the the thread/core that created it; 
        its name will not be visible to other threads/cores
 */
#define PKTIO_LOCAL  0x2

/**
 * @def PKTIO_PKT
 * @ingroup pktio_constants
 *      This defines the pktio channel is for NETCP
 */
#define PKTIO_PKT    0x4

/**
 * @def PKTIO_SB
 * @ingroup pktio_constants
 *      This defines the pktio channel is for sideband crypto
 */
#define PKTIO_SB     0x8
#define PKTIO_IFDMA  0x10  //define this if this channel is for ifrastructure dma  
    int flags2; /**< Flags for PKTIO channel configuration, @ref PKTIO_LOCAL , @ref PKTIO_GLOBAL, 
                    @ref PKTIO_PKT, @ref PKTIO_SB*/

/**
 * @def PKTIO_Q_ANY
 * @ingroup pktio_constants
 *      This defines the pktio IO queue number to be specified by the transport library. 
 */
#define PKTIO_Q_ANY QMSS_PARAM_NOT_SPECIFIED

    int qnum;       /**< PKTIO channel queue number */
    int max_n;      /**< Maximum number of packets read in 1 poll */
    PKTIO_FAST_PATH_CONFIG_T fast_path_cfg;  /** < @ref PKTIO_FAST_PATH_CONFIG_T */
    int queueType ;             /**<Queue Manager queue type,
                                    refer to ti/csl/csl_qm_queue.h */
} PKTIO_CFG_T;


/**
 *  @ingroup pktio_structures
 *  @brief PKTIO polling control struct, currently NOT_IMPLEMENTED
 *
 *  @details PKTIO polling control struct, currently NOT_IMPLEMENTED
 */
typedef struct PKTIO_POLL_Tag
{
} PKTIO_POLL_T;

/**
 *  @ingroup netapi_cb_functions
 *  @brief PKTIO_CB   Callback function to be issued on packet receive
 * 
 *  @details The application provides a callback function that gets invoked on packet receive
 *  @param[in]  channel     The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  p_recv      Pointer to the packets received.
 *  @param[in]  p_meta      Pointer to meta data associated with packet, @ref PKTIO_METADATA_T
 *  @param[in]  n_pkts      Number of packets received.
  * @param[in]  ts          Timestamp associted with received packets.
 *  @retval     none
 *  @pre       @ref netapi_pktioOpen
 */
typedef void (*PKTIO_CB)(struct PKTIO_HANDLE_tag* channel,
                         Ti_Pkt*                  p_recv[],
                         PKTIO_METADATA_T         p_meta[],
                         int                      n_pkts,
                         uint64_t                 ts);

/**
 *  @ingroup pktio_functions
 *  @brief PKTIO_SEND   PKTIO specific send function
 * 
 *  @details The application calls this PKTIO specific send function to transmit packet
 *  @param[in]  channel         The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  p_send          Pointer to the packet to send
 *  @param[in]  p_meta          Pointer to meta data associated with packet, @ref PKTIO_METADATA_T
 *  @param[out] p_err             Pointer to error code.
 *  @retval     none 
 *  @pre       @ref netapi_pktioOpen
 */
typedef int (*PKTIO_SEND)(struct PKTIO_HANDLE_tag * channel,
                          Ti_Pkt*                   p_send,
                          PKTIO_METADATA_T*         p_meta,
                          int*                      p_err);


/**
 *  @ingroup pktio_functions
 *  @brief PKTIO_POLL   PKTIO specific poll function
 * 
 *  @details The application calls this PKTIO specific POLL function
 *  @param[in]  channel     The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  p_poll_cfg  @ref PKTIO_POLL_T, currently NOT_IMPLEMENTED
 *  @param[out] p_err       Pointer to error code.
 *  @retval     none 
 *  @pre       @ref netapi_pktioOpen
 */
typedef int (*PKTIO_POLL)(struct PKTIO_HANDLE_tag * channel,
                          PKTIO_POLL_T*             p_poll_cfg,
                          int*                      p_err);


/**
 * @brief This defines an unused packet io channel slot
 */
#define PKTIO_NA 0


struct NETAPI_tag;

/**
 *  @ingroup pktio_structures
 *  @brief PKTIO handle structure definition.
 *
 *  @details PKTIO handle strucutre which is returned from call to @ref netapi_pktioCreate
 */
typedef struct PKTIO_HANDLE_Tag
{
/**
 * @def PKTIO_INUSE
 * @ingroup pktio_constants
 *      This defines whether the pktio channel entry is valid.
 */
#define PKTIO_INUSE 0xfeedfeed

    int inuse;                      /**<true is pktio channel is in use  */

/**
 * Set the "use_nwal" field to one of the defines listed below.
 * <br>
 * The following defines are used to populate the use_nwal field:
 *      @ref PKTIO_4_IPC , @ref PKTIO_4_ADJ_NWAL, @ref PKTIO_DEF_NWAL. @ref PKTIO_4_ADJ_SB., @ref PKTIO_DEF_SB
 */
    int use_nwal;                  

/**
 * @def PKTIO_4_IPC
 * @ingroup pktio_constants
 *      This define is for channels used for IPC between cores
 */
#define  PKTIO_4_IPC 0

/**
 * @def PKTIO_4_ADJ_NWAL
 * @ingroup pktio_constants
 *      This define is for NETCP (RX) channels whose associated queues are managed by NWAL
 */
#define  PKTIO_4_ADJ_NWAL 1

/**
 * @def PKTIO_DEF_NWAL
 * @ingroup pktio_constants
 *      This define is for NETCP channels that are tied to the default NWAL RX/TX queues
 */
#define  PKTIO_DEF_NWAL 2

/**
 * @def PKTIO_4_ADJ_SB
 *      This define is for (RX) channels used to get crypto side band results via application managed queues
 * @ingroup pktio_constants
 */
#define  PKTIO_4_ADJ_SB 3

/**
 * @def PKTIO_DEF_SB
 * @ingroup pktio_constants
 *      This define is for channels tied to the default crypto side band  queues
 */
#define  PKTIO_DEF_SB 4

    struct NETAPI_tag * back;           /**< back handle  to NETPAI instance */
    void * nwalInstanceHandle;          /**<save here for conveninece  this is the nwal handle set at init time*/
    PKTIO_CB cb;                        /**< callback for channel (for Rx) */
    PKTIO_CFG_T cfg;                    /**<configuration  of channel */
    Qmss_QueueHnd q;                    /**<the associated queue handle for channel */
    Qmss_Queue qInfo;                   /**<and its qm#/q# */
    int max_n;                          /**<max # of pkts to read in one poll */
    void * cookie;                      /**<app specific */
    PKTIO_SEND _send;                   /**<pktio type specific send function */
    PKTIO_POLL _poll;                   /**<pktio type specific POLL function */
    int poll_flags;                     /**< pktio flags to control polling options */
    char name[PKTIO_MAX_NAME+1];        /**< Name of pktio channel */
    Cppi_Handle             cppiHnd;    /* < cppi handle */
    Cppi_ChHnd              rxChHnd;    /* < cppi receive channel handle */
    Cppi_ChHnd              txChHnd;    /* < cppi transmit channe handle */
    nwalTxPSCmdInfo_t tx_psCmdInfo; /**<Command Label to be sent to NetCP for the packet flow */
}  PKTIO_HANDLE_T;


/**
 *  @ingroup pktio_structures
 *  @brief PKTIO control structure 
 *
 *  @details PKTIO control stucture for the control API. Allows operations on pktio channel such as CLEAR ,  DIVERT, etc
 */
typedef struct PKTIO_CONTROL_Tag
{
/**
 * @def PKTIO_CLEAR
 *      This defines is used to clear out the pktio channel
 */
#define PKTIO_CLEAR 0x1

// @cond NOT_IMPLEMENTED
/**
 * @def PKTIO_DIVERT
 *      This define is used to divert to a dest pktio channel 
 */
#define PKTIO_DIVERT 0x2
/// @endcond

/**
 * @def PKTIO_SET_POLL_FLAGS
 *      This define is used to set poll flags for a pktio channel
 */
#define PKTIO_SET_POLL_FLAGS 0x4 //control poll flags (netcp_rx only)
/**
 * @def PKTIO_UPDATE_FAST_PATH
 *      This define is used to update the command information template for 
 *      INFLOW mode of operation of a ptktio channel (netcp_tx only)
 */
#define PKTIO_UPDATE_FAST_PATH 0x8

/**<max # of pkts to read in one poll */
/**
 * @def PKTIO_UPDATE_MAX_PKTS_PER_POLL
 *      This define is used to update the maximum number of packets to read in 1 poll
 *      period for the pktio channel.
 */
#define PKTIO_UPDATE_MAX_PKTS_PER_POLL 0x10

    int op;                 /**< Control operation (CLEAR, DIVERT, ..) */
    PKTIO_HANDLE_T *dest;   /**< Handle to PKTIO channel (for DIVERT) */
    int poll_flags;         /**< Flags to indicate polling options */
} PKTIO_CONTROL_T;

/**
 *  @ingroup pktio_functions
 *  @brief API creates a NETAPI PKTIO channel 
 *
 *  @details This assigns global resources to a NETAPI pktio channel.
 *   Once created, the channel can be used to send and/or receive
 *   a Ti_Pkt. This can be used for communication with the
 *   the Network co-processor (NETCP) or for internal inter-processor
 *   communication. The channel is saved under the assigned name
 *   and can be opened by other netapi threads instances.
 *  @param[in]  netapi_handle   The NETAPI handle, @ref NETAPI_T 
 *  @param[in]  name            A pointer to the char string name for channel
 *  @param[in]  cb              Callback to be issued on packet receive, @ref PKTIO_CB
 *  @param[in]  p_cfg           Pointer to channel configuration, @ref PKTIO_CFG_T
 *  @param[out] err             Pointer to error code.
 *  @retval     Handle to the pktio instance or NULL on error, @ref PKTIO_HANDLE_T
 *  @pre       @ref netapi_init 
 */
PKTIO_HANDLE_T * netapi_pktioCreate(NETAPI_T        netapi_handle,
                                    char*           name,
                                    PKTIO_CB        cb,
                                    PKTIO_CFG_T*    p_cfg,
                                    int*            err);

/**
 *  @ingroup pktio_functions
 *  @brief API opens an existing  NETAPI PKTIO channel
 *
 *  @details This opens an NETAPI pktio channel for use. The channel
 *  must have already been created via @ref netapi_pktioCreate or may have
 *  been created internally during the netapi intialization.
 *   Once opened, the channel can be used to send and/or receive
 *   a Ti_Pkt. This can be used for communication with the 
 *   the Network co-processor (NETCP) or for internal inter-processor
 *   communication. 
 *
 *  @param[in]  netapi_handle   The NETAPI handle, @ref NETAPI_T 
 *  @param[in]  name            A pointer to the char string name for channel to open
 *  @param[in]  cb              Callback to be issued on packet receive, @ref PKTIO_CB
 *  @param[in]  p_cfg           Pointer to channel configuration, @ref PKTIO_CFG_T
 *  @param[out] err             Pointer to error code.
 *  @retval     Handle to the pktio instance or NULL on error, @ref PKTIO_HANDLE_T
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate
 */
PKTIO_HANDLE_T * netapi_pktioOpen(NETAPI_T      netapi_handle, 
                                 char*          name,
                                 PKTIO_CB       cb,
                                 PKTIO_CFG_T*   p_cfg,
                                 int*           err);

/**
 *  @ingroup pktio_functions
 *  @brief API controls an existing NETAPI PKTIO channel
 *
 *  @details This controls an opened pktio channel
 *
 *  @param[in]  handle      The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  cb          Callback to be issued on packet receive, @ref PKTIO_CB
 *  @param[in]  p_cfg       Pointer to channel configuration which (optional), @ref PKTIO_CFG_T
 *  @param[in]  p_control   Pointer to PKTIO control information (optional), @ref PKTIO_CONTROL_T
 *  @param[out] err         Pointer to error code.
 *  @retval     none
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
void netapi_pktioControl(PKTIO_HANDLE_T*    handle,
                         PKTIO_CB           cb,
                         PKTIO_CFG_T*       p_cfg,
                         PKTIO_CONTROL_T*   p_control,
                         int*               err);

/**
 *  @ingroup pktio_functions
 *  @brief API closes a NETAPI PKTIO channel
 *
 *  @details This closes a PKTIO channel
 *
 *  @param[in]  handle  The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[out] err    Pointer to error code.
 *  @retval     none
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
void netapi_pktioClose(PKTIO_HANDLE_T*  handle,
                       int*             err);

/**
 *  @ingroup pktio_functions
 *  @brief API deletes a NETAPI PKTIO channel
 *
 *  @details This deletes a PKTIO channel
 *
 *  @param[in]  handle  The PKTIO  handle, @ref PKTIO_HANDLE_T
 *  @param[out] err     Pointer to error code.
 *  @retval     none
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
void netapi_pktioDelete(PKTIO_HANDLE_T* handle,
                        int*            err);

/**
 *  @ingroup pktio_functions
 *  @brief  API sends a packet to a NETAPI PKTIO channel 
 *
 *  @details This sends a Ti_Pkt and associated meta data, 
 *  @ref PKTIO_METADATA_T to a channel. The channel
 *  must have already been created via @ref netapi_pktioCreate or opened
 *  via @ref netapi_pktioOpen.  It  may have
 *  been created internally during the netapi intialization.
 *  @param[in]  handle  The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  pkt     Pointer to the packet to send
 *  @param[in]  m       Pointer to meta data associated with packet, @ref PKTIO_METADATA_T
 *  @param[out] err     Pointer to error code.
 *  @retval     1 if packet sent, 0 if error 
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
static inline int  netapi_pktioSend(PKTIO_HANDLE_T*   handle,
                                    Ti_Pkt*           pkt,
                                    PKTIO_METADATA_T* m,
                                    int*              err)
{
   return handle->_send((struct PKTIO_HANDLE_tag *)handle, pkt, m, err);
}

/**
 *  @ingroup pktio_functions
 *  @brief API sends multiple packets to a NETAPI PKTIO channel
 *
 *  @details This sends an array of Ti_Pkt and associated meta data,
 *  @ref PKTIO_METADATA_T to a channel. The channel
 *  must have already been created via @ref netapi_pktioCreate or opened
 *  via @ref netapi_pktioOpen.  It  may have
 *  been created internally during the netapi intialization.
 *  @param[in]  handle  The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  pkt     Pointer to the packet to send
 *  @param[in]  m       Pointer to meta data associated with packet, @ref PKTIO_METADATA_T
 *  @param[in]  np      The number of packets in list to send
 *  @param[out] err     Pointer to error code.
 *  @retval             Number of packets sent, 0 if error
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
int netapi_pktioSendMulti(PKTIO_HANDLE_T*   handle,
                          Ti_Pkt*           pkt[],
                          PKTIO_METADATA_T* m[],
                          int               np,
                          int*              err);

/**
 *  @ingroup pktio_functions
 *  @brief  API polls a NETAPI PKTIO channel for received packets 
 *
 *  @details This api polls a pktio channel. Any pending data in the channel is
 *  passed to the @ref  PKTIO_CB registered when the channel was
 *  created or opened. The channel must
 *  have already been created via @ref netapi_pktioCreate or opened
 *  via @ref netapi_pktioOpen.  It  may have
 *  been created internally during the netapi intialization.
 *  @param[in]  handle      The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  p_poll_cfg  Pointer to pktio poll configuration. @ref PKTIO_POLL_T
 *  @param[out] err         Pointer to error code.
 *  @retval                 Number of packets received by poll 
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
static inline int netapi_pktioPoll(PKTIO_HANDLE_T* handle,
                                   PKTIO_POLL_T*   p_poll_cfg,
                                   int*            err)
{
    return handle->_poll((struct PKTIO_HANDLE_tag *) handle, p_poll_cfg, err);
}

/**
 *  @ingroup pktio_functions
 *  @brief API polls all NETAPI PKTIO channels associated with @ref NETAPI_T instance
 *         for received packets
 *
 *  @details This api polls all pktio channels attached to an instance.
 *  Any pending data in these channels are 
 *  passed to the @ref  PKTIO_CB registered when the channel was
 *  created or opened. The channels  must
 *  have already been created via @ref netapi_pktioCreate or opened
 *  via @ref netapi_pktioOpen.  They may have
 *  been created internally during the netapi intialization.
 *  @param[in]  handle      The PKTIO channel handle, @ref PKTIO_HANDLE_T
 *  @param[in]  p_poll_cfg  Pointer to pktio poll configuration. @ref PKTIO_POLL_T
 *  @param[out] err         Pointer to error code.
 *  @retval                 Number of packets received by poll 
 *  @pre       @ref netapi_init, @ref netapi_pktioCreate, @ref netapi_pktioOpen
 */
int netapi_pktioPollAll(NETAPI_T        handle, 
                        PKTIO_POLL_T*   p_poll_cfg, 
                        int*            err);


/**
 * @brief This define sets the max number of pkts to read in one poll in the @ref PKTIO_HANDLE_T
 */
#define netapi_pktioSetMaxN(handle,max_n) (handle)->max_n=max_n;

/**
 * @brief This define returns NETAPI handle stored in the @ref PKTIO_HANDLE_T
 */
#define netapi_pktioGetNetapiHandle(handle) (handle)->back


/**
 * @brief This define sets a user space application cookie in the @ref PKTIO_HANDLE_T
 */
#define netapi_pktioSetCookie(handle, cookie) (handle)->cookie = cookie

/**
 * @brief This define returns a previously set user space application cookie stored in the @ref PKTIO_HANDLE_T
 */
#define netapi_pktioGetCookie(handle) (handle)->cookie

/**
 * @brief This define returns a associate queue handle stored in the @ref PKTIO_HANDLE_T
 */
#define netapi_pktioGetQ(handle) (handle)->q

/**
 *  @ingroup pktio_functions
 *  @brief  API returns default packet queue to poll for netcp RX
 *  @note: these are expensive calls, so call once and save
 */
static inline Qmss_QueueHnd pktio_mGetDefaultNetCpQ(PKTIO_HANDLE_T *h)
{
    nwalGlobCxtInfo_t Info;
    nwal_getGlobCxtInfo(h->nwalInstanceHandle,&Info);
    return Info.rxDefPktQ;
}


/**
 *  @ingroup pktio_functions
 *  @brief  API returns L4Queue to poll for netcp RX (L4 classifier queue).
 *  @note: these are expensive calls, so call once and save
 */
static inline Qmss_QueueHnd pktio_mGetDefaultNetCPL4Q(PKTIO_HANDLE_T *h)
{
    nwalLocCxtInfo_t Info;
    nwal_getLocCxtInfo(h->nwalInstanceHandle,&Info);
    return Info.rxL4PktQ;
}


/**
 *  @ingroup pktio_functions
 *  @brief  API to perform descriptor push to QMSS Queue
 */
static inline void pktio_mQmssQueuePushDescSizeRaw(Qmss_QueueHnd    hnd,
                                                   void*            descAddr,
                                                   uint32_t         descSize)
{
    return(Qmss_queuePushDescSizeRaw(hnd,descAddr,descSize));
}

/**
 *  @ingroup pktio_functions
 *  @brief  API to perform descriptor pop from QMSS Queue
 */
static inline void* pktio_mQmssQueuePopRaw(Qmss_QueueHnd hnd)
{
    return(Qmss_queuePopRaw(hnd));
}

/**
 *  @ingroup pktio_functions
 *  @brief  API to retrieve NWAL global instance handle.
 */
static inline nwal_Inst pktio_mGetNwalInstance(PKTIO_HANDLE_T *h)
{
    return h->nwalInstanceHandle;
}

#ifdef __cplusplus
}
#endif

#endif
