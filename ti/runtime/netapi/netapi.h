/******************************************************************************
 * FILE PURPOSE:  User space access to transport resources on SOC
 ******************************************************************************
 * FILE NAME:   netapi.h
 *
 * DESCRIPTION: NETAPI definitions and data structures
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* ============================================================= */
/**
 *   @file  netapi.h
 *
 *   path  ti/runtime/netapi/netapi.h
 *
 *   @brief  Netapi main header file for user space transport library
 *
 */

/**  @mainpage Network API 
 *
 *   @section intro  Introduction
 *
 *   The network API provides a user space interface to TI SOC transport 
 *   Resources. The library includes:
 *      - general startup and setup for user space operations
 *      - memory heap and packet buffer  management
 *      - pktio  either to/from network or internal queues
 *      - timers for network stacks
 *      - netcp (network co-processor) configuration and control
 *      - utilities including user space synchronization primitivies
 *      - sample scheduling event loop
 *
 *    NETAPI allows user space transport to configure control the NETCP:
 *      - Classification of packets based on L2: MAC header fields
 *      - Classification of packets based on L3: IP header fields
 *      - Routing of packets to host based on L4 UDP or L5 GTPU ID
 *      - Unidirectional IPSec SA creation and deletion
 *      - Unidirectional IPSec Security Policy creation and deletion
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2010-2012 Texas Instruments, Inc.
 *  \par
 */

#ifndef __NETAPI__H
#define __NETAPI__H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "ti/drv/nwal/nwal.h"
#include "ti/runtime/hplib/hplib.h"
#include "netapi_types.h"
#include "netapi_tune.h"
#include "pktio.h"
#include "netcp_cfg.h"
#include "netapi_sec.h"
#include "netapi_sched.h"
#include "netapi_util.h"


/* Define NETAPI as a master group in Doxygen format and
 * add all NETAPI
 * definitions to this group.
 */
/** @defgroup netapi Network API
 *  @{
 */
/** @} */

/** @defgroup netapi_gen_functions NETAPI General Functions
 *  @ingroup netapi
 */


/** @defgroup netapi_cfg NETAPI Configuration Interface
 *  @ingroup netapi
 */
/** @defgroup cfg_functions NETAPI Configuration Functions
 *  @ingroup netapi_cfg
 */

/** @defgroup cfg_structures NETAPI Configuration Structures used in API's
 *  @ingroup netapi_cfg
 */

/** @defgroup cfg_constants NETAPI Configuration Constants
 *  @ingroup netapi_cfg
 */

/** @defgroup netapi_security NETAPI Security Interface
 *  @ingroup netapi
 */

/** @defgroup cfg_security_functions NETAPI Security Configuration Functions
 *  @ingroup netapi_security
 */

/** @defgroup cfg_security_structures NETAPI Security Configuration Structures used in API's
 *  @ingroup netapi_security
 */


/** @defgroup security_constants NETAPI Security Constants
 *  @ingroup netapi_security
 */

/** @defgroup netapi_pktio NETAPI PKTIO Interface
 *  @ingroup netapi
 */

/** @defgroup pktio_functions NETAPI PKTIO Functions
 *  @ingroup netapi_pktio
 */

/** @defgroup pktio_structures NETAPI PKTIO Structures used in API's
 *  @ingroup netapi_pktio
 */

/** @defgroup pktio_constants NETAPI PKTIO Constants
 *  @ingroup netapi_pktio
 */

/** @defgroup netapi_scheduler NETAPI Scheduler Interface
 *  @ingroup netapi
 */

/** @defgroup sched_functions NETAPI Scheduler Functions
 *  @ingroup netapi_scheduler
 */

/** @defgroup sched_structures NETAPI Scheduler Structures used in API's
 *  @ingroup netapi_scheduler
 */

/** @defgroup sched_constants NETAPI Scheduler Constants
 *  @ingroup netapi_scheduler
 */

/** @defgroup netapi_cb_functions NETAPI Callback Functions
 *  @ingroup netapi
 */

/** @defgroup tune_parameters NETAPI Tune Parameters
 *  @ingroup netapi
 */

/**
 * @def NETAPI_PROC_MASTER
 *      This defines the master core/thread for a process
 */
#define NETAPI_PROC_MASTER 3

/**
 * @def NETAPI_SYS_MASTER
 *      This defines the master core for the system
 *      also will be master core/thread of the process
 */
#define NETAPI_SYS_MASTER  2

/**
 * @def NETAPI_CORE_MASTER
 *      This defines the master thread  for a particular core
 */
#define NETAPI_CORE_MASTER 1

/**
 * @def  NETAPI_NO_MASTER
 *      This defines a non master thread which can be running on any core,, specifically for data only
 */
#define NETAPI_NO_MASTER   0  //data only




/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_init  API instantiates the NETAPI and allocated global resources.
 *
 *  @details The API will allocate global resources valid  per system level common
 *           across all ARM cores or per thread based on "master" argument.
 *           Intializes the following substems: pktio pklib qmss cppi nwal
 *
 *  @param[in]  master  Can be either @ref NETAPI_SYS_MASTER or @ref NETAPI_NO_MASTER
 *  @param[in]  p_cfg   (master mode)  pointer to @ref NETAPI_CFG_T or NULL to use netapi default configuration.
 *  @retval     Handle to the instance or NULL on error, @ref NETAPI_T
 *  @pre        none
 */
 NETAPI_T netapi_init(int                master,
                     NETAPI_CFG_T *     p_cfg);
/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_shutdown  API de-allocates all global resources allocated as part of @ref netapi_init
 *
 *  @details De-allocates global resources valid  per system level common across all ARM cores 
 *           or per thread based on "master" argument passed in at init time.
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     none 
 *  @pre        @ref netapi_init 
 */
void netapi_shutdown(NETAPI_T  p);

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getPktlibIfTable  API returns a Pktlib_HeapIfTable to use when creating
 *  pktlib heaps
 *
 *  @details Application will need a Pktlib_HeapIfTable in order to create its own heaps. This
 *           function returns a table that can be passed in the call to Pktlib_CreateHeap
 *           The memory used for these heaps is special with the following characteristics:
 *             - Specific alignment.
 *             - Must be contguous.
 *             - Must have a physical to virtual mapping that  is known by NETAPI. 
 *           Thus it must be completely managed by NETAPI. This interface table provides a
 *           malloc function that the pktlib heap library uses to allocate data for the heap
 *           buffers.
 *  @retval     Pktlib_HeapIfTable pointer 
 *  @pre        @ref netapi_init 
 */
Pktlib_HeapIfTable *netapi_getPktlibIfTable(void) ;



/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getDescRemainder  API is used to return the amount of free memory available
 *  for allocating descriptors for additonal Pktlib heaps.
 *
 *  @details The application can use this API to determine how much free memory is
 *           available for heap descriptors if it decides to create its own heap.
 *  @retval     Amount of memory available for heap descriptor storage (in bytes)
 *  @pre        @ref netapi_init 
 */
int  netapi_getDescRemainder(void);

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_netcpPoll  API is used to poll for NETCP configuration response messages.
 *
 *  @details Application, if implementing the scheduler,  will need to call this
 *           function periodically to check for NETCP configuration responses (eg
 *           statistics requests). 
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     none 
 *  @pre        @ref netapi_init 
 */
void netapi_netcpPoll(NETAPI_T  p);


/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_pollHeapGarbage  API is used to poll the garbage collection queue for 
 *                                  the internal NETAPI heaps and any application created heaps.
 *
 *  @details  This API is used to poll the netapi internal heaps and any 
 *            application-created heaps    that have been registered with the netapi instance. The
 *            poll function checks the garbage collection queue associated with the heap and returns
 *            descriptors and buffers when appropriate to the main free queue.
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     none 
 *  @pre        @ref netapi_init
 */
void netapi_pollHeapGarbage(NETAPI_T p);

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_getMemoryRegionId  API is used to return configured descriptor
 *         memory region Id
 *
 *  @details  API is used to return configured descriptor memory region Id
 *  @param[in]  p   The NETAPI handle, @ref NETAPI_T
 *  @retval     Memory Resgion Id, -1 on error
 *  @pre        @ref netapi_init
 */
int netapi_getMemoryRegionId(NETAPI_T  p);
#ifdef __cplusplus
}
#endif
#endif
