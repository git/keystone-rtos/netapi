/*******************************
 * file: netapi_err.h
 * purpose: netapi error codes
 **************************************************************
 * @file netapi.h
 * 
 * @brief DESCRIPTION:  netapi Error Definitions for user space transport
 *               library
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************/

#ifndef __NETAPI_ERR__
#define __NETAPI_ERR__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @ingroup cfg_constants
 * @brief  One per thread/core, used in most NETAPI function calls, returned from call to netapi_init().
 */
typedef void * NETAPI_T;



/**
 *   @defgroup netapi_RetValue NETAPI API Return Values
 *   @ingroup netapi
 */
/**  @ingroup nwal_RetValue */
typedef int16_t netapi_RetValue;

/** @defgroup netapi_errors NETAPI Error Return Values
 *  @ingroup netapi
 */

/**
 *  @def  NETAPI_ERR_OK
 *  @ingroup netapi_errors
 *       No error 
 */
#define NETAPI_ERR_OK  0

/**
 *  @def  NETAPI_ERR_NOMEM
 *  @ingroup netapi_errors
 *  @brief   out of memory error
 */
#define NETAPI_ERR_NOMEM -1

/**
 *  @def  NETAPI_ERR_BAD_INPUT
 *  @ingroup netapi_errors
 *  @brief   arguments or configuraiton is invalid
 */
#define NETAPI_ERR_BAD_INPUT -2

/**
 *  @def  NETAPI_ERR_QLLD 
 *  @ingroup netapi_errors
 *  @brief   QUEUE MANAGER Reports an error
 */
#define NETAPI_ERR_QLLD -3

/**
 *  @def  NETAPI_ERR_NOTFOUND
 *  @ingroup netapi_errors
 *  @brief   the resource cannot be located
 */
#define NETAPI_ERR_NOTFOUND -4
/**
 *  @def  NETAPI_ERR_BUSY
 *  @ingroup netapi_errors
 *  @brief   Temporarily out of resources or resource leak
 */
#define NETAPI_ERR_BUSY -5 

/**
 *  @def  NETAPI_ERR_NWAL_ERR0
 *  @ingroup netapi_errors
 *  @brief   NWAL subsytem reports error 
 */
#define NETAPI_ERR_NWAL_ERR0 -6

/**
 *  @def  NETAPI_ERR_NOT_IMPLEMENTED
 *  @ingroup netapi_errors
 *  @brief   Feature not implemented in this version
 */
#define NETAPI_ERR_NOT_IMPLEMENTED -7

/**
 *  @def  NETAPI_ERR_NORES
 *  @ingroup netapi_errors
 *  @brief   no free (hw) resources available 
 */
#define NETAPI_ERR_NORES -8

/**
 *  @def  NETAPI_ERR_NORES
 *  @ingroup netapi_errors
 *  @brief   error returned by NETCP PA firmware
 */
#define NETAPI_ERR_PA_FW -9


/**
 *  @def  NETAPI_ERR_ALREADY_CANCELLED
 *  @ingroup netapi_errors
 *  @brief   timer has already been cancelled
 */
#define NETAPI_ERR_ALREADY_CANCELLED -100

/**
 *  @def  NETAPI_ERR_NWAL_TX_ERR
 *  @ingroup netapi_errors
 *  @brief   error trying to send to NWAL.
 */
#define NETAPI_ERR_NWAL_TX_ERR -65536

#ifdef __cplusplus
}
#endif


#endif
