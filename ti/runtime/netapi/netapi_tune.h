/******************************************************************************
 * FILE PURPOSE:  netapi tunable parameters (compile time)
 ******************************************************************************
 * FILE NAME:   netapi_tune.h
 *
 * DESCRIPTION:Tuneable (compile time) parameters for user space transport library
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* ============================================================= */

/**
 *   @file netapi_tune.h
 *   @brief Netapi Tuneable (compile time) parameters for user space transport library
 */


#ifndef __NETAPI_TUNE__H
#define __NETAPI_TUNE__H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @ingroup tune_parameters
 * @def NETAPI_ENABLE_SECURITY
 *      Define this to enable securtiy.
 * @note Libraries using netapi need to be built with SA enabled
*/
#define NETAPI_ENABLE_SECURITY 


/**
 * @ingroup tune_parameters
 * @def NETAPI_USE_DDR
 *      Define this to enable use of cached DDR for buffers and descriptors. 
 * @note Do not define if USE_MSMC defined below
*/
#define NETAPI_USE_DDR


#ifdef NETAPI_USE_DDR
/**
 * @ingroup tune_parameters
 * @def NETAPI_TUNE_USE_CACHE_OPS
 *      Define this to enable use of cached DDR for buffers and descriptors. 
 * @note Do not define if USE_MSMC defined below
*/
#define NETAPI_TUNE_USE_CACHE_OPS   //for appleton, no cache coherency with netcp & ARM 
#endif

/**
 * @ingroup tune_parameters
 * @def NETAPI_USE_MSMC
 *      Define this to enable use of un-cached MSMC for buffers and descriptors 
 * @note Do not define if USE_DDR defined above 
*/
//#define NETAPI_USE_MSMC

#ifdef NETAPI_USE_MSMC
#ifdef NETAPI_USE_DDR
#error "only define NETAPO_USE_MSMC or NETAPI_USE_DDR"
#endif
#endif

/**
 * @ingroup tune_parameters
* @def TUNE_NETAPI_NUM_CORES
*       This defines the number of cores (theads) 
 */
#define TUNE_NETAPI_NUM_CORES 5

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_PERM_MEM_SZ
 *      This defines how much contiguous memory to grab. This is used for
 *      descriptors and buffers in the case of uncached configuration only.  
 *      descriptors and buffers.  Can't be bigger than  msmc if
 *      MSMC memory is being using uncached .  This can be set at netapi_init via NETAPI_CFG_T
 */
#define TUNE_NETAPI_PERM_MEM_SZ   (2*1024*1024) 

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_PKTIO
 *      This defines the number of GLOBAL pkt io channels
 */
#define TUNE_NETAPI_MAX_PKTIO 16

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_DEFAULT_BUFFER_SIZE
 *      This defines the size of the netapi default pktlib heap buffers This can be set at @ref netapi_init 
 */
#define TUNE_NETAPI_DEFAULT_BUFFER_SIZE 1600  


/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_DEFAULT_NUM_BUFFERS
 *      This defines the number of netapi default pktlib heap buffers (and assoc descriptors)
 *      this can be set at @ref netapi_init
 */
#define TUNE_NETAPI_DEFAULT_NUM_BUFFERS  200 

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_DEFAULT_NUM_SOLO_DESCRIPTORS
 *      This defines the number of netapi default pkt lib heap solo descriptors
 *      this can be set at @ref netapi_init
 */
#define TUNE_NETAPI_DEFAULT_NUM_SOLO_DESCRIPTORS  100 

/**
 * @ingroup tune_parameters
 * @def  NETAPI_INCLUDE_SCHED
 *      Define this to include the scheduler component
 */
#define NETAPI_INCLUDE_SCHED 

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM
 *      This defines the number of of QM descriptors (total). This can be set set in @ref netapi_init
 * @note Must be a power or 2
*/
#define TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM  2048 /* 16384 is abs max */


/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_NUM_LOCAL_DESC
 *      This defines the number of of QM descriptors (total). This can be set set in @ref netapi_init
*/
#define TUNE_NETAPI_NUM_LOCAL_DESC  64 


/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_NUM_GLOBAL_DESC
 *      This defines the number of global descriptors.
 * @note Must be a power or 2
*/
#define TUNE_NETAPI_NUM_GLOBAL_DESC  1024

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_DESC_SIZE
 *      This defines the descriptor size
 * @note This define should NOT be changes
 */
#define TUNE_NETAPI_DESC_SIZE  128

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_NWAL_ENABLE_PASS_LOOPBACK
 *      Define this to 1 for debutg mode where NETCP just loops pkts that tx back to rx
 */
#define TUNE_NETAPI_NWAL_ENABLE_PASS_LOOPBACK 0  /*1*/


/// @cond INTERNAL
//(10) NWAL (internal) config. Should not have to change
#define TUNE_NETAPI_CONFIG_MAX_PA_TO_SA_DESC       32
#define TUNE_NETAPI_CONFIG_MAX_SA_TO_PA_DESC       200
/// @endcond

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_MAC
 *      This defines the number of logical mac addresses
 */
#define TUNE_NETAPI_MAX_NUM_MAC                64

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_IP
 *      This defines the number of ip addresses
 */
#define TUNE_NETAPI_MAX_NUM_IP                64

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_PORTS_PER_CORE
 *      This defines the number of ports per core
 */
#define TUNE_NETAPI_MAX_NUM_PORTS_PER_CORE     4

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_PORTS
 *      This defines the number maximum number of ports
 */
#define TUNE_NETAPI_MAX_NUM_PORTS     (TUNE_NETAPI_MAX_NUM_PORTS_PER_CORE * (TUNE_NETAPI_NUM_CORES)) 

#ifdef NETAPI_ENABLE_SECURITY
/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_IPSEC_CHANNELS
 *      This defines the number maximum number of ipsec channels
 */
#define TUNE_NETAPI_MAX_NUM_IPSEC_CHANNELS     128
#else
/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_IPSEC_CHANNELS
 *      This defines the number maximum number of ipsec channels
 */
#define TUNE_NETAPI_MAX_NUM_IPSEC_CHANNELS     0
#endif

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_L2_L3_HDRS
 *      This defines the number maximum number of L2_L3 headers to reserve  in the nwal layer. This should be kept small as transport lib does not expose this inwal feature by default
 */
#define TUNE_NETAPI_MAX_NUM_L2_L3_HDRS         3

/**
 * @ingroup tune_parameters
 * @def TUNE_NETAPI_MAX_NUM_TRANS
 *      This defines the number maximum number of transactions with NETCP that can be outstanding at any one time 
 */
#define TUNE_NETAPI_MAX_NUM_TRANS              (TUNE_NETAPI_MAX_NUM_MAC + TUNE_NETAPI_MAX_NUM_IP + TUNE_NETAPI_MAX_NUM_PORTS + TUNE_NETAPI_MAX_NUM_IPSEC_CHANNELS)


/// @cond INTERNAL
/* PA control buffer pool (internal) */
#define TUNE_NETAPI_CONFIG_MAX_CTL_RXTX_BUF_SIZE 2400
#define TUNE_NETAPI_CONFIG_NUM_CTL_RX_BUF  16 
#define TUNE_NETAPI_CONFIG_NUM_CTL_TX_BUF  16 
/// @endcond

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_MAX_BURST_RCV
 *      This defines the maximum number of packets to receive in one pktio poll.
 */
#define TUNE_NETAPI_MAX_BURST_RCV 32

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_NUM_TIMER_CELLS
 *      This defines the number of hash bins in a timer group.
 */
#define TUNE_NETAPI_NUM_TIMER_CELLS  128
/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_NUM_GLOBAL_TIMERS
 *      This defines the number of global timer blocks
 */
#define TUNE_NETAPI_NUM_GLOBAL_TIMERS  4

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_MAX_HEAPS
 *      This defines the maximum number of heaps one NETAPI instance can create
 */
#define TUNE_NETAPI_MAX_HEAPS  4

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_MAX_CLASSIFIERS
 *      This defines the maximum number of classifiers
 */
#define TUNE_NETAPI_MAX_CLASSIFIERS 16

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_MAX_BUF_POOLS_IN_FLOW
 *      This defines the  maximum number of buffer pools in a flow - A flow is used by hardware
 *      when it needs a buffer for a receive packet.  This define allows the maximum number 
 *      of pools (free descriptor queues) to be defined for a flow, so that different sized packets 
 *      can be allocated from different memory areas.
 * @note This define should NOT be changed as HW assumes at most 4 now
 */
#define TUNE_NETAPI_MAX_BUF_POOLS_IN_FLOW  4


/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_MAX_FLOWS
 *      This defines the maximum number of flows
 */
#define TUNE_NETAPI_MAX_FLOWS  4   //app defined flows

/**
 * @ingroup tune_parameters
* @def  TUNE_NETAPI_MAX_SA
*       This defines the maximum number of security associations
*/
#define TUNE_NETAPI_MAX_SA 128   //rx&tx combined (so MAX_SA/4 tunnels typically)

/**
 * @ingroup tune_parameters
 * @def  TUNE_NETAPI_MAX_POLICY
 *      This defines the maximum number of security policies.
*/
#define TUNE_NETAPI_MAX_POLICY 128  //rx policies

#ifdef __cplusplus
}
#endif
#endif
