#!/bin/sh
# Source this script before starting the ipsecmgr command shell

# Variable to specify the local unix socket name for IPC with IPSec daemon
export IPSECMGR_APP_SOCK_NAME="/etc/app_sock"
# Variable to specify the unix socket name of the IPSec daemon
export IPSECMGR_DAEMON_SOCK_NAME="/etc/ipsd_sock"
# Variable to specify the log file to be used by the ipsecmgr library
export IPSECMGR_LOG_FILE="/var/run/ipsecmgr_app.log"

