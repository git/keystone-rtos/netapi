/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>

#include <ipsecmgr_ipc.h>
#include <ipsecmgr_syslog.h>

#include "cmd_shell_loc.h"

typedef pthread_t task_handle;

#define DEFAULT_STACK_SIZE	0x8000
static int task_create ( void *(start_routine)(void*), void* args, void* handle)
{
    int max_priority, err;
    pthread_t thread;
    pthread_attr_t attr;
    struct sched_param param;

    max_priority = sched_get_priority_max(SCHED_FIFO);
    err = pthread_attr_init(&attr);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_init failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setdetachstate failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setstacksize(&attr, DEFAULT_STACK_SIZE);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setstacksize failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setinheritsched failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setschedpolicy failed: (%s)\n", strerror(err));
        return err;
    }
    memset(&param, 0, sizeof(param));
    param.sched_priority = max_priority;
    err = pthread_attr_setschedparam(&attr, &param);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_attr_setschedparam failed: (%s)\n", strerror(err));
        return err;
    }
    err = pthread_create(&thread, &attr, start_routine, args);
    if (err) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "pthread_create failed: (%s)\n", strerror(err));
        return err;
    }
    *(pthread_t*)handle = thread;
    return 0;
}

static void task_wait (void *handle)
{
    pthread_join(*((pthread_t*)handle), NULL);
    return;
}

static void task_sleep(int time_in_msec)
{
    pthread_mutex_t fake_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t fake_cond = PTHREAD_COND_INITIALIZER;
    struct timespec ts;
    int rt;
    unsigned int sec, nsec;

    sec = time_in_msec/1000;
    nsec = (time_in_msec - (sec*1000)) * 1000000;

    /* Use the wall-clock time */
    clock_gettime(CLOCK_REALTIME, &ts);

    ts.tv_sec += sec;
    ts.tv_nsec += nsec;

    pthread_mutex_lock(&fake_mutex);
    rt = pthread_cond_timedwait(&fake_cond, &fake_mutex, &ts);
    pthread_mutex_unlock(&fake_mutex);
}

#define IPC_POLL_INTVL   1000

static void *ipc_poll(void *args)
{
    while(1) {
        ipsecmgr_ipc_poll();
        task_sleep(IPC_POLL_INTVL);
    } /* end while */
    return (void*)(0);
}

/* usage ipsecmgr_cmd_shell 
 */
int main(int argc, char **argv)
{
    ipsecmgr_ipc_cfg_t  ipc_cfg;
    ipsecmgr_ipc_user_recv_if_t recv_if;
    task_handle cmd_th, ipc_th;
    int status;
    int c;
    static char usage[] = "usage: %s -s <daemon id>\n";

    memset(&ipc_cfg, 0, sizeof(ipc_cfg));
    while ((c = getopt (argc, argv, "i:")) != -1)
    {
        switch (c)
        {
            case 'i':
                ipc_cfg.ipc_id = (uint32_t)strtol(optarg, NULL,0);
                if(!ipc_cfg.ipc_id)
                {
                    printf(usage, argv[0]);
                    exit(EXIT_FAILURE);
                }
                break;
        case '?':
            printf(usage, argv[0]);
            exit(EXIT_FAILURE);
        default:
            break;
        }
    }
    /* Start logging module */
    if (ipsecmgr_syslog_init()) {
        printf ("Failed to initialize syslog\n");
        return -1;
    }

    ipc_cfg.mode = IPC_MODE_USER_APP;

    if (ipsecmgr_ipc_init(&ipc_cfg)) {
        printf ("Failed to initialize IPC\n");
        return -1;
    }

    recv_if.offload_sp_rsp = cmd_shell_offload_sp_rsp;
    recv_if.stop_offload_rsp = cmd_shell_stop_offload_rsp;

    if (ipsecmgr_ipc_register_user_recv_iface(&recv_if)) {
        printf ("Failed to IPC recv interface\n");
        return -1;
    }

    /* Create the command line interpreter task */
    if (status = task_create(cmd_shell, NULL, &cmd_th)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "ERROR: Command shell task-create failed (%d)\n", status);
        return (-1);
    }

    /* Create the FP IPC poll task */
    if (status = task_create(ipc_poll, NULL, &ipc_th)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "ERROR: IPC recv task-create failed (%d)\n", status);
        return (-1);
    }

    task_wait(&cmd_th);
    return 0;
}

