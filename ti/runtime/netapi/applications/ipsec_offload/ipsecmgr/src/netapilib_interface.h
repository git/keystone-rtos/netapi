/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef __FPLIB_INTERFACE_H__
#define __FPLIB_INTERFACE_H__

#include "ti/runtime/netapi/netapi.h"
#include <netdb.h>


#define IPSECMGR_MAX_SA 64
/* Container for applicaion ID's for offloaded SA's */
typedef struct {
    int         in_use;
    uint32_t    saAppId;
    uint32_t    spAppId;
    uint32_t            spi;
    nwal_saAALG         authMode;       /**< Authentication Algorithm */
    nwal_saEALG         cipherMode;     /**< Encryption Algorithm */
} ipsecMgrAppId_T;

typedef struct {
    ipsecMgrAppId_T info_rx[IPSECMGR_MAX_SA];
    ipsecMgrAppId_T info_tx[IPSECMGR_MAX_SA];
    NETAPI_SA_STATS_T netapi_sa_stats_rx[IPSECMGR_MAX_SA];
    NETAPI_SA_STATS_T netapi_sa_stats_tx[IPSECMGR_MAX_SA];
} ipsecMgrShm_T;
typedef struct {
    int             flowId;
    int             qNum;
    PKTIO_HANDLE_T* pktio_channel;

    /* list to store offloaded RX SA appIds and RX Policies */
    ipsecMgrAppId_T rx_sa[IPSECMGR_MAX_SA];
    /* list to store offloaded TX SA appIds */
    ipsecMgrAppId_T tx_sa[IPSECMGR_MAX_SA];
    uint32_t        oseq_offset; /* offset to outbound/egress sequence number */
    int             sa_tx_flow;  /* flow SA to use to transmit packet */
    int             ipc_id;      /* ipc_id to give name uniqueness to socket
                                    connection with command shell */
    int             masterType;  /* Specifies to run the daemon as NETAPI_SYS_MASTER or
                                    NETAPI_PROC_MASTER */
} ipsecMgrMcb_t;

int netapilib_ifAddSA
(
    ipsecmgr_af_t           af,
    ipsecmgr_sa_id_t        *sa_id,
    ipsecmgr_sa_info_t      *sa_info,
    ipsecmgr_sa_dscp_map_cfg_t *dscp_map_cfg,
    ipsecmgr_ifname_t       *if_name,
    ipsecmgr_sa_encap_tmpl_t *encap,
    ipsecmgr_fp_handle_t    *sa_handle
);

int netapilib_ifAddSP
(
    ipsecmgr_af_t           af,
    ipsecmgr_selector_t     *sel,
    ipsecmgr_dir_t          dir,
    uint32_t                reqid,
    ipsecmgr_fp_handle_t    sa_handle,
    ipsecmgr_policy_id_t    policy_id,
    ipsecmgr_fp_handle_t    *sp_handle
);

int netapilib_ifDeleteSP
(
    ipsecmgr_fp_handle_t   sp_handle,
    ipsecmgr_policy_id_t   policy_id,
    ipsecmgr_dir_t         dir
);

int netapilib_ifDeleteSA
(
    ipsecmgr_fp_handle_t   sa_handle
);

int netapilib_ifGetSACtx
(
    ipsecmgr_fp_handle_t    sa_handle,
    ipsecmgr_sa_hw_ctx_t    *hw_ctx
);

#endif

