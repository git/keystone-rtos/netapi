/*
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/


/* IPSec Manager Include Files */
#include "ipsecmgr_snoop.h"
#include "ipsecmgr_ipc.h"
#include "ipsecmgr_syslog.h"

/* Local include */
#include "netapilib_interface.h"

/* Standard includes */
#include <dlfcn.h>
#include <stdlib.h>
#include <signal.h>
#include <stdarg.h>



#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <netinet/in.h> 
#include <string.h> 

#if defined(DEVICE_K2H)
#include <ti/drv/qmss/device/k2h/src/qmss_device.c>
#include <ti/drv/cppi/device/k2h/src/cppi_device.c>
char*  DTS_LOG_FILE_QUEUE_ETHx[] = {
 "/proc/device-tree/soc/netcp@2000000/netcp-interfaces/interface-0/rx-queue"};
#elif defined (DEVICE_K2K)
#include <ti/drv/qmss/device/k2k/src/qmss_device.c>
#include <ti/drv/cppi/device/k2k/src/cppi_device.c>
char*  DTS_LOG_FILE_QUEUE_ETHx[] = {
 "/proc/device-tree/soc/netcp@2000000/netcp-interfaces/interface-0/rx-queue"};
#elif defined (DEVICE_K2L)
#include <ti/drv/qmss/device/k2l/src/qmss_device.c>
#include <ti/drv/cppi/device/k2l/src/cppi_device.c>
char*  DTS_LOG_FILE_QUEUE_ETHx[] = {
 "/proc/device-tree/soc/netcp@2600000/netcp-interfaces/interface-0/rx-queue"};
#elif defined (DEVICE_K2E)
#include <ti/drv/qmss/device/k2e/src/qmss_device.c>
#include <ti/drv/cppi/device/k2e/src/cppi_device.c>
char*  DTS_LOG_FILE_QUEUE_ETHx[] = {
 "/proc/device-tree/soc/netcp@2400000/netcp-interfaces/interface-0/rx-queue"};

#else /*Default */
#include <ti/runtime/hplib/device/k2h/src/hplib_device.c>
#include <ti/runtime/netapi/device/k2h/src/netapi_device.c>
#include <ti/drv/qmss/device/k2h/src/qmss_device.c>
#include <ti/drv/cppi/device/k2h/src/cppi_device.c>
char*  DTS_LOG_FILE_QUEUE_ETHx[] = {
 "/proc/device-tree/soc/netcp@2000000/netcp-interfaces/interface-0/rx-queue"};

#endif

/**********************************************************************
 ************************** Local Definitions *************************
 **********************************************************************/

/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/
extern Rm_ServiceHandle   *rmClientServiceHandle;

static ipsecmgr_ipc_daemon_send_if_t *send_iface;
NETAPI_T netapi_handle;

ipsecMgrMcb_t globalDB;

static int use_rm =0;

/* Lock file for the daemon */
#define LOCK_FILE   "/var/lock/ipsecmgr_daemon"

/* snoop task */
static pthread_t    snoop_run_th;

static pthread_t stats_th;
static NETAPI_CFG_T our_netapi_default_cfg=
{
    TUNE_NETAPI_PERM_MEM_SZ,
    128,  //start of packet offset for hw to place data on rx for default flow
    TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM, //max number of descriptors in system
    TUNE_NETAPI_NUM_GLOBAL_DESC,        //total we will use
    TUNE_NETAPI_DEFAULT_NUM_BUFFERS,   //#descriptors+buffers in default heap
    64, //#descriptors w/o buffers in default heap
    TUNE_NETAPI_DEFAULT_BUFFER_SIZE+128+128,  //size of buffers in default heap
    128,    //tail room
    256,    //extra room
    0,
    NULL,
    0,
    0
};



static int QUIT = 0;
ipsecMgrShm_T* pIpsecMgrSaStats;

/* stub functions */
static void recv_cb(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
    return;
}

/* byte swap routine */
static unsigned int swap32 (unsigned int x)
{
    unsigned int y;

    y = (((x >> 24) & 0xff) <<  0) |
        (((x >> 16) & 0xff) <<  8) |
        (((x >>  8) & 0xff) << 16) |
        (((x >>  0) & 0xff) << 24) ;

    return (y);

}

void cleanup_sa_sp()
{
    int slot, error=0;;
    /* delete any offloaded rx SA's and policies */
    /* and delete any offloaded tx SA's */
    for (slot = 0; slot < IPSECMGR_MAX_SA; slot++)
    {
        if(globalDB.rx_sa[slot].in_use)
        {
            globalDB.rx_sa[slot].in_use = 0;
            if(globalDB.rx_sa[slot].spAppId)
            {
                netapi_secDelRxPolicy(netapi_handle,
                              (NETCP_CFG_IPSEC_POLICY_T) globalDB.rx_sa[slot].spAppId,
                              &error);
                ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
                "cleanup_sa_sp: SP deleted: sp_app_id: 0x%x, slot: %d, error: %d\n", 
                globalDB.rx_sa[slot].spAppId, slot, error);
            }
            netapi_secDelSA(netapi_handle,
                        NETCP_CFG_NO_INTERFACE,
                        (NETCP_CFG_SA_T) globalDB.rx_sa[slot].saAppId,
                        &error);
            ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
                "cleanup_sa_sp: SA deleted: sa_app_id: 0x%x, slot: %d, error: %d\n", 
                globalDB.rx_sa[slot].saAppId, slot, error);
            
        }
        if(globalDB.tx_sa[slot].in_use)
        {
            globalDB.tx_sa[slot].in_use = 0;
            netapi_secDelSA(netapi_handle,
                            NETCP_CFG_NO_INTERFACE,
                           (NETCP_CFG_SA_T) globalDB.tx_sa[slot].saAppId,
                            &error);
            ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
                "cleanup_sa_sp: SA deleted: sa_app_id: 0x%x, slot: %d, error: %d\n", 
                globalDB.tx_sa[slot].saAppId, slot, error);
        }
    }
}


static void* snoop_run_thread (void* arg)
{
    cpu_set_t cpu_set;
    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);

    hplib_utilSetupThread(0, &cpu_set, hplib_spinLock_Type_LOL);
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "snoop_run_thread: daemon entering forever event loop\n");

    while (1)
    {
        /* Poll for message from user application */
        ipsecmgr_ipc_poll();

        /* Poll for message from Kernel */
        ipsecmgr_snoop_run();
        if (QUIT == 1)
            break;
    }
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO,
        "snoop_run_thread: calling shutdown\n");
    ipsecmgr_snoop_shutdown ();
    cleanup_sa_sp();
    netapi_shutdown(netapi_handle);

        closeRm();
    return ((void*)NULL);
}

/**
 *  @b Description
 *  @n  
 *      SIGTERM handler.
 *
 *  @param[in]  signum
 *      signal number to terminate deamon.
 */
static void sig_term_handler(int signum)
{
    QUIT = 1;
}

/**
 *  @b Description
 *  @n  
 *      Function to implement task sleep functionality 
 *      for IPSecMgr
 *
 *  @param[in]  time_in_msec
 *      Time in milliseconds to sleep
 *
 *  @retval
 *      Not Applicable.
 */
static void task_sleep(uint32_t time_in_msec)
{
    pthread_mutex_t fake_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t fake_cond = PTHREAD_COND_INITIALIZER;
    struct timespec ts;
    int rt;
    unsigned int sec, nsec;

    sec = time_in_msec/1000;
    nsec = (time_in_msec - (sec*1000)) * 1000000;

    /* Use the wall-clock time */
    clock_gettime(CLOCK_REALTIME, &ts);

    ts.tv_sec += sec;
    ts.tv_nsec += nsec;

    pthread_mutex_lock(&fake_mutex);
    rt = pthread_cond_timedwait(&fake_cond, &fake_mutex, &ts);
    pthread_mutex_unlock(&fake_mutex);
}


/**
 *  @b Description
 *  @n  
 *      NETAPI Proxy's IPSecMgr Start Offload Response 
 *      message handler.
 *
 *      This function is called by the IPSecMgr library
 *      when it has a response ready corresponding to an
 *      Start Offload SP request issued by the user application.
 *
 *  @param[in]  rsp
 *      IPSecMgr's Start Offload SP response
 *  @param[in]  addr
 *      Destination address (user application) to send 
 *      the response to
 *  @param[in]  addr_size
 *      Size of destination address passed
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      ERROR   -   <0
 */
static int offload_sp_rsp_send
(
    ipsecmgr_snoop_offload_sp_rsp_param_t *rsp,
    void                                  *addr,
    uint32_t                              addr_size
)
{
    ipsecmgr_ipc_offload_sp_rsp_param_t offload_sp_rsp;

    offload_sp_rsp.type = rsp->type;
    offload_sp_rsp.result = rsp->result;
    offload_sp_rsp.trans_id = rsp->trans_id;
    offload_sp_rsp.err_code = rsp->err_code;
    offload_sp_rsp.sp_handle = rsp->sp_handle;
    offload_sp_rsp.sa_handle = rsp->sa_handle;

    if (addr_size != sizeof(ipsecmgr_ipc_addr_t)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "offload_sp_rsp_send: addr size not correct\n");
        return -1;
    }

    return send_iface->offload_sp_rsp(&offload_sp_rsp,
                                      (ipsecmgr_ipc_addr_t *)addr);
}
static void offload_sp_req_recv
(
    ipsecmgr_ipc_offload_sp_req_param_t *req,
    ipsecmgr_ipc_addr_t                 *src_addr
)
{
    ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,"offload_sp_req_recv called for policy id\n",
    req->policy_id);
    ipsecmgr_snoop_offload_sp_rsp_param_t rsp;
    uint32_t addr_size = sizeof(ipsecmgr_ipc_addr_t);
    ipsecmgr_snoop_offload_sp_req_param_t offload_sp_req;

    offload_sp_req.trans_id = req->trans_id;
    offload_sp_req.policy_id = req->policy_id;
    offload_sp_req.sa_flags = req->sa_flags;
    offload_sp_req.if_name = req->if_name;
    offload_sp_req.dscp_cfg = req->dscp_cfg;
    offload_sp_req.l5_selector = req->l5_selector;
    //offload_sp_req.oseq_offset = req->oseq_offset;

    memset(&rsp, 0, sizeof(rsp));
    rsp.trans_id = req->trans_id;

    if (ipsecmgr_snoop_offload_sp_req(&offload_sp_req, (void*)src_addr,
                                      addr_size)) {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "offload_sp_req_recv: snoop_offload_sp_req failed\n");
        rsp.result = RESULT_FAILURE;
        rsp.type = RSP_TYPE_ACK | RSP_TYPE_DONE;
    }
    else
    {
        rsp.result = RESULT_SUCCESS;
        rsp.type = RSP_TYPE_ACK;
    }
    
    if (offload_sp_rsp_send(&rsp, (void *)src_addr, addr_size))
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "offload_sp_req_recv: failed to send ACK for offload_sp\n");
    }
    return;
}


/**
 *  @b Description
 *  @n  
 *      NETAPI Proxy's IPSecMgr Stop Offload response message 
 *      handler.
 *
 *      This function is called by the IPSecMgr library
 *      when it has a response ready corresponding to an
 *      Stop Offload SP request issued by the user application.
 *
 *  @param[in]  rsp
 *      IPSecMgr's Stop Offload SP response
 *  @param[in]  addr
 *      Destination address (user application) to send 
 *      the response to
 *  @param[in]  addr_size
 *      Size of destination address passed
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      ERROR   -   <0
 */
static int stop_offload_rsp_send
(
    ipsecmgr_snoop_stop_offload_rsp_param_t *rsp,
    void                                    *addr,
    uint32_t                                addr_size
)
{
    ipsecmgr_ipc_stop_offload_rsp_param_t stop_offload_rsp;

    stop_offload_rsp.type = rsp->type;
    stop_offload_rsp.result = rsp->result;
    stop_offload_rsp.trans_id = rsp->trans_id;

    if (addr_size != sizeof(ipsecmgr_ipc_addr_t)) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR,
            "stop_offload_rsp_send: addr size not correct\n");
        return -1;
    }

    return send_iface->stop_offload_rsp(&stop_offload_rsp,
                                      (ipsecmgr_ipc_addr_t *)addr);
}

static void stop_offload_req_recv
(
    ipsecmgr_ipc_stop_offload_req_param_t   *req,
    ipsecmgr_ipc_addr_t                     *src_addr
)
{
    ipsecmgr_snoop_stop_offload_req_param_t stop_offload_req;
    uint32_t addr_size = sizeof(ipsecmgr_ipc_addr_t);
    ipsecmgr_snoop_stop_offload_rsp_param_t rsp;

    stop_offload_req.trans_id = req->trans_id;
    stop_offload_req.policy_id = req->policy_id;
    stop_offload_req.no_expire_sa = req->no_expire_sa;

    memset(&rsp, 0, sizeof(rsp));
    rsp.trans_id = req->trans_id;

    if (ipsecmgr_snoop_stop_offload(&stop_offload_req, (void*)src_addr,
                                      addr_size)) {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "stop_offload_req_recv: snoop_stop_offload failed\n");
        rsp.result = RESULT_FAILURE;
        rsp.type = RSP_TYPE_ACK | RSP_TYPE_DONE;
    }
    else
    {
        rsp.result = RESULT_SUCCESS;
        rsp.type = RSP_TYPE_ACK;
    }

    if (stop_offload_rsp_send(&rsp, (void *)src_addr, addr_size))
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "stop_offload_req_recv: failed to send ACK for stop_offload\n");
    }
    return;
}

static void stats_callback(NETAPI_T h)
{
    int i;
    for (i = 0; i < IPSECMGR_MAX_SA; i++)
    {
        /* Statistics for RX Tunnel */
        if(globalDB.rx_sa[i].in_use)
        {
            netapi_getSaStats(h,
                              globalDB.rx_sa[i].saAppId,
                              &(pIpsecMgrSaStats->netapi_sa_stats_rx[i]));
            if (pIpsecMgrSaStats->netapi_sa_stats_rx[i].validParams & NETAPI_IPSEC_STAT_VALID)
            {
                memcpy(&(pIpsecMgrSaStats->info_rx[i]),
                         &(globalDB.rx_sa[i]),
                         sizeof(ipsecMgrAppId_T));
            }
        }
    }
    for (i = 0; i < IPSECMGR_MAX_SA; i++)
    {
        /* Statistics for TX Tunnel */
        if (globalDB.tx_sa[i].in_use)
        {
            netapi_getSaStats(h,
                             globalDB.tx_sa[i].saAppId,
                             &(pIpsecMgrSaStats->netapi_sa_stats_tx[i]));
            if (pIpsecMgrSaStats->netapi_sa_stats_tx[i].validParams & NETAPI_IPSEC_STAT_VALID)
            {
                memcpy(&(pIpsecMgrSaStats->info_tx[i]),
                                        &(globalDB.tx_sa[i]),
                                        sizeof(ipsecMgrAppId_T));
            }
        }
    }
}

static void slow_path_thread(uint32_t index)
{
    NETAPI_T worker_nh;
    int err, i;;
    cpu_set_t cpu_set;

    CPU_ZERO( &cpu_set);
    CPU_SET( index, &cpu_set);

    hplib_utilSetupThread(index, &cpu_set, hplib_spinLock_Type_LOL);
    worker_nh = netapi_init(NETAPI_CORE_MASTER,NULL);

    if (worker_nh == NULL)
    {
        exit(1);
    }
    while(1)
    {
        if (QUIT == 1)
        {
            break;
        }
        sleep(2);
        stats_callback(worker_nh);
    }

    ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
        "slow_path_thread: calling netapi_shutdown\n");
    netapi_shutdown(worker_nh);
}
/**
 *  @b Description
 *  @n  
 *      Function to initialize IPSec Manager library.
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      ERROR   -   >0
 */
static int32_t init_ipsecmgr (void)
{
    struct ipsecmgr_snoop_fp_cfg_cb     fp_cfg_cb;
    struct ipsecmgr_snoop_mgnt_cb       mgnt_cb;
    struct ipsecmgr_snoop_platform_cb   plat_cb;
    ipsecmgr_ipc_daemon_recv_if_t       recv_if;
    ipsecmgr_ipc_cfg_t                  ipc_cfg;
    int32_t                             status;
    pthread_attr_t threadAttr;

    /* Initializations */
    ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO, 
                        "DEBUG: init_ipsecmgr() starting initialization\n");
    memset(&fp_cfg_cb, 0, sizeof(fp_cfg_cb));
    memset(&mgnt_cb, 0, sizeof(mgnt_cb));
    memset(&plat_cb, 0, sizeof(plat_cb));
    memset(&recv_if, 0, sizeof(recv_if));
    memset(&ipc_cfg, 0, sizeof(ipc_cfg));

    /* Initialize IPC library */
    ipc_cfg.mode = IPC_MODE_SNOOP_DAEMON;
    ipc_cfg.ipc_id = globalDB.ipc_id;
    if (ipsecmgr_ipc_init(&ipc_cfg)) {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "init_ipsecmgr: ipc_init failed\n");
        return -1;
    }
    else
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "init_ipsecmgr: ipc_init sucess\n");

    if (ipsecmgr_ipc_get_daemon_send_iface(&send_iface)) {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "init_ipsecmgr: ipc_get_daemon_send_iface failed\n");
        return -1;
    }
    recv_if.offload_sp_req = offload_sp_req_recv;
    recv_if.stop_offload_req = stop_offload_req_recv;
    /* Register ipsecmgr daemon recieve i/f */
    if (ipsecmgr_ipc_register_daemon_recv_iface(&recv_if)) {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "snoop_run: ipc_register_daemon_recv_iface failed\n");
        return -1;
    }

    /* Initialize the IPSec Manager Snoop library */
    fp_cfg_cb.add_sa        =   netapilib_ifAddSA;
    fp_cfg_cb.add_sp        =   netapilib_ifAddSP;
    fp_cfg_cb.del_sa        =   netapilib_ifDeleteSA;
    fp_cfg_cb.del_sp        =   netapilib_ifDeleteSP;
    fp_cfg_cb.get_sa_ctx    =   netapilib_ifGetSACtx;

    plat_cb.log_msg  = (ipsecmgr_snoop_log_msg_t)ipsecmgr_syslog_msg;
    plat_cb.sleep           =   task_sleep;

    mgnt_cb.offload_sp_rsp  =   offload_sp_rsp_send;
    mgnt_cb.stop_offload_rsp=   stop_offload_rsp_send;
    mgnt_cb.rekey_event     =   NULL; // No explicit notifications needed on Rekey completion

    if ((status = ipsecmgr_snoop_init (&fp_cfg_cb, &mgnt_cb, &plat_cb))) 
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
                            "DEBUG: init_ipsecmgr() ipsecmgr_snoop_init failed (%d)\n", status);
        return -1;
    }



#ifdef GDB_DEBUG
    snoop_run_thread(NULL);
#else
     if (pthread_create( &stats_th, (void*)NULL, (void*)slow_path_thread,
                     (void *) 0))
     {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "ERROR: stats collection thread failed to start, error code\n"); 
        return -1;
     }
    /* Create the task context for snoop library */
    if (pthread_create(&snoop_run_th, (void*) NULL, snoop_run_thread, NULL))
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "ERROR: snoop run thread failed to start, error code\n"); 
        return -1;
    }

    /* Setup signal handler for SIGTERM */
    if (signal(SIGTERM, sig_term_handler) == SIG_ERR) {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_WARN,
            "init_ipsecmgr: cannot handle SIGTERM\n");
    }
    /* Wait for the NETAPI Proxy task to finish its processing and exit. */
    pthread_join (stats_th, NULL);
    pthread_join (snoop_run_th, NULL);
#endif
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      Utility function to daemonize the current
 *      application.
 *
 *  @param[in]  lock_file
 *      Lock file to be used by the daemon
 */
static void daemonize (const char *lock_file)
{
    pid_t       pid, sid;
    int32_t     lock_fp = -1;
    char        str[10];

    /* already a daemon */
    if (getppid () == 1)
        return;

    /* Fork off the parent process */
    if ((pid = fork ()) < 0) 
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
                            "ERROR: daemonize() unable to fork daemon, code=%d (%s)",
                            errno, 
                            strerror(errno));
        exit (-1);
    }

    /* If we got a PID, then exit the parent process. */
    if (pid > 0)
        exit (0);

    /* At this point we are executing as the child process */

    /* Create a new SID for the child process */
    if ((sid = setsid ()) < 0) 
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
                            "ERROR: daemonize() unable to create a new session, code %d (%s)",
                            errno, 
                            strerror(errno));
        exit (-1);
    }

    /* Change the file mode mask */
    umask (027);

    /* Change the current working directory.  This prevents the current
     * directory from being locked; hence not being able to remove it. */
    if ((chdir("/")) < 0) 
    {
        ipsecmgr_syslog_msg(SYSLOG_LEVEL_ERROR, 
                            "ERROR: daemonize() unable to change directory to %s, code %d (%s)",
                            "/", 
                            errno, 
                            strerror(errno));
        exit (-1);
    }

    /* Create the lock file */
    if (lock_file) 
    {
        if ((lock_fp = open(lock_file, O_RDWR|O_CREAT, 0640)) < 0 ) 
        {
            ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR, 
                                "ERROR: daemonize() unable to create lock file %s, code=%d (%s)",
                                lock_file, 
                                errno, 
                                strerror(errno));
            exit (-1);
        }
    

        if (lockf (lock_fp, F_TLOCK, 0) < 0) 
            exit(-1); /* can not lock */

        /* Record pid to lockfile */
        snprintf (str, 10,"%d\n", getpid());
        write (lock_fp, str, strlen(str));
        close(lock_fp);
    }
    /* Cancel certain signals */
    signal (SIGCHLD, SIG_DFL); /* A child process dies */
    signal (SIGTSTP, SIG_IGN); /* Various TTY signals */
    signal (SIGTTOU, SIG_IGN);
    signal (SIGTTIN, SIG_IGN);
    signal (SIGHUP, SIG_IGN); /* Ignore hangup signal */
    signal (SIGTERM, sig_term_handler); /* catch SIGTERM */

    /* Redirect standard files to /dev/null */
    freopen( "/dev/null", "r", stdin);
    freopen( "/dev/null", "w", stdout);
    freopen( "/dev/null", "w", stderr);

    /* Done setting up the daemon */
    return;
}

int get_kernel_config()
{
    uint32_t temp=0;
    FILE *pDts = NULL;

    pDts = fopen(DTS_LOG_FILE_QUEUE_ETHx[0], "rb");

    if(pDts)
    {
        if(fread((void*)&temp, sizeof(uint32_t), 1, pDts))
        {
            globalDB.qNum = (int)swap32(temp);
        }
        fclose(pDts);
    }
    else
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "main: error opening device tree file: %s\n",DTS_LOG_FILE_QUEUE_ETHx[0]);
        return -1;
    }
#if (defined(DEVICE_K2L) || defined(DEVICE_K2E))
    globalDB.flowId = 0;
#else
    globalDB.flowId = 22;
#endif
    ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
            "get_kernel_config: flow: 0x%x, qNum: 0x%x\n", 
             globalDB.flowId, globalDB.qNum);
    return 0;
}

int create_pktio_channel()
{
    static int count = 0;
    int error = 0;
    char name[PKTIO_MAX_NAME];
    PKTIO_HANDLE_T *pktio_channel;
    PKTIO_CFG_T pktio_cfg;
    NETCP_CFG_ROUTE_T route;
    NETCP_CFG_FLOW_T flow;
    NETCP_CFG_EXCEPTION_PKT_T expPkt_appid = 0;

    memset(&pktio_cfg,0,sizeof(PKTIO_CFG_T));
    memset((void *)&route, 0, sizeof (NETCP_CFG_ROUTE_T));
    memset((void *)&flow, 0, sizeof (NETCP_CFG_FLOW_T));

    snprintf(&name[0],PKTIO_MAX_NAME, "%s","offload_0");
    pktio_cfg.qnum = globalDB.qNum;
    pktio_cfg.flags1 = PKTIO_RX;
    pktio_cfg.flags2 = PKTIO_GLOBAL | PKTIO_PKT;
    pktio_cfg.max_n = 8;
    pktio_cfg.queueType = Qmss_QueueType_GIC400_QUEUE;

    globalDB.pktio_channel = netapi_pktioCreate(netapi_handle,
                                               &name[0],
                                               (PKTIO_CB)recv_cb,
                                               &pktio_cfg,
                                               &error);
    if (!globalDB.pktio_channel)
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "create_pktio_channel: failed\n");
        return -1;
    }


    flow.dma_engine= 1;
    flow.flowid = globalDB.flowId;
    route.p_dest_q = globalDB.pktio_channel;
    route.p_flow = &flow;
    route.valid_params |= NETCP_CFG_VALID_PARAM_ROUTE_TYPE;
    route.routeType = NWAL_ROUTE_RX_INTF_W_FLOW;
    /* enable exception packet handling for fragmented packets */
    expPkt_appid = netapi_netcpCfgExceptions(netapi_handle,
                                             7,
                                             NETCP_CFG_ACTION_TO_SW,
                                             (NETCP_CFG_ROUTE_HANDLE_T) &route);

    return 0;
}
void print_ipsec_stats(Sa_IpsecStats_t* p_saIpsecStats, 
                                 nwal_saAALG auth,
                                 nwal_saEALG cipher, 
                                 uint32_t spi)
{
    printf("\nspi: 0x%x, Autentication mode: %d, Encryption Mode: %d\n",
            spi, auth, cipher);
    printf("IPSec replayOld:0x%x,replayDup:0x%x,authFail:0x%x \n",
                   p_saIpsecStats->replayOld,p_saIpsecStats->replayDup,p_saIpsecStats->authFail);
    printf("IPSec txESN:0x%x,rxESN:0x%x,pktEncHi:0x%x,pktEncLo:0x%x,pktDecHi:0x%x,pktDecLo:0x%x \n",
                   p_saIpsecStats->txESN,p_saIpsecStats->rxESN,p_saIpsecStats->pktEncHi,
                   p_saIpsecStats->pktEncLo,p_saIpsecStats->pktDecHi,p_saIpsecStats->pktDecLo);
}
/**
 *  @b Description
 *  @n  
 *      Entry point for the NETAPI Proxy application.
 *
 *  @param[in]  argc
 *      Number of arguments passed to the application
 *  @param[in]  argv
 *      Arguments passed to the application.
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      ERROR   -   >0
 */
int32_t main (int argc, char* argv[])
{
    int32_t             retVal;
    int i, iface;
    char* pTok = NULL;
    int ip_entry_count = 0;
    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    char host[NI_MAXHOST];
    struct sockaddr_in6 ipv6_addr;
    cpu_set_t cpu_set;
    int c;
    int statsQueryRequest = 0;
    void* pShmBase;
    void* pTemp;
    void* pShmEntry;
    int offloaded_sa = 0;
    static char usage[] = "usage: %s -s <outbound sequence number>\n \
               -f <SA transmit flow Id>\n \
               -i<daemon id>\n \
               -t <stats collection mode\n \
               -m <master Type ,sysMaster or procMaster >\n";

    memset(&globalDB, 0, sizeof(globalDB));
    globalDB.sa_tx_flow = -1;
    globalDB.masterType = NETAPI_SYS_MASTER;
    int temp;
    while ((c = getopt (argc, argv, "s:f:i:m:t:")) != -1)
    {
       switch (c)
       {
            case 's':
                /* check is user input 0 for oseq_offset (valid but also default)*/
                if(strncmp("0", optarg,1) == 0)
                {
                    globalDB.oseq_offset = 0;
                }
                else
                {
                    globalDB.oseq_offset = (uint32_t)strtol(optarg, NULL,0);
                    if(!globalDB.oseq_offset)
                    {
                        printf(usage, argv[0]);
                        exit(EXIT_FAILURE);
                    }
                }
                break;
            case 'f':
                /* check is user input 0 for sa_tx_flow(valid) */
                if(strncmp("0", optarg,1) == 0)
                {
                    globalDB.sa_tx_flow= 0;
                }
                else
                {
                    globalDB.sa_tx_flow = (uint32_t)strtol(optarg, NULL,0);
                    if(!globalDB.sa_tx_flow)
                    {
                        printf(usage, argv[0]);
                        exit(EXIT_FAILURE);
                    }
                }
                break;
            case 'i':
                /* check is user input 0 for daemon id, valid */
                 if(strncmp("0", optarg,1) == 0)
                 {
                     globalDB.ipc_id= 0;
                 }
                 else
                 {
                     globalDB.ipc_id = (uint32_t)strtol(optarg, NULL,0);
                     if(!globalDB.ipc_id)
                     {
                         printf(usage, argv[0]);
                         exit(EXIT_FAILURE);
                     }
                 }
                 break;
            case 'm':
                if(strcmp("sysMaster", optarg) == 0)
                {
                    globalDB.masterType = NETAPI_SYS_MASTER;
                }
                else if (strcmp("procMaster", optarg) == 0)
                {
                    globalDB.masterType = NETAPI_PROC_MASTER;
                }
                else
                {
                    printf(usage, argv[0]);
                    exit(EXIT_FAILURE);
                }
                break;
            case 't':
                if(strcmp("stats", optarg) == 0)
                   /* running in stats collection mode only */
                   statsQueryRequest = 1;
                else
                {
                    printf(usage, argv[0]);
                    exit(EXIT_FAILURE);
                }
                break;
            case '?':
                printf(usage, argv[0]);
                exit(EXIT_FAILURE);
            default:
                break;
        }
    }
    if(statsQueryRequest)
    {
        pShmBase = hplib_shmOpen();
        if (pShmBase)
        {
            if (globalDB.masterType == NETAPI_SYS_MASTER)
                pTemp = hplib_shmGetEntry(pShmBase, APP_ENTRY_1);
            else
                pTemp = hplib_shmGetEntry(pShmBase, APP_ENTRY_2);

            pIpsecMgrSaStats = (ipsecMgrShm_T*) pTemp;

            for (i = 0; i < IPSECMGR_MAX_SA; i ++)
            {
                if(pIpsecMgrSaStats->info_rx[i].in_use)
                {
                    offloaded_sa++;
                    print_ipsec_stats(&(pIpsecMgrSaStats->netapi_sa_stats_rx[i].saIpsecStats), 
                                 pIpsecMgrSaStats->info_rx[i].authMode,
                                 pIpsecMgrSaStats->info_rx[i].cipherMode,
                                 pIpsecMgrSaStats->info_rx[i].spi);
                }
            }
            for (i = 0; i < IPSECMGR_MAX_SA; i ++)
            {
                if(pIpsecMgrSaStats->info_tx[i].in_use)
                {
                    offloaded_sa++;
                    print_ipsec_stats(&(pIpsecMgrSaStats->netapi_sa_stats_tx[i].saIpsecStats), 
                                 pIpsecMgrSaStats->info_tx[i].authMode,
                                 pIpsecMgrSaStats->info_tx[i].cipherMode,
                                 pIpsecMgrSaStats->info_tx[i].spi);
                }
            }
        }
        if(!offloaded_sa)
            printf("NO Offloaded SA's\n");
        exit(EXIT_SUCCESS);
    }
    ipsecmgr_syslog_init();
    ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
                   "main: oseq_offset: %d, sa_tx_flow: %d\n",
                    globalDB.oseq_offset, globalDB.sa_tx_flow);

    if (initRm())
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
            "main: initRm() returned error\n");
        exit(1);
    }
    our_netapi_default_cfg.rmHandle = rmClientServiceHandle;


    /* assign main net_test thread to run on core 0 */
    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);
    hplib_utilSetupThread(0, &cpu_set, hplib_spinLock_Type_LOL);
    /* create netapi */
    netapi_handle = netapi_init(globalDB.masterType,
                                &our_netapi_default_cfg);
    if(netapi_handle == NULL)
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
                             "ERROR: netapi_init failed\n");
        return -1;
    }
    pShmBase = hplib_shmOpen();
    if (pShmBase)
    {
        if (globalDB.masterType == NETAPI_SYS_MASTER)
        {
            if (hplib_shmAddEntry(pShmBase,
                                  sizeof(ipsecMgrShm_T),
                                 APP_ENTRY_1) != hplib_OK)
            {
                return -1;
            }
            else
            {
                pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_1);
                pIpsecMgrSaStats =  (ipsecMgrShm_T*)pShmEntry;
                memset(pIpsecMgrSaStats,
                       0,
                       sizeof(ipsecMgrShm_T));
            }
        }
        else
        {
            if (hplib_shmAddEntry(pShmBase,
                                  sizeof(ipsecMgrShm_T),
                                 APP_ENTRY_2) != hplib_OK)
            {
                return -1;
            }
            else
            {
                pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_2);
                pIpsecMgrSaStats =  (ipsecMgrShm_T*)pShmEntry;
                memset(pIpsecMgrSaStats,
                       0,
                       sizeof(ipsecMgrShm_T));
            }
        }
    }
    else
    {
        exit(1);
    }

    if (get_kernel_config())
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
                            "ERROR: main: get_kernel_config() failed\n");
        return -1;
    }
    else
    {
        /* create pktio channel */
        if(create_pktio_channel())
        {
            ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
                            "ERROR: main: pktio channel creation failed\n");
            return -1;
        }
    }
     /*Create the proxy daemon. */
    ipsecmgr_syslog_msg(SYSLOG_LEVEL_INFO, "main: calling daemonize\n");
    daemonize (LOCK_FILE);

    /* Initialize and start the IPSec Mgr Snoop functionality */
    if ((retVal = init_ipsecmgr ()) < 0)
    {
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_ERROR,
                            "ERROR: IPSec Mgr init failed, error code %d \n",
                            retVal);
        return -1;
    }
    else
        ipsecmgr_syslog_msg (SYSLOG_LEVEL_INFO,
                             "main: ipsecmgr daemon shutdonw complete\n");

    exit(EXIT_SUCCESS);

}
