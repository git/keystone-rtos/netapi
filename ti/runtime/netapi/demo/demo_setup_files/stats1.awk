#--------------------------------------
# awk script to process stats
#  output from dpidemo and build
#  web page
#  stats1->  dump navl proto stats (part1)
#_____________________________________


BEGIN { state=0;
        thread=1;
        printf("<HTML>\n");
        printf("</HEAD>\n");
        printf("<BODY>\n");
        printf("<H2> DPI Protocol Statistics </H2>\n");
        
 }

$0 ~ "NAVL DPI stats"  {
  if (state==0)
  {
  state=1;
  printf("<p><b>  DPI Thread %d: </b> </p>\n", thread);
  printf("<table>\n");
  printf("<tr>\n");
#  printf("<th> colspan="6" <b>Results</b> </th> </tr> \n");
  }
}
 // {
 if ((state==1)&&(NF>=8))
 {
    printf("<tr> ");
    for(i=1;i<=6;i++) printf("<td> <b> %s </b> </td> ",$i);
    printf("</tr> \n");
 }
 else if ((state==2) && (NF>=6))
 {
    printf("<tr> ");
    for(i=1;i<=6;i++)  if (i==1) printf("<td> <b>%s</b> </td> ", $i); else  printf("<td> %s </td> ", $i);
    printf("</tr> \n");
 }
}
/--------/ { if (state==1) state=2}
/packets captured/ {if (state==2) {state=0; thread+=1;  printf("</table>\n"); printf("%s\n\n",$0);}}



END {
 printf("</PRE>\n");
 printf("<A href=\"\\index.html\"> <p><u> RETURN </u> </p> </A>\n");
 printf("</BODY>\n");
 printf("</HTML>\n");


}
