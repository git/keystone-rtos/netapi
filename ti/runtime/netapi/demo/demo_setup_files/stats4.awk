#--------------------------------------
# awk script to process stats
#  output from dpidemo and build
#  web page
#  stats3->  dump navl  mem stata
#_____________________________________


BEGIN { state=0;
        printf("<HTML>\n");
        printf("</HEAD>\n");
        printf("<BODY>\n");
        printf("<H2> Packet Summary : </H2>\n");
        lookup["nops"]="Number DPI Operations"
        lookup["nclass"]="Number Packets Classified";
        lookup["#mallocs"]="Number Malloc calls";
        lookup["#mbytes"]="Bytes Malloc'd";
        lookup["n_err"]="Number of errors";
        lookup["fops"]= "tbd";
        lookup["mCycles"]= "Malloc Cycles";
        lookup["fCycles"]= "tbd";
        tx[2]=0;
        
 }
/****stats/ {state=1;}
/NAVL DPI/  {state=0;}

/dpi stats:/ {

  printf("<table>\n");
  printf("<tr>\n");
  printf("<td> <b> Statistic </b> </td> <td> <b> Value </b> </td>\n");
  printf("</tr>\n");
  for(i=1;i<=NF;i++)
  {
    #printf("index = %d for %s\n", index($i,"="), $i);
    if (index($i,"=")!=0) 
    {
    split($i, val,"=");
    if (val[1] ~ "cycle")
    {
       val[1]= $(i-1) " cycle/pkt"
    }
    else 
    {
       val[1] = lookup[val[1]];
    }
    printf("<tr><td>%s</td> <td> %d </td> </tr> \n",val[1],val[2]);
    }
  }
 }
 /busy_cycles/ { }
 //  { if(state==1) {
  for(i=1; i<=NF;i++) {
    if ($i ~ "sb_rx=") {}
    else if ($i ~ "sb_tx=") {}
    else if ($i ~ "itx=") {}
    else if ($i ~ "+tx=") {}
    else if ($i ~ "min_rx=") {}
    else if ($i ~ "sec_tx=") {}
    else if ($i ~ "min_tx=") {}
    else if ($i ~ "rx=")
    {
      split($i, rx , "=");
    }
    else if ($i ~ "tx=")
    {
      if (tx[2] == 0)
      {
         split($i, tx , "=");
      }
    }
    else if ($i ~ "if")
    {
      if0 = $5;
      if1 = $6;
    }
    else if ($i ~ "core")
    {
      rx1 = $4;
      rx2 = $5;
    }

   }
  }
}


END {
printf("<tr><td>Thread 1 RX</td><td> %d </td> </tr> \n",rx1);
printf("<tr><td>Thread 2 RX</td><td> %d </td> </tr> \n",rx2);
printf("<tr><td>Interface 0 RX</td><td> %d </td> </tr> \n",if0);
printf("<tr><td>Interface 1 RX</td><td> %d </td> </tr> \n",if1);
printf("<tr><td>RX Total</td><td> %d </td> </tr> \n",rx[2]);
printf("<tr><td>TX Total</td><td> %d </td> </tr> \n",tx[2]);
printf("</table>\n\n");

 printf("</PRE>\n");
 printf("<A href=\"\\index.html\"> <p><u> RETURN </u> </p> </A>\n");
 printf("</BODY>\n");
 printf("</HTML>\n");


}
