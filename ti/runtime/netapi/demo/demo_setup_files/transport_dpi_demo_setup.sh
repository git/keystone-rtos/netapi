#!/bin/sh

export KERNEL_VER=$(uname -r)

# configure thttpd to run cgi scripts
sed -e 's|8080|8080 -c cgi-bin/*|g' -i /etc/init.d/thttpd
# reload the thttpd server
cd /etc/init.d
./thttpd restart

# start Resource Manager Server
/usr/bin/rmServer.out /usr/bin/device/k2e/global-resource-list.dtb /usr/bin/device/k2e/policy_dsp_arm.dtb
#copy over web files

cp /etc/transportnetlib/dpi_demo/index.html /srv/www/index.html
chmod -x /srv/www/index.html
cp /etc/transportnetlib/dpi_demo/stats1.awk /srv/www/stats1.awk
cp /etc/transportnetlib/dpi_demo/stats2.awk /srv/www/stats2.awk
cp /etc/transportnetlib/dpi_demo/stats3.awk /srv/www/stats3.awk
cp /etc/transportnetlib/dpi_demo/stats3.awk /srv/www/stats4.awk
cp /etc/transportnetlib/dpi_demo/*.gif      /srv/www/
cp /etc/transportnetlib/dpi_demo/dpicgi1.sh /srv/www/cgi-bin/dpicgi1.sh
cp /etc/transportnetlib/dpi_demo/dpicgi2.sh /srv/www/cgi-bin/dpicgi2.sh
cp /etc/transportnetlib/dpi_demo/dpicgi3.sh /srv/www/cgi-bin/dpicgi3.sh
cp /etc/transportnetlib/dpi_demo/dpicgi4.sh /srv/www/cgi-bin/dpicgi4.sh
cp /etc/transportnetlib/dpi_demo/dpicgi4.sh /srv/www/cgi-bin/dpicgi5.sh
chmod +x /srv/www/cgi-bin/dpicfgi*.sh


#install kernel module
echo $KERNEL_VER
insmod /lib/modules/$KERNEL_VER/extra/hplibmod.ko
#
# setup linux kernel bridge to handle broadcast packets
ifconfig eth0 0.0.0.0
#
ifconfig eth1 0.0.0.0
#create bridge
brctl addbr br0
#add i/fs to bridge
brctl addif br0 eth0
#
brctl addif br0 eth1
#
dhclient br0
#
ifconfig br0
# run transport_dpi_demo application
cd /usr/bin
./transport_dpi_demo

