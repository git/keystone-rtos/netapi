#--------------------------------------
# awk script to process stats
#  output from dpidemo and build
#  web page
#  stats2->  dump navl proto stats (part2)
#_____________________________________


BEGIN { state=0;
        thread=1;
        printf("<HTML>\n");
        printf("</HEAD>\n");
        printf("<BODY>\n");
        printf("<H2> DPI Cycle Statistics by Protocol </H2>\n");
        
 }

$0 ~ "NAVL DPI stats"  {
  if (state==0)
  {
  state=1;
  printf("<p><b>  DPI Thread %d: </b> </p>\n", thread);
  printf("<table>\n");
  printf("<tr>\n");
#  printf("<th> colspan="6" <b>Results</b> </th> </tr> \n");
  }
}
 // {
 if ((state==1)&&(NF>=8))
 {
    printf("<tr> ");
    printf("<td> <b> %s </b> </td> ",$1);
    for(i=7;i<=19;i++) printf("<td> <b> %s </b> </td> ",$i);
    printf("</tr> \n");
 }
 else if ((state==2) && (NF>=6))
 {
    printf("<tr> ");
    printf("<td> <b> %s </b> </td> ",$1);
    for(i=7;i<=19;i++)   printf("<td> %s </td> ", $i);
    printf("</tr> \n");
 }
}
/--------/ { if (state==1) state=2}
/packets captured/ {if (state==2) {state=0; thread+=1;  printf("</table>\n");}}



END {
 printf("</PRE>\n");
 printf("<A href=\"\\index.html\"> <p><u> RETURN </u> </p> </A>\n");
 printf("</BODY>\n");
 printf("</HTML>\n");


}
