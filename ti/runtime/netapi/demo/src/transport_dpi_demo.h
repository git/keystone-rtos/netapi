#include <stdlib.h>

#define MAC_HEADER_LEN          14
#define IP_HEADER_LEN           20
#define UDP_HEADER_LEN          8

#define MAX_NUM_INTERFACES 64
#define MAX_NUM_CORES 4

#define SP_THREAD_MASK 0xF0000000
#define THREAD_NUM_MASK 0x000000FF


typedef struct stats_t
{
    long itx;  //initially generated
    long itx2;
    long rx;
    long tx;
    long n_bad;
    long n_new;
    long n_class0_rx;   //count of pkts classified 
    long n_class1_rx;   //count of pkts classified 
    long n_class2_rx;   //count of pkts classified 
    long n_t1;
    long n_t2;
    long n_t3;
    long sec_tx;
    long sec_rx;
    long sb_tx;
    long sb_rx;
    long secp_rx;
    long n_auth_ok;
    unsigned long long  app_cycles;
    unsigned long long  send_cycles;
    unsigned long long  tx_cache_cycles;
    long rx_min;
    long tx_min;
    long if_rx[MAX_NUM_INTERFACES];
    long  core_rx[MAX_NUM_CORES];
    long  n_stats_cb;
    long  ip;
} STATS_T;

typedef struct head_t
{
	long ip[5];
	long udp[2];
} HEAD_T;



