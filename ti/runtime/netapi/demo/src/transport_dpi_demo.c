/******************************************
 * File: nt_bench.c   
 * Purpose:  benchmarks for NT.
 **************************************************************
 * FILE:  nt_bench.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application : benchmarks
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <sched.h>

//#include "trie.h"
#include <ti/runtime/netapi/netapi.h>
#include <ti/runtime/hplib/hplib.h>
#include "ti/runtime/netapi/pktio.h"
#include "transport_dpi_demo.h"
#include "navl_wrapper.h"
//#include "ti/runtime/netapi/test/net_test.h"
#include <ti/drv/sa/salld.h>

#include <ti/drv/qmss/device/k2e/src/qmss_device.c>
#include <ti/drv/cppi/device/k2e/src/cppi_device.c>

extern Rm_ServiceHandle   *rmClientServiceHandle;
extern NETCP_CFG_EXCEPTION_PKT_T expPkt_appid;


#define netapi_timing_start hplib_mUtilGetPmuCCNT

navl_wrapper_cfg_info_t *pNavlCfg;
navl_wrapper_pkt_stat_t *pStats1;
navl_wrapper_pkt_stat_t *pStats2;
navl_global_dpi_stats *pGlobDpiStats;
navl_wrapper_mem_stats_t *pNavlMemStats;


void* pTemp;


STATS_T stats;
paSysStats_t netcp_stats;
//struct dpi_stats dpis;

#define VDPI
#ifdef VDPI
static int DPI=0;  //1 to enable
static int DUMP_DPI_CONN=0;
#endif


void* pShmBase;
void *pShmEntry;



static int scnt=0;
volatile static int QUIT=0;
static int XMIT=0;
static int CAP=0;
volatile int RESET=0; //to reset stats
static int NTH=1;
volatile static int PKTGEN=0;
int pkt_len=64;



NETCP_CFG_MACIF_T mac[NUM_PROCS];
NETCP_CFG_MACIF_T mac0;
NETCP_CFG_MACIF_T mac1;

hplib_spinLock_T dpi_demo_thread_lock;


static char usage[] = "usage: %s -s \n";




//int procs =2; 

#define HPLIB_THREADID 0  // for main: HPLIB THREAD INSTANCE
//__thread int our_core;
static unsigned char dummy_mac[]={0xff,0xff,0xff,0xff,0xff,0xff,0x00,0x01,0x02,0x03,0x04,0x05,0x08,0x00};

void house(NETAPI_SCHED_HANDLE_T *s);
void our_stats_cb_mt(NETAPI_T h, paSysStats_t* pPaStats);
void our_stats_cb(NETAPI_T h, paSysStats_t* pPaStats);

//sig handler
void netTest_utilMySig(int x)
{
  QUIT=1;
  scnt+=1;
  printf(">net_test_dpi: recv'd signal %d cnt=%d\n",x,scnt);
  if (scnt > 10) {printf(">dpi-demo: WARNING EXITING WITH PROPER SHUTDOWN LUTS LEFT ACTIVE\n");exit(1);}

}


void recv_cb_bridge(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts );


/*************debug********************/
void netTest_utilDumpDescr(unsigned long *p, int n)
{
   printf("--------dump of descriptor %d %x\n", n, (int) p);
   printf("> %x %x %x %x %x %x %x %x\n",p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]);
   printf("> %x %x %x %x %x %x %x %x\n",p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]);
   printf("-----------------------------\n");
}
void netTest_utilDumpHeader(unsigned long *p, int n, int a, int r)
{
   printf("--------dump of header %d %x appID=%x flag1=%x\n", n, (int) p,a,r);
   printf("> %0x %0x %0x %0x %0x %0x %0x %0x\n",
          ntohl(p[0]),ntohl(p[1]),ntohl(p[2]),ntohl(p[3]),
          ntohl(p[4]),ntohl(p[5]),ntohl(p[6]),ntohl(p[7]) );
#if 0
   printf("> %x %x %x %x %x %x %x %x\n",p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]);
   printf("> %x %x %x %x %x %x %x %x\n",p[16],p[17],p[18],p[19],p[20],p[21],p[22],p[23]);
   printf("> %x %x %x %x %x %x %x %x\n",p[24],p[25],p[26],p[27],p[28],p[29],p[30],p[31]);
#endif
   printf("-----------------------------\n");
}
/*****************************************/

void house(NETAPI_SCHED_HANDLE_T * s)
{
    int err;
    NETAPI_SCHED_SHUTDOWN_T sched_shutdown;
    int coreid;  //who we are
    NETAPI_T nh= netapi_schedGetHandle(s);
    coreid=(int) netapi_getCookie(nh);
    
    if (QUIT)
    {
        sched_shutdown.shutdown_type = NETAPI_SCHED_SHUTDOWN_NOW;
        netapi_schedClose(s,&sched_shutdown,&err); 
        return;
    }


#ifdef VDPI
    if (DUMP_DPI_CONN )
        navl_dump_conn_info();
#endif


    /* only slow path threads get netcp stats, this needs to be set in cookie
       during slow path thread creation*/
    if (coreid & SP_THREAD_MASK)
    {
        netapi_netcpCfgReqStats(nh, our_stats_cb_mt, 0,&err);
    }

}

unsigned long long CALIB=0;
unsigned long long calibrate_idle(void)
{
    volatile unsigned long long  at1;
    volatile unsigned long long  at2;
    volatile unsigned long pt1;
    volatile unsigned long pt2;
    unsigned long long calib;
    at1 = hplib_mUtilGetTimestamp();
    pt1=netapi_timing_start();
    for(;;)
    {
       pt2=netapi_timing_start()   ;
       if ((pt2-pt1) >= 100000) break;
    }
    at2 = hplib_mUtilGetTimestamp();
    
    calib = ((unsigned long long) (pt2-pt1))/(at2-at1);
    printf("calibrate:   arm time=%lld  -> arm cycles=%d calib=%lld\n", at2-at1, pt2-pt1, calib);
    
    return calib;
}

/*******************************************
 *************NETAPI OBJECTS***************
 *****************************************/
static NETAPI_CFG_T our_netapi_default_cfg=
{
TUNE_NETAPI_PERM_MEM_SZ,
128,  //start of packet offset for hw to place data on rx for default flow
TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM, //max number of descriptors in system
TUNE_NETAPI_NUM_GLOBAL_DESC,        //total we will use
TUNE_NETAPI_DEFAULT_NUM_BUFFERS,   //#descriptors+buffers in default heap
64, //#descriptors w/o buffers in default heap
TUNE_NETAPI_DEFAULT_BUFFER_SIZE+128+128,  //size of buffers in default heap
128,       //tail room
256,      //extra room
0,
NULL
};

Pktlib_HeapHandle OurHeap;     //default heap, used by producer
PKTIO_HANDLE_T * netcp_rx_chan;
PKTIO_HANDLE_T * netcp_rx_chan2;
PKTIO_HANDLE_T * netcp_tx_chan;

PKTIO_CFG_T our_chan_cfg={PKTIO_RX_TX, PKTIO_LOCAL, PKTIO_Q_ANY, 8};
PKTIO_CFG_T netcp_rx_cfg={PKTIO_RX, PKTIO_NA, PKTIO_NA, 8};
PKTIO_CFG_T netcp_rx_cfg2={PKTIO_RX, (PKTIO_GLOBAL|PKTIO_PKT), PKTIO_Q_ANY, 8};
PKTIO_CFG_T netcp_tx_cfg={PKTIO_TX, PKTIO_NA, PKTIO_NA, 8};
NETAPI_T netapi_handle;
NETAPI_SCHED_HANDLE_T * our_sched;
NETAPI_SCHED_HANDLE_T * scheduler[TUNE_NETAPI_NUM_CORES];
NETAPI_SCHED_CONFIG_T our_sched_cfg={
  NETAPI_SCHED_DURATION|NETAPI_SCHED_CBV, 0, house, 5000000  //every 5000000 poll loops
};

NETCP_CFG_IP_T ip_rule0;
NETCP_CFG_IP_T ip_rule1;


PKTIO_CFG_T direct_to_cpsw_cfg={PKTIO_TX, PKTIO_GLOBAL, 648, 8};
PKTIO_HANDLE_T * cpsw_tx_chan;

PKTIO_CONTROL_T zap_channel_control={PKTIO_CLEAR, NULL};
PKTIO_CONTROL_T poll_cannel_control={PKTIO_SET_POLL_FLAGS, NULL, nwal_POLL_DEFAULT_GLOB_PKT_Q};

//template for fast path
nwalTxPktInfo_t txPktInfoNoCrypto =
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    0,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    0,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    0               ,                                                                                    /* saAhIcvOffBytes */
    0,                                                                                                 /* saAhMacSize */
    0,                                              /* etherLenOffBytes */
    MAC_HEADER_LEN,         /* ipOffBytes */
    MAC_HEADER_LEN + IP_HEADER_LEN,                                        /* l4OffBytes */
    UDP_HEADER_LEN,                                                             /* l4HdrLen */
    0,                                                                         /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};


NETCP_CFG_ROUTE_T  test_route=
{
0,
NULL,
NULL,
0,
0,
0,
1
};

NETCP_CFG_FLOW_HANDLE_T kernelFlow22;
NETCP_CFG_FLOW_HANDLE_T kernelFlow23;

/*************************END NETAPI OBJECTS***********************/

static unsigned char all_mac[]={0,0,0,0,0,0};
nwalIpAddr_t all_ip={0,0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


static unsigned char all_dest[]={0xff,0xff,0xff,0xff,0xff,0xff};




static unsigned long last_header[32/sizeof(unsigned long)];
static unsigned long last_desc[64/sizeof(unsigned long)];

//stats
#define MAX_CORE 4
int pkt_rx[MAX_CORE]; 
int pkt_tx[MAX_CORE]; 
unsigned long long pkt_rx_cycles[MAX_CORE]={0L};
unsigned long long pkt_tx_cycles[MAX_CORE]={0L};
unsigned long long pkt_cb_cycles[MAX_CORE]={0L};
unsigned long long idle_cycles[MAX_CORE]={0L};
volatile unsigned long long start_time[MAX_CORE];
unsigned long long end_time[MAX_CORE];
unsigned long long pkt_stall[MAX_CORE]={0L};
//*********************************
// packet generator
//*********************************
void gen_pkts(int np, int out_port);

/******************************************************
 * stats callback
 *******************************************************/
void our_stats_cb_mt(NETAPI_T h, paSysStats_t* pPaStats)
{
  stats.n_stats_cb +=1;
  if(pPaStats) memcpy(&netcp_stats,pPaStats, sizeof(paSysStats_t));
}

void our_stats_cb(NETAPI_T h, paSysStats_t* pPaStats)
{
    uint32_t numFreeDataPackets;
    uint32_t            numZeroBufferPackets;
    uint32_t            numPacketsinGarbage;
    Pktlib_HeapStats    pktLibHeapStats;
    int i,j;
    unsigned long long bcpp;
    unsigned long long bcpp_noc;
    unsigned long long bcpp_app;
    unsigned long long bcpp_tx;
    unsigned long long npL;
    unsigned long long cyclesL;
    unsigned long long ccyclesL; //cache cycles
    unsigned long long tmp_npL[TUNE_NETAPI_NUM_CORES];
    unsigned long long tmp_cyclesL[TUNE_NETAPI_NUM_CORES];
    unsigned long long tmp_ccyclesL[TUNE_NETAPI_NUM_CORES]; //cache cycles
    NETAPI_SA_STATS_T netapi_sa_stats;

    printf(">*****stats @ %lld (#cbs%d) \n", hplib_mUtilGetTimestamp(),stats.n_stats_cb);
    //printf("netcp_tx_handle check %x\n", netcp_tx_chan->back);
    printf(">itx=%d rx=%d tx=%d bad=%d slow=%d \n>rx_class0=%d rx_class1=%d rx_class2=%d  secRx=%d  secPRX=%d sb_rx=%d sb_tx=%d auth_ok=%d sec_tx=%d  min_rx=%d min_tx=%d ip=%d\n",
             stats.itx, stats.rx, stats.tx, stats.n_bad, stats.n_new, 
             stats.n_class0_rx, stats.n_class1_rx, 
             stats.n_class2_rx, stats.sec_rx, stats.secp_rx, stats.sb_rx, stats.sb_tx, stats.n_auth_ok,
             stats.sec_tx, stats.rx_min, stats.tx_min, stats.ip);
    printf(">if rx stats:  %d %d %d\n",stats.if_rx[0],stats.if_rx[1],stats.if_rx[2]);


    printf(">core rx stats:  %d %d %d\n",stats.core_rx[1],stats.core_rx[2],stats.core_rx[3]);


    for (j= 1;j < NUM_PROCS;j++)
    {
        tmp_npL[j]=0LL; tmp_cyclesL[j]=0LL; tmp_ccyclesL[j]=0LL;
        netapi_schedGetStats(scheduler[j],&tmp_npL[j],&tmp_cyclesL[j],&tmp_ccyclesL[j]);
        npL += tmp_npL[j];
        cyclesL += tmp_cyclesL[j];
        ccyclesL += tmp_ccyclesL[j];
    }

    if (npL && stats.rx)
    {
        bcpp = cyclesL/npL; 
        bcpp_noc = (cyclesL-ccyclesL)/npL; 
        bcpp_app = (stats.app_cycles-stats.tx_cache_cycles)/stats.rx;
    }
    else {bcpp = bcpp_noc=bcpp_app=0L;}
    if (stats.tx)
    {
        bcpp_tx = (stats.send_cycles-stats.tx_cache_cycles)/stats.tx;
    }
    else
    {
        bcpp_tx = 0L;
    }
    printf(">         ++ busy cycles pp=%lld (%lld wo cache ops) (app+tx= %lld) (tx= %lld) ++\n",
         bcpp,bcpp_noc,bcpp_app, bcpp_tx);


#ifdef VDPI
    navl_return_stats(
    &pGlobDpiStats->n_ops,
    &pGlobDpiStats->n_class,
    &pGlobDpiStats->min_time,
    &pGlobDpiStats->max_time,
    &pGlobDpiStats->tot,
    &pGlobDpiStats->m_op,
    &pGlobDpiStats->m_bytes,
    &pGlobDpiStats->n_err,
    &pGlobDpiStats->f_op,
    &pGlobDpiStats->m_cycles,
    &pGlobDpiStats->f_cycles);

    printf("dpi stats:  nops=%d nclass=%d min cycle=%d max  cycle=%d ave cycle=%lld #mallocs=%d #mbytes=%d n_err=%d fops=%d mCycles=%d fCycles=%d\n",
    pGlobDpiStats->n_ops,
    pGlobDpiStats->n_class,
    pGlobDpiStats->min_time,
    pGlobDpiStats->max_time,
    pGlobDpiStats->n_ops? pGlobDpiStats->tot/pGlobDpiStats->n_ops : 0,
    pGlobDpiStats->m_op,
    pGlobDpiStats->m_bytes,
    pGlobDpiStats->n_err,
    pGlobDpiStats->f_op, pGlobDpiStats->m_cycles, pGlobDpiStats->f_cycles);
    for(i=0; i< NUM_FP_PROCS;i++)
    {
        navl_results(i);
    }
#endif
    if(pPaStats)
    {
        printf("C1 number of packets:           %d\n", pPaStats->classify1.nPackets);
        printf("C1 number IPv4 packets:         %d\n", pPaStats->classify1.nIpv4Packets);
        printf("C1 number IPv6 packets:        %d\n", pPaStats->classify1.nIpv6Packets);
        printf("C1 number Custom packets:        %d\n", pPaStats->classify1.nCustomPackets);
        printf("C1 number SRIO packets:        %d\n", pPaStats->classify1.nSrioPackets);
        printf("C1 number LLC/SNAP Fail packets:        %d\n", pPaStats->classify1.nLlcSnapFail);
        printf("C1 number table matched:        %d\n", pPaStats->classify1.nTableMatch);
        printf("C1 number failed table matched: %d\n", pPaStats->classify1.nNoTableMatch);
        printf("C1 number IP Fragmented packets: %d\n", pPaStats->classify1.nIpFrag);
        printf("C1 number IP Depth Overflow: %d\n", pPaStats->classify1.nIpDepthOverflow);
        printf("C1 number VLAN Depth Overflow: %d\n", pPaStats->classify1.nVlanDepthOverflow);
        printf("C1 number GRE Depth Overflow: %d\n", pPaStats->classify1.nGreDepthOverflow);
        printf("C1 number MPLS Packets: %d\n", pPaStats->classify1.nMplsPackets);
        printf("C1 number of parse fail:        %d\n",pPaStats->classify1.nParseFail);
        printf("C1 number of Invalid IPv6 Opt:  %d\n", pPaStats->classify1.nInvalidIPv6Opt);
        printf("C1 number of TX IP Fragments:  %d\n", pPaStats->classify1.nTxIpFrag);
        printf("C1 number of silent discard:    %d\n",pPaStats->classify1.nSilentDiscard);
        printf("C1 number of invalid control:   %d\n", pPaStats->classify1.nInvalidControl);
        printf("C1 number of invalid states:    %d\n",pPaStats->classify1.nInvalidState);
        printf("C1 number of system fails:      %d\n",pPaStats->classify1.nSystemFail);
        printf("C2 number Packets  :           %d\n",pPaStats->classify2.nPackets);
        printf("C2 number udp           :      %d\n",pPaStats->classify2.nUdp);
        printf("C2 number tcp           :      %d\n",pPaStats->classify2.nTcp);
        printf("C2 number Custom       :      %d\n",pPaStats->classify2.nCustom);
        printf("C2 number silent drop   :      %d\n",pPaStats->classify2.nSilentDiscard);
        printf("C2 number invalid cntrl :      %d\n\n",pPaStats->classify2.nInvalidControl);
        printf("C2 number Modify Stats Cmd Fail :      %d\n\n",pPaStats->modify.nCommandFail);
    }
    Pktlib_getHeapStats(OurHeap, &pktLibHeapStats);

    printf("main heap stats>  #free=%d #zb=%d #garbage=%d\n", pktLibHeapStats.numFreeDataPackets,
                                pktLibHeapStats.numZeroBufferPackets, pktLibHeapStats.numPacketsinGarbage);
    printf("               >  #dataBufThreshStatus=%d #dataBufStarvCounter=%d #zBufThreshStatus=%d #zBufStarvCounter=%d \n", 
                        pktLibHeapStats.dataBufferThresholdStatus,pktLibHeapStats.dataBufferStarvationCounter,
                        pktLibHeapStats.zeroDataBufferThresholdStatus, pktLibHeapStats.zeroDataBufferStarvationCounter);
}

NETAPI_T worker_nh[MAX_NUM_CORES];

void slow_path_thread(uint32_t index)
{
    int err, i;;
    uint32_t thread_num;
    cpu_set_t cpu_set;

    /* index being passed in is the core we want to run the thread on */
    thread_num = index;
    printf("slow_path_thread, mypid: %d, core_id %d\n", gettid(), thread_num);

    CPU_ZERO( &cpu_set);
    for (i = 0; i < 1;i++)
    {
        CPU_SET( i, &cpu_set);
    }
    hplib_utilSetupThread(thread_num, &cpu_set, hplib_spinLock_Type_LOL);
    worker_nh[thread_num] = netapi_init(NETAPI_CORE_MASTER,NULL);

    if (worker_nh[thread_num] == NULL)
    {
        printf("slow_path_thread: netapi_init failure, exiting\n");
        exit(1);
    }
    netapi_setCookie(worker_nh[thread_num],(void*)(thread_num | SP_THREAD_MASK));

    scheduler[thread_num] =netapi_schedOpen(worker_nh[thread_num],&our_sched_cfg, &err);
    if (!scheduler[thread_num]) 
    {
        printf("sched create failed for core%d\n",thread_num);
        goto ERR_slow_path_thread;
    }
    scheduler[thread_num]->config.yield = TRUE;
    scheduler[thread_num]->config.pollGarbageQ = TRUE;
    scheduler[thread_num]->config.pollCtrlQ = TRUE;
    printf("Slow Path thread: %d setup complete, running on ARM CORE: %d\n",
    index,index);


    netapi_schedRun(scheduler[thread_num], &err);

ERR_slow_path_thread:
    printf("slow_path_thread: calling netapi_shutdown\n");
    netapi_shutdown(worker_nh[thread_num]);
}


void fast_path_thread(uint32_t index)
{
    int err, i;
    PKTIO_HANDLE_T *rx_chan;
    PKTIO_HANDLE_T *sb_rx_chan;
    uint32_t thread_num;
    int navlHandle;


    cpu_set_t cpu_set;

    CPU_ZERO( &cpu_set);
    thread_num = index;
    printf("fast_path_thread: core %d\n", index);


    CPU_SET( thread_num, &cpu_set);
    hplib_utilSetupThread(thread_num, &cpu_set, hplib_spinLock_Type_LOL);


    hplib_mSpinLockLock(&dpi_demo_thread_lock);
    worker_nh[thread_num]=netapi_init(NETAPI_CORE_MASTER,NULL);
    
    if (worker_nh[thread_num] == NULL)
    {
        printf("fast_path_thread: netapi_init failure, exiting\n");
        hplib_mSpinLockUnlock(&dpi_demo_thread_lock);
        exit(1);
    }
    else
    {
#ifdef VDPI
            navlHandle = navl_per_thread_init(thread_num);
#endif
    }
    hplib_mSpinLockUnlock(&dpi_demo_thread_lock);


    if (worker_nh[thread_num] == NULL)
    {
        printf("fast_path_thread: netapi_init failure, exiting\n");
        exit(1);
    }
    
    /* open netcp default RX channels*/
    rx_chan = netapi_pktioOpen(worker_nh[thread_num], NETCP_RX, (PKTIO_CB) recv_cb_bridge, &netcp_rx_cfg,  &err);


    netapi_setCookie(worker_nh[thread_num],(void*)thread_num);
    
    scheduler[thread_num] =netapi_schedOpen(worker_nh[thread_num],
                                            &our_sched_cfg,
                                            &err);
    if (!scheduler[thread_num]) 
        {
        printf("sched create failed for core%d\n",thread_num);
        goto ERR_fast_path_thread;
        //exit(1);
    }

 
    scheduler[thread_num]->config.yield = FALSE;
    scheduler[thread_num]->config.pollGarbageQ = FALSE;
    scheduler[thread_num]->config.pollCtrlQ = FALSE;
   /* Entry point to scheduler */


    printf("Fast Path thread: %d setup complete, running on ARM CORE: %d\n",
    index,index);
    netapi_schedRun(scheduler[thread_num], &err);

ERR_fast_path_thread:
#ifdef VDPI
        navl_fini(navlHandle);
#endif
    netapi_pktioClose(rx_chan, &err);

    printf("fast_path_thread: calling netapi_shutdown\n");
    netapi_shutdown(worker_nh[thread_num]);
}


/******************************
* main program
*****************************/
int main(int argc, char **argv)
{
    int err,i;
    int j;
    int32_t             errCode;
    Pktlib_HeapIfTable*  pPktifTable;
    Pktlib_HeapCfg heapCfg;
    long t1, t2 ;
    cpu_set_t cpu_set;
    int c;
    int statsQueryRequest = 0;
    pthread_t *thrs;
    int p;




#if 1

    if (argc == 2)
    {
        printf("main: argument %s\n", argv[1]);
        if(!(strcmp(argv[1], "stats")))
        {
            statsQueryRequest =1;
            printf("querying for stats\n");
        }
    }
    printf("statsQueryReqeust: %d\n", statsQueryRequest);


#endif


    if (!statsQueryRequest)
    {
        if (initRm())
        {
            printf("main: initRm() returned error\n");
            exit(1);
        }

        signal(SIGINT,netTest_utilMySig);
        CPU_ZERO( &cpu_set);
        CPU_SET( 0, &cpu_set);
        hplib_utilSetupThread(HPLIB_THREADID, &cpu_set, hplib_spinLock_Type_LOL);
    
        /* create netapi */
        our_netapi_default_cfg.rmHandle = rmClientServiceHandle;
        netapi_handle = netapi_init(NETAPI_SYS_MASTER,
                                    &our_netapi_default_cfg);
        if (netapi_handle == NULL)
        {
            printf("main: netapi_init failure, exiting\n");
            exit(1);
        }
        /* allocate segment for shared memory for packet stats */
        /* allocate packet statistics */
        pShmBase = hplib_shmOpen();
        if (pShmBase)
        {
            if (hplib_shmAddEntry(pShmBase,
                              sizeof(navl_wrapper_pkt_stat_t)
                             * MAX_PROTOCOLS *NUM_FP_PROCS + sizeof(navl_wrapper_cfg_info_t),
                             APP_ENTRY_1) != hplib_OK)
            {
                printf("main:  hplib_shmAddEntry failure\n");
                return -1;
            }
            else
            {
                pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_1);
                pNavlCfg =  (navl_wrapper_cfg_info_t*)pShmEntry;
                memset(pNavlCfg,
                       0,
                       sizeof(navl_wrapper_pkt_stat_t) * MAX_PROTOCOLS *NUM_FP_PROCS+ sizeof(navl_wrapper_cfg_info_t));
                pNavlCfg =  (navl_wrapper_cfg_info_t*)pShmEntry;
                pNavlCfg->enable_dpi = 0; /* disable DPI by default */
            }
            if (hplib_shmAddEntry(pShmBase, sizeof(navl_global_dpi_stats), APP_ENTRY_2) != hplib_OK)
            {
                printf("main:  hplib_shmAddEntry failure\n");
                return -1;
            }
            else
            {
                pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_2);
                pGlobDpiStats = (navl_global_dpi_stats*) pShmEntry;
                memset(pGlobDpiStats, 0, sizeof(navl_global_dpi_stats));
                pGlobDpiStats->min_time=100000000;
            }
            if (hplib_shmAddEntry(pShmBase, sizeof(navl_wrapper_stat_t)* (NUM_FP_PROCS *NUM_MEM_CTX * NUM_MEM_OBJ),
                                  APP_ENTRY_3) != hplib_OK)
            {
                printf("main:  hplib_shmAddEntry failure\n");
                return -1;
            }
            else
            {
                pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_3);
                pNavlMemStats = (navl_wrapper_mem_stats_t*) pShmEntry;
                memset(pNavlMemStats, 0, sizeof(navl_wrapper_mem_stats_t));
            }
            
        }
        else
        {
            printf("main: hplib_shmOpen failure, exiting\n");
            exit(1);
        }
            /* open the main heap */
        OurHeap = Pktlib_findHeapByName("netapi");
        if (!OurHeap)
        {
            printf("findheapbyname fail\n");
            exit(1);
        }

        //if we want to relay network packets, we create a handle to the 
        //default netcp receive queue here
        netcp_rx_chan= netapi_pktioOpen(netapi_handle, NETCP_RX, (PKTIO_CB) recv_cb_bridge, &netcp_rx_cfg,  &err);
        if (!netcp_rx_chan)
        {
            printf("pktio open RX failed err=%d\n",err);
            exit(1);
        }

        netcp_tx_chan= netapi_pktioOpen(netapi_handle, NETCP_TX, (PKTIO_CB) NULL, &netcp_tx_cfg,  &err);
        if (!netcp_tx_chan)
        {
            printf("pktio open TX failed err=%d\n",err);
            exit(1);
        }
        else  //install a fast path template into the NETCP TX channel
        {
            PKTIO_CONTROL_T control2;
            control2.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg2;
            memset(&cfg2, 0, sizeof(PKTIO_CFG_T));
            cfg2.fast_path_cfg.fp_send_option = PKTIO_FP_NO_CRYPTO_NO_CKSUM_PORT;
            cfg2.fast_path_cfg.txPktInfo= &txPktInfoNoCrypto;
            netapi_pktioControl(netcp_tx_chan, NULL, &cfg2, &control2, &err);
       }


        if (navl_setup() < 0)
        {
            printf("main: navl_setup failure, exiting\n");
            exit(1);
        }

        /*********************************************/
        /*****************end NETAPI STARTUP**********/
        /*********************************************/

        //now creaate a simple netcp rule
        //to get a lot of packets
        mac0 = netapi_netcpCfgCreateMacInterface(
                      netapi_handle,
                      &all_mac[0],
                      NULL,
                      0,
                      1,
                      (NETCP_CFG_ROUTE_HANDLE_T)  NULL,
                      (NETCP_CFG_VLAN_T ) NULL ,  //future
                      0x0800,
                      1,
                      &err);
        if (err) {printf("addmac0 failed %d\n",err); exit(1); }
        else printf("addmac0 sucess\n");

        mac1 = netapi_netcpCfgCreateMacInterface(
                      netapi_handle,
                      &all_mac[0],
                      NULL,
                      1,
                      2,
                      (NETCP_CFG_ROUTE_HANDLE_T)  NULL,
                      (NETCP_CFG_VLAN_T ) NULL ,  //future
                      0x0800,
                      1,
                      &err);
        if (err) {printf("addmac1 failed %d\n",err); exit(1); }
        else printf("addmac1 sucess\n");

        //calibrate idle
        CALIB = calibrate_idle();

        //**************************************
        //Create a slow path thread
        //***************************************
        thrs = malloc( sizeof( pthread_t ) * NUM_PROCS );
        if (thrs == NULL)
        {
            perror( "malloc" );
            return -1;
        }
        printf( "dpi-demo: Starting slow_path_thread on core 0\n");

        if (pthread_create( &thrs[0], NULL, (void*)slow_path_thread,
                          (void *)0 ))  //start at core 0
        {
            perror( "pthread_create" );
            exit(1);
        }


        for (j= 1;j < NUM_PROCS;j++)
        {
            printf( "dpi-demo: Starting fast_path_thread on core 1\n");
            if (pthread_create( &thrs[j], NULL, (void*)fast_path_thread,
                          (void *)j ))  //start at core 1
            {
                perror( "pthread_create" );
                exit(1);
            }
        }
    }
    else
    {
        
        pShmBase = hplib_shmOpen();
        if (pShmBase)
        {
            pTemp = hplib_shmGetEntry(pShmBase, APP_ENTRY_1);
            pNavlCfg =  (navl_wrapper_cfg_info_t*)pTemp;

            pStats1 = pTemp + sizeof(navl_wrapper_cfg_info_t);

        
            pStats2 = pTemp + sizeof(navl_wrapper_cfg_info_t) +
                    (sizeof(navl_wrapper_pkt_stat_t)*pNavlCfg->num_protocols);

            pTemp = hplib_shmGetEntry(pShmBase, APP_ENTRY_2);
            pGlobDpiStats = (struct dpi_stats*) pTemp;

            printf("dpi stats:  nops=%d nclass=%d min cycle=%d max cycle=%d ave cycle=%lld #mallocs=%d #mbytes=%d n_err=%d fops=%d mCycles=%d fCycles=%d\n",
            pGlobDpiStats->n_ops,
            pGlobDpiStats->n_class,
            pGlobDpiStats->min_time,
            pGlobDpiStats->max_time,
            pGlobDpiStats->n_ops? pGlobDpiStats->tot/pGlobDpiStats->n_ops : 0,
            pGlobDpiStats->m_op,
            pGlobDpiStats->m_bytes,
            pGlobDpiStats->n_err,
            pGlobDpiStats->f_op, pGlobDpiStats->m_cycles, pGlobDpiStats->f_cycles);
            for(i=0;i < NUM_FP_PROCS; i++)
            {
                navl_results2(i);
            }
            exit(1);
        }
    }


        //this thread of execution (main) now just waits on user input
        for(;;)
        {
            printf(">");
            c=getchar();
            if (c=='C')
            {
                CAP=!CAP; 
                printf("CAPTURE= %d\n", CAP);
            }
            else if (c=='q') {QUIT=1;break;}
            else if (c=='s')
                our_stats_cb(netapi_handle, &netcp_stats);
    #ifdef VDPI
               else if (c=='c') 
                {navl_clear_stats();printf("> Clearing DPI stats\n");}
               else if (c=='v') navl_set_verbose();
               else if (c=='p') 
                 {DUMP_DPI_CONN = !DUMP_DPI_CONN;printf(">  **DPI CONN DUMP is %s ** \n", DUMP_DPI_CONN ?"enabled":"disabled");}
               else if (c=='d')
                 {
                    pNavlCfg->enable_dpi = !pNavlCfg->enable_dpi;
                    printf("enable_dpi flag: %d\n", pNavlCfg->enable_dpi);
                    printf(">  **DPI is %s ** \n", pNavlCfg->enable_dpi?"enabled":"disabled");
                 }
    #endif
            else if (c=='!') {system("sh");}
    
            else if ((c=='h')||(c=='?'))
            {
                printf("> 'q' to quit,  's' for stats,'d' to dump capture\n,> 'h' for help\n ");
            }
    #if 1
            else if (c=='r')
            {
                netTest_utilDumpHeader(&last_header[0], 0,0,0);
                netTest_utilDumpDescr(&last_desc[0], 0);
            }
    #endif
        }



#ifdef VDPI
            navl_done();
#endif

        //wait for completion 
        printf("main task now pending on thread completion\n");
        for (i = 0; i < NUM_PROCS; i++)
                pthread_join( thrs[i], NULL );

        free( thrs );

        /*************************************************
        ************CLEAN UP****************************
        ************************************************/
        //get rid of rule, in the case that we are relaying packets
        //also close our netcp rx channel
        netapi_netcpCfgDelMac(netapi_handle,0,&err);
        netapi_netcpCfgDelMac(netapi_handle,1,&err);
    
        netapi_pktioClose(netcp_rx_chan,&err);
        netapi_pktioClose(netcp_tx_chan,&err);


        //done
        netapi_shutdown(netapi_handle);


}
#if 1
static inline void send_it(Ti_Pkt *tip, int len, int out_port)
{
  int err=0;
  PKTIO_METADATA_T meta2 = {PKTIO_META_TX,{0},0};
  nwalTxPktInfo_t meta_tx2={0};
  int coreid=Osal_nwalGetProcId();
  if (len<60)
  {
     unsigned int templen;
     char * p_pkt;
     len=60;
     Pktlib_getDataBuffer(tip,(uint8_t**)&p_pkt,&templen);//ignore templen
     Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc *) tip, p_pkt,len);
  }
  Pktlib_setPacketLen(tip,len);
  meta_tx2.txFlag1 = NWAL_TX_FLAG1_META_DATA_VALID;
  meta_tx2.ploadLen = len ;
  meta_tx2.enetPort=out_port;
  meta2.u.tx_meta=&meta_tx2;
  stats.tx+=1;
  if(coreid<MAX_NUM_CORES)
    pkt_tx[coreid]+=1;
  netapi_pktioSend(netcp_tx_chan,tip,&meta2,&err);
}
#endif
void recv_cb_bridge(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
int i;
int len;
int p;
Ti_Pkt * tip;
unsigned int appid;
unsigned int templen;
char * p_pkt;
unsigned long t1;
unsigned long t2;
unsigned long long ct1;
unsigned long long ct2;
unsigned short ip_pl;
unsigned long long n_c_ops;
int ifno;
int out_port;

int coreid=Osal_nwalGetProcId();


pasahoLongInfo_t* protoInfo;

t1=netapi_timing_start();
ct1 =Osal_cache_op_measure(&n_c_ops);
for(i=0;i<n_pkts;i++)
{
        
        tip = p_recv[i];
        appid = ((unsigned int)meta[i].u.rx_meta->appId)&0xff000000;
        if (appid == NETAPI_NETCP_MATCH_GENERIC_IP) 
        {
           stats.ip+=1;
        }

        protoInfo=nwal_mGetProtoInfo(tip);
        ifno = nwal_mGetRxEmacPort( protoInfo);
        if (ifno ==1) out_port=2; else out_port=1;
        if(coreid<MAX_NUM_CORES) stats.core_rx[coreid]+=1;
        if (ifno < MAX_NUM_INTERFACES) stats.if_rx[ifno]+=1;
        Pktlib_getDataBuffer(tip,(uint8_t**)&p_pkt,&templen);//ignore templen
        if (CAP==coreid)
        {
            memcpy((unsigned char *)&last_header[0],p_pkt,32);
            memcpy((unsigned char*)&last_desc[0],tip,64);
        }
        len = Pktlib_getPacketLen(tip);//real length, subtract mac trailer
        stats.rx+=1;
        //printf("recv_cb_bridge: appId: 0x%x, out_port: %d\n", appid, out_port);
        if (appid == NETAPI_NETCP_MATCH_GENERIC_MAC)
        {
#ifdef VDPI
            {
                if (pNavlCfg->enable_dpi)
                    navl_process_pkt(p_pkt, len);
            }
#endif
        }

    
 //printf("recv_cb_bridge: coreId: %d, outPort %d\n", coreid, out_port);
        //Pktlib_freePacket(tip);
       send_it(tip,len,out_port);
}
t2=netapi_timing_start();
ct2 =Osal_cache_op_measure(&n_c_ops);
stats.app_cycles +=  (unsigned long long) (t2-t1);
stats.tx_cache_cycles += (unsigned long long) (ct2-ct1);
return;
}

#define NTOPOP 150
volatile Ti_Pkt * pHd[NTOPOP];

#define PKTGEN_PKT_LEN pkt_len
#define MAXP 4   //max ports 
void gen_pkts(int np, int out_port)
{
    int i;
    int p=0;
    unsigned long * pI ;
    Ti_Pkt * tip;
    int len;
    unsigned char * pData;
    int cstall=0;
    int coreid = Osal_nwalGetProcId();
    for(i=0;i<np;)
    {
        //set out output port
        if (out_port)
        {
            p=out_port;
        }
        else //flip flop
        {
            p+=1;
            if(p>MAXP) p=1;
        }
        //get a packet
        tip=Pktlib_allocPacket(OurHeap,PKTGEN_PKT_LEN);
        pI = (unsigned long *) tip;
        if (!tip)
        {
            pkt_stall[coreid]+=1;
            cstall+=1;
            if (cstall >= 100000) 
            {
                printf("worker core %d, max stall hit,, exiting.\n",coreid); 
	        return;
	    }
            continue;
        }
        cstall=0;
        Pktlib_getDataBuffer(tip,&pData,&len);
        memcpy(pData,&dummy_mac,14);
        Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc *) tip, pData,PKTGEN_PKT_LEN);
        Pktlib_setPacketLen(tip,PKTGEN_PKT_LEN);
        pI[1]=0x80000000;
        //pI[2] &= 0xfff0ffff ;move to pktio send function

        //capture packet just in case
        if (CAP==coreid)
        {
            unsigned int templen;
            char * p_pkt;
            Pktlib_getDataBuffer(tip,(uint8_t**)&p_pkt,&templen);//ignore templen
            memcpy((unsigned char *)&last_header[0],p_pkt,32);
            memcpy((unsigned char*)&last_desc[0],tip,64);
        }

        //send  packet
        send_it(tip, PKTGEN_PKT_LEN, p);
        pkt_tx[coreid]+=1;
        i+=1;
   }

    return;
}
