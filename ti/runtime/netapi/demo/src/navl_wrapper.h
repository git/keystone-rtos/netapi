/******************************************************************************
 * FILE PURPOSE:  User space access to transport resources on SOC
 ******************************************************************************
 * FILE NAME:   navl_wrapper.h
 *
 * DESCRIPTION: NAVL Wrapper definitions and data structures
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2014
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* ============================================================= */
/**
 *   @file  netapi.h
 *
 *   path  ti/runtime/netapi/demo/src/navl_wrapper.h
 *
 *   @brief  
 *
 */



#ifndef __NAVL_WRAPPER_H
#define __NAVL_WRAPPER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <navl/navl.h>

#define NUM_PROCS 3

#define NUM_FP_PROCS 2

#define MAX_PROTOCOLS 1500
#define NUM_MEM_CTX 50
#define NUM_MEM_OBJ 50

#define MAX_BIN 10
typedef struct 
{
    uint32_t num_threads;
    uint32_t num_protocols;
    uint32_t enable_dpi;
    int64_t alloc_curr;
} navl_wrapper_cfg_info_t;




typedef struct
{
unsigned long n_ops;
unsigned long n_class;
unsigned long min_time;
unsigned long max_time;
unsigned long long tot;
unsigned long m_op;
unsigned long m_bytes;
unsigned long n_err;
unsigned long f_op;
unsigned long m_cycles;
unsigned long f_cycles;
unsigned long long malloc_cycles;
unsigned long long free_cycles;
int malloc_inst;
int malloc_bytes;
int free_inst;
} navl_global_dpi_stats;




/* for tracking packet stats */
typedef struct
{
    uint64_t packets;
    uint64_t bytes;
    char name[9];
    uint64_t cycles;
    uint64_t cycles_nomem;
    unsigned long cycles_max;
    unsigned long cycles_min;
    uint64_t class; //# packets classified
    long bin_cycles[MAX_BIN];
    uint32_t pad[3];
} navl_wrapper_pkt_stat_t;



/* for tracking packets per thread/core basis */
typedef struct 
{
    navl_wrapper_cfg_info_t     navl_cfg;
    navl_wrapper_pkt_stat_t     stats_pkt[NUM_FP_PROCS];
} navl_wrapper_shm_pkt_stats_t;




/* for tracking memory stats */
typedef struct
{
    int64_t curr;
    int64_t peak;
    int64_t fail;
} navl_wrapper_stat_t;

typedef struct 
{
    navl_wrapper_stat_t mstats[NUM_FP_PROCS][NUM_MEM_CTX][NUM_MEM_OBJ];
    char                    ctx_name[NUM_FP_PROCS][NUM_MEM_CTX][64];
    char                    obj_name[NUM_FP_PROCS][NUM_MEM_OBJ][64];
} navl_wrapper_mem_stats_t;



/* instance variables */
typedef struct {
/* handles */
    navl_handle_t           navl;

    /* configuration */
    const char             *config_capfile;
    const char             *config_plugins;
    int                     config_num_proto;
    int                     config_conn_id_attr;
    int                     config_http_attr;
    int                     config_num_memctx;
    int                     config_num_memobj;

    /* options */
    int                     option_dpi;
    int                     option_simple;
    int                     option_track_memory;
    int                     option_limit_memory;
    int                     option_tunnel;
    int                     option_realtime_mode;
    int                     option_verbose;

    /* diagnostics */
    int                     error_navl;

    /* statistics */
//#define NUM_MEM_CTX 50
//#define NUM_MEM_OBJ 50
    navl_wrapper_mem_stats_t    *stats_mem;
    //char                    ctx_name[NUM_MEM_CTX][64];
    //char                    obj_name[NUM_MEM_OBJ][64];
    navl_wrapper_pkt_stat_t *stats_pkt[NUM_FP_PROCS];
    uint64_t                stats_conns;

    /* misc vars */
    int                     running;
    int64_t                 alloc_curr;
    int64_t                 alloc_peak;
} navl_mcb_t;

#ifdef __cplusplus
}
#endif
#endif
