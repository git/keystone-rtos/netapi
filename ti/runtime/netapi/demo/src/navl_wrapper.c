#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <getopt.h>
#include <errno.h>
#include <assert.h>
#include <stdarg.h>
#include "ti/runtime/hplib/hplib.h"
#include "ti/runtime/netapi/netapi_types.h"
#include "navl_wrapper.h"
/* timing */

#define netapi_timing_start hplib_mUtilGetPmuCCNT

navl_wrapper_cfg_info_t *pNavlCfg;
navl_global_dpi_stats *pDpiStats;
void* pShmBase;
void *pShmEntry;

//int free_inst=0;

static unsigned long long timing=0LL;

static char last_url[256];
int class=0;
void clear_pkt_stats();
void navl_clear_stats(void)
{
    memset(pDpiStats, 0, sizeof(navl_global_dpi_stats));
    pDpiStats->min_time=100000000;
    clear_pkt_stats();
}
void navl_return_stats(int * Pn_ops, int * Pn_class, unsigned long * Pmin_time, unsigned long * Pmax_time, unsigned long long *Ptiming, int * Pmalloc_inst, int *Pmalloc_bytes, int * Pn_err, int *Pfree_inst, unsigned long *Pmalloc_cycles, unsigned long *Pfree_cycles)
{
    *Pn_ops=pDpiStats->n_ops;
    *Pn_class=pDpiStats->n_class;
    *Pmin_time=pDpiStats->min_time;
    *Pmax_time=pDpiStats->max_time;
    *Ptiming=timing;
    *Pmalloc_inst = pDpiStats->malloc_inst;
    *Pmalloc_bytes= pDpiStats->malloc_bytes;
    *Pn_err=pDpiStats->n_err;
    *Pfree_inst = pDpiStats->free_inst; 
    *Pmalloc_cycles= pDpiStats->malloc_inst ? pDpiStats->malloc_cycles/pDpiStats->malloc_inst: 0;
    *Pfree_cycles= pDpiStats->free_inst? pDpiStats->free_cycles/pDpiStats->free_inst : 0;
}

//#define MAX_BIN 10
static long bins[MAX_BIN]={10000, 12500, 15000,17500,20000, 25000,30000,35000,40000,50000};
static char* binStr[MAX_BIN]={"10K", "12.5K", "15K","17.5K","20K","25K","30K","35K","40K","50K"};

void add2bin(long cycles, long p_bins[])
{
    int i;
    for(i=0;i<MAX_BIN-1;i++)
      if (cycles<bins[i]) {p_bins[i]+=1;return;}
     p_bins[MAX_BIN-1] += 1; //max
}



navl_mcb_t *g_reader = NULL;
const char *g_exename = NULL;

static int  navl_wrapper_init(int argc, char *argv[]);
static void *navl_wrapper_malloc(size_t);
static void  navl_wrapper_free(void *);
static int   navl_wrapper_log_message(const char *level, const char *func, const char *format, ...);

static int navl_classify_callback(navl_handle_t handle, navl_result_t result, navl_state_t state, navl_conn_t conn, void *arg, int error);

static void  navl_wrapper_mem_stat_print(int threadId);

/* external definitions */
void bind_navl_externals();

void navl_set_verbose(void)
{
    g_reader->option_verbose=!g_reader->option_verbose;
    printf(">**DPI Now in %s mode\n",g_reader->option_verbose?"verbose":"nonverbose");
}
void navl_dump_conn_info()
{
navl_diag(g_reader->navl, "TCP", NULL);
}


#define navl_wrapper_error() \
do { \
    fprintf(stderr, "%s failed in %s:%u", g_exename, __FUNCTION__, __LINE__); \
    if (g_reader) { \
    if (g_reader->error_navl) \
    fprintf(stderr, " with navl error %s", get_error_string(navl_error_get(g_reader->navl))); \
    } \
    fprintf(stderr, "\n"); \
} while (0)

int navl_setup(void)
{
    return (navl_wrapper_init(0, NULL));
}


static const char *
get_state_string(navl_state_t state)
{
switch (state)
    {
        case NAVL_STATE_INSPECTING:
            return "INSPECTING";
        case NAVL_STATE_MONITORING:
            return "MONITORING";
        case NAVL_STATE_CLASSIFIED:
            return "CLASSIFIED";
        case NAVL_STATE_TERMINATED:
            return "TERMINATED";
        default:
            return "UNKNOWN";
    }
}

static const char *
get_confidence_string(int confidence)
{
    switch (confidence)
    {
        case 50:
            return "PORT";
        case 100:
            return "DPI";
        default:
            return "NONE";
    }
}

static const char *
get_error_string(int error)
{
    switch (error)
    {
        case 0:
            return "None";
        case ENOMEM:
            return "No memory available";
        case EPROTO:
            return "Protocol error";
        case ENOTCONN:
            return "No connection allocated";
        case EEXIST:
            return "Object exists";
        case EINVAL:
            return "Invalid parameter";
        case ECANCELED:
            return "Operation cancelled";
        case ENOENT:
            return "No such file or directory";
        case EPROTONOSUPPORT:
            return "Protocol not supported";
        default:
            return "Unknown";
    }
}

static int
navl_wrapper_init(int argc, char *argv[])
{
    static navl_mcb_t reader;

    int ret;
    int i,j;

    g_reader = &reader;

    g_reader->navl = -1;

    g_reader->config_capfile = NULL;
    g_reader->config_plugins = "plugins";
    g_reader->config_num_proto = 0;
    g_reader->config_conn_id_attr = 0;
    g_reader->config_http_attr = 0;
    g_reader->config_num_memctx = NUM_MEM_CTX;
    g_reader->config_num_memobj = NUM_MEM_OBJ;

    g_reader->option_dpi = 1;
    g_reader->option_simple = 0;
    g_reader->option_track_memory = 1;
    g_reader->option_limit_memory = 0;
    g_reader->option_realtime_mode = 1;
    g_reader->option_verbose = 0;

    g_reader->error_navl = 0;

    for(j=0;j< NUM_FP_PROCS;j++)
        g_reader->stats_pkt[j] = NULL;

    g_reader->stats_conns = 0;

    g_reader->running = 1;
    g_reader->alloc_curr = 0;
    g_reader->alloc_peak = 0;

    g_exename = "dpi_demo";


    /* allocate segment for shared memory for packet stats */
     /* allocate packet statistics */
    pShmBase = hplib_shmOpen();
    if (pShmBase)
    {
        pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_1);
        pNavlCfg =  (navl_wrapper_cfg_info_t*)pShmEntry;
        memset(pNavlCfg,
               0,
               sizeof(navl_wrapper_pkt_stat_t) * MAX_PROTOCOLS *NUM_FP_PROCS+ sizeof(navl_wrapper_cfg_info_t));
        pNavlCfg =  (navl_wrapper_cfg_info_t*)pShmEntry;
        pNavlCfg->num_protocols = g_reader->config_num_proto;
        g_reader->stats_pkt[0] = pShmEntry + sizeof(navl_wrapper_cfg_info_t);

        g_reader->stats_pkt[1] = pShmEntry + sizeof(navl_wrapper_cfg_info_t) +
                (sizeof(navl_wrapper_pkt_stat_t)*MAX_PROTOCOLS); 

        for (j = 0; j < NUM_FP_PROCS; j++)
        {
            for(i=0;i< MAX_PROTOCOLS;i++)
            {
                g_reader->stats_pkt[j][i].cycles_min=10000000;
                g_reader->stats_pkt[j][i].cycles_max=0;

            }
        }
        pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_2);
        pDpiStats = (navl_global_dpi_stats*) pShmEntry;

        pShmEntry = hplib_shmGetEntry(pShmBase, APP_ENTRY_3);
        g_reader->stats_mem = (navl_wrapper_mem_stats_t*) pShmEntry;
    }
    else
    {
        printf("navl_wrapper_init: hplib_shmOpen failure\n");
        return NETAPI_ERR_NOMEM;
    }
    

    /* EXTERNAL BINDINGS GO HERE */
    {
        /* Bind the platform specific functions. */
        bind_navl_externals();

        /* Private overrides for this example application. */
        navl_log_message = navl_wrapper_log_message;
        if (g_reader->option_track_memory)
        {
            navl_malloc_local = navl_wrapper_malloc;
            navl_free_local = navl_wrapper_free;
            navl_malloc_shared = navl_wrapper_malloc;
            navl_free_shared = navl_wrapper_free;
        }
    }

    /* open the navl library */
    if ((g_reader->navl = navl_open(g_reader->config_plugins)) == -1)
        navl_wrapper_error();

    /* set logging level to "error | fatal" */
    if (navl_config_set(g_reader->navl, "system.loglevel", "48") == -1)
        navl_wrapper_error();

    /* determine the max protocol index */
    if ((ret = navl_proto_max_index(g_reader->navl)) == -1)
    {
        printf("navl_proto_max_index error\n");
        navl_wrapper_error();
    }

    /* the number of protocols is the max + 1 */
    g_reader->config_num_proto = (ret + 1);
    pNavlCfg->num_protocols = g_reader->config_num_proto;

    return NETAPI_ERR_OK;
}

//per thread init. Call this in worker thread context
int navl_per_thread_init(uint32_t thread_num)
{
    int  ret;
    int c;

     /* initialize this thread for classification */
    if (navl_init(g_reader->navl))
    {
        printf("navl_init error\n");
        navl_wrapper_error();
    }
#if 0
        /* enable connection tracking */
        if (navl_attr_enable(g_reader->navl, "conn.id", 1) == -1)
                navl_wrapper_error();

#ifdef HTTP_ATTRIB
        /* enable http url */
        if (navl_attr_enable(g_reader->navl, "http.request.url",1) == -1)
                navl_wrapper_error();
#endif

        /* lookup the key for conn.id */
        if ((g_reader->config_conn_id_attr = navl_attr_key_get(g_reader->navl, "conn.id")) == -1)
                navl_wrapper_error();
#ifdef HTTP_ATTRIB
        /* lookup the key for http.request.host */
        if ((g_reader->config_http_attr = navl_attr_key_get(g_reader->navl, "http.request.url")) == -1)
                navl_wrapper_error();
#endif
#endif
    /* simulated realtime */
    if (g_reader->option_realtime_mode == 2)
        navl_clock_set_mode(g_reader->navl, 1);


#if 0
    /* determine the max protocol index */
    if ((ret = navl_proto_max_index(g_reader->navl)) == -1)
    {
        printf("navl_proto_max_index error\n");
        navl_wrapper_error();
    }

    /* the number of protocols is the max + 1 */
    g_reader->config_num_proto = (ret + 1);
#endif


    /* now fetch all the protocol name ahead of time do we don't have to lookup them up on each packet */
    for (ret = 0; ret != g_reader->config_num_proto; ret++)
        navl_proto_get_name(g_reader->navl, ret,
                            g_reader->stats_pkt[thread_num-1][ret].name,
                            sizeof(g_reader->stats_pkt[thread_num-1][ret].name));


 /* fetch all the memory tag names */
    for (c = 0; c < NUM_MEM_CTX; c++)
        navl_memory_ctx_name(g_reader->navl, c, g_reader->stats_mem->ctx_name[thread_num-1][c],
                            sizeof(g_reader->stats_mem->ctx_name[thread_num-1][c]));
        //navl_memory_ctx_name(g_reader->navl, c, g_reader->ctx_name[c], sizeof(g_reader->ctx_name[c]));
    for (c = 0; c < NUM_MEM_OBJ; c++)
        navl_memory_obj_name(g_reader->navl, c, g_reader->stats_mem->obj_name[thread_num-1][c],
                            sizeof(g_reader->stats_mem->obj_name[thread_num-1][c]));

        //navl_memory_obj_name(g_reader->navl, c, g_reader->obj_name[c], sizeof(g_reader->obj_name[c]));

    return 1;
}


static uint64_t
msec_time(struct timeval *tv)
{
    return ((uint64_t)tv->tv_sec * 1000) + (tv->tv_usec / 1000);
}

static void
msec_delay(uint64_t msecs)
{
    struct timeval tv = { msecs / 1000, (msecs % 1000) * 1000 };
    select(0, 0, 0, 0, &tv);
}

#if 1
typedef struct
{
    const uint8_t *data;
    uint32_t       size;
    uint32_t       sequence;
    int32_t        appidx;
    uint64_t       connid;
} navl_wrapper_packet_t;
#else
typedef struct
{
	const uint8_t    *data;
	uint32_t          size;
	uint32_t          sequence;
	int32_t           appidx;
	int32_t           num_cb;
} navl_wrapper_packet_t;
#endif


#if 1
static int
navl_classify_callback(navl_handle_t handle,
                            navl_result_t result,
                            navl_state_t state,
                            navl_conn_t conn,
                            void *arg,
                            int error)
{
    int idx, protoid = 0, confidence = 0;
    char buf[256] = {0};
    navl_iterator_t it;
    navl_wrapper_packet_t *packet = (navl_wrapper_packet_t *)arg;
    int threadId =Osal_nwalGetProcId();

    packet->appidx = navl_app_get(g_reader->navl, result, &confidence);
    if((state==NAVL_STATE_CLASSIFIED) ||
       (state==NAVL_STATE_TERMINATED) ||
       (state==NAVL_STATE_MONITORING))
    {
        pDpiStats->n_class+=1;
        class =1;
    }
    else
        class=0;
#if 0
    if (navl_proto_find_index(g_reader->navl, "HTTP") == packet->appidx)
    {
        it = navl_proto_find(g_reader->navl, result, navl_proto_find_index(g_reader->navl, "HTTP"));
        if (navl_proto_valid(g_reader->navl, it))
            navl_attr_get(g_reader->navl,
            it, 
            g_reader->config_http_attr, 
            &last_url,
            sizeof(last_url));
    }
#endif
    if (g_reader->option_verbose)
    {
        /* Build the stack string */
        for (idx = 0, it = navl_proto_first(g_reader->navl, result); navl_proto_valid(g_reader->navl, it); navl_proto_next(g_reader->navl, it))
        {
            protoid = navl_proto_get_index(g_reader->navl, it);
            if (!packet->connid)
            {
                if (navl_proto_find_index(g_reader->navl, "IP") == protoid)
                {
#if 0
                    navl_attr_get(g_reader->navl, it, g_reader->config_conn_id_attr, &packet->connid, sizeof(packet->connid));
                    if (packet->connid > g_reader->stats_conns)
                    g_reader->stats_conns = packet->connid;
#endif
                }
            }
            idx += sprintf(&buf[idx], "/%s", g_reader->stats_pkt[threadId-1][protoid].name);
        }
        printf(" Pkt: %u (%u bytes), Conn: %" PRIu64 ", App: %s (%s), State: %s, Stack: %s, Error: %s\n", packet->sequence
        , packet->size, packet->connid, g_reader->stats_pkt[threadId-1][packet->appidx].name
        , get_confidence_string(confidence), get_state_string(state), buf, get_error_string(error));
    }

    /* Continue tracking the flow */
    return 0;
}
#else
static int
navl_classify_callback(navl_handle_t handle,
                            navl_result_t result,
                            navl_state_t state,
                            navl_conn_t conn,
                            void *arg,
                            int error)
{
	int idx, protoid = 0, confidence = 0;
	char buf[256] = {0};
	navl_iterator_t it;
	navl_conn_id_t conn_id = navl_conn_id_get(handle, conn);
	navl_wrapper_packet_t *packet = (navl_wrapper_packet_t *)arg;

	/* Always display the outer packet; optionally display encapsulated data */
	if (!packet->num_cb || g_reader->option_tunnel)
	{
		packet->appidx = navl_app_get(g_reader->navl, result, &confidence);

		if (g_reader->option_verbose)
		{
			if (conn_id > g_reader->stats_conns)
				g_reader->stats_conns = conn_id;

			/* Build the stack string */
			for (idx = 0, it = navl_proto_first(g_reader->navl, result); navl_proto_valid(g_reader->navl, it); navl_proto_next(g_reader->navl, it))
			{
				protoid = navl_proto_get_index(g_reader->navl, it);
				idx += sprintf(&buf[idx], "/%s", g_reader->stats_pkt[fp_thread][protoid].name);
			}

			printf(" Pkt: %u (%u bytes), Conn: %" PRIu64 ", App: %s (%s), State: %s, Stack: %s, Error: %s\n"
				, packet->sequence, packet->size, conn_id, g_reader->stats_pkt[fp_thread][packet->appidx].name
				, get_confidence_string(confidence), get_state_string(state), buf, get_error_string(error));
		}
	}

	packet->num_cb++;

	/* Continue tracking the flow */
	return 0;
}
#endif

//process the packet
__thread navl_wrapper_packet_t packet = { NULL, 0, 0 };
int navl_process_pkt(unsigned char *p_pkt, int len)
{
    volatile unsigned long v1;
    volatile unsigned long v2;
    unsigned long temp=0;
    uint64_t last = 0;
    uint64_t next = 0;
    unsigned long long mf1;
    unsigned long long mf2;
    int threadId =Osal_nwalGetProcId();
    mf1= pDpiStats->malloc_cycles+pDpiStats->free_cycles;
    
    v1 = netapi_timing_start();
    
/* update the current packet */
    packet.sequence++;
    packet.size = len;
    packet.appidx = 0;
    //packet.connid = 0;
    packet.data=p_pkt;

    /* "real" realtime */
    if (g_reader->option_realtime_mode == 1)
    {
        //next = msec_time(0LL);//dal -> get ts here
        next=0LL;
        if (last)
        msec_delay(next - last);
        last = next;
    }
    if (navl_classify(g_reader->navl,
                      NAVL_ENCAP_ETH,
                      packet.data,
                      packet.size,
                      NULL,
                      0,
                      navl_classify_callback,
                      &packet) == -1)
    printf(" Pkt: %u (%u bytes), Error: %s\n", packet.sequence,
                                               packet.size,
                get_error_string(navl_error_get(g_reader->navl)));

    /* Update the stats. If classification was not enabled, then the appidx will be 0 and all packets
     * captured will be accumulated there */
    g_reader->stats_pkt[threadId-1][packet.appidx].packets++;
    g_reader->stats_pkt[threadId-1][packet.appidx].bytes += packet.size;
    if(class)
        g_reader->stats_pkt[threadId-1][packet.appidx].class++;
    //update timing
    v2 = netapi_timing_start();
    temp=v2-v1;
    mf2= pDpiStats->malloc_cycles + pDpiStats->free_cycles;
    timing+= (unsigned long long) temp;
    g_reader->stats_pkt[threadId-1][packet.appidx].cycles += (unsigned long long) temp;
    g_reader->stats_pkt[threadId-1][packet.appidx].cycles_nomem += ((unsigned long long) temp - (mf2-mf1));

    if (g_reader->stats_pkt[threadId-1][packet.appidx].cycles_min > temp)
        g_reader->stats_pkt[threadId-1][packet.appidx].cycles_min = temp;

    if (g_reader->stats_pkt[threadId-1][packet.appidx].cycles_max  < temp)
    {
        g_reader->stats_pkt[threadId-1][packet.appidx].cycles_max = temp;

    }
    

    add2bin((temp - (unsigned long)(mf2-mf1)),&g_reader->stats_pkt[threadId-1][packet.appidx].bin_cycles[0]); 
    pDpiStats->n_ops+=1;
    if (temp > pDpiStats->max_time) pDpiStats->max_time = temp;
    if (temp< pDpiStats->min_time) pDpiStats->min_time = temp;
    return 1;
}

int navl_done(void)
{
    navl_fini(g_reader->navl);
    navl_close(g_reader->navl);
    return 1;
}

void navl_results(int fp_thread)
{
    int idx;
    int i;
    uint64_t total_packets, total_bytes;
    void* pShmEntry;

    total_packets = 0;
    total_bytes = 0;

    if (g_reader->option_dpi)
    {
        printf("\n NAVL DPI stats for CORE ID %d\n", fp_thread + 1);
        printf("\n AppProto    Packets  Class   Bytes        Cycles/Pkt      Cyclesnomem/Pkt  (min)  (max) ");
        for(i=0;i<MAX_BIN;i++)
            printf("<%s ",binStr[i]);
        printf("\n ----------------------------------------------------------------------------------------------------------------------------------------------------\n");
    }

    //for (idx = 0; idx < g_reader->config_num_proto; idx++)
    for (idx = 0; idx < pNavlCfg->num_protocols; idx++)
    {
        if (g_reader->option_dpi)
        {
            if (g_reader->stats_pkt[fp_thread][idx].packets)
            {
                /* We need to provide protocol definitions */	
                printf(" %-12s%-12" PRIu64 " %" PRIu64 " %" PRIu64 "      %" PRIu64 "       %" PRIu64 "    %ld    %ld    " , 
                        g_reader->stats_pkt[fp_thread][idx].name,
                        g_reader->stats_pkt[fp_thread][idx].packets, 
                        g_reader->stats_pkt[fp_thread][idx].class, 
                        g_reader->stats_pkt[fp_thread][idx].bytes,
                        g_reader->stats_pkt[fp_thread][idx].cycles/g_reader->stats_pkt[fp_thread][idx].packets, 
                        g_reader->stats_pkt[fp_thread][idx].cycles_nomem/g_reader->stats_pkt[fp_thread][idx].packets,
                        g_reader->stats_pkt[fp_thread][idx].cycles_min,
                        g_reader->stats_pkt[fp_thread][idx].cycles_max);
                for(i=0;i<MAX_BIN;i++) printf("%ld ",g_reader->stats_pkt[fp_thread][idx].bin_cycles[i]);
                printf("\n");
            }
        }

        total_packets += g_reader->stats_pkt[fp_thread][idx].packets;
        total_bytes += g_reader->stats_pkt[fp_thread][idx].bytes;
    }

    if (!total_packets)
        printf("\n No packets captured.\n");
    else
        printf("\n %" PRIu64 " packets captured (%" PRIu64 " bytes)\n", total_packets, total_bytes);

    if (g_reader->stats_conns)
        printf(" %" PRIu64 " connections tracked\n", g_reader->stats_conns);

    printf("\n");
    if (g_reader->option_track_memory)
        navl_wrapper_mem_stat_print(fp_thread);


    if (pNavlCfg->alloc_curr != 0)
        printf("Bytes not freed: %" PRIi64 "\n", pNavlCfg->alloc_curr);

    if (g_reader->alloc_peak != 0)
           printf("Peak allocated: %" PRIi64 "\n", g_reader->alloc_peak);

    printf("attr test: last http utl= %s\n",last_url);
}

static void 
navl_wrapper_mem_stat_print(int threadId)
{
    int i, j;
    navl_wrapper_stat_t *stat;
    char *name;

    printf("                                          Curr      Peak      Fail   \n");
    printf("-----------------------------------------------------------------------");

    for (i = 0; i < NUM_MEM_CTX; i++)
    {
        stat = &g_reader->stats_mem->mstats[threadId][i][0];
        if (stat->peak == 0)
        continue;

        printf("\n\t%s\n", g_reader->stats_mem->ctx_name[threadId][i]);
        //printf("\n\t%s\n", g_reader->ctx_name[i]);

        for (j = NUM_MEM_OBJ - 1; j >= 0; j--)
        {
            navl_wrapper_stat_t *stat = &g_reader->stats_mem->mstats[threadId][i][j];
            if (stat->peak == 0)
            continue;
            name = j ? g_reader->stats_mem->obj_name[threadId][j] : (char *)"other";

            //name = j ? g_reader->obj_name[j] : (char *)"other";

            if (stat->peak || stat->fail)
            printf("\t\t%-20s%10" PRIu64 "%10" PRIu64 "%10" PRIu64 "\n", name, stat->curr, stat->peak, stat->fail);
        }
    }
    printf("\n\n");
}


void navl_results2(int fp_thread)
{
    int idx;
    int i,j;
    uint64_t total_packets, total_bytes;
    void* pShmEntry;
    void* pShmBase;
    void* pTemp;
    navl_wrapper_cfg_info_t *pNavlCfg;
    navl_wrapper_pkt_stat_t *pStats1;
    navl_wrapper_pkt_stat_t *pStats2;
    navl_mcb_t g_reader;
    navl_wrapper_stat_t *stat;
    char *name;

    pShmBase = hplib_shmOpen();
    if (pShmBase)
    {
        pTemp = hplib_shmGetEntry(pShmBase, APP_ENTRY_1);
        pNavlCfg =  (navl_wrapper_cfg_info_t*)pTemp;
    
        g_reader.stats_pkt[0] = pTemp + sizeof(navl_wrapper_cfg_info_t);
    
    
        g_reader.stats_pkt[1] = pTemp + sizeof(navl_wrapper_cfg_info_t) +
                (sizeof(navl_wrapper_pkt_stat_t)*pNavlCfg->num_protocols);

        pTemp = hplib_shmGetEntry(pShmBase, APP_ENTRY_3);
        g_reader.stats_mem = (navl_wrapper_mem_stats_t*) pTemp;
    }
    total_packets = 0;
    total_bytes = 0;

    //if (g_reader->option_dpi)
    {
        printf("\n NAVL DPI stats for CORE ID %d\n", fp_thread);
        printf("\n AppProto    Packets  Class   Bytes        Cycles/Pkt      Cyclesnomem/Pkt  (min)  (max) ");
        for(i=0;i<MAX_BIN;i++)
            printf("<%s ",binStr[i]);
        printf("\n ----------------------------------------------------------------------------------------------------------------------------------------------------\n");
    }

    //for (idx = 0; idx < g_reader->config_num_proto; idx++)
    for (idx = 0; idx < pNavlCfg->num_protocols; idx++)
    {
        //if (g_reader->option_dpi)
        {
            if (g_reader.stats_pkt[fp_thread][idx].packets)
            {
                /* We need to provide protocol definitions */	
                printf(" %-12s%-12" PRIu64 " %" PRIu64 " %" PRIu64 "      %" PRIu64 "       %" PRIu64 "    %ld    %ld    " , 
                        g_reader.stats_pkt[fp_thread][idx].name,
                        g_reader.stats_pkt[fp_thread][idx].packets, 
                        g_reader.stats_pkt[fp_thread][idx].class, 
                        g_reader.stats_pkt[fp_thread][idx].bytes,
                        g_reader.stats_pkt[fp_thread][idx].cycles/g_reader.stats_pkt[fp_thread][idx].packets, 
                        g_reader.stats_pkt[fp_thread][idx].cycles_nomem/g_reader.stats_pkt[fp_thread][idx].packets,
                        g_reader.stats_pkt[fp_thread][idx].cycles_min,
                        g_reader.stats_pkt[fp_thread][idx].cycles_max);
                for(i=0;i<MAX_BIN;i++) printf("%ld ",g_reader.stats_pkt[fp_thread][idx].bin_cycles[i]);
                printf("\n");
            }
        }

        total_packets += g_reader.stats_pkt[fp_thread][idx].packets;
        total_bytes += g_reader.stats_pkt[fp_thread][idx].bytes;
    }

    if (!total_packets)
        printf("\n No packets captured.\n");
    else
        printf("\n %" PRIu64 " packets captured (%" PRIu64 " bytes)\n", total_packets, total_bytes);


    printf("\n");
    //if (g_reader.option_track_memory)
    printf("                                          Curr      Peak      Fail   \n");
    printf("-----------------------------------------------------------------------");

    for (i = 0; i < NUM_MEM_CTX; i++)
    {
        stat = &g_reader.stats_mem->mstats[fp_thread][i][0];
        if (stat->peak == 0)
        continue;

        printf("\n\t%s\n", g_reader.stats_mem->ctx_name[fp_thread][i]);
        //printf("\n\t%s\n", g_reader->ctx_name[i]);

        for (j = NUM_MEM_OBJ - 1; j >= 0; j--)
        {
            navl_wrapper_stat_t *stat = &g_reader.stats_mem->mstats[fp_thread][i][j];
            if (stat->peak == 0)
            continue;
            name = j ? g_reader.stats_mem->obj_name[fp_thread][j] : (char *)"other";

            //name = j ? g_reader->obj_name[j] : (char *)"other";

            if (stat->peak || stat->fail)
            printf("\t\t%-20s%10" PRIu64 "%10" PRIu64 "%10" PRIu64 "\n", name, stat->curr, stat->peak, stat->fail);
        }
    }
    printf("\n\n");

#if 0
    if (g_reader.stats_conns)
        printf(" %" PRIu64 " connections tracked\n", g_reader.stats_conns);

    if (pNavlCfg->alloc_curr != 0)
        printf("Bytes not freed: %" PRIi64 "\n", pNavlCfg->alloc_curr);

    if (g_reader.alloc_peak != 0)
           printf("Peak allocated: %" PRIi64 "\n", g_reader.alloc_peak);

    printf("attr test: last http utl= %s\n",last_url);
#endif
}



/* Private memory stamp type for tracking memory with navl_wrapper_malloc/free */
typedef struct
{
    size_t      size;       /* size of allocation */
    short       ctx_tag;    /* ctx axis */
    short       obj_tag;    /* obj axis */
    char        mem[0];
} memstamp_t;

static void *navl_wrapper_malloc(size_t size)
{
    navl_handle_t handle;
    navl_wrapper_stat_t *stat_mem;
    int tag;
    int ctx_tag, obj_tag;
    unsigned long t1;
    unsigned long t2;
   int threadId =Osal_nwalGetProcId();
    pDpiStats->malloc_inst+=1;
    pDpiStats->malloc_bytes+=size;
    t1=netapi_timing_start();

    assert(size);

    /* In this context, the handle should be read using navl_handle_get() since
     * the application cached handle (in this case g_reader->navl) will not be set
     * until navl_open() returns. For this simple test we just assert that the handle
     * is infact valid. */
    handle = navl_handle_get();
    assert(handle != 0);

    /* Fetch the tags associated with this allocation. They will be used below to record
     * statistics about allocations within the library on 2 axis. The upper 16 bits contain
     * a memory obj tag and the lower 16 bits contain the allocation context tag. These
     * tags are indices, the upper of which is available through navl_memory_ctx_num()
     * and navl_memory_obj_num() resp. They are generated dynamically and may differ  between
     * configurations. These total number of indices are available ONLY AFTER navl_open() 
     * returns.
     */
    tag = navl_memory_tag_get(handle);
    obj_tag = (tag >> 16);
    ctx_tag = (tag & 0x0000FFFF);

    /* You could do something better here and reallocate the matrix */
    if (ctx_tag >= g_reader->config_num_memctx || obj_tag >= g_reader->config_num_memobj)
    {
        assert(0);
        return NULL;
    }

    stat_mem = &g_reader->stats_mem->mstats[threadId][ctx_tag][obj_tag];

    /* check limits */
    if (!g_reader->option_limit_memory || (pNavlCfg->alloc_curr + size < g_reader->option_limit_memory))
    {
        memstamp_t *ptr = (memstamp_t *)malloc(size + sizeof(memstamp_t));
        if (ptr)
        {
            /* track peak values */
            if ((pNavlCfg->alloc_curr += size) > g_reader->alloc_peak)
            g_reader->alloc_peak = pNavlCfg->alloc_curr;

            if ((stat_mem->curr += size) > stat_mem->peak)
            stat_mem->peak = stat_mem->curr;

            ptr->size = size;
            ptr->ctx_tag = ctx_tag;
            ptr->obj_tag = obj_tag;
            t2=netapi_timing_start();
            pDpiStats->malloc_cycles += (unsigned long long)  (t2-t1);
            return ptr->mem;
        }
    }
    stat_mem->fail += size;
    return NULL;
}

static void 
navl_wrapper_free(void *p)
{
    unsigned long t1;
    unsigned long t2;
    int threadId =Osal_nwalGetProcId();
    pDpiStats->free_inst += 1;
    t1=netapi_timing_start();
    if (!p)
        return;

    memstamp_t *ptr = (memstamp_t *)((char *)p - offsetof(memstamp_t, mem));
    navl_wrapper_stat_t *stat_mem = &g_reader->stats_mem->mstats[threadId][ptr->ctx_tag][ptr->obj_tag];

    assert(p == ptr->mem);

    stat_mem->curr -= ptr->size;
    pNavlCfg->alloc_curr -= ptr->size;

    free(ptr);
    t2=netapi_timing_start();
    pDpiStats->free_cycles += (unsigned long long)  (t2-t1);
}

static int
navl_wrapper_log_message(const char *level, const char *func, const char *format, ... )
{
    int res = 0;
    char buf[4096];
    va_list va;
    va_start(va, format);

    res = snprintf(buf, 4096, "%s: %s: ", level, func);
    res += vsnprintf(buf + res, 4096 - res, format, va);
    printf("%s\n", buf);
    va_end(va);
    return res;
}

//clear stats
void clear_pkt_stats()
{
    int ret, i;
    

    for (i=0;i < NUM_FP_PROCS;i++)
    {
        memset(g_reader->stats_pkt[i], 0, (sizeof(navl_wrapper_pkt_stat_t)*MAX_PROTOCOLS));
        /* now fetch all the protocol name ahead of time do we don't have to lookup them up on each packet */
        for (ret = 0; ret != MAX_PROTOCOLS; ret++)
        {
            g_reader->stats_pkt[i][ret].cycles_min=10000000;
            navl_proto_get_name(g_reader->navl, ret,
                            g_reader->stats_pkt[i][ret].name,
                            sizeof(g_reader->stats_pkt[i][ret].name));
        }
    }
}
