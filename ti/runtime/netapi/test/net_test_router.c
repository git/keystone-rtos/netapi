/******************************************************************************
 * File: net_test_router.c
 * Purpose: net_test_router application
 ******************************************************************************
 * FILE:  net_test_router.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  net_test_router application
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************/

#include "netapi.h"
#include "net_test_sa_utils.h"
#include "net_test_utils.h"
#include "net_test_thread_utils.h"

#include <signal.h>
#include <pthread.h>
#include <sys/resource.h>
#include "router.h"
extern int QUIT;
extern Rm_ServiceHandle   *rmClientServiceHandle;


#if defined(DEVICE_K2H)
#include <ti/drv/qmss/device/k2h/src/qmss_device.c>
#include <ti/drv/cppi/device/k2h/src/cppi_device.c>
#elif defined (DEVICE_K2K)
#include <ti/drv/qmss/device/k2k/src/qmss_device.c>
#include <ti/drv/cppi/device/k2k/src/cppi_device.c>
#elif defined (DEVICE_K2L)
#include <ti/drv/qmss/device/k2l/src/qmss_device.c>
#include <ti/drv/cppi/device/k2l/src/cppi_device.c>
#elif defined (DEVICE_K2E)
#include <ti/drv/qmss/device/k2e/src/qmss_device.c>
#include <ti/drv/cppi/device/k2e/src/cppi_device.c>
#else /*Default */
#include <ti/drv/qmss/device/k2h/src/qmss_device.c>
#include <ti/drv/cppi/device/k2h/src/cppi_device.c>
#endif
/* Global definitions */

extern Rm_ServiceHandle   *rmClientServiceHandle;

hplib_spinLock_T net_test_thread_lock;
netTestConfig_t netTestCfg;
static  netTestConfigFile_t config_file;

char    input_file_name[] = "/etc/transportnetlib/test/net_test_config.txt";

nwal_RetValue       nwalRetVal;
Pktlib_HeapHandle ourHeap;

PKTIO_HANDLE_T *netcp_tx_chan_no_crypto;
PKTIO_HANDLE_T *netcp_rx_chan;
PKTIO_HANDLE_T *netcp_tx_chan_esp;
PKTIO_HANDLE_T *netcp_tx_chan_ah;




PKTIO_CFG_T netcp_rx_cfg={PKTIO_RX, PKTIO_NA, PKTIO_NA, 8};
PKTIO_CFG_T netcp_tx_cfg={PKTIO_TX, PKTIO_NA, PKTIO_NA, 8};



NETCP_CFG_EXCEPTION_PKT_T expPkt_appid;

Trie *p_trie_sa_rx;
Trie *p_trie_sa_tx;


#include "router.c"
Trie * our_router;

OUR_ROUTE_T routes[MAX_ROUTES]=

{
    {0,{0xD4,0xbe,0xd9,0x00,0xd3,0x7e, 0x00,0x01,0x02,0x03,0x14,0x02,0x08,0x00},0},
    {0,{0x00,0x00,0x0,0x00,0x0,0x0, 0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00},0},
    {0,{0xD4,0xbe,0xd9,0x00,0xd3,0x7e, 0x00,0x01,0x02,0x03,0x14,0x02,0x08,0x00},0},
    {0,{0x00,0x15,0x60,0xa1,0xf7,0xbe, 0x00,0x01,0x02,0x03,0x04,0x05,0x08,0x00},0},
    {0,{0xd4,0xbe,0xd9,0x00,0xd3,0x7e, 0x00,0x01,0x02,0x03,0x04,0x05,0x08,0x00},0}
};
unsigned int ip[MAX_ROUTES]={BE(0x0a0100c8),BE(0x0a00000a),BE(0x0a02000a),BE(0xc0a8010a),BE(0x9eda6719)};

void recv_cb_router(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts );


extern netTestStats_T stats[];
extern paSysStats_t netcp_stats;

/*******************************************
 *************NETAPI OBJECTS***************
 *****************************************/
static NETAPI_CFG_T our_netapi_default_cfg=
{
    TUNE_NETAPI_PERM_MEM_SZ,
    128,  //start of packet offset for hw to place data on rx for default flow
    TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM, //max number of descriptors in system
    TUNE_NETAPI_NUM_GLOBAL_DESC,        //total we will use
    TUNE_NETAPI_DEFAULT_NUM_BUFFERS,   //#descriptors+buffers in default heap
    64, //#descriptors w/o buffers in default heap
    TUNE_NETAPI_DEFAULT_BUFFER_SIZE+128+128,  //size of buffers in default heap
    128,  //tail room
    256,  //extra room
    0,
    NULL,
    -1,
    -1
};



void house(NETAPI_SCHED_HANDLE_T *s);
NETAPI_T netapi_handle;
NETAPI_SCHED_HANDLE_T * our_sched;
#ifdef netTest_MULTI_THREAD
NETAPI_SCHED_HANDLE_T * scheduler[TUNE_NETAPI_NUM_CORES];
#endif
NETAPI_SCHED_CONFIG_T our_sched_cfg={
  NETAPI_SCHED_DURATION|NETAPI_SCHED_CBV, 0, house, 5000000  //every 5000000 poll loops
};


NETCP_CFG_IP_T ip_rule[NET_TEST_MAX_IP];
NETCP_CFG_MACIF_T mac[NET_TEST_MAX_MAC];



/* security objects. (for loopback mode) */
netTestSA_t sa_info[MAX_SEC_INDEX];


NETCP_CFG_IPSEC_POLICY_T rx_policy[MAX_SEC_INDEX];




/*************************END NETAPI OBJECTS***********************/

void update_header(netTestHead_T * p_head, int len)
{
   unsigned char *p = (unsigned char *) &p_head->udp[1];
   len -= (20+14);
   /* update ip checksum */
   /* update udp checksum */
   /* update length */
   *p= (len&0xff00)>>8;
   *(p+1) = len&0xff;
}

#ifdef netTest_MULTI_THREAD
/* Templates to build command labels at startup up time, required by open_pktio_tx_channels() */
nwalTxPktInfo_t txPktInfoESP = 
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO| NWAL_TX_FLAG1_DO_IPV4_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    1,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    0               ,                                                                                    /* saAhIcvOffBytes */
    0,                                                                                                 /* saAhMacSize */
    0,                                                                                                  /* etherLenOffBytes */
    netTest_MAC_HEADER_LEN,                                                        /* ipOffBytes */
    0,                          /* l4OffBytes */
    0,                         /* l4HdrLen */
    0,                                                                                   /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};


nwalTxPktInfo_t txPktInfoAH = 
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_DO_IPSEC_AH_CRYPTO| NWAL_TX_FLAG1_DO_IPV4_CHKSUM | NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    0,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    netTest_MAC_HEADER_LEN + netTest_IP_HEADER_LEN + netTest_IPSEC_AH_FIXED_HDR_SIZE,                    /* saAhIcvOffBytes */
    12,                                                                                                 /* saAhMacSize */
    0,                                                                                                  /* etherLenOffBytes */
    netTest_MAC_HEADER_LEN,   /* ipOffBytes */
    0,                    /* l4OffBytes */
    netTest_UDP_HEADER_LEN,                                                                            /* l4HdrLen */
    0,                                                                                                  /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};

nwalTxPktInfo_t txPktInfoNoCrypto = 
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_DO_IPV4_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    0,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    0,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    0               ,                                                                                    /* saAhIcvOffBytes */
    0,                                                                                                 /* saAhMacSize */
    0,                                                                                                  /* etherLenOffBytes */
    netTest_MAC_HEADER_LEN,         /* ipOffBytes */
    netTest_MAC_HEADER_LEN +netTest_IP_HEADER_LEN,                                        /* l4OffBytes */
    netTest_UDP_HEADER_LEN,                                                             /* l4HdrLen */
    0,                                                                         /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};

void close_pktio_channels(void)
{
    int err;
    netapi_pktioClose(netcp_tx_chan_no_crypto ,&err);
    netapi_pktioClose(netcp_tx_chan_esp ,&err);
    netapi_pktioClose(netcp_tx_chan_ah ,&err);
}



void open_pktio_tx_channels()
{
    int err;
    /* open netcp default TX channels for non-ipsec*/
    netcp_tx_chan_no_crypto= netapi_pktioOpen(netapi_handle, NETCP_TX, NULL, &netcp_tx_cfg,  &err);
    if (!netcp_tx_chan_no_crypto)
    {
        netapi_Log("pktio open TX failed err=%d\n",err);
        exit(1);
    }
    else
    {
            PKTIO_CONTROL_T control;
            control.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg;
            cfg.fast_path_cfg.fp_send_option = PKTIO_FP_NO_CRYPTO_NO_CKSUM_PORT;
            cfg.fast_path_cfg.txPktInfo= &txPktInfoNoCrypto;
            netapi_pktioControl(netcp_tx_chan_no_crypto, NULL, &cfg, &control, &err);
    }
    /* open netcp default TX for ESP packets */
    netcp_tx_chan_esp= netapi_pktioOpen(netapi_handle, NETCP_TX, NULL, &netcp_tx_cfg,  &err);
    if (!netcp_tx_chan_esp)
    {
        netapi_Log("pktio open TX failed err=%d\n",err);
        exit(1);
    }
    else
    {
        if(netTestCfg.ipsec_mode_tx == IPSEC_MODE_TX_INFLOW)
        {
            PKTIO_CONTROL_T control;
            control.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg;
            cfg.fast_path_cfg.fp_send_option = PKTIO_FP_ESP_L3CKSUM_PORT;
            cfg.fast_path_cfg.txPktInfo= &txPktInfoESP;
            netapi_pktioControl(netcp_tx_chan_esp, NULL, &cfg, &control, &err);
        }
    }

    /*/* open netcp default  TX for AH packets */
    netcp_tx_chan_ah= netapi_pktioOpen(netapi_handle, NETCP_TX, NULL, &netcp_tx_cfg,  &err);
    if (!netcp_tx_chan_ah)
    {
        netapi_Log("pktio open TX failed err=%d\n",err);
        exit(1);
    }
    else
    {
        if(netTestCfg.ipsec_mode_tx == IPSEC_MODE_TX_INFLOW)
        {
            PKTIO_CONTROL_T control;
            control.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg;
            cfg.fast_path_cfg.fp_send_option = PKTIO_FP_AH_PORT;
            cfg.fast_path_cfg.txPktInfo= &txPktInfoAH;
            netapi_pktioControl(netcp_tx_chan_ah, NULL, &cfg, &control, &err);
        }
    }
}




NETAPI_T worker_nh[TUNE_NETAPI_NUM_CORES];
void slow_path_thread(uint32_t index)
{
    int err,i;
    uint32_t thread_num;
    PKTIO_HANDLE_T *rx_chan;
    PKTIO_HANDLE_T *sb_tx_chan;
    PKTIO_HANDLE_T *sb_rx_chan;
    cpu_set_t cpu_set;

    thread_num = netTestCfg.sp_thread_num[index];
    printf("slow_path_thread for index %d  called for thread %d\n", index, thread_num);

    CPU_ZERO( &cpu_set);
#ifdef CORTEX_A8
   for (i = netTestCfg.sp_proc_start[index]; i <= netTestCfg.sp_proc_end[index];i++)
    {
        printf("slow_path_thread: setting cpu %d to cpu_set\n", i);
        CPU_SET( i, &cpu_set);
    }
    hplib_utilSetupThread(0, &cpu_set, hplib_spinLock_Type_LOL);
#else
    for (i = netTestCfg.sp_proc_start[index]; i <= netTestCfg.sp_proc_end[index];i++)
    {
        printf("slow_path_thread: setting cpu %d to cpu_set\n", i);
        CPU_SET( i, &cpu_set);
    }
    hplib_utilSetupThread(thread_num, &cpu_set, hplib_spinLock_Type_LOL);
#endif
    worker_nh[thread_num]=netapi_init(NETAPI_CORE_MASTER,
                                      NULL);
    if (worker_nh[thread_num] == NULL)
    {
        printf("slow_path_thread: netapi_init failure, exiting\n");
        exit(1);
    }
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) worker_nh[thread_num];

   /* open netcp RX channel */
    //rx_chan = netapi_pktioOpen(worker_nh[thread_num], NETCP_RX, (PKTIO_CB) recv_cb_router, &netcp_rx_cfg,  &err);



    netapi_setCookie(worker_nh[thread_num],(void*) (thread_num | NET_TEST_SP_THREAD_MASK));
      
    scheduler[thread_num] =netapi_schedOpen(worker_nh[thread_num],&our_sched_cfg, &err);
    if (!scheduler[thread_num]) 
    {
        netapi_Log("sched create failed for core%d\n",thread_num); 
        exit(1);
    }
      scheduler[thread_num]->config.yield = NETAPI_FALSE;
      scheduler[thread_num]->config.pollGarbageQ = NETAPI_TRUE;
      scheduler[thread_num]->config.pollCtrlQ = NETAPI_TRUE;
            /*********************************************/
    /**************Entry point into scheduler ****/
    /*********************************************/
    netapi_schedRun(scheduler[thread_num], &err);
    netapi_Log("slow_path_thread: core %d worker thread done\n",thread_num);

    //netapi_pktioClose(rx_chan, &err);
    netapi_shutdown(worker_nh[thread_num]);
}


void fast_path_thread(uint32_t index)
{
    int err,i;
    uint32_t thread_num;
    PKTIO_HANDLE_T *rx_chan;
    PKTIO_HANDLE_T *sb_tx_chan;

    cpu_set_t cpu_set;

    thread_num = netTestCfg.fp_thread_num[index];
    printf("fast_path_thread for index %d  called for thread %d\n", index, thread_num);
    CPU_ZERO( &cpu_set);
#ifdef CORTEX_A8
    for (i = netTestCfg.fp_proc_start[index]; i <= netTestCfg.fp_proc_end[index];i++)
    {
        printf("fast_path_thread: setting cpu %d to cpu_set\n", i);
        CPU_SET( i, &cpu_set);
    }
    hplib_utilSetupThread(thread_num, &cpu_set, hplib_spinLock_Type_LOL);
#else
    for (i = netTestCfg.fp_proc_start[index]; i <= netTestCfg.fp_proc_end[index];i++)
    {
        printf("fast_path_thread: setting cpu %d to cpu_set\n", i);
        CPU_SET( i, &cpu_set);
    }
    hplib_utilSetupThread(thread_num, &cpu_set, hplib_spinLock_Type_LOL);
#endif
    hplib_mSpinLockLock(&net_test_thread_lock);
    worker_nh[thread_num]=netapi_init(NETAPI_CORE_MASTER,
                                      NULL);
    if (worker_nh[thread_num] == NULL)
    {
        printf("fast_path_thread: netapi_init failure, exiting\n");
        hplib_mSpinLockUnlock(&net_test_thread_lock);
        exit(1);
    }
    hplib_mSpinLockUnlock(&net_test_thread_lock);
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) worker_nh[thread_num];

   /* open netcp RX channel */
    rx_chan = netapi_pktioOpen(worker_nh[thread_num], NETCP_RX, (PKTIO_CB) recv_cb_router, &netcp_rx_cfg,  &err);
    netapi_setCookie(worker_nh[thread_num],(void*)thread_num);

    scheduler[thread_num] =netapi_schedOpen(worker_nh[thread_num],&our_sched_cfg, &err);
    if (!scheduler[thread_num]) 
        {
        netapi_Log("sched create failed for core%d\n",thread_num); 
        exit(1);
    }

    /*********************************************/
    /**************Entry point into scheduler ****/
    /*********************************************/
    scheduler[thread_num]->config.yield = NETAPI_FALSE;
     scheduler[thread_num]->config.pollGarbageQ = NETAPI_FALSE;
     scheduler[thread_num]->config.pollCtrlQ = NETAPI_FALSE;
     //sleep(100000);
    netapi_schedRun(scheduler[thread_num], &err);
    netapi_Log("fast_path_thread: core %d worker thread done\n",thread_num);

    netapi_pktioClose(rx_chan, &err);
    netapi_shutdown(worker_nh[thread_num]);
}
#endif

/***************************************
 ********** test driver*****************
 ***************************************/
int main(int argc, char **argv)
{
    int err,i;
    Pktlib_HeapCfg      heapCfg;
    int32_t             errCode;
    Pktlib_HeapIfTable*  pPktifTable;
    FILE * fpr = NULL;

    cpu_set_t cpu_set;
    /* install signal handler for ^c */
    signal(SIGINT,netTest_utilMySig);

    if (argc == 2)
    {
        fpr = fopen(argv[1], "r");
    }
    else
    {
        fpr = fopen(input_file_name, "r");
    }
    if (fpr == NULL)
    {
        exit(1);
    }
    else
    {
        if (initRm())
        {
            printf("main: initRm() returned error\n");
            exit(1);
        }
        hplib_mSpinLockInit(&net_test_thread_lock );
        our_netapi_default_cfg.rmHandle = rmClientServiceHandle;
        memset(&config_file, 0, sizeof(netTestConfigFile_t));
        memset(&netTestCfg, 0, sizeof(netTestConfig_t));
        netTest_utilProcessConfigFile(fpr,&config_file);
        netTest_utilParseMac(&config_file);

        /* parse slow path/fast path thread configuration parameters */
        netTest_utilParseThreadParams(&config_file);

        netTest_utilParseIP(&config_file);

        netTest_utilParseIpsecMode(&config_file);

        /* DSP mac processing */
        parse_dsp_mac(&config_file.dsp_mac[0]);

       /* DSP IP processing */
        parse_dsp_ip(&config_file.dsp_ip[0]);

        netTest_utilParseRoutes(&config_file, &routes[0], &our_router);
        
        /* IPSEC interface number processing */
        parse_simple_param_u32((char*)&config_file.ipsec_if_no[0], &netTestCfg.ipsec_if_no);

        netTest_utilParseSA(&config_file);
    }

    memset(&sa_info, 0, sizeof(sa_info));

#ifdef netTest_MULTI_THREAD
    /* assign main net_test thread to run on core 0 */
    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);
    hplib_utilSetupThread(0, &cpu_set, hplib_spinLock_Type_LOL);
#endif
    /* create netapi */
    our_netapi_default_cfg.rmHandle = rmClientServiceHandle;
    netapi_handle = netapi_init(NETAPI_SYS_MASTER,
                                &our_netapi_default_cfg);

    if (netapi_handle == NULL)
    {
        printf("main: netapi_init failure, exiting\n");
        exit(1);
    }
    /* configure expection packet handling with netapi */
    expPkt_appid = netapi_netcpCfgExceptions(netapi_handle,
                                             7,
                                             NETCP_CFG_ACTION_TO_SW,
                                             (NETCP_CFG_ROUTE_HANDLE_T) NULL);

    /* open the main heap */
    ourHeap = Pktlib_findHeapByName("netapi");
    if (!ourHeap)
    {
        netapi_Log("Pktlib_findHeapByName()  fail\n");
        exit(1);
    }

    /* Open all required PKTIO TX channels */
    open_pktio_tx_channels();

    netapi_Log("net_test> %d bytes left in our CMA area\n", netapi_getBufMemRemainder());
    /* create scheduler instance */
    our_sched =netapi_schedOpen(netapi_handle,&our_sched_cfg, &err);
    if (!our_sched) {netapi_Log("sched create failed\n"); exit(1);}


    /*create net_test MAC interfaces, attach IP to created MAC interfaces */
    netTest_utilCreateInterfaces(netTestCfg.num_macs, netTestCfg.num_ips,1);

    /* Lookup Database for SA context, this is used by packet processing routines to get RX and TX SA information*/
    p_trie_sa_rx = trie_new();
    p_trie_sa_tx = trie_new();
   if (!p_trie_sa_rx || !p_trie_sa_tx)
        {netapi_Log("trie alloc for SA  failed\n"); exit(1);}

    /* Create RX SA's, RX Policy and TX SA's, all SA configuration parameters are read from net_test_config.txt file */
    netTest_utilCreateSecAssoc();


#ifdef netTest_MULTI_THREAD
{
    int c;
    /* create and set affinity of slow path and fast path threads to
    * specific CPU cores as specified in the net_test_config.txt file */
     netTest_utilCreateSpFpThreads(netTestCfg.num_sp_threads, 
                                  (NET_TEST_FUNC_PTR) slow_path_thread,
                                  netTestCfg.num_fp_threads,
                                  (NET_TEST_FUNC_PTR) fast_path_thread);

        //this thread of execution (main) now just waits on user input
        for(;;)
        {
           printf(">");
           c=getchar();
           if (c=='q') {QUIT=1;break;}
           else if (c=='s') netTest_utilsStatsCb(netapi_handle, &netcp_stats);
           else if (c=='h') printf("'q' to quit,  's' for stats, 'h' for help\n");
        }
        netTest_utilRemoveSpFpThreads(netTestCfg.num_sp_threads, netTestCfg.num_fp_threads);
}
#else
    /*********************************************/
    /**************Entry point into scheduler ****/
    /*********************************************/
    netapi_schedRun(our_sched, &err);
#endif

/* done */
netTest_utilsStatsCb(netapi_handle, NULL);



    /* cleanup*/
    netTest_utilDeleteSecAssoc();
    netTest_utilDeleteInterfaces(netTestCfg.num_macs, netTestCfg.num_ips);

    /* close pktio channels we opened via open_pktio_tx_channels() */
    close_pktio_channels();
    netapi_shutdown(netapi_handle);
}

static inline void send_it(Ti_Pkt *tip, int len, netTestSA_t * p_sec, int out_port)
{
  unsigned long st1;
  unsigned long st2;
  int err=0;
  PKTIO_METADATA_T meta2 = {PKTIO_META_TX,{0},0};
  nwalTxPktInfo_t meta_tx2={0};
  st1=hplib_mUtilGetPmuCCNT();
#ifdef netTest_MULTI_THREAD
    int coreid=Osal_nwalGetProcId();  //who we are(thread local)
    //int coreid = our_core;
#else
    int coreid=0;
#endif
  if (len<60) 
  { 
     unsigned int templen;
     char * p_pkt;
     len=60; 
     Pktlib_getDataBuffer(tip,(uint8_t**)&p_pkt,&templen);//ignore templen
     Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc *) tip, p_pkt,len);
     stats[coreid].tx_min+=1;
  }
  Pktlib_setPacketLen(tip,len);
  meta_tx2.txFlag1 = (NWAL_TX_FLAG1_DO_IPV4_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID );
  meta_tx2.startOffset = 0;
  meta_tx2.ipOffBytes = netTest_MAC_HEADER_LEN;
  meta_tx2.ploadLen = len ;
  meta_tx2.enetPort=out_port;
  if(p_sec)
  {   
       meta_tx2.txFlag1 |= NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO ;
       meta2.sa_handle = (void*)p_sec->tx_tunnel;
       meta_tx2.saOffBytes=netTest_MAC_HEADER_LEN + netTest_IP_HEADER_LEN;
       meta_tx2.saPayloadLen=len-netTest_MAC_HEADER_LEN - netTest_IP_HEADER_LEN;   //don't include tag, mac and outer header
       meta2.u.tx_meta=&meta_tx2;
       netapi_pktioSend(netcp_tx_chan_esp,tip,&meta2,&err);
       stats[coreid].sec_tx+=1;
  }
  else
  {
      meta2.u.tx_meta=&meta_tx2;
      netapi_pktioSend(netcp_tx_chan_no_crypto,tip,&meta2,&err);

  }
  stats[coreid].tx +=1;
  st2=hplib_mUtilGetPmuCCNT();
  stats[coreid].send_cycles += (unsigned long long) (st2-st1);  
} 
void recv_cb_router(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
    int i;
    int len;
    int p;
    Ti_Pkt * tip;
    unsigned int templen;
    char * p_pkt;
    netTestHead_T temp_head;
    unsigned int appid;
    IP_netTestHead_T th;
    netTestSA_t *sec_data=NULL;
    unsigned long t1;
    unsigned long t2;
    unsigned long long ct1;
    unsigned long long ct2;
    unsigned short ip_pl;
    unsigned long long n_c_ops;
    int ifno;
    int out_port;
#ifdef netTest_MULTI_THREAD
        int coreid=Osal_nwalGetProcId();  //who we are(thread local)
        //int coreid = our_core;
#else
    int coreid=0;
#endif
    t1=hplib_mUtilGetPmuCCNT();
    ct1 =Osal_cache_op_measure(&n_c_ops);
    for(i=0;i<n_pkts;i++)
    {
        ifno = ((unsigned int)meta[i].u.rx_meta->appId)&0xff;
        if(coreid<TUNE_NETAPI_NUM_CORES) 
        {
            stats[coreid].rx+=1;
            if (ifno < TUNE_NETAPI_MAX_NUM_MAC)
                stats[coreid].if_rx[ifno]+=1;
        }
        tip = p_recv[i];
        Pktlib_getDataBuffer(tip,(uint8_t**)&p_pkt,&templen);//ignore templen
        len = Pktlib_getPacketLen(tip)-4;//real length, subtract mac trailer
        appid = ((unsigned int)meta[i].u.rx_meta->appId)&0xff000000;
        switch(appid)
        {
           case(NETAPI_NETCP_MATCH_IPSEC):
           case(NETAPI_NETCP_MATCH_IPSEC_POLICY):
           {
             int tailen=12+2;
             memcpy(&temp_head,&p_pkt[14],sizeof(netTestHead_T));
             if (!netTest_utilCheckHeader(&temp_head,&meta[i])) {
                stats[coreid].n_bad+=1;
                Pktlib_freePacket(tip); 
                continue;
             }
             tailen+=p_pkt[len-12-2]; //padding length  (12)should come from sec_ptr
             p_pkt = &p_pkt[8+16+20];   //16= iv len, should come from sec_ptr
             len -= (8+16+20+tailen);  //16= iv len should come from sec ptr
            
             //now check inner headder.
             memcpy(&th,&p_pkt[14],20);
             if (!netTest_utilCheckHeader(&temp_head,&meta[i])) {
                stats[coreid].n_bad+=1;
                Pktlib_freePacket(tip);
                continue;
             }
             Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc *) tip, p_pkt,len);
             Pktlib_setPacketLen(tip,len);
             
             if (route_pkt(our_router, tip, &th, p_pkt, &len,&sec_data,&out_port)<0)
              {
                stats[coreid].n_bad+=1;
                Pktlib_freePacket(tip);
             }
             else
             {
               send_it(tip,len,sec_data,out_port);
             }
             break;
           }
           case(NETAPI_NETCP_MATCH_GENERIC_MAC):
              if((p_pkt[12]!=0x8)||(p_pkt[13]!=0x00)) 
              {
                stats[coreid].n_new+=1;
                Pktlib_freePacket(tip); 
                continue;
              }
              if (!netTest_utilCheckHeader(&temp_head,&meta[i])) 
              {
                stats[coreid].n_bad+=1;
                Pktlib_freePacket(tip);
                continue;
              }
              memcpy(&th,&p_pkt[14],20);
              ip_pl= (((unsigned char *)&th.w1)[2]<<8) | ((unsigned char *)&th.w1)[3];
              if ((ip_pl+14)<60)
              {
                len-= (60-(ip_pl+14));
                stats[coreid].rx_min+=1;
              }
              Pktlib_setPacketLen(tip,len);
              if (route_pkt(our_router, tip, &th, p_pkt, &len,&sec_data,&out_port)<0)
              {
                stats[coreid].n_bad+=1;
                Pktlib_freePacket(tip);
              }
              else
              {
                 send_it(tip,len,sec_data,out_port);
              }
              break;
           case(NETAPI_NETCP_MATCH_GENERIC_IP):
              Pktlib_freePacket(tip); 
              stats[coreid].n_new=1;
              break;
           default:
              stats[coreid].n_new+=1;
              Pktlib_freePacket(tip);
              break;
       }
}
t2=hplib_mUtilGetPmuCCNT();
ct2 =Osal_cache_op_measure(&n_c_ops);
stats[coreid].app_cycles +=  (unsigned long long) (t2-t1);
stats[coreid].tx_cache_cycles += (unsigned long long) (ct2-ct1);
return;
}

/* STUB functions required for compilation */
void recv_cb(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
}

void recv_sb_cb(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
}

