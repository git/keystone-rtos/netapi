#ifndef __NET_TEST_SA_UTILS_H__
#define __NET_TEST_SA_UTILS_H__

//#include "net_test.h"
//#include "net_test_utils.h"
//#include "net_test_thread_utils.h"
#include "ti/runtime/netapi/netapi.h"

#define netTest_MAC_HEADER_LEN          14
#define netTest_IP_HEADER_LEN           20
#define netTest_UDP_HEADER_LEN          8
#define netTest_ESP_HEADER_LEN          8
#define netTest_AES_CBC_IV_LEN          16
#define netTest_AES_CTR_IV_LEN   8
#define netTest_3DES_CBC_IV_LEN         8
#define netTest_AES_GCM_IV_LEN          8
#define netTest_AES_CCM_IV_LEN          8
#define netTest_AES_GMAC_IV_LEN          8
#define netTest_NULL_IV_LEN             0
#define netTest_ICV_LEN                 12
#define netTest_HMAC_SHA256_RFC_4868_ICV_LEN 16
#define netTest_AES_GCM_CCM_ICV_LEN     16
#define netTest_AES_GMAC_ICV_LEN     16

#define netTest_IPSEC_AH_PKT    1
#define netTest_IPSEC_ESP_PKT   2


#define netTest_IPSEC_AH_FIXED_HDR_SIZE 12


#define netTest_NULL_ESP_HEADER_LEN 0

typedef struct {
    nwalDmTxPayloadInfo_t       tx_payload_info;
    uint8_t                     inner_ip_offset;
    NETCP_CFG_SA_T              rx_tunnel;
    void*                       rx_data_mode_handle;
    void*                       rx_inflow_mode_handle;
    NETCP_CFG_SA_T              tx_tunnel;
    void*                       tx_data_mode_handle;
    void*                       tx_inflow_mode_handle;
    uint8_t                     enc_key_length;
    uint8_t                     auth_tag_size;
    nwalTxPktInfo_t             tx_pkt_info;
    nwal_saEALG                 cipherMode;
    nwal_saAALG                 authMode;
    /*stuff for routing use case */
    unsigned int                src;  //BE
    unsigned int                dst;  //BE
    unsigned int                spi;  //BE
    unsigned int                seq;  //BE
    int                         iv_len; //iv len (size of iv in pkt)
    int                         bl; //block len (for padding calc)

   /* Save NETCP CMD details during create time */
    nwalTxDmPSCmdInfo_t         rx_dmPSCmdInfo;
    nwalTxDmPSCmdInfo_t         tx_dmPSCmdInfo;
    nwalTxPSCmdInfo_t           tx_psCmdInfo;
    uint32_t                    swInfo0;
    uint32_t                    swInfo1;

    uint32_t                    dir;
    uint32_t                    tunnel_id;
    nwalSecKeyParams_t*         key_params;
} netTestSA_t;

void  netTest_utilBuildSADB(int i);
int netTest_utilCreateSecAssoc(void);

void netTest_utilDeleteSecAssoc(void);

#endif
