/**********************************************************
 * file: fdb.h
 * purpose: netcp configurations routines
 **************************************************************
 * FILE: fdb.c
 * 
 * DESCRIPTION:  linux bridge forwarding data base processing for
 *               user space offload
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ****************************************************************************/
#include "stdlib.h"
#include "stdio.h"
#include "netapi.h"
//#include "fdb.h"

#define __u8  unsigned char
#define __u32  unsigned int
#define FDBMAX 128
#define PFDBMAX 8
#define OFFMAX  32
#define THRESH_OFF  50   //aging timer threshold: don't bother offloading entries < THRESH_OFF

#define MAX_INTERFACES 2
#define MAX_DSCP_ENTRIES 64
#define MAX_PBIT_ENTRIES 8

#define ntBridge_INIT_CONFIG_FLOW_BASE              "flowBase"
#define ntBridge_INIT_CONFIG_QUEUE_BASE              "queueBase"
#define ntBridge_INIT_CONFIG_VLAN_ID                "vlanId"
#define ntBridge_INIT_CONFIG_INGRESS_DEF_PRI        "ingressDefPri"
#define ntBridge_INIT_CONFIG_PORT                   "port"
#define ntBridge_INIT_CONFIG_DSCP_MAP_DEFAULT       "dscp_map_default"
#define ntBridge_INIT_CONFIG_PBIT_MAP_DEFAULT       "pbit_map_default"

//#define ntBridge_INIT_CONFIG_DSCP_MAP               "dscpMap"
//#define ntBridge_INIT_CONFIG_PBIT_MAP               "pbitMap"
#define ntBridge_INIT_CONFIG_QOS_MODE               "mode"


typedef struct{
    char flowBase[512];
    char queueBase[512];
    char vlanId[512];
    char ingressDefPri[512];
    char port[512];
    char dscpMapQDefault[512];
    char dscpMapFDefault[512];
    char dscpMapQ[64][512];
    char dscpMapF[64][512];
    char pbitMapQDefault[512];
    char pbitMapFDefault[512];
    char pbitMapQ[8][512];
    char pbitMapF[8][512];
    char ctrlBitMap[512];
} ntBridgeCfgFile_T;


typedef struct{
    char mac[32][512];
    char switch_port[32][512];
} ntBridgeAleCfgFile_T;

typedef struct {
unsigned char           mac[32][6];
uint8_t                 switch_port[32];
} ntBridgeAleCfg_T;

//the bridge fdb entry
typedef struct our_fdb_entry
{
      uint8_t mac_addr[6];
      uint8_t switch_port;
      uint8_t is_local;
      uint32_t ageing_timer_value; //jiffies
      uint32_t unused;
} FDB_ENTRY_T;

//our offload db
typedef struct offload_db_t
{
  __u8 mac_addr[6];
  __u8 switch_port;   //
  __u8 ifno;   //
  __u32 spare0;
  __u32 flags;
#define OUR_FSB_INUSE 0x80000000
#define OUR_FSB_PERM  0x40000000
#define OUR_FSB_LEARN  0x20000000
#define OUR_FSB_SKIP  0x00000001
} OFFLOAD_DB_T;



















