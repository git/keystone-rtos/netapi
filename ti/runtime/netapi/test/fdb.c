/**********************************************************
 * file: fdb.c
 * purpose: netcp configurations routines
 **************************************************************
 * FILE: fdb.c
 * 
 * DESCRIPTION:  linux bridge forwarding data base processing for
 *               user space offload
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ****************************************************************************/
#include "stdlib.h"
#include "stdio.h"
#include "netapi.h"
#include "fdb.h"


extern FDB_ENTRY_T         ale_cfg[];
extern NETCP_CFG_ROUTE_T test_route[];
extern uint16_t num_learned_macs;



static OFFLOAD_DB_T our_fdb[2][OFFMAX];        //list offloaded, current and next
static OFFLOAD_DB_T perm_do[PFDBMAX];         //white list
static OFFLOAD_DB_T perm_dont[FDBMAX];     //black list`
static OFFLOAD_DB_T learned[OFFMAX];     //filtered learned list`
static int cur_off =-1;  //which db has been loaded
static int n_cur_off=0;  //# of items offloaded

//linux bridge fdb stuff
//static struct our_fdb_entry linux_fdb[FDBMAX];

//**********************************************************
//Internal prototypes
//**********************************************************

//find a free slot
int ntoff_find_free_slot(OFFLOAD_DB_T * pdb,int sz)
{
int s;
for(s=0;s<sz;s++)
{
   if (pdb[s].flags==0x00000000)
   {
      return s;
   }
}
return -1;
}

//find matching slot
int  ntoff_find_slot(OFFLOAD_DB_T * pdb,__u8 * p_mac, __u8 switch_port,int sz)
{
int s;
for(s=0;s<sz;s++)
{
   if (pdb[s].flags==0x00000000) continue;
   if (pdb[s].switch_port != switch_port) continue;
   if (memcmp(p_mac, &pdb[s].mac_addr[0],6)) continue;
      return s;
}
return -1;
}

//delete
void ntoff_delete(OFFLOAD_DB_T pdb[], int sz)
{
}

//startup
int ntoff_start(void)
{
  return 0;
}

//shutdown
int ntoff_stop(void)
{
   ntoff_delete(&our_fdb[cur_off][0], n_cur_off);
   return 0;
}

//*************************************************
//manipulate the permanent do & don't lists
//************************************************
int ntoff_add_do_perm(unsigned char *p_mac,int switch_port)
{
  int s = ntoff_find_free_slot(perm_do,PFDBMAX);
  if (s<0) return -1; //no room
  memcpy(perm_do[s].mac_addr,p_mac,6);
  perm_do[s].switch_port=switch_port;
  perm_do[s].flags=OUR_FSB_INUSE|OUR_FSB_PERM;
  return 0;
}
int ntoff_del_do_perm(unsigned char *p_mac, int switch_port)
{
  int s = ntoff_find_slot(perm_do,p_mac,switch_port,PFDBMAX);
  if (s<0) return -1; //not found
  perm_do[s].flags=0x00000000; //mark it is as free
  return 0;

}
int ntoff_add_dont_perm(unsigned char *p_mac, int switch_port)
{
  int s = ntoff_find_free_slot(perm_dont,FDBMAX);
  if (s<0) return -1; //no room
  memcpy(perm_dont[s].mac_addr,p_mac,6);
  perm_dont[s].switch_port=switch_port;
  perm_dont[s].flags=OUR_FSB_INUSE|OUR_FSB_PERM;
  return 0;

}
int ntoff_del_dont_perm(unsigned char *p_mac, int switch_port)
{
  int s = ntoff_find_slot(perm_dont,p_mac,switch_port,FDBMAX);
  if (s<0) return -1; //not found
  perm_dont[s].flags=0x00000000; //mark it is as free
  return 0;
}
//**************************
//ADD macs
//**************************
int ntoff_add_macs(NETAPI_T h, NETCP_CFG_ROUTE_HANDLE_T r , int fast)
{
    int i;
    int err;
    NETCP_CFG_ROUTE_HANDLE_T route;
    NETCP_CFG_FLOW_T flow;
    nwalLocCxtInfo_t info;
    NETAPI_HANDLE_T * n = (NETAPI_HANDLE_T *) h;

    memset(&route, 0, sizeof(NETCP_CFG_ROUTE_HANDLE_T));
    memset(&flow, 0, sizeof(NETCP_CFG_FLOW_T));

    memset(&info, 0, sizeof(nwalLocCxtInfo_t));

    nwal_getLocCxtInfo(((NETAPI_GLOBAL_T*) (n->global))->nwal_context.nwalInstHandle, &info);


    n_cur_off=0;
    //perm entries 1st
    for(i=0;i<PFDBMAX;i++)
    {
        if (perm_do[i].flags & OUR_FSB_INUSE)
        {
            netapi_netcpCfgCreateMacInterface(
                          h,
                          &perm_do[i].mac_addr[0],
                          NULL,
                          n_cur_off,
                          0,
                          //perm_do[i].switch_port == 1 ? (NETCP_CFG_ROUTE_HANDLE_T)&test_route[0]:(NETCP_CFG_ROUTE_HANDLE_T)&test_route[1],
                          (NETCP_CFG_ROUTE_HANDLE_T)&test_route[perm_do[i].switch_port -1],
                          (NETCP_CFG_VLAN_T ) NULL,
                          0,
                          1,
                          &err);
          perm_do[i].ifno = n_cur_off; 
          n_cur_off+=1;
        }
    }
    printf("ntoff_add_mac: num_learned_macs: %d\n", num_learned_macs);
    //learned list next
    for(i=0; (i < num_learned_macs) && (i < 32) ;i++)

    //for(i=0;(i<OFFMAX)&&(n_cur_off<OFFMAX);i++)
    {
        if (learned[i].flags & OUR_FSB_INUSE)
        {
            printf("ntoff_add_macs: index: %d\n", i);
            netapi_netcpCfgCreateMacInterface(
                      h,
                      &learned[i].mac_addr[0],
                      NULL,
                      n_cur_off,
                      0,
                      //learned[i].switch_port == 1 ? (NETCP_CFG_ROUTE_HANDLE_T)&test_route[0]:(NETCP_CFG_ROUTE_HANDLE_T)&test_route[1],
                      (NETCP_CFG_ROUTE_HANDLE_T)&test_route[learned[i].switch_port-1],
                      (NETCP_CFG_VLAN_T ) NULL,
                      0,
                      1,
                      &err);
            learned[i].ifno = n_cur_off;
            n_cur_off+=1;
        }
    }
    return n_cur_off;
}
//***************************
//DEL MACS
//***************************
int ntoff_del_macs(NETAPI_T h)
{
int i;
int err;

    //perm  entries
    for(i=0;i<PFDBMAX;i++)
    {
        if (perm_do[i].flags & OUR_FSB_INUSE)
        {
            netapi_netcpCfgDelMac(h, perm_do[i].ifno ,&err);
            perm_do[i].ifno = 0;
             n_cur_off-=1;
        }
    }
    //learned entries 
    for(i=0;(i<OFFMAX)&&(n_cur_off>=0);i++)
    {
        if (learned[i].flags & OUR_FSB_INUSE)
        {
            netapi_netcpCfgDelMac(h, learned[i].ifno ,&err);
            learned[i].ifno = 0;
            n_cur_off-=1;
        }
    }
    return 0;

}



//*************************
//do_offload
//  - read what linux is seeing
//  - build newoffload list:
//    -- perm do list +
//    -- newoffload (sorted by maxage)
//    -- oldoffload
//*************************
#if 0
int ntoff_do_offload(NETAPI_T nh, int fast)
{
   int next_cur = !cur_off;
   int i;
   int new_count=0;
   //read bridges DB
   new = ntoff_read_fdb(linux_fdb,FDBMAX);
   new = nt_sort_and_filter_fdb(linux_fdb,learned,new); //sort list, filter out ones that match perm_do[], perm_dont[];

   //build the new offload db
   //1st copy over permanent entries frin perm_do[] list
   for(i=0; (new_count<OFFMAX)&&(i<PFDBMAX); )
   {
     if (perm_do[i].switch_port == 0xff)
     {
           i+=1;
     }
     else
     {
           our_fdb[next_cur][new_count].mac_addr= perm_do[i].mac_addr;
           our_fdb[next_cur][new_count].switch_port = perm_do[i].switch_port;
           our_fdb[next_cur][new_count].flags=OUR_FSB_INUSE|OUR_FSB_PERM;
           i+=1;
           new_count+=1;
     }
   }
   //now copy over sorted & filtered  new entries from linux's db
   for (i=0;(new_count<OFFMAX) && (i<new))
   {
           our_fdb[next_cur][new_count].mac_addr= linux_fdb[i].mac_addr;
           our_fdb[next_cur][new_count].switch_port= linux_fdb[i].switch_port;
           our_fdb[next_cur][new_count].flags=OUR_FSB_INUSE;
           i+=1;
           new_count+=1;
   }
   //if we have room, now copy ones from the old list that we want to keep
   for (i=0; (new_count<OFFMAX) && (i<OFFMAX); )
   {
      
   }

   //mark the rest of the entries as not in use
   for(i=new_count; i< OFFMAX; i++)  our_fdb[next_cur][new_count].flags=0x00000000;
   

   //delete old entries
   ntoff_delete(&our_fdb[cur_off][0], n_cur_off);

   //add new entries
   ntoff_add(&our_fdb[next_cur][0], new_count);

   //flip entries
   cur_off= next_cur;
   n_cur_off= new_count;

}


//******************
// read the fdb
// return #entries
//******************
int ntoff_read_fdb(struct our_fdb_entry fdb[], int max_fdb)
{
    FILE * f = fopen("/sys/class/net/br0/brforward", "r");
      //FILE * f = fopen("blah.fdb", "r");
    int n=0;
      if (f) {
           n = fread(fdb, sizeof(struct our_fdb_entry),max_fdb , f);
           fclose(f);
      } 
      return n;
}
#endif

/********************************
 * filter & sort learned list
 *********************************/
int ntoff_sort_and_filter(struct our_fdb_entry raw[],OFFLOAD_DB_T pl[]  ,int sz)
{
    int i,j;
    int s;

    for(i=0,j=0; (i<sz)&&(j<OFFMAX);)
    {
        if(raw[i].is_local) {i+=1; continue;}
        if(raw[i].ageing_timer_value > THRESH_OFF) { i+=1; continue;}
        s = ntoff_find_slot(perm_dont,&raw[i].mac_addr[0], raw[i].switch_port,FDBMAX);
        if (s>=0) {i+=1; continue;}
        s = ntoff_find_slot(perm_do,&raw[i].mac_addr[0], raw[i].switch_port,PFDBMAX);
        if (s>=0) {i+=1; continue;}
        //OK to learn
        pl[j].switch_port = raw[i].switch_port;
        memcpy(&pl[j].mac_addr[0], &raw[i].mac_addr[0],6);
        pl[j].flags = OUR_FSB_INUSE | OUR_FSB_LEARN;
        j+=1;
        i+=1;
    }
    //zap the rest of the table
    for(i=j;i<OFFMAX;i++)  pl[i].flags= 0x00;

    return j;
}

/*******************************
 * learn what the Linux bridge knows
 ******************************************/
int ntoff_learn(void)
{
    int new;
    new = ntoff_sort_and_filter(&ale_cfg, learned,32);
    return 0;

}

//*******************************
//show stuff
//*******************************
void nt_print_fdb(char* title, OFFLOAD_DB_T *p, int sz)
{
    int i;
    printf("%s \n", title);
    printf("MAC           switch_port    FLAGS\n");
    
    for (i=0;i<sz;i++)
    {
    if (p[i].flags & OUR_FSB_INUSE) printf("%x-%x-%x-%x-%x-%x   %d  %x\n",
                                           p[i].mac_addr[0],
                                           p[i].mac_addr[1],
                                           p[i].mac_addr[2],
                                           p[i].mac_addr[3],
                                           p[i].mac_addr[4],
                                           p[i].mac_addr[5],
                                           p[i].switch_port, p[i].flags);
}
}

void ntoff_show(void)
{  
   nt_print_fdb("perm_dont:",perm_dont,FDBMAX);
   nt_print_fdb("perm do:", perm_do,PFDBMAX);
   nt_print_fdb("learned:", learned, OFFMAX);
}

//#define TEST
#ifdef TEST
unsigned char m1[6] = {0x00,0x01,0x2,0x3,0x4,0x5};
unsigned char m2[6] = {0x00,0x01,0x2,0x3,0x4,0x6};

void main()
{
    ntoff_start();
    ntoff_add_do_perm(m1,1);
    ntoff_add_do_perm(m2,2);
    ntoff_learn();
    ntoff_show();
    ntoff_del_do_perm(m2,2);
    ntoff_del_do_perm(m1,1);
    ntoff_stop();

}

#endif
