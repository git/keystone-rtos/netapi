/******************************************
 * File: net_test_sa_utils.c
 * Purpose: net_test application security associations utilities
 **************************************************************
 * FILE:  net_test_sa_utils.c
 * 
 * DESCRIPTION:  net_test application security associations utilities
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************/

#include "net_test.h"
#include "ti/runtime/netapi/netapi.h"
#include "net_test_sa_utils.h"
#include "net_test_utils.h"

#include <signal.h>
#include <pthread.h>

#include <ti/drv/sa/salld.h>
#include <ti/drv/pa/pa.h>

extern NETAPI_T netapi_handle;
extern netTestConfig_t netTestCfg;
extern netTestConfig_t config;
extern netTestSA_t sa_info[];
extern NETCP_CFG_IPSEC_POLICY_T rx_policy[];

/* pktio channels externs */
extern PKTIO_HANDLE_T *netcp_rx_chan;
extern PKTIO_HANDLE_T *netcp_rx_chan2;
extern PKTIO_HANDLE_T *netcp_tx_chan;
extern PKTIO_HANDLE_T *netcp_tx_chan_ah;
extern PKTIO_HANDLE_T *netcp_sb_tx_chan;
extern PKTIO_HANDLE_T *netcp_sb_rx_chan;
extern PKTIO_CFG_T our_chan_cfg;
extern PKTIO_CFG_T netcp_rx_cfg;
extern PKTIO_CFG_T netcp_rx_cfg2;
extern PKTIO_CFG_T netcp_tx_cfg;
extern PKTIO_CFG_T netcp_sb_rx_cfg;
extern PKTIO_CFG_T netcp_sb_tx_cfg;
/* end pktio channels externs */

extern Trie *p_trie_sa_rx;
extern Trie *p_trie_sa_tx;



void  netTest_utilBuildSADB(int i)
{
    long tmp_spi;
    long tmp_tunnel;
    if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_HMAC_SHA1) &&
        (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_AES_CBC))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;
        
        sa_info[i].tx_payload_info.encOffset = netTest_MAC_HEADER_LEN +
                                               netTest_IP_HEADER_LEN +
                                               netTest_ESP_HEADER_LEN +
                                               netTest_AES_CBC_IV_LEN;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_ICV_LEN;

        sa_info[i].iv_len=16;
        sa_info[i].bl=16;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0; 
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
       
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID ;
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);

        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
            trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_NULL) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_AES_CTR))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset = netTest_MAC_HEADER_LEN +
                                               netTest_IP_HEADER_LEN +
                                               netTest_ESP_HEADER_LEN +
                                               netTest_AES_CTR_IV_LEN;
        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = 0;
        sa_info[i].iv_len=8;
        sa_info[i].bl=8;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;

        sa_info[i].key_params = &netTestCfg.key_params[i];

        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO| NWAL_TX_FLAG1_DO_UDP_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID ;
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_HMAC_SHA2_256) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_AES_CTR))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset = netTest_MAC_HEADER_LEN +
                                               netTest_IP_HEADER_LEN +
                                               netTest_ESP_HEADER_LEN +
                                               netTest_AES_CTR_IV_LEN;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_ICV_LEN;
        sa_info[i].iv_len=8;
        sa_info[i].bl=8;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;

        sa_info[i].key_params = &netTestCfg.key_params[i];

        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO| NWAL_TX_FLAG1_DO_UDP_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID ;
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_HMAC_SHA2_256) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_3DES_CBC))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset = netTest_MAC_HEADER_LEN +
                                               netTest_IP_HEADER_LEN +
                                               netTest_ESP_HEADER_LEN +
                                               netTest_3DES_CBC_IV_LEN;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_ICV_LEN;

        sa_info[i].iv_len=8;
        sa_info[i].bl=8;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
       
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID ;

        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_HMAC_MD5) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_NULL))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset =         netTest_MAC_HEADER_LEN +
                                                       netTest_IP_HEADER_LEN +
                                                       netTest_NULL_ESP_HEADER_LEN +
                                                       netTest_NULL_IV_LEN +
                                                       24;
        sa_info[i].iv_len=0;
        sa_info[i].bl=4;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_ICV_LEN;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_AH_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID;
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_NULL) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_AES_GCM))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset =         netTest_MAC_HEADER_LEN +
                                                       netTest_IP_HEADER_LEN +
                                                       netTest_ESP_HEADER_LEN +
                                                       netTest_AES_GCM_IV_LEN;

        sa_info[i].iv_len=0;
        sa_info[i].bl=4;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_AES_GCM_CCM_ICV_LEN;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
       
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID;

        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_NULL) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_NULL))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;
         sa_info[i].tx_payload_info.encOffset =         netTest_MAC_HEADER_LEN +
                                                       netTest_IP_HEADER_LEN +
                                                       netTest_ESP_HEADER_LEN;

        sa_info[i].iv_len=0;
        sa_info[i].bl=0;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = 0;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID;

        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }

    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_NULL) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_AES_CCM))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset =         netTest_MAC_HEADER_LEN +
                                                       netTest_IP_HEADER_LEN +
                                                       netTest_ESP_HEADER_LEN +
                                                       netTest_AES_CCM_IV_LEN;

        sa_info[i].iv_len=0;
        sa_info[i].bl=4;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_AES_GCM_CCM_ICV_LEN;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
       
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID;
        
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_AES_XCBC) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_NULL))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset =         netTest_MAC_HEADER_LEN +
                                                       netTest_IP_HEADER_LEN +
                                                       netTest_ESP_HEADER_LEN +
                                                       netTest_NULL_IV_LEN;

        sa_info[i].iv_len=0;
        sa_info[i].bl=4;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        //sa_info[i].tx_payload_info.ploadLen = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_ICV_LEN;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
       
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM|
                                         NWAL_TX_FLAG1_META_DATA_VALID;
        
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }
    else if ((netTestCfg.sa[i].authMode== NWAL_SA_AALG_GMAC) &&
             (netTestCfg.sa[i].cipherMode == NWAL_SA_EALG_NULL))
    {
        /* static configuration, will not change */
        sa_info[i].tx_payload_info.aadSize = 0;
        sa_info[i].tx_payload_info.pAad = NULL;
        sa_info[i].tx_payload_info.pAuthIV = NULL;
        sa_info[i].tx_payload_info.authOffset = netTest_MAC_HEADER_LEN +
                                                netTest_IP_HEADER_LEN;

        sa_info[i].tx_payload_info.encOffset =         netTest_MAC_HEADER_LEN +
                                                       netTest_IP_HEADER_LEN +
                                                       netTest_ESP_HEADER_LEN +
                                                       netTest_AES_GMAC_IV_LEN;

        sa_info[i].iv_len=0;
        sa_info[i].bl=4;

        /* dynamic configuration, will  be calculated on the fly */
        sa_info[i].tx_payload_info.authSize = 0;
        sa_info[i].tx_payload_info.encSize = 0;
        sa_info[i].tx_payload_info.pEncIV = 0;
        sa_info[i].tx_payload_info.pAuthIV = 0;

        sa_info[i].cipherMode = netTestCfg.sa[i].cipherMode;
        sa_info[i].authMode = netTestCfg.sa[i].authMode;
        sa_info[i].inner_ip_offset = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].auth_tag_size = netTest_AES_GMAC_ICV_LEN;

        sa_info[i].tx_pkt_info.enetPort = 0;
        sa_info[i].tx_pkt_info.ipOffBytes = sa_info[i].tx_payload_info.encOffset;
        sa_info[i].tx_pkt_info.l4HdrLen = netTest_UDP_HEADER_LEN;
        sa_info[i].tx_pkt_info.l4OffBytes = sa_info[i].inner_ip_offset + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.startOffset = 0;
        sa_info[i].tx_pkt_info.lpbackPass = 0;
        sa_info[i].tx_pkt_info.ploadLen = 0;
        sa_info[i].tx_pkt_info.pPkt = NULL;
        sa_info[i].tx_pkt_info.saOffBytes = netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN;
        sa_info[i].tx_pkt_info.saPayloadLen = 0;
        sa_info[i].tx_pkt_info.pseudoHdrChecksum =0;
       
        sa_info[i].tx_pkt_info.txFlag1 = NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO |
                                         NWAL_TX_FLAG1_DO_UDP_CHKSUM |
                                         NWAL_TX_FLAG1_META_DATA_VALID;
        
        sa_info[i].dir =netTestCfg.sa[i].dir;
        tmp_spi = netTest_utilHtonl((long)(netTestCfg.sa[i].spi));
        tmp_tunnel = netTest_utilHtonl((long)(netTestCfg.tunnel_id[i]));
        sa_info[i].spi =tmp_spi;
        sa_info[i].tunnel_id = tmp_tunnel;
        sa_info[i].src = *((long *)&netTestCfg.sa[i].src.ipv4[0]);
        sa_info[i].dst = *((long *)&netTestCfg.sa[i].dst.ipv4[0]);
        if (netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
           trie_insert(p_trie_sa_rx,(char *)&tmp_spi,4, (void *) &sa_info[i]);
        }
        else
        {
           trie_insert(p_trie_sa_tx,(char *)&tmp_tunnel,4, (void *) &sa_info[i]);
        }
    }

}
NETCP_CFG_ROUTE_T routeInfo;
PKTIO_HANDLE_T *pPktioHandle;

int netTest_utilCreateDefaultFlow(NETAPI_T handle, int masterType)
{
    int err = 0;
    PKTIO_CFG_T pktio_cfg;
    Qmss_QueueHnd   qHandle;

    memset(&pktio_cfg, 0, sizeof (PKTIO_CFG_T));
    memset(&routeInfo, 0, sizeof (NETCP_CFG_ROUTE_T));

    pktio_cfg.flags1 = PKTIO_RX | PKTIO_GLOBAL;
    pktio_cfg.flags2 = PKTIO_PKT;

    if (nwal_CreateGenPurposeQueue (&qHandle) == nwal_OK)
    {
        pktio_cfg.qnum = qHandle ;
        pktio_cfg.max_n = 8;
    }
    else
    {
        return -1;
    }

    if(masterType == NETAPI_PROC_MASTER)
    {
        pPktioHandle = netapi_pktioCreate(handle,
                                               "procMaster",
                                               (PKTIO_CB)recv_cb,
                                               &pktio_cfg,
                                               &err);
    }
    else if(masterType == NETAPI_SYS_MASTER)
    {
        pPktioHandle = netapi_pktioCreate(handle,
                                               "sysMaster",
                                               (PKTIO_CB)recv_cb,
                                               &pktio_cfg,
                                               &err);
    }
    else;

    if (!pPktioHandle)
    {
        return -1;
    }
    routeInfo.p_dest_q = pPktioHandle;
    
    routeInfo.p_flow = (NETCP_CFG_FLOW_T*)NETCP_DEFAULT_FLOW;
    routeInfo.valid_params = NETCP_CFG_VALID_PARAM_ROUTE_TYPE;

    return 0;
}
int netTest_utilCreateSecAssoc(void)
{
    nwal_RetValue       nwalRetVal;
    int err = 0;
    int i;
    nwalSaIpSecId_t  nwalSaIpSecId;
    uint32_t saId;
    for (i=0; i < netTestCfg.num_sa;i++)
    {
        err = 0;
        if(netTestCfg.sa[i].dir == NWAL_SA_DIR_INBOUND)
        {
            netTest_utilBuildSADB(i);
            saId = i;
            sa_info[i].rx_tunnel = netapi_secAddSA(
            netapi_handle,
            netTestCfg.ipsec_if_no, //iface #0 
            &netTestCfg.sa[i],
            &netTestCfg.key_params[i],
            netTestCfg.ipsec_mode_rx == IPSEC_MODE_RX_SIDEBAND ? NETAPI_SEC_SA_SIDEBAND: NETAPI_SEC_SA_INFLOW,
            (NETCP_CFG_ROUTE_HANDLE_T)&routeInfo,
            &(sa_info[i].rx_data_mode_handle),
            &(sa_info[i].rx_inflow_mode_handle),
            (void*) saId,
            &err);
            if (err)
            {
                exit(1);
            }


            if (netTestCfg.ipsec_mode_rx == IPSEC_MODE_RX_INFLOW)
            {
                //assume inner and outer ip is the same
                rx_policy[i]= netapi_secAddRxPolicy( netapi_handle,
                             sa_info[i].rx_tunnel,  //link to tunnel above
                             4,         //ipv4
                             &netTestCfg.sa[i].src,
                             &netTestCfg.sa[i].dst,
                            NULL,  // no qualifiers
                            (NETCP_CFG_ROUTE_HANDLE_T)&routeInfo,
                            NULL, //user_data
                             &err);
                if (err)
                {
                    exit(1);
                }
            }
            else
            {
                rx_policy[i] = 0;
            }
        }
        //tx SA security stuff
        if(netTestCfg.sa[i].dir == NWAL_SA_DIR_OUTBOUND)
        {
            netTest_utilBuildSADB(i);
            saId = i;
            sa_info[i].tx_tunnel = netapi_secAddSA( netapi_handle,
                 0, //iface #0 
                 &netTestCfg.sa[i],
                 &netTestCfg.key_params[i],
                netTestCfg.ipsec_mode_tx == IPSEC_MODE_TX_SIDEBAND ? NETAPI_SEC_SA_SIDEBAND: NETAPI_SEC_SA_INFLOW,
                NULL,  //use default route 
                &(sa_info[i].tx_data_mode_handle),
                &(sa_info[i].tx_inflow_mode_handle),
                (void*)saId,
                &err);
            if (err) {exit(1);}
        }
    }
    return err;
}


void netTest_utilDeleteSecAssoc()
{
    int err,i;
    for (i=0; i < netTestCfg.num_sa;i++)
    {
        err = 0;
        if(sa_info[i].dir == NWAL_SA_DIR_INBOUND)
        {
            if (rx_policy[i])
                netapi_secDelRxPolicy(netapi_handle, rx_policy[i], &err);
                //delete tunnels
            netapi_secDelSA(netapi_handle, 0, sa_info[i].rx_tunnel, &err);
        }
        
        if(sa_info[i].dir == NWAL_SA_DIR_OUTBOUND)
        {
            netapi_secDelSA(netapi_handle, 0, sa_info[i].tx_tunnel, &err);
        }
    }
}
