#ifndef __ROUTER_H__
#define __ROUTER_H__



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


#include "net_test.h"
#include "net_test_sa_utils.h"
#include "net_test_utils.h"

typedef struct
{
  unsigned long w1;
  unsigned long w2;
  unsigned long w3;
  unsigned long src;
  unsigned long dst;
} IP_netTestHead_T;

Trie * route_init(void);
void route_add(Trie * Pt, unsigned long * Pdest_ipBE, void * Pour_route);


int route_pkt(Trie *Pt, void * Ppkt, IP_netTestHead_T *Phead, unsigned char * Pbuf, int * Plen, netTestSA_t **Prs, int *PoutPort);
int process_tunnel(Trie *Pt, void *Ppkt, IP_netTestHead_T * Phead,
                   unsigned char * Pbuf, int* Plen, OUR_ROUTE_T * Proute,
                   int * PoutPort);

#endif
