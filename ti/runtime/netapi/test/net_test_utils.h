#ifndef __NET_TEST_UTIL_H__
#define __NET_TEST_UTIL_H__


#include "trie.h"
//#include "net_test.h"
#include "ti/runtime/netapi/netapi.h"
#include "net_test_sa_utils.h"

#define MAX_SEC_INDEX          64
#define NET_TEST_MAX_MAC                64
#define NET_TEST_MAX_IP                 64
#define NET_TEST_MAX_FP_THREAD 5
#define NET_TEST_MAX_SP_THREAD 5
#define MAX_LINE_LENGTH 512
#define MAX_ROUTES 16

#define TEMP_STR_LEN 32

#define BE(x) ( (((x)&0xff000000)>>24) | (((x)&0xff0000)>>8) | (((x)&0xff00)<<8) | (((x)&0xff)<<24)  )

/* The input strings requirEd for parsing certain configuration
 * paremeters from the config file */
#define netTest_INIT_CONFIG_IPSEC_MODE_RX          "ipsec_mode_rx"
#define netTest_INIT_CONFIG_IPSEC_MODE_TX           "ipsec_mode_tx"
#define netTest_INIT_CONFIG_IPSEC_IF_NO             "ipsec_if"
#define netTest_INIT_CONFIG_DSP_MAC                 "dsp_mac"
#define netTest_INIT_CONFIG_DSP_IP                  "dsp_ip"
#define netTest_INIT_CONFIG_STATIC_LOOPBACK_PORT    "dest_udp_port_config"
#define netTest_CONFIG_STRING_LEN 512

//IPSEC MODE(only choose one rx and one tx)
#define IPSEC_MODE_RX_INFLOW            1
#define IPSEC_MODE_TX_INFLOW            2
#define IPSEC_MODE_RX_SIDEBAND          3
#define IPSEC_MODE_TX_SIDEBAND          4

#define netTest_PKT_LEN 1400

#define NET_TEST_SP_THREAD_MASK 0xF0000000
#define NET_TEST_THREAD_NUM_MASK 0x000000FF

typedef struct {
    unsigned char           mac[NET_TEST_MAX_MAC][6];
    uint8_t                 num_macs;
    uint8_t                 switch_port[NET_TEST_MAX_MAC];
    nwalIpAddr_t            ip[NET_TEST_MAX_IP];
    uint8_t                 num_ips;
    uint8_t                 attach_iface[NET_TEST_MAX_IP];
    nwalIpAddr_t            local_ipsec_ip;
    nwalIpAddr_t            remote_ipsec_ip;
    uint8_t                 ipsec_mode_rx;
    uint8_t                 ipsec_mode_tx;
    int                     ipsec_if_no;
    int                     dest_udp_port_config;
    uint8_t                 fp_proc_start[NET_TEST_MAX_FP_THREAD];
    uint8_t                 fp_proc_end[NET_TEST_MAX_FP_THREAD];
    uint32_t                fp_thread_num[NET_TEST_MAX_FP_THREAD];
    uint8_t                 num_fp_threads;
    uint8_t                 sp_proc_start[NET_TEST_MAX_SP_THREAD];
    uint8_t                 sp_proc_end[NET_TEST_MAX_SP_THREAD];
    uint32_t                sp_thread_num[NET_TEST_MAX_SP_THREAD];
    uint8_t                 num_sp_threads;
    
    uint8_t                 auth_key[MAX_SEC_INDEX][36];
    uint8_t                 encr_key[MAX_SEC_INDEX][36];
    uint32_t                tunnel_id[MAX_SEC_INDEX];
    NETAPI_SEC_SA_INFO_T    sa[MAX_SEC_INDEX];
    uint8_t                 num_sa;
    nwalSecKeyParams_t      key_params[MAX_SEC_INDEX];
    uint8_t                 dsp_mac;
    uint8_t                 dsp_ip;

} netTestConfig_t;

typedef struct
{
    char    dir[netTest_CONFIG_STRING_LEN];
    char    spi[netTest_CONFIG_STRING_LEN];
    char    proto[netTest_CONFIG_STRING_LEN];
    char    saMode[netTest_CONFIG_STRING_LEN];
    char    ipType[netTest_CONFIG_STRING_LEN];
    char    src[netTest_CONFIG_STRING_LEN];
    char    dst[netTest_CONFIG_STRING_LEN];
    char    replayWindow[netTest_CONFIG_STRING_LEN];
    char    authMode[netTest_CONFIG_STRING_LEN];
    char    cipherMode[netTest_CONFIG_STRING_LEN];
    char    esnLo[netTest_CONFIG_STRING_LEN];
    char    esnHi[netTest_CONFIG_STRING_LEN];
    char    encKeySize[netTest_CONFIG_STRING_LEN];
    char    macKeySize[netTest_CONFIG_STRING_LEN];
    char    auth_key[netTest_CONFIG_STRING_LEN];
    char    encr_key[netTest_CONFIG_STRING_LEN];
    char    tunnel_id[netTest_CONFIG_STRING_LEN];
} netTestConfigSA_t;

typedef struct{
    char mac[NET_TEST_MAX_MAC][netTest_CONFIG_STRING_LEN];
    char switch_port[NET_TEST_MAX_MAC][netTest_CONFIG_STRING_LEN];
    char ip[NET_TEST_MAX_IP][netTest_CONFIG_STRING_LEN];
    char attach_iface [NET_TEST_MAX_MAC][netTest_CONFIG_STRING_LEN];
    char ipsec_mode_rx[netTest_CONFIG_STRING_LEN];
    char ipsec_mode_tx[netTest_CONFIG_STRING_LEN];
    char routes[MAX_ROUTES][netTest_CONFIG_STRING_LEN];
    char ports[MAX_ROUTES][netTest_CONFIG_STRING_LEN];
    char dst_ips[MAX_ROUTES][netTest_CONFIG_STRING_LEN];
    char paths[MAX_ROUTES][netTest_CONFIG_STRING_LEN];
    char ipsec_if_no[netTest_CONFIG_STRING_LEN];
    char fp_thread_num[NET_TEST_MAX_FP_THREAD][netTest_CONFIG_STRING_LEN];
    char sp_thread_num[NET_TEST_MAX_SP_THREAD][netTest_CONFIG_STRING_LEN];
    char sp[NET_TEST_MAX_SP_THREAD][netTest_CONFIG_STRING_LEN];
    char fp[NET_TEST_MAX_FP_THREAD][netTest_CONFIG_STRING_LEN];
    char tunnel_id[netTest_CONFIG_STRING_LEN];
    netTestConfigSA_t sa_config[MAX_SEC_INDEX][netTest_CONFIG_STRING_LEN];
    char dsp_mac[netTest_CONFIG_STRING_LEN];
    char dsp_ip[netTest_CONFIG_STRING_LEN];
    char dest_udp_port_config[netTest_CONFIG_STRING_LEN];
} netTestConfigFile_t;

typedef struct {
    int iface;
    int ipcsum;
    int l4csum;
} LastPktInfo_t;


//#define netTestSA_t netTestSA_t

typedef struct our_route_t
{
   int out_port;
   unsigned char out_mac[14];
   netTestSA_t * sec_ptr;
} OUR_ROUTE_T;

typedef struct {
    long itx;  //initially generated
    long itx2;
    long rx;
    long tx;
    long n_bad;
    long n_new;
    long n_class0_rx;   //count of pkts classified 
    long n_class1_rx;   //count of pkts classified 
    long n_class2_rx;   //count of pkts classified 
    long n_t1;
    long n_t2;
    long n_t3;
    long sec_tx;
    long sec_rx;
    long sb_tx;
    long sb_rx;
    long secp_rx;
    long n_auth_ok;
    unsigned long long  app_cycles;
    unsigned long long  send_cycles;
    unsigned long long  tx_cache_cycles;
    unsigned long long total_decrypt_time;
    unsigned long long total_encrypt_time;
    long rx_min;
    long tx_min;
    long if_rx[TUNE_NETAPI_MAX_NUM_MAC];
    long  core_rx;
    long  n_stats_cb;
    long  ip;
    long exceptionPktsFrag;
    long exceptionPktsOther;
} netTestStats_T;

typedef struct
{
    long ip[5];
    long udp[2];
} netTestHead_T;






void house(NETAPI_SCHED_HANDLE_T *s);
void netTest_utilCreateInterfaces(uint8_t num_macs, uint8_t num_ips, Bool match_action_host);
void netTest_utilDeleteInterfaces(uint8_t num_macs, uint8_t num_ips);

int netTest_utilCheckHeader(netTestHead_T * p_head, PKTIO_METADATA_T * p_meta);
void netTest_utilMySig(int x);
unsigned long netTest_utilPeek(unsigned long * p);
void netTest_utilDumpDescr(unsigned long *p, int n);
void netTest_utilDumpHeader(unsigned long *p, int n, int a, int r);
void   netTest_utilDumpBuffer(unsigned long * buf,uint32_t buf_length);
long netTest_utilHtonl(long x);
unsigned char netTest_utilHex2Dec(char *p_s);
uint16_t netTest_utilOnesComplementAdd (uint16_t v1, uint16_t v2);
uint16_t netTest_utilOnesCompChkSum (uint8_t *p, uint32_t nwords);
uint16_t netTest_utilGetIPv4PsudoChkSum (uint8_t *data, uint16_t payloadLen);
void netTest_utilPrintIPSecStats(Sa_IpsecStats_t     *p_saIpsecStats, nwal_saAALG auth, nwal_saEALG cipher);
void netTest_utilPrintDataModeStats(Sa_DataModeStats_t     *p_saDataModeStats, nwal_saAALG auth, nwal_saEALG cipher);
void netTest_utilStatsCbMt(NETAPI_T h, paSysStats_t* pPaStats);
void netTest_utilsStatsCb(NETAPI_T h, paSysStats_t* pPaStats);


void netTest_utilParseOneMac(char * p_mac_str, unsigned char *p_mac);
void netTest_utilParseOneKey(char *p_key_str, unsigned char *p_key);
void netTest_utilParseOneIP(char * p_ip_addr_str, unsigned char * p_ip);
void netTest_utilParseMac(netTestConfigFile_t *pConfig);
void netTest_utilParseIP(netTestConfigFile_t *pConfig);
void netTest_utilParseThreadParams(netTestConfigFile_t *pConfig);
void netTest_utilParseIpsecMode(netTestConfigFile_t *pConfig);
void netTest_utilParseAuthMode(char *auth_mode_str, nwal_saAALG *auth_mode);
void netTest_utilParseEncryptMode(char *ency_mode_str, nwal_saEALG*encr_mode);
void netTest_utilParseProto(char *proto_str, nwal_IpSecProto *proto);
void netTest_utilParseSaMode(char *mode_str, nwal_saMode *mode);
void netTest_utilParseIPType(char *ip_type_str, nwal_IpType *ipType);
void netTest_utilParseSADir(char* dir_str, nwal_SaDir *dir);
void netTest_utilParseSA(netTestConfigFile_t *pConfig);
void netTest_utilParseRoutes(netTestConfigFile_t* pConfig, OUR_ROUTE_T* routes, Trie** our_router);
void netTest_utilProcessConfigFile(FILE* fpr, netTestConfigFile_t* pConfig);
void recv_cb(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts );

void parse_simple_param_u8(char* input_str, uint8_t *val);
void parse_simple_param_u16(char* input_str, uint16_t *val);
void parse_simple_param_u32(char* input_str, uint32_t *val);


#endif

