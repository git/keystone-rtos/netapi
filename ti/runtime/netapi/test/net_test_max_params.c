/******************************************
 * File: net_test.c
 * Purpose: Application for testing out max configuration parameters
 *          such as SA's, MACs/IP interfaces
 **************************************************************
 * FILE:  net_test.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************/

#include "net_test.h"
#include "ti/drv/nwal/test/fw_rm.h"
#include <signal.h>
#include <pthread.h>

extern int QUIT;
extern netTestStats_T stats[TUNE_NETAPI_NUM_CORES];
extern paSysStats_t netcp_stats;
/* Global definitions */
#ifdef netTest_MULTI_THREAD
    cpu_set_t cpu_set;
#endif


#define SA_CREATE_LOOP_COUNT 64
#define IP_CREATE_LOOP_COUNT 64
#define MAC_CREATE_LOOP_COUNT 59


netTestConfig_t netTestCfg;
static  netTestConfigFile_t config_file;

char    input_file_name[] = "net_test_config_max_iface.txt";

nwal_RetValue       nwalRetVal;
Pktlib_HeapHandle ourHeap;

PKTIO_HANDLE_T *netcp_rx_chan;
PKTIO_HANDLE_T *netcp_tx_chan_no_crypto;
PKTIO_HANDLE_T *netcp_tx_chan_esp;
PKTIO_HANDLE_T *netcp_tx_chan_ah;
PKTIO_HANDLE_T *netcp_sb_tx_chan;
PKTIO_HANDLE_T *netcp_sb_rx_chan;

PKTIO_CFG_T our_chan_cfg={PKTIO_RX_TX, PKTIO_LOCAL, PKTIO_Q_ANY, 8};
PKTIO_CFG_T netcp_rx_cfg={PKTIO_RX, PKTIO_NA, PKTIO_NA, 8};
PKTIO_CFG_T netcp_rx_cfg2={PKTIO_RX, (PKTIO_GLOBAL|PKTIO_PKT), PKTIO_Q_ANY, 8};
PKTIO_CFG_T netcp_tx_cfg={PKTIO_TX, PKTIO_NA, PKTIO_NA, 8};
PKTIO_CFG_T netcp_sb_rx_cfg={PKTIO_RX, PKTIO_NA, PKTIO_NA, 8};
PKTIO_CFG_T netcp_sb_tx_cfg={PKTIO_TX, PKTIO_NA, PKTIO_NA, 8};


NETCP_CFG_EXCEPTION_PKT_T expPkt_appid;

Trie *p_trie_sa_rx;
Trie *p_trie_sa_tx;


/*******************************************
 *************NETAPI OBJECTS***************
 *****************************************/
static NETAPI_CFG_T our_netapi_default_cfg=
{
TUNE_NETAPI_PERM_MEM_SZ,
128,  //start of packet offset for hw to place data on rx for default flow
TUNE_NETAPI_QM_CONFIG_MAX_DESC_NUM, //max number of descriptors in system
TUNE_NETAPI_NUM_GLOBAL_DESC,        //total we will use
TUNE_NETAPI_DEFAULT_NUM_BUFFERS,   //#descriptors+buffers in default heap
64, //#descriptors w/o buffers in default heap
TUNE_NETAPI_DEFAULT_BUFFER_SIZE+128+128,  //size of buffers in default heap
128,       //tail room
256,      //extra room
0,
NULL,
18,
0x2000
};

NETAPI_T netapi_handle;
NETAPI_SCHED_HANDLE_T * our_sched;
#ifdef netTest_MULTI_THREAD
NETAPI_SCHED_HANDLE_T * scheduler[TUNE_NETAPI_NUM_CORES];
#endif
NETAPI_SCHED_CONFIG_T our_sched_cfg={
  NETAPI_SCHED_DURATION|NETAPI_SCHED_CBV, 0, house, 5000000  //every 5000000 poll loops
};

NETCP_CFG_IP_T ip_rule[NET_TEST_MAX_IP];
NETCP_CFG_MACIF_T mac[NET_TEST_MAX_MAC];



/* security objects. (for loopback mode) */
netTestSA_t sa_info[MAX_SEC_INDEX];

NETCP_CFG_IPSEC_POLICY_T rx_policy[MAX_SEC_INDEX];

/* stub functions */
void recv_cb(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
    return;
}

void flip_and_send_pkt(Ti_Pkt *tip,  unsigned char * p_pkt, int len, int flag, uint16_t enet_port)
{
    return;
}



void recv_sb_cb(struct PKTIO_HANDLE_Tag * channel, Ti_Pkt* p_recv[],
                         PKTIO_METADATA_T meta[], int n_pkts,
                         uint64_t ts )
{
    return;
}

Trie * route_init(void)
{
    return NULL;
}
void route_add(Trie * Pt, unsigned long * Pdest_ipBE, void * Pour_route)
{
}

#ifdef netTest_MULTI_THREAD
NETAPI_T worker_nh[TUNE_NETAPI_NUM_CORES];

void slow_path_thread(uint32_t thread_num)
{
    return;
}

void fast_path_thread(uint32_t thread_num)
{
    return;
}

#endif

/* Templates to build command labels at startup up time, required by open_pktio_tx_channels() */
nwalTxPktInfo_t txPktInfoESP = 
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_DO_IPSEC_ESP_CRYPTO| NWAL_TX_FLAG1_DO_UDP_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    0,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    0               ,                                                                                    /* saAhIcvOffBytes */
    0,                                                                                                 /* saAhMacSize */
    0,                                                                                                  /* etherLenOffBytes */
    0,                                                                                                  /* ipOffBytes */
    0,                                                                                                  /* l4OffBytes */
    netTest_UDP_HEADER_LEN,                                                                             /* l4HdrLen */
    0,                                                                                                  /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};


nwalTxPktInfo_t txPktInfoAH = 
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_DO_IPSEC_AH_CRYPTO| NWAL_TX_FLAG1_DO_UDP_CHKSUM | NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    0,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    netTest_MAC_HEADER_LEN  + netTest_IP_HEADER_LEN,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    netTest_MAC_HEADER_LEN + netTest_IP_HEADER_LEN + netTest_IPSEC_AH_FIXED_HDR_SIZE,                    /* saAhIcvOffBytes */
    12,                                                                                                 /* saAhMacSize */
    0,                                                                                                  /* etherLenOffBytes */
    0,                                             /* ipOffBytes */
    0,                                                                                                  /* l4OffBytes */
    netTest_UDP_HEADER_LEN,                                                                            /* l4HdrLen */
    0,                                                                                                  /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};

nwalTxPktInfo_t txPktInfoNoCrypto = 
{
    NULL,                                                                                               /* p_pkt */
    NWAL_TX_FLAG1_DO_UDP_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID,      /* txFlags */
    0,                                                                                                  /* lpbackPass */
    0,                                                                                                  /* enetport */
    0,                                                                                                  /* msuSize */
    0,                                                                                                   /* startOffset */
    0,                                                    /* saOffBytes */
    0,                                                                                                  /* saPayLoadLen */
    0               ,                                                                                    /* saAhIcvOffBytes */
    0,                                                                                                 /* saAhMacSize */
    0,                                                                                                  /* etherLenOffBytes */
    0,                                                                                                  /* ipOffBytes */
    netTest_MAC_HEADER_LEN +netTest_IP_HEADER_LEN,                                        /* l4OffBytes */
    netTest_UDP_HEADER_LEN,                                                             /* l4HdrLen */
    0,                                                                         /* pseudoHdrChecksum */
    0                                                                                                   /* pLoadLen */
};


void close_pktio_channels(void)
{
    int err;
    netapi_pktioClose(netcp_tx_chan_esp ,&err);
    netapi_pktioClose(netcp_tx_chan_ah ,&err);
    netapi_pktioClose(netcp_sb_tx_chan ,&err);
    netapi_pktioClose(netcp_tx_chan_no_crypto,&err);
}

void open_pktio_tx_channels(void)
{
    int err;
    /* open netcp default  TX for ESP packets */
    netcp_tx_chan_esp= netapi_pktioOpen(netapi_handle, NETCP_TX, NULL, &netcp_tx_cfg,  &err);
    if (!netcp_tx_chan_esp)
    {
        printf("pktio open TX failed err=%d\n",err);
        exit(1);
    }
    else
    {
        if(netTestCfg.ipsec_mode_tx == IPSEC_MODE_TX_INFLOW)
        {
            PKTIO_CONTROL_T control;
            control.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg;
            cfg.fast_path_cfg.fp_send_option = PKTIO_FP_ESP_L4CKSUM_PORT;
            cfg.fast_path_cfg.txPktInfo= &txPktInfoESP;
            netapi_pktioControl(netcp_tx_chan_esp, NULL, &cfg, &control, &err);
        }
    }

    /* open netcp default  TX for AH packets */
    netcp_tx_chan_ah= netapi_pktioOpen(netapi_handle, NETCP_TX, NULL, &netcp_tx_cfg,  &err);
    if (!netcp_tx_chan_ah)
    {
        printf("pktio open TX failed err=%d\n",err);
        exit(1);
    }
    else
    {
        if(netTestCfg.ipsec_mode_tx == IPSEC_MODE_TX_INFLOW)
        {
            PKTIO_CONTROL_T control;
            control.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg;
            cfg.fast_path_cfg.fp_send_option = PKTIO_FP_AH_L4CKSUM_PORT;
            cfg.fast_path_cfg.txPktInfo= &txPktInfoAH;
            netapi_pktioControl(netcp_tx_chan_ah, NULL, &cfg, &control, &err);
        }
    }

    /* open netcp default  TX channels for non-Crypto packets */
    netcp_tx_chan_no_crypto= netapi_pktioOpen(netapi_handle, NETCP_TX, NULL, &netcp_tx_cfg,  &err);
    if (!netcp_tx_chan_no_crypto)
    {
        printf("pktio open TX failed err=%d\n",err);
        exit(1);
    }
    else
    {
        if(netTestCfg.ipsec_mode_tx == IPSEC_MODE_TX_INFLOW)
        {
            PKTIO_CONTROL_T control;
            control.op = PKTIO_UPDATE_FAST_PATH;
            PKTIO_CFG_T cfg;
            cfg.fast_path_cfg.fp_send_option = PKTIO_FP_L4CKSUM_PORT;
            cfg.fast_path_cfg.txPktInfo= &txPktInfoNoCrypto;
            netapi_pktioControl(netcp_tx_chan_no_crypto, NULL, &cfg, &control, &err);
        }
    }

    /* open netcp default  TX channels for SB crypto */
    netcp_sb_tx_chan= netapi_pktioOpen(netapi_handle, NETCP_SB_TX, NULL, &netcp_sb_tx_cfg,  &err);
    if (!netcp_sb_tx_chan)
    {
        printf("pktio open SB TX failed err=%d\n",err);
        exit(1);
    }
}
/***************************************
 ********** test driver*****************
 ***************************************/
int main(int argc, char **argv)
{
    int err,i;
    Pktlib_HeapCfg      heapCfg;
    int32_t             errCode;
    Pktlib_HeapIfTable*  pPktifTable;
    FILE * fpr = NULL;


    setvbuf(stdout,NULL,_IONBF,0);
     /* install signal handler for ^c */
    signal(SIGINT,netTest_utilMySig);

    if (argc == 2)
    {
        printf("main: filename1 %s\n", argv[1]);
        fpr = fopen(argv[1], "r");
    }
    else
    {
        fpr = fopen(input_file_name, "r");
    }
    if (fpr == NULL)
    {
        printf("error opening configfile\n");
        exit(1);
    }
    else
    {
        memset(&config_file, 0, sizeof(netTestConfigFile_t));
        memset(&netTestCfg, 0, sizeof(netTestConfig_t));
        netTest_utilProcessConfigFile(fpr,&config_file);
#if 1
        netTest_utilParseMac(&config_file);

        /* parse slow path/fast path thread configuration parameters */
        netTest_utilParseThreadParams(&config_file);

        netTest_utilParseIP(&config_file);

        netTest_utilParseIpsecMode(&config_file);

        /* DSP mac processing */
        parse_dsp_mac(&config_file.dsp_mac[0]);

       /* DSP IP processing */
        parse_dsp_ip(&config_file.dsp_ip[0]);

        /* IPSEC interface number processing */
        parse_simple_param_u32((char*)&config_file.ipsec_if_no[0], &netTestCfg.ipsec_if_no);

        netTest_utilParseSA(&config_file);
#endif
    }

    memset(&sa_info, 0, sizeof(sa_info));


#ifdef netTest_MULTI_THREAD
    /* assign main net_test thread to run on core 0 */
    CPU_ZERO( &cpu_set);
    CPU_SET( 0, &cpu_set);
    hplib_utilSetupThread(0, &cpu_set, hplib_spinLock_Type_LOL);
#endif

    /* create netapi */
    netapi_handle = netapi_init(NETAPI_SYS_MASTER, &our_netapi_default_cfg);
    if (netapi_handle == NULL)
    {
        printf("main: netapi_init failure, exiting\n");
        exit(1);
    }

    /* open the main heap */
    ourHeap = Pktlib_findHeapByName("netapi");
    if (!ourHeap)
    {
        printf("Pktlib_findHeapByName()  fail\n");
        exit(1);
    }

    open_pktio_tx_channels();

    /* create scheduler instance */
    our_sched =netapi_schedOpen(netapi_handle,&our_sched_cfg, &err);
    if (!our_sched) {printf("sched create failed\n"); exit(1);}


    /*create net_test MAC interfaces, attach IP to created MAC interfaces */
    netTest_utilCreateInterfaces(netTestCfg.num_macs, netTestCfg.num_ips,0);

    /* lookup Database for SA context, this is used by packet processing routines to get RX and TX SA information*/
    p_trie_sa_rx = trie_new();
    p_trie_sa_tx = trie_new();
    if (!p_trie_sa_rx || !p_trie_sa_tx)
        {printf("trie alloc for SA  failed\n"); exit(1);}


#ifdef TEST_SA
    /* Create RX SA's, RX Policy and TX SA's, all SA configuration parameters are read from net_test_config.txt file */
    netTest_utilCreateSecAssoc();
#endif
#ifdef netTest_MULTI_THREAD
{
    int c;


        //this thread of execution (main) now just waits on user input
        for(;;)
        {
            printf(">");
            c=getchar();
            if (c=='q')
            {
                QUIT=1;
                break;
            }
            else if (c=='h')
                printf("'q' to quit,  's' for stats, 'h' for help\n");
        }
}
#else
    /*********************************************/
    /**************Entry point into scheduler ****/
    /*********************************************/
    netapi_schedRun(our_sched, &err);
#endif

    /* cleanup*/
     netTest_utilDeleteSecAssoc();

    netTest_utilDeleteInterfaces(netTestCfg.num_macs, netTestCfg.num_ips);

    /* close pktio channels we opened via open_pktio_tx_channels() */
    close_pktio_channels();
    
    netapi_shutdown(netapi_handle);

}

