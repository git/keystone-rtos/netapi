#ifndef __NET_TEST_UTILS_H__
#define __NET_TEST_UTILS_H__


#include "net_test.h"


typedef void (*NET_TEST_FUNC_PTR)(int thread_num);


void netTest_utilCreateSpFpThreads(uint8_t num_sp_threads,
                                   NET_TEST_FUNC_PTR slow_path_thread,
                                   uint8_t num_fp_threads,
                                   NET_TEST_FUNC_PTR fast_path_thread);


void netTest_utilRemoveSpFpThreads(uint8_t num_sp_threads,
                                   uint8_t num_fp_threads);


#endif
