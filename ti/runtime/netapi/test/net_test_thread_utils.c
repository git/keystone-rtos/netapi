/******************************************
 * File: net_test_thread_utils.c
 * Purpose: net_test application thread utilities
 **************************************************************
 * FILE:  net_test_thread_utils.c
 * 
 * DESCRIPTION:  net_test application thread utilities
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************/

#include "net_test.h"
#include <signal.h>
#include <pthread.h>
#include "router.h"
#include "net_test_thread_utils.h"
extern netTestConfig_t netTestCfg;


/* Global definitions */
pthread_t *sp_threads;
pthread_t *fp_threads;

void netTest_utilCreateSpFpThreads(uint8_t num_sp_threads,NET_TEST_FUNC_PTR slow_path_thread,
                                   uint8_t num_fp_threads, NET_TEST_FUNC_PTR fast_path_thread)
{
    int i;

    sp_threads = malloc( sizeof( pthread_t ) * num_sp_threads );
    if (sp_threads == NULL)
    {
        perror( "malloc" );
        exit(1);
    }

    for (i = 0; i < num_sp_threads; i++)
    {
        if (pthread_create( &sp_threads[i], NULL, (void*)slow_path_thread,
                    (void *) i))
        {
            perror( "pthread_create" );
            exit(1);
        }
    }
    fp_threads = malloc( sizeof( pthread_t ) * num_fp_threads);
    if (fp_threads == NULL)
    {
        perror( "malloc" );
        exit(1);
    }
    for (i = 0; i < num_fp_threads; i++)
    {
        if (pthread_create( &fp_threads[i], NULL, (void*)fast_path_thread,
                    (void *) i))
        {
            perror( "pthread_create" );
            exit(1);
        }
    }
}
void netTest_utilRemoveSpFpThreads(uint8_t num_sp_threads, uint8_t num_fp_threads)
{
    int i;
    //wait for completion 
    netapi_Log("net_test loopback main task now pending on slow/fast path completion\n");
    for (i = 0; i < num_sp_threads; i++)
    {
        pthread_join( sp_threads[i], NULL );
        printf("Slow Path thread %d has exited\n", i);
    }
    for (i = 0; i < num_fp_threads; i++)
    {
        pthread_join( fp_threads[i], NULL );
        printf("Fast Path thread %d has exited\n", i);
    }
    free( sp_threads);
    free (fp_threads);
}
