/******************************************
 * File: net_test_utils.c
 * Purpose: net_test application general utilities
 **************************************************************
 * FILE:  net_test_utils.c
 * 
 * DESCRIPTION:  net_test application general utilities
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *****************************************/

#include "net_test_utils.h"
#include <signal.h>
#include <pthread.h>
#include "router.h"

#include "ti/drv/nwal/test/fw_rm.h"
#include <ti/drv/sa/salld.h>
#include <ti/drv/pa/pa.h>

extern NETAPI_T netapi_handle;
extern netTestConfig_t netTestCfg;
extern netTestConfig_t config;
extern netTestSA_t sa_info[];
extern NETAPI_SCHED_HANDLE_T * scheduler[];
extern Pktlib_HeapHandle ourHeap;
extern NETCP_CFG_IP_T ip_rule[];
extern NETCP_CFG_MACIF_T mac[];



netTestStats_T stats[NET_TEST_MAX_SP_THREAD + NET_TEST_MAX_FP_THREAD +1];
paSysStats_t netcp_stats;



static LastPktInfo_t lpInfo;
int netTest_utilCheckHeader(netTestHead_T * p_head, PKTIO_METADATA_T * p_meta)
{
#ifdef netTest_MULTI_THREAD
    int coreid=Osal_nwalGetProcId();  //who we are(thread local)
#else
    int coreid=0;
#endif
    if (NWAL_RX_FLAG1_META_DATA_VALID & p_meta->u.rx_meta->rxFlag1)
    {
        lpInfo.iface = ((unsigned int)p_meta->u.rx_meta->appId) &0xff; //last byte is interface num
        lpInfo.ipcsum =(p_meta->u.rx_meta->rxFlag1 & NWAL_RX_FLAG1_IPV4_CHKSUM_VERIFY_MASK )== NWAL_RX_FLAG1_IPV4_CHKSUM_VERIFY_ACK ? 1 : 0;
        lpInfo.l4csum = (p_meta->u.rx_meta->rxFlag1 & NWAL_RX_FLAG1_L4_CHKSUM_VERIFY_MASK )== ((NWAL_RX_FLAG1_L4_CHKSUM_VERIFY_ACK) << NWAL_RX_FLAG1_L4_CHKSUM_VERIFY_SHIFT) ? 1 : 0; 
        if ((unsigned int)p_meta->u.rx_meta->appId & NETAPI_NETCP_MATCH_IPSEC)
        {
        stats[coreid].sec_rx++;
        }
        if ((unsigned int)p_meta->u.rx_meta->appId & NETAPI_NETCP_MATCH_IPSEC_POLICY)
        {
        stats[coreid].secp_rx++;
        }

        if ((unsigned int)p_meta->u.rx_meta->appId & NETAPI_NETCP_MATCH_CLASS)
        {
            int c= ((unsigned int)p_meta->u.rx_meta->appId >>8)&0xffff;
        if (c==0)  stats[coreid].n_class0_rx +=1;
        else if (c==1) stats[coreid].n_class1_rx +=1;
        else if (c==2) stats[coreid].n_class2_rx +=1;
        else netapi_Log("**NET_TEST RX -unknown class: %x\n",  p_meta->u.rx_meta->appId);
    }
}

 return 1;
}


static int scnt=0;
int QUIT;
void netTest_utilMySig(int x)
{
    QUIT=1;
    scnt+=1;
    printf(">net_test: recv'd signal %d cnt=%d\n",x,scnt);
    if (scnt > 10)
    {
        printf(">net_test: WARNING EXITING WITH PROPER SHUTDOWN, LUTS LEFT ACTIVE\n");
        exit(1);
    }
}
unsigned long netTest_utilPeek(unsigned long * p)
{
    return *p;
}
void netTest_utilDumpDescr(unsigned long *p, int n)
{
    printf("--------dump of descriptor %d %x\n", n, (int) p);
    printf("> %x %x %x %x %x %x %x %x\n",p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]);
    printf("> %x %x %x %x %x %x %x %x\n",p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]);
    printf("-----------------------------\n");
}
void netTest_utilDumpHeader(unsigned long *p, int n, int a, int r)
{
    printf("--------dump of header %d %x appID=%x flag1=%x\n", n, (int) p,a,r);
    printf("> %x %x %x %x %x %x %x %x\n",p[0],p[1],p[2],p[3],p[4],p[5],p[6],p[7]);
    printf("> %x %x %x %x %x %x %x %x\n",p[8],p[9],p[10],p[11],p[12],p[13],p[14],p[15]);
    printf("> %x %x %x %x %x %x %x %x\n",p[16],p[17],p[18],p[19],p[20],p[21],p[22],p[23]);
    printf("> %x %x %x %x %x %x %x %x\n",p[24],p[25],p[26],p[27],p[28],p[29],p[30],p[31]);
    printf("-----------------------------\n");
}

void   netTest_utilDumpBuffer
(
    unsigned long *                      buf,
    uint32_t                      buf_length
)
{
    uint8_t                       count = 0;
    uint16_t                      dump_size;
    uint8_t*                     tmp_buf;
    uint8_t                       row_count;
    static uint8_t                first = 0;

    //if(first > 2) return;

    //first++;

    dump_size = buf_length ;

    tmp_buf = (uint8_t *)(buf);

    printf("netapi *:  - 8 bit word hex Length: %d Start \n",buf_length);
    do
    {
    row_count = (dump_size - count);

        if(row_count == 0)
        {
            break;
        }

        if(row_count > 4)
        {
            row_count = 4;
        }

        switch (row_count)
        {
            case 4:
            {
                printf("netapi *:%02d : %02x    %02x    %02x    %02x \n",
                      count,tmp_buf[0],tmp_buf[1],tmp_buf[2],tmp_buf[3]);
                break;
            }
            case 3:
            {
                printf("netapi *: %02d : %02x    %02x    %02x \n",
                      count,tmp_buf[0],tmp_buf[1],tmp_buf[2]);
                break;
            }

            case 2:
            {
                printf("netapi *: %02d : %02x    %02x \n",
                      count,tmp_buf[0],tmp_buf[1]);
                break;
            }

            case 1:
            {
                printf("netapi *: %02d : %02x \n",
                      count,tmp_buf[0]);
                break;
            }

        }

        tmp_buf = tmp_buf + row_count;
        count = count +  row_count;

    }while(count < dump_size);

    printf("netapi *:  - Byte hex Dump End \n");

}
long netTest_utilHtonl(long x)
{
    long temp = (x&0xff000000)>>24 | (x&0xff0000)>>8 | (x&0xff00)<<8 |  (x&0xff)<<24 ;
    return temp;
}

unsigned char netTest_utilHex2Dec(char *p_s)
{
    int val;
    sscanf(p_s,"%x",&val); 
    return val&0xff;
}

/********************************************************************
 *  FUNCTION PURPOSE: Ones complement addition utility
 ********************************************************************
 ********************************************************************/
uint16_t netTest_utilOnesComplementAdd (uint16_t v1, uint16_t v2)
{
  uint32_t result;

  result = (uint32_t)v1 + (uint32_t)v2;
  result = (result >> 16) + (result & 0xffff);
  result = (result >> 16) + (result & 0xffff);

  return ((uint16_t)result);
}

/********************************************************************
 *  FUNCTION PURPOSE: Ones complement checksum utility
 ********************************************************************
 ********************************************************************/
 uint16_t netTest_utilOnesCompChkSum  (uint8_t *p, uint32_t nwords)
{
  uint16_t chksum = 0;
  uint16_t v;
  uint32_t i;
  uint32_t j;

  for (i = j = 0; i < nwords; i++, j+=2)  {
    v = (p[j] << 8) | p[j+1];
    chksum = netTest_utilOnesComplementAdd (chksum, v);
  }
  return (chksum);
} /* utilOnesCompChkSum */

/**************************************************************************************
 * FUNCTION PURPOSE: Compute ipv4 psudo checksum
 **************************************************************************************
 * DESCRIPTION: Compute ipv4 psudo checksum
 **************************************************************************************/
uint16_t netTest_utilGetIPv4PsudoChkSum (uint8_t *data, uint16_t payloadLen)
{
  uint16_t psudo_chksum;

  psudo_chksum = netTest_utilOnesCompChkSum (&data[12], 4);
  psudo_chksum = netTest_utilOnesComplementAdd(psudo_chksum, (uint16_t) data[9]);
  psudo_chksum = netTest_utilOnesComplementAdd(psudo_chksum, payloadLen);

  return (psudo_chksum);

} /* utilGetIpv4PsudoChkSum */

#define CHECK_SET_PARAM(ARG1, ARG2)     \
    do { \
        if(strcmp(key, ARG1) == 0) { \
        if(d1)strncpy(ARG2,d1,netTest_CONFIG_STRING_LEN); \
        continue; \
        } \
    } while(0)

#define CHECK_SET_PARAM2(ARG1, ARG2,  ARG3)     \
    do { \
        if(strcmp(key, ARG1) == 0) { \
        if(d1) strncpy(ARG2,d1,netTest_CONFIG_STRING_LEN); \
        if(d2) strncpy(ARG3,d2,netTest_CONFIG_STRING_LEN); \
        continue; \
        } \
    } while(0)

#define CHECK_SET_PARAM_SA(ARG1, ARG2, ARG3, ARG4, ARG5, \
                                                   ARG6, ARG7, ARG8,  ARG9, ARG10, \
                                                   ARG11, ARG12,ARG13, ARG14, ARG15, ARG16,ARG17, ARG18)     \
    do { \
        if(strcmp(key, ARG1) == 0) { \
        if(d1) strncpy(ARG2,d1,netTest_CONFIG_STRING_LEN); \
        if(d2) strncpy(ARG3,d2,netTest_CONFIG_STRING_LEN); \
        if(d3) strncpy(ARG4,d3,netTest_CONFIG_STRING_LEN); \
        if(d4) strncpy(ARG5,d4,netTest_CONFIG_STRING_LEN); \
        if(d5) strncpy(ARG6, d5,netTest_CONFIG_STRING_LEN); \
        if(d6) strncpy(ARG7,d6,netTest_CONFIG_STRING_LEN); \
        if(d7) strncpy(ARG8,d7,netTest_CONFIG_STRING_LEN); \
        if(d8) strncpy(ARG9,d8,netTest_CONFIG_STRING_LEN); \
        if(d9) strncpy(ARG10,d9,netTest_CONFIG_STRING_LEN); \
        if(d10) strncpy(ARG11,d10,netTest_CONFIG_STRING_LEN); \
        if(d11) strncpy(ARG12,d11,netTest_CONFIG_STRING_LEN); \
        if(d12) strncpy(ARG13,d12,netTest_CONFIG_STRING_LEN); \
        if(d13) strncpy(ARG14,d13,netTest_CONFIG_STRING_LEN); \
        if(d14) strncpy(ARG15,d14,netTest_CONFIG_STRING_LEN); \
        if(d15) strncpy(ARG16,d15,netTest_CONFIG_STRING_LEN); \
        if(d16) strncpy(ARG17,d16,netTest_CONFIG_STRING_LEN); \
        if(d17) strncpy(ARG18,d17,netTest_CONFIG_STRING_LEN); \
        continue; \
        } \
    } while(0)

void netTest_utilPrintIPSecStats(Sa_IpsecStats_t     *p_saIpsecStats, nwal_saAALG auth, nwal_saEALG cipher)
{
#if 0
    if(retVal != nwal_OK)
    {
        System_System_netapi_Log("CORE: %d Error getting IP Sec Stats: Ret Status: %d \n",
                       retVal);
        return(nwal_FALSE);
    }
    if((p_saIpsecStats->pktEncHi) ||(p_saIpsecStats->pktEncLo))
    {
        Debug_netapi_Log("------------- IPSec TX (Encryption Channel) Stats BEGIN --\n");
    }
    else
    {
        netapi_Log("------------- IPSec RX (Decryption Channel) Stats BEGIN --\n");
    }
#endif
    printf("\nAutentication mode: %d, Encryption Mode: %d\n", auth, cipher);
    printf("IPSec replayOld:0x%x,replayDup:0x%x,authFail:0x%x \n",
                   p_saIpsecStats->replayOld,p_saIpsecStats->replayDup,p_saIpsecStats->authFail);
    printf("IPSec txESN:0x%x,rxESN:0x%x,pktEncHi:0x%x,pktEncLo:0x%x,pktDecHi:0x%x,pktDecLo:0x%x \n",
                   p_saIpsecStats->txESN,p_saIpsecStats->rxESN,p_saIpsecStats->pktEncHi,
                   p_saIpsecStats->pktEncLo,p_saIpsecStats->pktDecHi,p_saIpsecStats->pktDecLo);
}

void netTest_utilPrintDataModeStats(Sa_DataModeStats_t     *p_saDataModeStats, nwal_saAALG auth, nwal_saEALG cipher)
{

    printf("\nAutentication mode: %d, Encryption Mode: %d\n", auth, cipher);
    printf(" Packets processedHi: 0x%x, Packets processed Lo:0x%x\n",
            p_saDataModeStats->pktHi, p_saDataModeStats->pktLo);
}

/******************************************************
 * stats callback
 *******************************************************/
void netTest_utilStatsCbMt(NETAPI_T h, paSysStats_t* pPaStats)
{

#ifdef netTest_MULTI_THREAD
    int coreid=Osal_nwalGetProcId();  //who we are(thread local)
    //int coreid = our_core;
#else
int coreid=0;
#endif

  stats[coreid].n_stats_cb +=1;
  if(pPaStats) memcpy(&netcp_stats,pPaStats, sizeof(paSysStats_t));
}
void netTest_utilsStatsCb(NETAPI_T h, paSysStats_t* pPaStats)
{
    uint32_t numFreeDataPackets;
    uint32_t            numZeroBufferPackets;
    uint32_t            numPacketsinGarbage;
    Pktlib_HeapStats    pktLibHeapStats;
    int i,j;
    unsigned long long bcpp = 0;
    unsigned long long bcpp_noc = 0;
    unsigned long long bcpp_app = 0;
    unsigned long long bcpp_tx = 0;
    unsigned long long npL = 0;
    unsigned long long cyclesL = 0;
    unsigned long long ccyclesL = 0; //cache cycles
    unsigned long long tmp_npL[TUNE_NETAPI_NUM_CORES];
    unsigned long long tmp_cyclesL[TUNE_NETAPI_NUM_CORES];
    unsigned long long tmp_ccyclesL[TUNE_NETAPI_NUM_CORES]; //cache cycles
    NETAPI_SA_STATS_T netapi_sa_stats;

    uint8_t num_threads =  netTestCfg.num_fp_threads;
    for (j= 0;j < num_threads;j++)
    {
        if (netTestCfg.fp_thread_num[j] == 0)
            continue;
        i = netTestCfg.fp_thread_num[j];
        printf("Per THREAD Statistics for Thread %d\n", netTestCfg.fp_thread_num[j]);;
        printf(">*****stats @ %lld (#cbs%d) \n", hplib_mUtilGetTimestamp(),stats[i].n_stats_cb);
        printf(">itx=%d rx=%d tx=%d bad=%d slow=%d \n>rx_class0=%d rx_class1=%d rx_class2=%dsecRx=%d\t"
            "secPRX=%d sb_rx=%d sb_tx=%d auth_ok=%d sec_tx=%d  min_rx=%d min_tx=%d  fragPkt=%d, exceptionPktOther=%d\n",
         stats[i].itx, stats[i].rx, stats[i].tx, stats[i].n_bad, stats[i].n_new, 
         stats[i].n_class0_rx, stats[i].n_class1_rx,  stats[i].n_class2_rx, stats[i].sec_rx,
         stats[i].secp_rx, stats[i].sb_rx, stats[i].sb_tx, stats[i].n_auth_ok,
         stats[i].sec_tx, stats[i].rx_min, stats[i].tx_min, stats[i].exceptionPktsFrag, stats[i].exceptionPktsOther);
        printf(">if rx stats:  %d %d %d\n",stats[i].if_rx[0],stats[i].if_rx[1],stats[i].if_rx[2]);
        printf(">thread rx stats:  %d \n",stats[i].core_rx);

        if (stats[i].rx && stats[i].tx)
            printf("decrypt time per packet(avg): %lu, encrypt  time per packet(avg): %lu\n", 
            (unsigned long) stats[i].total_decrypt_time/stats[i].rx, (unsigned long) stats[i].total_encrypt_time/stats[i].tx);


         netapi_schedGetStats(scheduler[i],&tmp_npL[i],&tmp_cyclesL[i],&tmp_ccyclesL[i]);
        if (tmp_npL[i] && stats[i].rx)
        {
            bcpp = tmp_cyclesL[i]/tmp_npL[i];
            bcpp_noc = bcpp;
            bcpp_app = (stats[i].app_cycles)/stats[i].rx;
        }
        if (stats[i].tx)
        {
            bcpp_tx = (stats[i].send_cycles)/stats[i].tx;
        }
        else
        {
            bcpp_tx = 0L;
        }
        printf(">         ++ busy cycles pp=%lld (%lld wo cache ops) (app+tx= %lld) (tx= %lld) ++\n",
         bcpp,bcpp_noc,bcpp_app, bcpp_tx);
        printf("\n");
    }



if(pPaStats)
{
       printf("C1 number of packets:           %d\n", pPaStats->classify1.nPackets);
       printf("C1 number IPv4 packets:         %d\n", pPaStats->classify1.nIpv4Packets);
       printf("C1 number IPv6 packets:        %d\n", pPaStats->classify1.nIpv6Packets);
       printf("C1 number Custom packets:        %d\n", pPaStats->classify1.nCustomPackets);
       printf("C1 number SRIO packets:        %d\n", pPaStats->classify1.nSrioPackets);
       printf("C1 number LLC/SNAP Fail packets:        %d\n", pPaStats->classify1.nLlcSnapFail);
       printf("C1 number table matched:        %d\n", pPaStats->classify1.nTableMatch);
       printf("C1 number failed table matched: %d\n", pPaStats->classify1.nNoTableMatch);
       printf("C1 number IP Fragmented packets: %d\n", pPaStats->classify1.nIpFrag);
       printf("C1 number IP Depth Overflow: %d\n", pPaStats->classify1.nIpDepthOverflow);
       printf("C1 number VLAN Depth Overflow: %d\n", pPaStats->classify1.nVlanDepthOverflow);
       printf("C1 number GRE Depth Overflow: %d\n", pPaStats->classify1.nGreDepthOverflow);
       printf("C1 number MPLS Packets: %d\n", pPaStats->classify1.nMplsPackets);
       printf ("C1 number of parse fail:        %d\n",pPaStats->classify1.nParseFail);
       printf("C1 number of Invalid IPv6 Opt:  %d\n", pPaStats->classify1.nInvalidIPv6Opt);
       printf("C1 number of TX IP Fragments:  %d\n", pPaStats->classify1.nTxIpFrag);
       printf ("C1 number of silent discard:    %d\n",pPaStats->classify1.nSilentDiscard);
       printf("C1 number of invalid control:   %d\n", pPaStats->classify1.nInvalidControl);
       printf ("C1 number of invalid states:    %d\n",pPaStats->classify1.nInvalidState);
       printf ("C1 number of system fails:      %d\n",pPaStats->classify1.nSystemFail);
       printf ("C2 number Packets  :           %d\n",pPaStats->classify2.nPackets);
       printf ("C2 number udp           :      %d\n",pPaStats->classify2.nUdp);
       printf ("C2 number tcp           :      %d\n",pPaStats->classify2.nTcp);
       printf ("C2 number Custom       :      %d\n",pPaStats->classify2.nCustom);
       printf ("C2 number silent drop   :      %d\n",pPaStats->classify2.nSilentDiscard);
       printf ("C2 number invalid cntrl :      %d\n\n",pPaStats->classify2.nInvalidControl);
       printf ("C2 number Modify Stats Cmd Fail :      %d\n\n",pPaStats->modify.nCommandFail);
}
Pktlib_getHeapStats(ourHeap, &pktLibHeapStats);
printf("main heap stats>  #free=%d #zb=%d #garbage=%d\n", pktLibHeapStats.numFreeDataPackets,
                                pktLibHeapStats.numZeroBufferPackets, pktLibHeapStats.numPacketsinGarbage);
printf("               >  #dataBufThreshStatus=%d #dataBufStarvCounter=%d #zBufThreshStatus=%d #zBufStarvCounter=%d \n", 
                        pktLibHeapStats.dataBufferThresholdStatus,pktLibHeapStats.dataBufferStarvationCounter,
                        pktLibHeapStats.zeroDataBufferThresholdStatus, pktLibHeapStats.zeroDataBufferStarvationCounter);

#if 0
netapi_Log("pa2sa descriptor area dump\n");
for(i=0;i<TUNE_NETAPI_CONFIG_MAX_PA_TO_SA_DESC;i++)
{
   extern long * pa2sa_descr_base;
   long * tip= &pa2sa_descr_base[32*i]; 
   netTest_utilDumpDescr(tip, i);
}
netapi_Log("sa2pa descriptor area dump\n");
for(i=0;i<TUNE_NETAPI_CONFIG_MAX_SA_TO_PA_DESC;i++)
{
   extern long * sa2pa_descr_base;
   long * tip= &sa2pa_descr_base[32*i]; 
   netTest_utilDumpDescr(tip, i);
}
#endif
    for (i = 0; i < netTestCfg.num_sa; i++)
    {
        /* Statistics for RX Tunnel */
        memset(&netapi_sa_stats, 0, sizeof(netapi_sa_stats));
        if (netTestCfg.sa[i].dir ==NWAL_SA_DIR_INBOUND )
        {
            netapi_getSaStats(netapi_handle, sa_info[i].rx_tunnel, &netapi_sa_stats);
            if (netapi_sa_stats.validParams & NETAPI_IPSEC_STAT_VALID)
            {
                netTest_utilPrintIPSecStats(&(netapi_sa_stats.saIpsecStats), 
                                 netTestCfg.sa[i].authMode,
                                 netTestCfg.sa[i].cipherMode);
            }
            if (netapi_sa_stats.validParams & NETAPI_SIDEBAND_DATA_MODE_STAT_VALID)
            {
                netTest_utilPrintDataModeStats(&(netapi_sa_stats.dataModeStats),
                                   netTestCfg.sa[i].authMode,
                                   netTestCfg.sa[i].cipherMode);
            }
        }
        else if (netTestCfg.sa[i].dir ==NWAL_SA_DIR_OUTBOUND)
        {
            netapi_getSaStats(netapi_handle, sa_info[i].tx_tunnel, &netapi_sa_stats);
            if (netapi_sa_stats.validParams & NETAPI_IPSEC_STAT_VALID)
            {
                 netTest_utilPrintIPSecStats(&(netapi_sa_stats.saIpsecStats), 
                                   netTestCfg.sa[i].authMode,
                                   netTestCfg.sa[i].cipherMode);
            }
            if (netapi_sa_stats.validParams & NETAPI_SIDEBAND_DATA_MODE_STAT_VALID)
            {
                netTest_utilPrintDataModeStats(&(netapi_sa_stats.dataModeStats),
                                    netTestCfg.sa[i].authMode,
                                    netTestCfg.sa[i].cipherMode);
            }
        }
        else
            netapi_Log("netTest_utilsStatsCb: invalid SA direction\n");
    }
    netapi_dump_internal_heap_stats();
}




void parse_dsp_mac(char * p_mac_str)
{
    if (strlen(&p_mac_str[0]))
    {
        sscanf(p_mac_str,"mac%d",&netTestCfg.dsp_mac);
    }
}

void parse_dsp_ip(char * p_ip_str)
{
    if (strlen(&p_ip_str[0]))
    {
        sscanf(p_ip_str,"ip%d",&netTestCfg.dsp_ip);
    }
}

void netTest_utilParseOneKey(char *p_key_str, unsigned char *p_key)
{
    int index = 0;
    int i;
    if (strlen(&p_key_str[0]))
    {
        char * pch = strtok (&p_key_str[0],",");

        while (pch != NULL)
        {
            p_key[index] = netTest_utilHex2Dec(pch);
            index++;
            pch = strtok (NULL,",");
        }
    }
}

void netTest_utilParseOneIP(char * p_ip_addr_str, unsigned char * p_ip)
{
    int index = 0;
    int i;
    if (strlen(&p_ip_addr_str[0]))
    {
        char * pch = strtok (&p_ip_addr_str[0],".");

        while (pch != NULL)
        {
            p_ip[index] = atoi(pch);
            index++;
            pch = strtok (NULL,".");
        }
    }
}

void netTest_utilParseOneMac(char * p_mac_str, unsigned char *p_mac)
{
    int index = 0;
    int i;
    if (strlen(&p_mac_str[0]))
    {
        char *pch = strtok (&(p_mac_str[0]),"-");

        while (pch != NULL)
        {
            p_mac[index] = netTest_utilHex2Dec(pch);
            index++;
            pch = strtok (NULL,"-");
        }
    }
}

void netTest_utilParseMac(netTestConfigFile_t *pConfig)
{
    int i;
    int port = 0;
    for(i=0;i<NET_TEST_MAX_MAC;i++)
    {
        if (strlen(&pConfig->mac[i][0]))
        {
            netTestCfg.num_macs++;
            netTest_utilParseOneMac(&pConfig->mac[i][0],&netTestCfg.mac[i][0]);
            sscanf(&pConfig->switch_port[i][0],"swp%d",&netTestCfg.switch_port[i]);
        }
    }
    netapi_Log("netTest_utilParseMac: number of mac address %d\n", netTestCfg.num_macs);

}
void netTest_utilParseIP(netTestConfigFile_t *pConfig)
{
    int i;

    for(i=0;i<NET_TEST_MAX_IP;i++)
    {
        if (strlen(&pConfig->ip[i][0]))
        {
            netTestCfg.num_ips++;
            netTest_utilParseOneIP(&pConfig->ip[i][0],&netTestCfg.ip[i].ipv4[0]);
            sscanf(&pConfig->attach_iface[i][0],"mac%d",&netTestCfg.attach_iface[i]);
        }
    }
    netapi_Log("netTest_utilParseIP: number of ip address %d\n", netTestCfg.num_ips);
}
void netTest_utilParseIpsecMode(netTestConfigFile_t *pConfig)
{

    if (strlen(&pConfig->ipsec_mode_rx[0]))
    {
        if (strcmp(pConfig->ipsec_mode_rx, "SIDEBAND") == 0)
        {
            netTestCfg.ipsec_mode_rx = IPSEC_MODE_RX_SIDEBAND;
        }
        else if (strcmp(pConfig->ipsec_mode_rx, "INFLOW") == 0)
        {
            netTestCfg.ipsec_mode_rx = IPSEC_MODE_RX_INFLOW;
        }
        else
        {
            netapi_Log("netTest_utilParseIpsecMode(), invalid RX ipsec mode in config file \n");
        }
    }

    if (strlen(&pConfig->ipsec_mode_tx[0]))
    {
        if (strcmp(pConfig->ipsec_mode_tx, "SIDEBAND") == 0)
        {
            netTestCfg.ipsec_mode_tx = IPSEC_MODE_TX_SIDEBAND;
        }
        else if (strcmp(pConfig->ipsec_mode_tx, "INFLOW") == 0)
        {
            netTestCfg.ipsec_mode_tx = IPSEC_MODE_TX_INFLOW;
        }
        else
        {
            netapi_Log("netTest_utilParseIpsecMode(), invalid TX ipsec mode in config file \n");
        }
    }

}

void netTest_utilParseAuthMode(char *auth_mode_str, nwal_saAALG *auth_mode)
{
    if (strlen(auth_mode_str))
    {
        netapi_Log("strlen of auth_mode_str is %d\n", strlen(auth_mode_str));

        if (strcmp(auth_mode_str, "NULL") == 0)
        {
            *auth_mode= NWAL_SA_AALG_NULL;
        }
        else if (strcmp(auth_mode_str, "HMAC_MD5") == 0)
        {
            *auth_mode= NWAL_SA_AALG_HMAC_MD5;
        }
        else if (strcmp(auth_mode_str, "HMAC_SHA1") == 0)
        {
            *auth_mode= NWAL_SA_AALG_HMAC_SHA1;
        }
        else if (strcmp(auth_mode_str, "HMAC_SHA2_224") == 0)
        {
            *auth_mode= NWAL_SA_AALG_HMAC_SHA2_224;
        }
        else if (strcmp(auth_mode_str, "HMAC_SHA2_256") == 0)
        {
            *auth_mode= NWAL_SA_AALG_HMAC_SHA2_256;
        }
        else if (strcmp(auth_mode_str, "GMAC") == 0)
        {
            *auth_mode= NWAL_SA_AALG_GMAC;
        }
        else if (strcmp(auth_mode_str, "AES_XCBC") == 0)
        {
            *auth_mode= NWAL_SA_AALG_AES_XCBC;
        }
        else
        {
            netapi_Log("netTest_utilParseAuthMode: invalid auth mode specified\n");
        }
    }
}

void netTest_utilParseEncryptMode(char *ency_mode_str, nwal_saEALG*encr_mode)
{
    if (strlen(ency_mode_str))
    {
        if (strcmp(ency_mode_str, "NULL") == 0)
        {
            *encr_mode= NWAL_SA_EALG_NULL;
        }
        else if (strcmp(ency_mode_str, "AES_CTR") == 0)
        {
            *encr_mode= NWAL_SA_EALG_AES_CTR;
        }
        else if (strcmp(ency_mode_str, "AES_CBC") == 0)
        {
            *encr_mode= NWAL_SA_EALG_AES_CBC;
        }
        else if (strcmp(ency_mode_str, "3DES_CBC") == 0)
        {
            *encr_mode= NWAL_SA_EALG_3DES_CBC;
        }
        else if (strcmp(ency_mode_str, "AES_CCM") == 0)
        {
            *encr_mode= NWAL_SA_EALG_AES_CCM;
        }
        else if (strcmp(ency_mode_str, "AES_GCM") == 0)
        {
            *encr_mode= NWAL_SA_EALG_AES_GCM;
        }
        else if (strcmp(ency_mode_str, "AES_XCBC") == 0)
        {
            *encr_mode= NWAL_SA_AALG_AES_XCBC;
        }
        else
        {
            netapi_Log("netTest_utilParseEncryptMode: invalid auth mode specified\n");
        }
    }
}

void netTest_utilParseProto(char *proto_str, nwal_IpSecProto *proto)
{
    if (strlen(proto_str))
    {
        if (strcmp(proto_str, "ESP") == 0)
        {
            *proto= nwal_IpSecProtoESP;
            netapi_Log("netTest_utilParseProto(): setting proto  to ESP\n");
        }
        else if (strcmp(proto_str, "AH") == 0)
        {
            *proto = nwal_IpSecProtoAH;
            netapi_Log("netTest_utilParseProto(): setting proto to AH\n");
        }
        else
        {
            netapi_Log("netTest_utilParseProto(), invalid RX ipsec mode in config file \n");
        }
    }
}



void netTest_utilParseSaMode(char *mode_str, nwal_saMode *mode)
{
    if (strlen(mode_str))
    {
        if (strcmp(mode_str, "TUNNEL") == 0)
        {
            *mode= nwal_SA_MODE_TUNNEL;
        }
        else if (strcmp(mode_str, "TRANSPORT") == 0)
        {
            *mode = nwal_SA_MODE_TRANSPORT;
        }
        else
        {
            netapi_Log("netTest_utilParseSaMode(), invalid RX ipsec mode in config file \n");
        }
    }
}

void netTest_utilParseIPType(char *ip_type_str, nwal_IpType *ipType)
{
    if (strlen(ip_type_str))
    {
        if (strcmp(ip_type_str, "IPV4") == 0)
        {
            *ipType= nwal_IPV4;
        }
        else if (strcmp(ip_type_str, "IPV6") == 0)
        {
            *ipType = nwal_IPV6;
        }
        else
        {
            netapi_Log("netTest_utilParseIPType(), invalid RX ipsec mode in config file \n");
        }
    }
}


void parse_simple_param_u8(char* input_str, uint8_t *val)
{
    if (strlen(input_str))
    {
        *val = (uint8_t)strtol(input_str, NULL, 0);
    }
}

void parse_simple_param_u16(char* input_str, uint16_t *val)
{
    if (strlen(input_str))
    {
        *val = (uint16_t)strtol(input_str, NULL, 0);
    }
}

void parse_simple_param_u32(char* input_str, uint32_t *val)
{
    if (strlen(input_str))
    {
        *val = (uint32_t)strtol(input_str, NULL, 0);
    }
}

void netTest_utilParseSADir(char* dir_str, nwal_SaDir *dir)
{
    if (strlen(dir_str))
    {
        if (strcmp(dir_str, "INBOUND") == 0)
        {
            *dir= NWAL_SA_DIR_INBOUND;
        }
        else if (strcmp(dir_str, "OUTBOUND") == 0)
        {
            *dir = NWAL_SA_DIR_OUTBOUND;
        }
        else
            netapi_Log("netTest_utilParseSADir: invalid direction\n");
    }
}
void netTest_utilParseThreadConfig(char * p_thread_str, uint8_t* start, uint8_t* end)
{
    int index = 0;
    int i;

    if (strlen(p_thread_str))
    {
        char *pch = strtok (&(p_thread_str[0]),"-");
        *start = atoi(pch);
        pch = strtok (NULL,"-");
        *end = atoi(pch);
    }
}

void netTest_utilParseThreadParams(netTestConfigFile_t *pConfig)
{
    int i;
    for(i=0;i<NET_TEST_MAX_FP_THREAD;i++)
    {
        if (strlen(&pConfig->fp[i][0]))
        {
            netTestCfg.num_fp_threads++;
            netTest_utilParseThreadConfig(&pConfig->fp[i][0],
                                         (uint8_t*)&netTestCfg.fp_proc_start[i],
                                         (uint8_t*)&netTestCfg.fp_proc_end[i]);
        }
        if (strlen(&pConfig->fp_thread_num[i][0]))
        {
            netTestCfg.fp_thread_num[i] = atoi((char*)(&pConfig->fp_thread_num[i]));
        }
    }

    for(i=0;i<NET_TEST_MAX_SP_THREAD;i++)
    {
        if (strlen(&pConfig->sp[i][0]))
        {
            netTestCfg.num_sp_threads++;
            netTest_utilParseThreadConfig(&pConfig->sp[i][0],
                                         (uint8_t*)&netTestCfg.sp_proc_start[i],
                                         (uint8_t*)&netTestCfg.sp_proc_end[i]);
        }
        if (strlen(&pConfig->sp_thread_num[i][0]))
        {
            netTestCfg.sp_thread_num[i] = atoi((char*)(&pConfig->sp_thread_num[i]));
        }
    }
}
void netTest_utilParseSA(netTestConfigFile_t *pConfig)
{
    int i;
    for(i=0;i<MAX_SEC_INDEX;i++)
    {
        if (strlen((char*)&pConfig->sa_config[i][0].dir))
        {
            netTest_utilParseSADir((char*) &pConfig->sa_config[i][0].dir, &netTestCfg.sa[i].dir);
            parse_simple_param_u32((char*)&pConfig->sa_config[i][0].spi, &netTestCfg.sa[i].spi);
            netTest_utilParseProto((char*)&pConfig->sa_config[i][0].proto, &netTestCfg.sa[i].proto);
            netTest_utilParseSaMode((char*)&pConfig->sa_config[i][0].saMode, &netTestCfg.sa[i].saMode);
            netTest_utilParseIPType((char*)&pConfig->sa_config[i][0].ipType, &netTestCfg.sa[i].ipType);
            netTest_utilParseOneIP((char*)&pConfig->sa_config[i][0].src, (unsigned char *)&netTestCfg.sa[i].src);
            netTest_utilParseOneIP((char*)&pConfig->sa_config[i][0].dst, (unsigned char *)&netTestCfg.sa[i].dst);
            parse_simple_param_u32((char*)&pConfig->sa_config[i][0].replayWindow, &netTestCfg.sa[i].replayWindow);
            netTest_utilParseAuthMode((char*)&pConfig->sa_config[i][0].authMode, &netTestCfg.sa[i].authMode);
            netTest_utilParseEncryptMode((char*)&pConfig->sa_config[i][0].cipherMode, &netTestCfg.sa[i].cipherMode);
            parse_simple_param_u32((char*)&pConfig->sa_config[i][0].esnLo, &netTestCfg.sa[i].esnLo);
            parse_simple_param_u32((char*)&pConfig->sa_config[i][0].esnHi, &netTestCfg.sa[i].esnHi);
            parse_simple_param_u16((char*)&pConfig->sa_config[i][0].encKeySize, &netTestCfg.key_params[i].encKeySize);
            parse_simple_param_u16((char*)&pConfig->sa_config[i][0].macKeySize, &netTestCfg.key_params[i].macKeySize);


            netTest_utilParseOneKey((char*) &pConfig->sa_config[i][0].encr_key, &netTestCfg.encr_key[i][0]);
            netTestCfg.key_params[i].pEncKey = (uint8_t*)&netTestCfg.encr_key[i][0];

            netTest_utilParseOneKey((char*)&pConfig->sa_config[i][0].auth_key, &netTestCfg.auth_key[i][0]);
            netTestCfg.key_params[i].pAuthKey= (uint8_t*)&netTestCfg.auth_key[i][0];


            parse_simple_param_u32((char*)&pConfig->sa_config[i][0].tunnel_id, &netTestCfg.tunnel_id[i]);

            netTestCfg.num_sa++;
        }
    }
}
void netTest_utilParseRoutes(netTestConfigFile_t *pConfig, OUR_ROUTE_T *routes, Trie ** our_router)
{
    int i;
    int said=0;
    for(i=0;i<MAX_ROUTES;i++)
    {
        int port;
        if (pConfig->routes[i][0])
        {
            port=atoi(&pConfig->ports[i][0]);
            if((port<1)||(port>2)) continue; //bad port #: only 1 or 2 valid

            if(strncmp(&pConfig->routes[i][0],"MAC",3)==0)
            {
               routes[i].out_port = port;
               netTest_utilParseOneMac(&pConfig->routes[i][3],&routes[i].out_mac[0]);
               //memcpy(&routes[i].out_mac[6], ((port==1) ?&config.mac0[0]: &config.mac1[0] ),6); 
               memcpy(&routes[i].out_mac[6], ((port==1) ?&netTestCfg.mac[0][0]: &netTestCfg.mac[1][0] ),6);
               routes[i].out_mac[12]=0x08;
               routes[i].out_mac[13]=0x00;
               routes[i].sec_ptr=NULL;
            }
            else if (strncmp(&pConfig->routes[i][0],"SA",2)==0)
            {      
               said=atoi(&pConfig->routes[i][2]) ;
               routes[i].sec_ptr=&sa_info[said];
            }
        }
    }
    *our_router = route_init();
    for (i=0;i<MAX_ROUTES;i++)
    {
      unsigned long ip_be;
      int route_index;
      if (pConfig->dst_ips[i][0])
      {
         netTest_utilParseOneIP(&pConfig->dst_ips[i][0],(unsigned char *)&ip_be);
         sscanf(&pConfig->paths[i][0],"route%d",&route_index);
         route_add(*our_router,&ip_be,(void*)&routes[route_index]);
      }
    }
}

void netTest_utilProcessConfigFile(FILE * fpr, netTestConfigFile_t *pConfig)
{
    char line[MAX_LINE_LENGTH + 1];
    int i;
    static int sa_count = 0;
 
    char *key, *ep;
    char * d1, *d2, *d3, *d4;
    char * d5, *d6, *d7, *d8;
    char * d9, *d10, *d11, *d12;
    char * d13, *d14, *d15, *d16, *d17;
    char tokens[] = " :=;\n";
    char temp_str[TEMP_STR_LEN];
    memset(line, 0, MAX_LINE_LENGTH + 1);
    memset(pConfig, 0, sizeof(netTestConfigFile_t));
    while (fgets(line, MAX_LINE_LENGTH + 1, fpr))
    {
        if(line[0]=='#')
            continue; //skip comment
        key  = (char *)strtok(line, tokens);

        d1 = (char *)strtok(NULL, tokens);
        if (!key)
            continue;
        if (!d1) 
            continue;

        if(strlen(d1) == 0) 
        {
            continue;
        }


        d2 = (char *)strtok(NULL, tokens);
        d3 = (char *)strtok(NULL, tokens);
        d4 = (char *)strtok(NULL, tokens);
        d5 = (char *)strtok(NULL, tokens);
        d6 = (char *)strtok(NULL, tokens);
        d7 = (char *)strtok(NULL, tokens);
        d8 = (char *)strtok(NULL, tokens);
        d9 = (char *)strtok(NULL, tokens);
        d10 = (char *)strtok(NULL, tokens);
        d11 = (char *)strtok(NULL, tokens);
        d12 = (char *)strtok(NULL, tokens);
        d13 = (char *)strtok(NULL, tokens);
        d14 = (char *)strtok(NULL, tokens);
        d15 = (char *)strtok(NULL, tokens);
        d16 = (char *)strtok(NULL, tokens);
        d17 = (char *)strtok(NULL, tokens);

        CHECK_SET_PARAM(netTest_INIT_CONFIG_IPSEC_MODE_RX,&(pConfig->ipsec_mode_rx[0]));
        CHECK_SET_PARAM(netTest_INIT_CONFIG_IPSEC_MODE_TX,&(pConfig->ipsec_mode_tx[0]));
        CHECK_SET_PARAM(netTest_INIT_CONFIG_IPSEC_IF_NO,&(pConfig->ipsec_if_no[0]));
        CHECK_SET_PARAM(netTest_INIT_CONFIG_DSP_MAC,&(pConfig->dsp_mac[0]));
        CHECK_SET_PARAM(netTest_INIT_CONFIG_DSP_IP,&(pConfig->dsp_ip[0]));
        CHECK_SET_PARAM(netTest_INIT_CONFIG_STATIC_LOOPBACK_PORT,&(pConfig->dest_udp_port_config[0]));


        for(i=0;i<MAX_SEC_INDEX;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "sa%d",i);
            CHECK_SET_PARAM_SA(temp_str, 
                              (char*)&pConfig->sa_config[i][0].dir,
                              (char*)&pConfig->sa_config[i][0].spi,
                              (char*)&pConfig->sa_config[i][0].proto,
                              (char*)&pConfig->sa_config[i][0].saMode,
                              (char*)&pConfig->sa_config[i][0].ipType,
                              (char*)&pConfig->sa_config[i][0].src,
                              (char*)&pConfig->sa_config[i][0].dst,
                              (char*)&pConfig->sa_config[i][0].replayWindow,
                              (char*)&pConfig->sa_config[i][0].authMode,
                              (char*)&pConfig->sa_config[i][0].cipherMode,
                              (char*)&pConfig->sa_config[i][0].esnLo,
                              (char*)&pConfig->sa_config[i][0].esnHi,
                              (char*)&pConfig->sa_config[i][0].encKeySize,
                              (char*)&pConfig->sa_config[i][0].macKeySize,
                              (char*)&pConfig->sa_config[i][0].encr_key,
                              (char*)&pConfig->sa_config[i][0].auth_key,
                              (char*)&pConfig->sa_config[i][0].tunnel_id);
        }

        for(i=0;i<MAX_ROUTES;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "route%d",i);
            CHECK_SET_PARAM2(temp_str,&pConfig->routes[i][0],&pConfig->ports[i][0] );
        }

        for(i=0;i<MAX_ROUTES;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "dstip%d",i);
            CHECK_SET_PARAM2(temp_str,&pConfig->dst_ips[i][0],&pConfig->paths[i][0] );
        }
        for(i=0;i<NET_TEST_MAX_MAC;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "mac%d",i);
            CHECK_SET_PARAM2(temp_str,&pConfig->mac[i][0],&pConfig->switch_port[i][0] );
        }
        for(i=0;i<NET_TEST_MAX_IP;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "ip%d",i);
            CHECK_SET_PARAM2(temp_str,&pConfig->ip[i][0], &pConfig->attach_iface[i][0]);
        }
        for(i=0;i<NET_TEST_MAX_FP_THREAD;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "fp%d",i);
            CHECK_SET_PARAM2(temp_str, &pConfig->fp_thread_num[i][0],&pConfig->fp[i][0]);
        }
        for(i=0;i<NET_TEST_MAX_SP_THREAD;i++)
        {
            snprintf(temp_str, TEMP_STR_LEN-1, "sp%d",i);
            CHECK_SET_PARAM2(temp_str, &pConfig->sp_thread_num[i][0],&pConfig->sp[i][0]);
        }
    }
}

//******************************************************
//use scheduling housekeeping callback to generate pkts
//******************************************************
static int done_burst=0;
void house(NETAPI_SCHED_HANDLE_T * s)
{
    Ti_Pkt * tip;
    unsigned int len;
    nwalTxPktInfo_t meta_tx = {0};
    PKTIO_METADATA_T meta = {PKTIO_META_TX,{0},0};
    int err;
    static int house_pkts_gened=0;
    int p;
    unsigned char * pIpHdr,* pData;
    unsigned int vv1,vv2,vv3;
    unsigned int sum_vv1=0;
    unsigned int sum_vv2=0;
    unsigned int sum_vv3=0;
    unsigned int sum_vv4=0;
    unsigned int sum_vv5=0;

    unsigned int nwal_flow_vv1,nwal_flow_vv2;
    unsigned int nwal_sum_vv1=0;
    unsigned int nwal_sum_vv2=0;
    unsigned int nwal_sum_vv3=0;
    unsigned int nwal_sum_vv4=0;
    unsigned int nwal_sum_vv5=0;
    unsigned int nwal_sum_vv6=0;

    unsigned int nwal_sum_flow_vv1=0;
    unsigned int nwal_sum_flow_vv2=0;
    unsigned long long cache_op_b1;
    unsigned long long  cache_op_b2;
    unsigned long long  n_c_ops;
    static int first =0;
    Cppi_HostDesc*      pPktDesc;
    NETAPI_SCHED_SHUTDOWN_T sched_shutdown;

    uint32_t coreid = 0;  //who we are
#ifdef netTest_MULTI_THREAD
    NETAPI_T nh= netapi_schedGetHandle(s);
    coreid=(uint32_t) netapi_getCookie(nh);

    if (QUIT) 
    {
        sched_shutdown.shutdown_type = NETAPI_SCHED_SHUTDOWN_NOW;
        netapi_schedClose(s,&sched_shutdown,&err); 
        return;
    }

    
    /* only slow path threads get netcp stats */
    if (coreid & NET_TEST_SP_THREAD_MASK)
    {
        netapi_netcpCfgReqStats(nh, netTest_utilStatsCbMt, 0,&err); 
    }

    coreid = NET_TEST_THREAD_NUM_MASK & coreid;
#else
    coreid = NET_TEST_THREAD_NUM_MASK & coreid;
    if(done_burst)
    {
        house_pkts_gened+=TX_BURST;
        netapi_Log("net_test> request stats at n=%d \n",house_pkts_gened);
        netapi_netcpCfgReqStats(netapi_handle, netTest_utilsStatsCb, 0,&err);
        if (err!=0) {netapi_Log("stats req failed\n");}
        if (house_pkts_gened >= np2process+ 100)
        {
            sched_shutdown.shutdown_type = NETAPI_SCHED_SHUTDOWN_NOW;
            netapi_schedClose(s,&sched_shutdown,&err);
        }
        return;
    }
    done_burst=1;
    Osal_cache_op_measure_reset();
    memset(&meta_tx,0,sizeof(meta_tx));
    for(p=0;p<TX_BURST;p++) {  
    //reguest stats 
    if ((house_pkts_gened>0) && (! (house_pkts_gened%1000)) )
    {
       netapi_Log("net_test> request stats at n=%d \n",house_pkts_gened);
       netapi_netcpCfgReqStats(netapi_handle, netTest_utilsStatsCb, 0,&err); 
       if (err!=0) {netapi_Log("stats req failed\n");}
    }


    if (house_pkts_gened >= np2process+ 100)
    {
        //shutdown
        sched_shutdown.shutdown_type = NETAPI_SCHED_SHUTDOWN_NOW;
        netapi_schedClose(s,&sched_shutdown,&err);
        continue;
    }

    else if (house_pkts_gened >= np2process) { house_pkts_gened+=1;  continue;}

    /* manufacture a pkt to transmit */
    tip = get_pkt(house_pkts_gened, &len, ourHeap, netTest_PKT_LEN,&testPkt[0]  , TEST_netTest_PKT_LEN);
    if(!tip) { house_pkts_gened +=1; continue; }


    /* set the pkt length */
    vv1 = hplib_mUtilGetPmuCCNT();
    Pktlib_setPacketLen(tip, len);

    /* set up meta data */
    meta.sa_handle=nwal_HANDLE_INVALID;
    /* #define BENCH_UDP_SEND */
#ifdef BEND_UDP_SEND
    meta_tx.txFlag1 = (NWAL_TX_FLAG1_DO_IPV4_CHKSUM | NWAL_TX_FLAG1_DO_UDP_CHKSUM| NWAL_TX_FLAG1_META_DATA_VALID);
    meta_tx.startOffset = 0;
    /* GONE in V2 meta_tx.pktLen = len; */
    meta_tx.ipOffBytes = TEST_PKT_IP_OFFSET_BYTES;
    meta_tx.l4OffBytes = TEST_PKT_UDP_OFFSET_BYTES;
    meta_tx.l4HdrLen = TEST_PKT_UDP_HDR_LEN;
    //GONE in V2 meta_tx.ploadOffBytes = TEST_PKT_PLOAD_OFFSET_BYTES;
    meta_tx.ploadLen = TEST_PAYLOAD_LEN;

    Pktlib_getDataBuffer(tip,&pData,&len);
    if(house_pkts_gened &0x1)
    {
        memcpy(&pData[6],&netTestCfg.mac[1][0] ,6);
    }
    pIpHdr = pData + meta_tx.ipOffBytes;
    meta_tx.pseudoHdrChecksum =
        netTest_utilGetIPv4PsudoChkSum(pIpHdr,(TEST_PAYLOAD_LEN+TEST_PKT_UDP_HDR_LEN));
#else
    Pktlib_getDataBuffer(tip,&pData,&len);
    if(house_pkts_gened &0x1)
    {
        memcpy(&pData[6],&netTestCfg.mac[1][0] ,6);
    }
    meta_tx.txFlag1 = NWAL_TX_FLAG1_META_DATA_VALID;
    meta_tx.startOffset = 0;
    meta_tx.ploadLen = TEST_PAYLOAD_LEN;
#endif
   /* post it to netcp tx channel*/
   meta.u.tx_meta=&meta_tx;
#ifdef DEBUG_DESC
    if (house_pkts_gened<16) netTest_utilDumpDescr((long *) tip, house_pkts_gened);
    else if (house_pkts_gened>99) netTest_utilDumpDescr((long *) tip,house_pkts_gened);
#endif

    if(!first)
    {
        first++;
        nwal_flow_vv1= hplib_mUtilGetPmuCCNT();
        if(nwal_initPSCmdInfo(pktio_mGetNwalInstance(netcp_tx_chan),
                             &meta_tx,
                             &flowPSCmdInfo) != nwal_OK)
        {
            netapi_Log("nwal_initPSCmdInfo() ERROR \n");
        }
        nwal_flow_vv2= hplib_mUtilGetPmuCCNT();
        nwal_sum_flow_vv1 += (nwal_flow_vv1-vv1); 
        nwal_sum_flow_vv2 += (nwal_flow_vv2-nwal_flow_vv1); 
    }
    cache_op_b1= Osal_cache_op_measure(&n_c_ops);
    vv2= hplib_mUtilGetPmuCCNT();
#ifdef BEND_UDP_SEND
    nwal_mCmdSetL4CkSumPort(  tip,
                             &flowPSCmdInfo,
                             TEST_PKT_UDP_OFFSET_BYTES,
                             (TEST_PKT_UDP_HDR_LEN + TEST_PAYLOAD_LEN),
                             meta_tx.pseudoHdrChecksum,
                             meta_tx.enetPort);
#else
    nwal_mCmdSetPort  (tip,
                      &flowPSCmdInfo,  //could be NULL
                      0);  //port 0 -> switch decides

#endif

    pPktDesc = Pktlib_getDescFromPacket(tip);
    /* Send the packet out to transmit Q*/
    Qmss_queuePushDescSize (flowPSCmdInfo.txQueue, 
                        pPktDesc, 
                        NWAL_DESC_SIZE);
    vv3= hplib_mUtilGetPmuCCNT();
    cache_op_b2= Osal_cache_op_measure(&n_c_ops);

    sum_vv1 += (vv2-vv1);
    if(!house_pkts_gened)
    {
        /* first packet. Take out the PS command label creation cost */
        sum_vv1 = sum_vv1 - nwal_sum_flow_vv2;
    }

    sum_vv3 += (vv3-vv2)-(long) (cache_op_b2-cache_op_b1); //sub out cache op cost

    // netapi_Log("pktio send. full=%d metadata=%d netapi_pktioSend=%d\n", vv3-vv1,  vv2-vv1,  vv3-vv2);
    stats[coreid].itx +=1;
    house_pkts_gened +=1;
    }
    {
        unsigned long long  ccycles;
        ccycles =Osal_cache_op_measure(&n_c_ops);
        if (sum_vv1) 
        {
            netapi_Log("BURST NWAL Fast send %d pkts.  metadata=%d Cmd Label Creation Cost=%d  nwal Fast Send Cost (less cacheop)= %d n_c_ops=%lld cache_op_time=%lld (pp-> %d)\n", 
              stats[coreid].itx, sum_vv1/stats[coreid].itx,  nwal_sum_flow_vv2, sum_vv3/stats[coreid].itx, 
              n_c_ops, ccycles, n_c_ops? (ccycles/(n_c_ops/2L)) : 0);
#if 0
            netapi_Log("NWAL Profile Cycles: Prof1= %d,Prof2=%d,Prof3=%d,Prof4=%d,Prof5=%d ,Prof6=%d \n",
              nwal_sum_vv1/stats[coreid].itx,nwal_sum_vv2/stats[coreid].itx,nwal_sum_vv3/stats[coreid].itx,
              nwal_sum_vv4/stats[coreid].itx,nwal_sum_vv5/stats[coreid].itx,nwal_sum_vv6/stats[coreid].itx);
  
#endif  

            if(stats[core_id].itx2)
            {
                netapi_Log("nwal_flowSend Profile Cycles: Prof1= %d,Prof2=%d \n",
                nwal_sum_flow_vv1/stats[coreid].itx2,nwal_sum_flow_vv2/stats[coreid].itx2);
            }
        }
    }
#endif
}
NETCP_CFG_ROUTE_T test_route;

void netTest_utilCreateInterfaces(uint8_t num_macs, uint8_t num_ips, Bool match_action_host)
{
    int err, i;

    if (match_action_host)
    {
        memset(&test_route, 0, sizeof(NETCP_CFG_ROUTE_T));
        test_route.valid_params = NETCP_CFG_VALID_PARAM_MATCH_ACTION_DEST;
        test_route.match_destination = NETCP_CFG_ACTION_TO_SW;
        test_route.routeType = 0;
        test_route.p_flow = NULL;
        test_route.p_dest_q = NULL;
    
        for (i = 0; i < num_macs; i++)
        {
            /* add mac intefaces */
            mac[i] = netapi_netcpCfgCreateMacInterface(
                              netapi_handle,
                              &netTestCfg.mac[i][0],
                              NULL,
                              i,
                              netTestCfg.switch_port[i],
                              (NETCP_CFG_ROUTE_HANDLE_T)  &test_route,
                              (NETCP_CFG_VLAN_T ) NULL ,  //future
                              0,
                              1, 
                              &err);
           if (err)
               netapi_Log("netapi_netcpCfgCreateMacInterface failed with error: %d\n",err);
    
        }
    }
    else
    {
        for (i = 0; i < num_macs; i++)
        {
            /* add mac intefaces */
            mac[i] = netapi_netcpCfgCreateMacInterface(
                              netapi_handle,
                              &netTestCfg.mac[i][0],
                              NULL,
                              i,
                              netTestCfg.switch_port[i],
                              (NETCP_CFG_ROUTE_HANDLE_T)  NULL,
                              (NETCP_CFG_VLAN_T ) NULL ,  //future
                              0,
                              1, 
                              &err);
           if (err)
               netapi_Log("netapi_netcpCfgCreateMacInterface failed with error: %d\n",err);
    
        }

    }
    for (i = 0; i < num_ips; i++)
    {
        //attach an IP to this interface
        ip_rule[i]=netapi_netcpCfgAddIp(
                          netapi_handle,
                          netTestCfg.attach_iface[i],
                          nwal_IPV4,
                          (nwalIpAddr_t*)&netTestCfg.ip[i].ipv4[0],
                          NULL,
                          NULL,  //all IP
                          (NETCP_CFG_ROUTE_HANDLE_T) NULL,
                          (void*)NULL,
                          &err
                          );
        if (err)
            netapi_Log("netapi_netcpCfgAddIp: add ip failed with error %d\n", err);
    }
}

void netTest_utilDeleteInterfaces(uint8_t num_macs, uint8_t num_ips)
{
    int err,i;
    //delete IPs and MAC Interfacess
    for (i = 0; i < num_ips; i++)
    {
        err = 0;
        netapi_netcpCfgDelIp(netapi_handle, netTestCfg.attach_iface[i], nwal_IPV4, 
                      NULL, NULL, ip_rule[i], &err);
    }
    for (i = 0; i < num_macs; i++)
    {
        err = 0;
        netapi_netcpCfgDelMac(netapi_handle,i,&err);
    }
    
    //netapi_netcpCfgDelMac(netapi_handle,0,&err);
    //netapi_netcpCfgDelMac(netapi_handle,1,&err);

}


