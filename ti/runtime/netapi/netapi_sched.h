/**************************************************************************************
 * FILE PURPOSE:  Netapi scheduler module
 ***************************************************************************************
 * FILE NAME:   netapi_sched.h
 *
 * DESCRIPTION: Netapi sample  scheduler header file for  user space transport library
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  @file netapi_sched.h
 *  @brief netapi scheduler header file for user space transport library
 */


#ifndef __NETAPI_SCHED__
#define __NETAPI_SCHED__
#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "ti/csl/tistdtypes.h"
#include "netapi_types.h"
#include "ti/runtime/netapi/src/netapi_loc.h"

//#include "netapi.h"

/**
 * @brief This defines the handle to the NETAPI scheduling context.
 * Each user space transport application thread/core should have its own
 * scheduler context.
 */
struct  NETAPI_SCHED_HANDLE_Tag;


/**
 *  @ingroup netapi_cb_functions
 *  @brief NETAPI_SCHED_CB   Callback function for scheduling context house keeping.
 *  This allows application to set a function to be periodically called by scheduler
 *  to perform house keeping operations. If NULL, then no callback will be invoked
 * 
 *  @details The application provides a callback function that the NETAPI scheduling context
 *  will call on  a periodic/ low priority basis.
 *  @param[in]  h   The handle to the NETAPI scheduling context
 *  @retval     none 
 *  @pre        @ref netapi_init
 */
typedef void (*NETAPI_SCHED_CB)(struct NETAPI_SCHED_HANDLE_Tag *h);

/**
 *  @ingroup sched_structures
 *  @brief NETAPI scheduler configuration structure.
 *
 *  @details Pointer to this structure is passed in the call to @ref netapi_schedOpen API.
 */
typedef struct NETAPI_SCHED_CONFIG_Tag
{
/**
 * @def NETAPI_SCHED_DURATION
 * @ingroup sched_constants
 *      Duration configuration flag for scheduler
 */
#define NETAPI_SCHED_DURATION 0x1

/**
 * @def NETAPI_SCHED_CBV
 * @ingroup sched_constants
 *      This defines that the housekeeping call back option of scheduler context has been configured.
 */
#define NETAPI_SCHED_CBV  0x2

/// @cond NOT_IMPLEMENTED
/**
 * @def NETAPI_SCHED_POWER
 * @ingroup sched_constants
 *      This defines the power option of scheduler context being configured.
 */
#define NETAPI_SCHED_POWER 0x4

/**
 * @def NETAPI_SCHED_FINE
 * @ingroup sched_constants
 *      This defines the fine tune option of scheduler context being configured.
 */
#define NETAPI_SCHED_FINE  0x8
/// @endcond
/**
 * @def NETAPI_SCHED_FOREVER
 * @ingroup sched_constants
 *      This defines is used to configure scheduler to run forever.
 */
#define NETAPI_SCHED_FOREVER 0L

    int             valid_flags;            /**< Flags used to configure the scheduler, @ref NETAPI_SCHED_DURATION,
                                                 @ref NETAPI_SCHED_CBV. */
    uint64_t        duration;               /**< Duration scheduler is configured to run, 0 == forever,
                                                 non-zero value is ticks for scheduler to run */
    NETAPI_SCHED_CB house_cb;               /**< House keeping callback */

    uint32_t        interval;               /**< How many poll loop intervals after which to call the house keeping
                                                 callback function*/

    int             power_control;          /**< Currently NOT_IMPLEMENTED  */


/**
 * @def NETAPI_SCHED_POWER_ALWAYS_OFF
 * @ingroup sched_constants
 *      This define is used to configure scheduler power_control option to be always off.
 */
#define NETAPI_SCHED_POWER_ALWAYS_OFF 0

/**
 * @def NETAPI_SCHED_POWER_ALWAYS_ON
 * @ingroup sched_constants
 *      This define is used to configure scheduler power_control option to be always on.
 */
#define NETAPI_SCHED_POWER_ALWAYS_ON 1


    int             idle_time;              /**< Currently NOT_IMPLEMENTED  */
    Bool            yield;                  /**< Option to yield in scheduling loop if no packets received
                                                 from any NETAPI PKTIO channel associated with NETAPI instance.*/
    Bool            pollCtrlQ;              /**< Option to poll the NETCP control queues associated with the NETAPI
                                                 intance in scheduling loop. */
    Bool            pollGarbageQ;           /**< Option to poll the internal heaps and any application heaps that have
                                                 been registerd with NETAPI instance. in scheduling loop. */
} NETAPI_SCHED_CONFIG_T;

/**
 *  @ingroup sched_structures
 *  @brief NETAPI scheduler statisics.
 *
 *  @details This structure is returned from call to @ref netapi_schedOpen API.
 */
typedef struct NETAPI_SCHED_STATS_Tag
{
    unsigned long long num_pkts;        /**< Number of packets processed in sheduler context */
    unsigned long long busy_cycles;     /**< Number of CPU cyles scheduler was busy processing packets */
    unsigned long long cache_cycles;    /**< Number of CPU cyles scheduler was busy in cache operations */
} NETAPI_SCHED_STATS_T;

/**
 *  @ingroup sched_structures
 *  @brief NETAPI scheduler configuration structure.
 *
 *  @details Pointer to this structure is passed in the call to @ref netapi_schedOpen API.
 */
typedef struct NETAPI_SCHED_HANDLE_Tag
{
/**
 * @def NETAPI_SCHED_STATE_CLOSE
 * @ingroup sched_constants
 *      This define indicates the state of the scheduler to be CLOSE state or idle
 */
#define NETAPI_SCHED_STATE_CLOSE 0x0

/**
 * @def NETAPI_SCHED_STATE_CLOSE_IN_PROGRESS
 * @ingroup sched_constants
 *      This define indicates the state of the scheduler is being shutdown state
 */
#define NETAPI_SCHED_STATE_CLOSE_IN_PROGRESS 1

/**
 * @def NETAPI_SCHED_STATE_OPEN
 * @ingroup sched_constants
 *      This define indicates the state of the scheduler is OPEN or running
 */
#define NETAPI_SCHED_STATE_OPEN 2


    volatile int            state;              /**< @ref NETAPI_SCHED_STATE_CLOSE ,
                                                     @ref NETAPI_SCHED_STATE_CLOSE_IN_PROGRESS ,
                                                     @ref NETAPI_SCHED_STATE_OPEN */
    void*                   back;               /**< Pointer back to NETAPI handle */
    NETAPI_SCHED_CONFIG_T   config;             /**< NETAPI scheduler configuration */
    uint64_t                start;              /**< Start time of NETAPI scheduler context */ 
    volatile int            shutdown_reason;    /**< Currently NOT_IMPLEMENTED  */
    volatile uint64_t       shutdown_time;      /**< Time till scheduler context will be shutdown/closed */
    NETAPI_SCHED_STATS_T    stats;              /**< Per scheduler satistics */
} NETAPI_SCHED_HANDLE_T;




/// @cond NOT_IMPLEMENTED
/* return codes for sched run */
#define NETAPI_SCHED_RETURN_ERR         0       /**<unknown, err */
#define NETAPI_SCHED_RETURN_TO          1       /**<returned after timeout */
#define NETAPI_SCHED_RETURN_SHUTDOWN    2       /**<returned after shutdown */
/// @endcond

/**
 *  @ingroup sched_structures
 *  @brief NETAPI scheduler shutdown structure.
 *  @details  This structure is passed an an argument for @ref netapi_schedClose.
 */
typedef struct NETAPI_SCHED_SHUTDOWN_Tag
{
/**
 * @def NETAPI_SCHED_SHUTDOWN_NOW
 * @ingroup sched_constants
 *      This define is used to shudown the scheduling context immediately 
 */
#define NETAPI_SCHED_SHUTDOWN_NOW 0

/// @cond NOT_IMPLEMENTED
/**
 * @def NETAPI_SCHED_SHUTDOWN_TO
 * @ingroup sched_constants
 *      This define is used to shudown the scheduling context after a short while.
 */
#define NETAPI_SCHED_SHUTDOWN_TO  1

/**
 * @def NETAPI_SCHED_SHUTDOWN_NEXT_IDLE
 * @ingroup sched_constants
 *      This define is used to shudown the scheduling context during next idle period
 */
#define NETAPI_SCHED_SHUTDOWN_NEXT_IDLE  2
/// @endcond
    int shutdown_type;                      /**< @ref NETAPI_SCHED_SHUTDOWN_NOW*/

    int timeout;                            /**< Currently NOT_IMPLEMENTED  */
} NETAPI_SCHED_SHUTDOWN_T;

/**
 *  @ingroup sched_functions
 *  @brief netapi_schedOpen: API to open a scheduling context.
 *
 *  @details API to open a scheduling context.
 *  @param[in]  n   the NETAPI handle, @ref NETAPI_T
 *  @param[in] p_config :pointer to @ref NETAPI_SCHED_CONFIG_T
  *  @param[out] p_err    Pointer to error code.
 *  @retval     Handle associated with created scheduler context, @ref NETAPI_SCHED_HANDLE_T
 *  @pre        @ref netapi_init 
 */
NETAPI_SCHED_HANDLE_T * netapi_schedOpen(NETAPI_T                   n,
                                         NETAPI_SCHED_CONFIG_T*     p_config,
                                         int*                       p_err);

/// @cond NOT_IMPLEMENTED
/**
 *  @ingroup sched_functions
 *  @brief netapi_schedControl: API to re-configure a scheduling context.
 *
 *  @details API to re-configure a scheduling context.
 *  @param[in]  s           The NETAPI scheduling context handle, @ref NETAPI_SCHED_HANDLE_T
 *  @param[in] p_config     Pointer to @ref NETAPI_SCHED_CONFIG_T
 *  @param[out] p_err    Pointer to error code.
 *  @retval     Handle associated with created scheduler context, @ref NETAPI_SCHED_HANDLE_T
 *  @pre        @ref netapi_schedOpen 
 */
int netapi_schedControl(NETAPI_SCHED_HANDLE_T*  s,
                        NETAPI_SCHED_CONFIG_T*  p_config,
                        int*                    p_err);
/// @endcond

/**
 *  @ingroup sched_functions
 *  @brief netapi_schedRun: API for main entry point to scheduler.
 *
 *  @details API for main entry point to scheduler. User application gives up control to scheduler.
  *  @param[in]  s           The NETAPI scheduling context handle, @ref NETAPI_SCHED_HANDLE_T
 *  @param[out] p_err: error code, zero on sucess, non-zero on failure
 *  @retval     always 1
 *  @pre        @ref netapi_schedOpen 
 */
int netapi_schedRun(NETAPI_SCHED_HANDLE_T*  s,
                    int*                    p_err);

/**
 *  @ingroup sched_functions
 *  @brief netapi_schedClose: API to close a scheduling context.
 *
 *  @details API to close a scheduling context.
 *  @param[in]  s           The NETAPI scheduling context handle, @ref NETAPI_SCHED_HANDLE_T
 *  @param[in]  p_close  @ref NETAPI_SCHED_SHUTDOWN_T
 *  @param[out] p_err    Pointer to error code.
 *  @retval     NO USED, ALWAYS 1
 *  @pre        @ref netapi_schedOpen 
 */
int netapi_schedClose(NETAPI_SCHED_HANDLE_T*    s, 
                      NETAPI_SCHED_SHUTDOWN_T*  p_close,
                      int*                      p_err);

/**
 *  @ingroup sched_functions
 *  @brief netapi_schedGetHandle: API to get the NETAPI handle from scheduling context.
 *
 *  @details API to get the NETAPI handle from scheduling context.
 *  @param[in]  s The NETAPI scheduling context handle, @ref NETAPI_SCHED_HANDLE_T
 *  @retval     Handle to NETAPI instance
 */
static NETAPI_T netapi_schedGetHandle(NETAPI_SCHED_HANDLE_T *s)
{
    return (NETAPI_T)s->back;
}

/**
 *  @ingroup sched_functions
 *  @brief netapi_schedGetStats: API to get scheduling context statistics.
 *
 *  @details API to get scheduling context statistics.
 *  @param[in]  s           The NETAPI scheduling context handle, @ref NETAPI_SCHED_HANDLE_T
 *  @param[in]  p_pkts  total number of packets processed by scheduler poll loop
 *  @param[in]  p_cycles total number cycles taken by scheduler poll loop
 *  @param[in]  p_ccycles  total number of cache control cycles taken by scheduler poll loop
 *  @retval     none
 */
void netapi_schedGetStats(NETAPI_SCHED_HANDLE_T*    s,
                          unsigned long long*       p_pkts, 
                          unsigned long long*       p_cycles,
                          unsigned long long*       p_ccycles);

#ifdef __cplusplus
}
#endif

#endif
