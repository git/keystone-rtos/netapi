/******************************************************************************
 * FILE PURPOSE:  netapi NETCP configuration API header file
 ******************************************************************************
 * FILE NAME:   netcp_cfg.h
 *
 * DESCRIPTION:netapi NETCP configuration API header  file for user space transport library
 *
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* ============================================================= */

/**
 *   @file netcp_cfg.h
 *   @brief Netapi NETCP configuration API header file for user space transport library
 */



#ifndef __NETCP_CFG__H
#define __NETCP_CFG__H

#ifdef __cplusplus
extern "C" {
#endif

//#include "netapi.h"

/**
 *  @ingroup cfg_structures
 *  @brief NETCP Flow ID configuaration informaation
 *  @details A flow defines a set of free queues (pktlib heap) for hardware to use to get
 *          free packet descriptor(s) and buffer(s) to use when receiving a packet. CPPI flow ID for
 *          default case, use NETCP_DEFAULT_FLOW
 */
typedef struct NETCP_CFG_FLOW_Tag
{
    int flowid;         /**< flow id*/
    int dma_engine;     /**< dma engine (QMSS, NETCP)*/
} NETCP_CFG_FLOW_T;


/**
 *  @ingroup cfg_structures
 *  @brief NETAPI configuration information
 *
 *  @details The parameters in this structure are used to configure NETAPI.
 */
typedef struct NETAPI_CFG_Tag
{
 int def_mem_size;                  /**<  Bytes of CMA memory we have allocated */
 int def_flow_pkt_rx_offset;        /**<  Offset in pkt buffer for hw to start RX */
 int def_max_descriptors;           /**<  Number of descriptors in system  (must be power of 2), 2^14 max */
 int def_tot_descriptors_for_us;    /**<  Number of descriptors to create in our region (must be power of 2)*/
 int def_heap_n_descriptors;        /**<  Number of descriptor plus buffers in default heap*/
 int def_heap_n_zdescriptors;       /**<  Number of zero len descriptors in defaut heap*/
 int def_heap_buf_size;             /**<  Size of buffers in default heap, max amount of area for packet data */
 int def_heap_tailroom_size;        /**<  Size of tailroom in reserve */
 int def_heap_extra_size;           /**<  Size of extra space at end of buffer */ 
 int def_multi_process;             /**<  Flag to indicate if NETAPI init is for multi-process environment */
 void *rmHandle;                    /**<  Optional: Resource Manager (RM) server handle to use RM for re                                                    souce allocations*/
 int memoryRegion;                  /**<  Optional: Specify the descriptor memory region to be used,
                                          must be a valid memory  region index
                                          (0 to Maximum number of memory regions supported
                                          Not used when using Resouce Manager(RM)as QMSS via RM will
                                          return available Memory Region Id.
                                          */
 int start_index;                   /**<  Optional: Used to leave holes by configuring dummy regions which
                                          can be later configured with actual values. May either be
                                          specified by the user (for example, to select "internal" or
                                          "external" linking RAM, or set to QMSS_START_INDEX_NOT_SPECIFIED,
                                          QMSS_PARAM_NOT_SPECIFIED to have LLD/RM select a compatible startIndex
                                          from anywhere with free indicies, or QMSS_START_INDEX_INTERNAL.
                                          for internal linking RAM, QMSS_START_INDEX_EXTERNAL for external linking RAM. 
                                          Not used when using Resouce Manager(RM)as QMSS via RM will
                                          return available Memory Region Id and determine avialable start index.
                                          */
}  NETAPI_CFG_T;

/* @note:
   each buffer will be allocated:  def_heap_buf_size+def_heap_extra_size bytes
   each descriptor attached to these buffers will have rigBufferLen of:
      def_heap_buf_size.
   for default RX flow,  for rx packet, the bufptr  will be def_flow_pkt_rx_offset.
   for detault RX flow,  threshold (ie max # of bytes in buffer) will be:
       def_heap_buf_size - def_heap_tailroom_size-def_flow_pkt_rx_offset


 RX Packet from NetCP

Headroom [Application]     Packet [HW]                Tailroom [Application]    Extra Space [Application]
<-----------------------><--------------------------><------------------------><----------------------->

Cppi_HostDesc->origBufferLen
<----------------------------------------------------------------------------->
Cppi_HostDesc->origBuffPtr
|
\/
|------------def_heap_buf_size-------------------------------------------------|--def_heap_extra_size--|
| def_flow_pkt_rx_offset| max Cppi_HostDesc->buffLen | def_heap_tailroom_size  |   Extra Size          |
                        ^
                        |
                     Cppi_HostDesc->buffPtr
*/


/**
 * @ingroup cfg_constants
 * @brief  This defines the handle to a NETCP configured Flow. A Flow is a set of pktlib heaps that h/w can use to obtain free packets.
 */
typedef NETCP_CFG_FLOW_T* NETCP_CFG_FLOW_HANDLE_T;

/**
 * @ingroup cfg_constants
 * @def NETCP_DEFAULT_FLOW
 * @brief This defines the default FLOW for NETCP to use.
 * The default flow uses the default pktlib heap created by netapi_init; i.e.
 * NETCP will allocate descriptors and buffers for received packets from this heap.
 */
#define NETCP_DEFAULT_FLOW  (NETCP_CFG_FLOW_HANDLE_T) NULL



/**
 * @ingroup cfg_constants
 * @def NETCP_DEFAULT_ROUTE
 * @brief This defines the NETCP default route.  This route has NETCP send received packets to the default NETCP 
 * pktio channel using descriptors and buffers from the default flow. The default route is created by netapi_init
 */
#define NETCP_DEFAULT_ROUTE (NETCP_CFG_ROUTE_HANDLE_T) NULL


/**
 *  @ingroup cfg_constants
 *  @{
 *
 *  @name   Valid Parameter configuration for NETCP_CFG_ROUTE_T
 *  @brief  Valid Parameter configuration
 *
 *  @details Valid Parameter to configure optional parameters.
 */
/* @{ */
/**
 *  @def  NETCP_CFG_VALID_PARAM_ROUTE_TYPE
 *        Valid Route Type configuration
 *
 */

#define NETCP_CFG_VALID_PARAM_ROUTE_TYPE        0x01
/*  @}  */
/* @{ */
/**
 *  @def  NETCP_CFG_VALID_PARAM_MATCH_ACTION_DEST
 *        Valid Match Destination configuration present,
 *        When set, NetCP to terminate packet at configured destination
 *        if classification matches as specified by match_destination of
 *        @NETCP_CFG_ROUTE_T
 *
 */
#define NETCP_CFG_VALID_PARAM_MATCH_ACTION_DEST        0x02
/*  @}  */
/* @{ */
/**
 *  @def  NETCP_CFG_VALID_PARAM_FAIL_ACTION_DEST
 *        Valid Fail Destination configuration present,
 *        When set, NetCP to terminate packet at configured destination
 *        if next route classification fails as specified by fail_destination of
 *        @NETCP_CFG_ROUTE_T
 *
 */
#define NETCP_CFG_VALID_PARAM_FAIL_ACTION_DEST          0x04
/*  @}  */
/** @} */



/**
 *  @ingroup cfg_structures
 *  @brief NETCP application defined route information.
 *  @details This structure is used to define a packet receive route.  A route consists of a 
 *           flow where to get free descriptors and buffers to hold the packet, and a destination 
 *           queue where to place the packet. Priority routing based on VLAN priority bits,
 *           DSCP/TOS, and received input port is supported. In the case
 *           of priority based routing, the PASS will forward the matchd packeed to the desired
 *           queue which is equal to the base queue plus an offset. This offset is sepcififed
 *           by the VLAN prioirty or DSCP/TOS value, or received input port.
 *
 */
typedef struct NETCP_CFG_ROUTE_Tag
{
    uint32_t            valid_params;       /**< Specifies which route config params
                                                 are valid */
    NETCP_CFG_FLOW_T    *p_flow;            /**< NULL or NETCP_DEFAULT_FLOW for default
                                              *flow,@ref NETCP_CFG_FLOW_T
                                              */
    PKTIO_HANDLE_T      *p_dest_q;          /**< NULL for default destination queue */

    nwalRouteType_t     routeType;          /**< Optional: Routing priority,
                                              *  @see nwal.h for nwalRouteType_t
                                              */
   uint16_t             egress_swith_port;  /* learned swithc port #, from ale_table */
    uint16_t            match_destination;  /**< Optional: NetCP to terminate packet at specified destination 
                                                  if classification matches
                                                  @see nwal.h for nwal_matchAction_t*/
    uint16_t            fail_destination;   /**< Optional: NetCP to terminate packet at specified destination 
                                                  if next route classification match fails 
                                                  @see nwal.h for nwal_failAction_t*/

} NETCP_CFG_ROUTE_T;


/**
 * @ingroup cfg_constants
 * @brief  Handle to a NETCP route.
 * @details Application to use this handle to identify a NETCP route. A NETCP route defines the
 *          pktio channel for packets received by NETCP
 *    and the flow to use.
 */
typedef NETCP_CFG_ROUTE_T* NETCP_CFG_ROUTE_HANDLE_T;




/**
 * @ingroup cfg_constants
 * @def NETCP_CFG_ACTION_DISCARD
 *      This defines the NETCP action to discard packet.
 */
#define NETCP_CFG_ACTION_DISCARD NWAL_MATCH_ACTION_DISCARD
/**
 * @ingroup cfg_constants
 * @def  NETCP_CFG_ACTION_CONTINUE
 *      This defines the NETCP action to pass packet ono the next classifier
 */
#define NETCP_CFG_ACTION_CONTINUE NWAL_MATCH_ACTION_CONTINUE_NEXT_ROUTE
/**
 * @ingroup cfg_constants
 * @def NETCP_CFG_ACTION_TO_SW
 *      This defines the NETCP action to pass packet to User space application
 */
#define NETCP_CFG_ACTION_TO_SW    NWAL_MATCH_ACTION_HOST

/**
 * @ingroup cfg_constants
 * @def NETCP_CFG_ALL_EXCEPTIONS
 *      This defines NETCP configuration for all Exepction Packets.
 */
#define NETCP_CFG_ALL_EXCEPTIONS 0xff

/**
 * @ingroup cfg_constants
 * @brief General APP_ID Type definition.
 */
typedef uint32_t NETCP_CFG_APP_ID_T;


/**
 * @ingroup cfg_constants
 * @brief  Handle to NETCP VLAN configuration (FUTURE).
 * @details Application to use this handle to identify a VLAN configuration.
 */
typedef void * NETCP_CFG_VLAN_T;

/**
 * @ingroup cfg_constants
 * @brief  NETCP PA LLD handle associated with an SA
 * @details Application to use this handle to identify a PA PLLD handle associated with an SA.
 */
typedef void * NETCP_CFG_PA_HANDLE_T;

/**
 * @ingroup cfg_constants
 * @brief  NETCP SA LLD handle associated with an SA
 * @details Application to use this handle to identify a SA LLD handle associated with an SA.
 */
typedef void * NETCP_CFG_SA_HANDLE_T;

/**
 * @ingroup cfg_constants
 * @brief  AppID for packets matching a  MAC interface rule
 */
typedef uint32_t NETCP_CFG_MACIF_T;

/**
 * @ingroup cfg_constants
 * @brief AppID for packets matching an IP interface rule
 */
typedef uint32_t NETCP_CFG_IP_T;

/**
 * @ingroup cfg_constants
 * @brief This define is used to identify the application id associated with a created SA (IPSEC security association) rule
 */
typedef uint32_t NETCP_CFG_SA_T;


/**
 * @ingroup cfg_constants
 * @brief AppId for packets matching an NETCP IPSEC policy rule
 */
typedef uint32_t NETCP_CFG_IPSEC_POLICY_T;



/**
 * @ingroup cfg_constants
 * @brief  AppID for packets being classified as  type exception.
 */
typedef uint32_t NETCP_CFG_EXCEPTION_PKT_T;

/**
 * @ingroup cfg_constants
 *@brief This define is to be used in AddIp, AddClassifier, addSA, etc. to indicate that the rule can be bound to any MAC address.
 */
#define NETCP_CFG_NO_INTERFACE 0xff



/**
 * @note  APPIDs are present in RX packet meta data and tell "how far" the packet got
 * through the classification rules of NETCP. 
 * APPID is 32 bits:
 * bits 31-24 = NETAPI_NETCP_MATCH_STAGE
 * bits 23-8  = NETAPI_NETCP_MATCH_ID identifier 
 * bits  7-0  = NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE logical mac interface
*/

#define NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT 0 
#define NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK 0xFF

#define NETAPI_NETCP_MATCH_ID_SHIFT 8 
#define NETAPI_NETCP_MATCH_ID_MASK 0xFFFF

#define NETAPI_NETCP_MATCH_STAGE_SHIFT 24
#define NETAPI_NETCP_MATCH_STAGE_MASK 0xFF


/**
 * @brief Helper function to get match stage associated with application ID.
 */
#define netapi_cfgGetMatchStage(appid)  (((appid) >> NETAPI_NETCP_MATCH_STAGE_SHIFT) & NETAPI_NETCP_MATCH_STAGE_MASK)

/**
 * @brief Helper function to get match id associated with application ID.
 */
#define netapi_cfgGetMatchId(appid) (((appid) >> NETAPI_NETCP_MATCH_ID_SHIFT) & NETAPI_NETCP_MATCH_ID_MASK)

/**
 * @brief Helper function to get logical match interface associated with application ID.
 */
#define netapi_cfgGetMatchLogicalMacIface(appid) (((appid) >> NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT) & \
                                                    NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK)


/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_MATCH_GENERIC_MAC
 *      This define is used for an APPID that indicates that a packet matched a MAC entry.
 *      Logical MAC interface location:
 *          Refer to NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT and 
 *          NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK.
 *      Packet did not match any other rule.
 */
#define NETAPI_NETCP_MATCH_GENERIC_MAC  0x10000000

/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_MATCH_GENERIC_IP
 *      This define is used for an APPID that indicates that a packet matched a MAC entry.
 *      Logical MAC interface location:
 *          Refer to NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT and 
 *          NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK.
 *       IP rule number for this interface location:
 *          Refer to NETAPI_NETCP_MATCH_ID_SHIFT and 
 *          NETAPI_NETCP_MATCH_ID_MASK.
 *      Packet also matched a generic IP rule attached to that interface.
 *      Packet did not match any other rule.
 */
#define NETAPI_NETCP_MATCH_GENERIC_IP   0x20000000

/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_MATCH_CLASS
 *      This define is used for an APPID that indicates that a packet matched a MAC entry.
 *      Logical MAC interface location:
 *          Refer to NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT and 
 *          NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK.
 *      Classifer ID location:
 *          Refer to NETAPI_NETCP_MATCH_ID_SHIFT and 
 *          NETAPI_NETCP_MATCH_ID_MASK.
 *      Packet also matched a generic IP rule attached to 
 *      that interface OR a general IP rule added as part of the classifier or it matched a combination
 *      of ISPEC SA rule and a policy check.  In addition, packet matched a L4 port rule that was added 
 *      as part of a classifer. Packet did not match any other rule.
 */
#define NETAPI_NETCP_MATCH_CLASS        0x80000000

/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_MATCH_CLASS_L3
 *      This define is used for an APPID that indicates that a  packet matched a MAC entry.
 *      Logical MAC interface location:
 *          Refer to NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT and 
 *          NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK.
 *      Packet also  matched a general IP rule added as part 
 *      of a classifier.  But it not match a L4 port or any other rule. 
 *      We cannot determine what classifer partially matched so Bytes 3-2 are not applicable
 */
#define NETAPI_NETCP_MATCH_CLASS_L3     0x40000000

/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_MATCH_IPSEC
 *      This define is used for an APPID that indicates that a packet matched a MAC entry.
 *      Logical MAC interface location:
 *          Refer to NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT and 
 *          NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK.
 *      SA ID location:
 *          Refer to NETAPI_NETCP_MATCH_ID_SHIFT and 
 *          NETAPI_NETCP_MATCH_ID_MASK.
 *      Packet also matched an IPSEC SA  rule (matched proto, destination ip and SPI).  
 *      Packet did not match any other rule (so may have failed a policy check)
 */
#define NETAPI_NETCP_MATCH_IPSEC        0x01000000  


/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_MATCH_IPSEC_POLICY
 *      This define is used for an APPID that indicates that a packet matched a MAC entry
 *      Logical MAC interface location:
 *          Refer to NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_SHIFT and 
 *          NETAPI_NETCP_MATCH_LOGICAL_MAC_IFACE_MASK.
 *      Packet also matched an IPSEC SA rule (matched proto, 
 *      dest ip and SPI).  Packet also matched a POLICY RULE (this is a check of the inner IP).
 *      IPSEC RX Policy ID location:
 *          Refer to NETAPI_NETCP_MATCH_ID_SHIFT and 
 *          NETAPI_NETCP_MATCH_ID_MASK.
 *      Packet did not match any other rule 
 */
#define NETAPI_NETCP_MATCH_IPSEC_POLICY 0x02000000  //lower byte==interface, Or' in SA id (16 bits)

/**
 * @ingroup cfg_constants
 * @def NETAPI_NETCP_CFG_MATCH_EXCEPTION
 *      This define is used for an APPID that indicates that a packet is of type exception. 
 *      Actual exception id is in byte 0 of APPID.
 */
#define NETAPI_NETCP_CFG_MATCH_EXCEPTION     0x08000000



/**
 *  @ingroup cfg_structures
 *  @brief NETCP flow configuration information.
 *  @details This structure is used to define key parameters for the receive flow to be created.
 *           These include the flow index to use (or can be left un-specified), the dma_index
 *           (specifying out of which CPPI DMA engine the flow should be allocated),
 *           the receive offset (the byte offset into each buffer where received data should be placed),
 *           and the drop policy for the DMA channel to use if there is no free buffer available (drop or block)
 *
 */
typedef struct NETCP_CFG_FLOW_CONFIG_Tag
{
   int              flow_index;     /**< flow index to use or NETAPI_NETCP_FLOW_INDEX_ANY */
/**
 * @def NETAPI_NETCP_FLOW_INDEX_ANY
 * @ingroup cfg_constants
 *      This define is used to let NETAPI pick the flow index to use(for flow_index field)
 */
#define NETAPI_NETCP_FLOW_INDEX_ANY  CPPI_PARAM_NOT_SPECIFIED

   int              dma_index;      /**< allocate flow out of which DMA */
/**
 * @def NETAPI_DMA_INFRASTRUCTURE
 * @ingroup cfg_constants
 *      This define is used specify a flow in the QMSS CPPI DMA (for dma_index field)
 */
#define NETAPI_DMA_INFRASTRUCTURE 0
/**
 * @def NETAPI_DMA_NETCP
 * @ingroup cfg_constants
 *      This define us usee specify a flow in the NETCP CPPI DMA (for dma_index field)
 */
#define NETAPI_DMA_NETCP 1

   int              recv_offset;    /**< start of packet offset */

   int              block;          /**< TRUE => DMA will wait for free descriptor if heap(s) are empty.
                                         FALSE => DMA will discard */
/**
 * @def NETAPI_FLOW_DROP
 * @ingroup cfg_constants
 *      This define is used to indicate that the flow should institute a Block policy.
 *      This means that the DMA should wait for a free descriptor/buffer to come available if
 *      the free poll is empty (for the block field)
 */
#define NETAPI_FLOW_BLOCK 1
/**
 * @def NETAPI_FLOW_DROP
 * @ingroup cfg_constants
 *      This define us used to indicate that the flow should institute a Drop policy.
 *      This means that the DMA should NOT wait for a free descriptor/buffer to come available
 *      if the free poll is empty. The transfer will be aborted and the data will dropped (for block field)
 */
#define NETAPI_FLOW_DROP 0

   PKTIO_HANDLE_T * p_dest_q;       /**<destination queue for this flow (may be overwrritten by source DMA) */
} NETCP_CFG_FLOW_CONFIG_T;

 /**
 *  @ingroup cfg_structures
 * @brief
 *  The structure contains the NETAPI Physical Memory Address Device configuration for
 *  QMSS and PASS Perihperals.
 *
 * @details
 *  The structure contains the NETAPI Physical Memory Address Device configuration for
 *  QMSS and PASS Perihperals.
 */
typedef struct NETCP_CFG_GLOB_DEVICE_PARAMS_Tag
{
    int         fNssGen2;           /**< 1: NSS Gen2 device */
    uint32_t    cslNetCpCfgRegs;    /**< Base address of NETCP configuration Registers */
    uint32_t    cslQmssCfgBase;     /**< Base address of QMSS configuration Registers */
    uint32_t    cslQmssDataBase;    /**< Base address of QMSS Data Registers */
    uint32_t    cslNetCpCfgSaCfgRegs;/**< Base address of SA configuration Registers */

} NETCP_CFG_GLOB_DEVICE_PARAMS_T;




/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgAddFlow   API to add a flow
 * 
 *  @details This api is used to add a flow
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  n    number of Pktlib_HeapHandle
 *  @param[in]  handles[]   Handles to Pktlib_HeapHandle
 *  @param[in]  sizes[]     must be <= heap corresponding heap size-recv_offset-any desired tail room
 *  @param[in]  p_cfg   @ref NETCP_CFG_FLOW_CONFIG_T
 *  @param[out] err     pointer to error return
 *  @retval     NETCP flow handle, @ref NETCP_CFG_FLOW_HANDLE_T
 *  @pre       @ref netapi_init
 */
NETCP_CFG_FLOW_HANDLE_T netapi_netcpCfgAddFlow(NETAPI_T h,
                                            int n, 
                                            Pktlib_HeapHandle handles[],
                                            int sizes[],
                                            NETCP_CFG_FLOW_CONFIG_T * p_cfg,
                                            int * err );

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgDelFlow   API to delete a flow
 * 
 *  @details This api is used to delete a flow.
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  p    handle to NETCP  flow
 *  @param[out] err     pointer to error return
 *  @retval     none
 *  @pre       @ref netapi_init, netapi_netcpCfgAddFlow
 */
void netapi_netcpCfgDelFlow(NETAPI_T h ,
                                            NETCP_CFG_FLOW_HANDLE_T p ,
                                            int * err);

/**
 *  @ingroup cfg_functions
 *  @brief  API attaches an IP address and qualifier to a MAC interface
 * 
 *  @details This api is used to add an IP address to a MAC interface along
 *            with optional IP qualifier. A route, @ref NETCP_CFG_ROUTE_HANDLE_T,or NULL for default 
 *            may be specified to indicate where to send packets matching the MAC interface MAC address, the
 *            supplied IP address and any qualifier.  This API adds a rule to the NETCP level 1 lookup tables.
 *            Packets arriving that match this rule are identified in meta data with Appid=  NETAPI_NETCP_MATCH_GENERIC_IP
 *            Note: An IP address must be attached to enable NETCP receive Checksum offload feature
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  iface_no    interface number (0,1,..)
 *  @param[in]  ipType  type of IP address (V4 for V6)
 *  @param[in]  ip_addr destination or local
 *  @param[in]  ip_rem_addr source or remote
 *  @param[in]  ip_qualifiers   ip_qualifiers (all 0 for no qualifiers). This can be used to apply special handling for
 *                              diffserv category for example.
 *  @param[in]  route       handle of a created route or NULL to use internal default route, @ref NETCP_CFG_ROUTE_HANDLE_T
 *  @param[in]  user_data     Optional: pointer to user provided data associated with IP
 *  @param[in]  ip_addr remote
 *  @param[out] err     pointer to error return
 
 *  @retval     returned AppID for attached rule. This is returned in RX meta data for packets matching this rule and no other, @ref NETCP_CFG_IP_T
 *  @pre       @ref netapi_init
 */
NETCP_CFG_IP_T  netapi_netcpCfgAddIp(NETAPI_T                   h,
                                     int                        iface_no,
                                     nwal_IpType                ipType,
                                     nwalIpAddr_t*              ip_addr,
                                     nwalIpAddr_t*              ip_rem_addr,
                                     nwalIpOpt_t*               ip_qualifiers,
                                     NETCP_CFG_ROUTE_HANDLE_T   route,
                                     void*                      user_data,
                                     int*                       err);

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgDelIp   API to delete IP interface
 * 
 *  @details This api is used to delete an IP interface
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  iface_no    interface number (0,1,..)
 *  @param[in]  ipType  type of IP address (V4 for V6)
 *  @param[in]  ip_addr      ip_address
 *  @param[in]  ip_qualifiers   ip_qualifiers (all 0 for no qualifiers). This can be used to apply special handling for
 *                  diffserv category for example.
 *  @param[in]  ip_rule_id      @ref NETCP_CFG_IP_T
 *  @param[out] err     pointer to error return
 *  @retval     returned AppID for attached rule. This is returned in RX meta data for packets matching this rule and no other, @ref NETCP_CFG_IP_T
 *  @pre       @ref netapi_init , @ref netapi_netcpCfgAddIp
 */
void netapi_netcpCfgDelIp(NETAPI_T          h, 
                          int               iface_no,
                          nwal_IpType       ipType,
                          nwalIpAddr_t*     ip_addr,
                          nwalIpOpt_t*      ip_qualifiers, 
                          NETCP_CFG_IP_T    ip_rule_id,
                          int*              err);

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgCreateMacInterface  API to insert a MAC interface rule in the NETCP hardware
 *  lookup engines.
 * 
 *  @details This api is used to insert a MAC interface in the NETCP hardware lookup engines.
 *      Once it is created, the MAC interface can be used to receive packets. The API
 *      adds a rule to the NETCP 1st level lookup tables to route all packets with destination
 *      MAC matching supplied argument and not matching any other lookup entry (see @ref netapi_netcpCfgAddIp) to
 *      the supplied route, @ref NETCP_CFG_ROUTE_T, (or default route).
 *      Packets arriving that match this rule are identified in meta data with Appid=  NETAPI_NETCP_MATCH_GENERIC_MAC
 *      Note: The internal SOC switch (if operating in full swithc mode) may need to  be "taught" that this mac
 *      address is present by transmitting a packet with destination mac = this interface mac address.
 *  @param[in]  h   NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  p_mac   pointer to 6 byte MAC address for local interface
 *  @param[in]  p_mac_remote   pointer to 6 byte MAC address for remote interface
 *  @param[in]  iface_no    interface number (0,1,..) 
 *  @param[in]  switch_port     (0 don't care, 1 switch port 1, 1 switch port 2) [only 0 supported currenly] 
 *  @param[in]  route   handle of a created route or NULL to use internal default route, @ref NETCP_CFG_ROUTE_HANDLE_T
 *  @param[in]  vlan    [future[ vlan configuration . Set to NULL, @ref NETCP_CFG_VLAN_T
 *  @param[in]  etherType  Ethertype field.

 *  @param[in]  state   [future] interface state (0=down, 1= up)
 *  @param[out] err     pointer to error return
 *  @retval     returns AppID for interface (this is returned in meta data for received packets matching this rule an no others, @ref NETCP_CFG_MACIF_T
 *  @pre       @ref netapi_init 
 */
NETCP_CFG_MACIF_T  netapi_netcpCfgCreateMacInterface(NETAPI_T                   h,
                                                     uint8_t*                   p_mac,
                                                     uint8_t*                   p_mac_remote,
                                                     int                        iface_no,
                                                     int                        switch_port,
                                                     NETCP_CFG_ROUTE_HANDLE_T   route,
                                                     NETCP_CFG_VLAN_T           vlan,
                                                     uint16_t                   etherType,
                                                     int                        state,
                                                     int *                      err);

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgDelMac   API to delete MAC  interface
 * 
 *  @details This api is used to delete a MAC interface
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  iface_no    interface number (0,1,..)
 *  @param[out] err     pointer to error return
 *  @retval     returned AppID for attached rule. This is returned in RX meta data for packets matching this rule and no other, @ref NETCP_CFG_IP_T
 *  @pre       @ref netapi_init , @ref netapi_netcpCfgCreateMacInterface 
 */
void netapi_netcpCfgDelMac(NETAPI_T     h,
                           int          iface_no,
                           int*         err);


/**
 * @brief This defines handle to installed classifier returned by API.  Pkts matching this classifier will have meta data with this tag.
 *  Also used to delete classifier
 */
typedef uint32_t NETCP_CFG_CLASS_T;


/**
 *  @ingroup cfg_structures
 *  @brief NETAPI Class L4 Configuration
 *
 *  @details This structure contains Classifier L4 Configuration. In this type of classifier, the  L2 and L3 header match criteria are implied by the iface and ip entries.  L4 packet match criteria are defined by the proto and appProto fields ( L4 protocol id and ports)
 */
typedef struct NETCP_CFG_CLASS_L4_Tag
{
    int                 iface;      /**< Indicates which MAC interface packet should be received on*/
    NETCP_CFG_IP_T      ip;         /**< IP rule to match: see @ref NETCP_CFG_IP_T */
    nwal_appProtoType_t proto;      /**< L4 proto (-1 for don't care)*/
    nwalAppProto_t      appProto;   /**< L4 Ports or equivalent */

} NETCP_CFG_CLASS_L4_T;


/**
 *  @ingroup cfg_structures
 *  @brief NETAPI Classifier L4 plus IPSEC policy configuration
 *
 *  @details This structure contains Classifier L4 plus IPSEC policy configuration. In this type of classifier, the  L2,L3 header match criteria are implied by the iface and ip_policy fields. The L4 match criteria are implied by the proto and appProto fields. 
 */
//classifier L4 + policy  (L2, L3 (outer), tunnel, L3 (inner)  implied by policy
typedef struct NETCP_CFG_CLASS_L4_IPSEC_Tag
{
    int                       iface;      /**< Indicates which MAC interface packet should be received from */
    NETCP_CFG_IPSEC_POLICY_T  ip_policy;  /**< IPSEC policy configuration. see @ref NETCP_CFG_IPSEC_POLICY_T */
    nwal_appProtoType_t       proto;      /**< L4 proto (-1 for don't care)*/
    nwalAppProto_t            appProto;   /**< L4 Ports or equivalent */

} NETCP_CFG_CLASS_L4_IPSEC_T;



/**
 *  @ingroup cfg_structures
 *  @brief NETAPI Classifier L4/L3 configuration
 *
 *  @details This structure contains Class L4 + L3 Classifier configuration. In this type of classifier the L2 header match criteria is implied by the iface field.  The L3 header match criteria is implied by the ipType, ip_addr and ip_qulaifier fields.  L4 match criteris is implied by the proto and appProto fields.  A failed route can be optionally included to tell NETCP what to do if the L3 portion of the classifier matches but not the L4 portion.
 */
typedef struct NETCP_CFG_CLASS_L3_L4_Tag
{
    int                         iface;          /**< Indicates which MAC interface packet is from */
    nwal_IpType                 ipType;         /**< IP address type, IPV4 or IPV6 */
    nwalIpAddr_t*               ip_addr;        /**< IP address to match */
    nwalIpOpt_t*                ip_qualifiers;  /**< IP address qualifiers */
    NETCP_CFG_ROUTE_HANDLE_T    p_fail_route;   /**< What to do if L3 matches but L4 fails AND L3 is a 
                                                     new rule.(if exisitng rule, then existing fail
                                                     route will be used). */
    nwal_appProtoType_t         proto;          /**< L4 proto (-1 for don't care)*/
    nwalAppProto_t              appProto;       /**< Ports or equivalent */
} NETCP_CFG_CLASS_L3_L4_T;

/**
 *  @ingroup cfg_structures
 *  @brief NETAPI Classifier configuration
 *
 *  @details This structure contains the NETAPI classifer configuration.  This is a union of the different classifier types above
 */
typedef struct NETCP_CFG_CLASSIFIER_Tag
{

/**
 * Classifer type which can be set to one of the following defines:
 * <br>
 *      @ref NETCP_CFG_CLASS_TYPE_L4 , @ref NETCP_CFG_CLASS_TYPE_L3_L4, _
 */
    int classType;

/**
 * @def NETCP_CFG_CLASS_TYPE_L4
 * @ingroup cfg_constants
 *      This defines classifier type to be Class L4. Class L4 classifiers specifiy the L4 protocol information of the packets to matched;  the L2,L3 portions of the classifier are implied by supplied handles from the mac interface create and IP Add APIs
 */
#define NETCP_CFG_CLASS_TYPE_L4  0

/**
 * @def NETCP_CFG_CLASS_TYPE_L3_L4
 * @ingroup cfg_constants
 *      This defines classifier type to be Class L4/L3 .  Class L3_L4 classifiers specify both the IP address (L3) and the L4 protocol information of the packets to be matched.
 */
#define NETCP_CFG_CLASS_TYPE_L3_L4  1

     union
    {
        NETCP_CFG_CLASS_L3_L4_T     c_l3_l4;    /**< @ref NETCP_CFG_CLASS_L3_L4_T */
        NETCP_CFG_CLASS_L4_T        c_l4;       /**< @ref NETCP_CFG_CLASS_L4_T */
        NETCP_CFG_CLASS_L4_IPSEC_T  c_l4_ipsec; /**< @ref NETCP_CFG_CLASS_L4_IPSEC_T */
    } u;                                        /**< union for classifier type configuration structure */
} NETCP_CFG_CLASSIFIER_T;



/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgAddClass   API to attach a classifier rule to NETCP.
 *      This can be used to route a particular packet flow to a specific PKTIO channel
 * 
 *  @details This api can be used to route a particular packet flow to a particular PktIO channel, using a specific 
 *  pktLib heap, and/or have NetCP attach a tag (classifier id) to the incoming packet.
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  p_class definition of the classifier
 *  @param[in]  p_route handle to NETCP route.
 *  @param[in]  action       what to do with packet: one of NETCP_CFG_ACTION_TO_SW, DISCARD or CONTINUE
 *  @param[in]  user_data     Optional: pointer to user provided data associated with SA
 *  @param[out] err     pointer to error return
 *  @retval     returned AppID for attached rule. This is returned in RX meta data for packets matching this rule and no other, @ref NETCP_CFG_IP_T
 *  @pre       @ref netapi_init
 */NETCP_CFG_CLASS_T netapi_netcpCfgAddClass(NETAPI_T                   h,
                                             NETCP_CFG_CLASSIFIER_T*    p_class,
                                             NETCP_CFG_ROUTE_HANDLE_T   p_route,
                                             int                        action,
                                             void*                      user_data,
                                             int*                       err);



/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgDelClass   API to delete a preconfigured classifier
 * 
 *  @details This API can be used to delete a preconfigured classifier
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  classId
 *  @param[out] err     pointer to error return
 *  @retval     none
 *  @pre       @ref netapi_init, @ref netapi_netcpCfgAddClass
 */
void netapi_netcpCfgDelClass(NETAPI_T           h,
                             NETCP_CFG_CLASS_T  classId,
                             int*               err);


/**
 *  @ingroup netapi_cb_functions
 *  @brief NETCP_CFG_STATS_CB   Callback function that is used to return statistics from NETCP
 * 
 *  @details The application provides a callback function that NETAPI  uses to report statistics.
 *  The request for stats is generated from the @ref netapi_netcpCfgReqStats API.
 *  Note: to receive this stats callback, the @ref netapi_netcpPoll function must be called
 *  @param[in]  h   NETAPI instance handle, @ref NETAPI_T
 *  @param[out]  pPaStats    the PA (NETCP packet accelerator subsystem) statistics block 
 *  @retval     none 
 *  @pre       @ref netapi_init , @ref netapi_netcpCfgReqStats, @ref netapi_netcpPoll
 */
typedef void (*NETCP_CFG_STATS_CB)(NETAPI_T      h,
                                   paSysStats_t* pPaStats);

/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgReqStats   API to request statistics from NETCP
 * 
 *  @details This api is used to request a statistics from NETCP.  This will generate a stats request
 *  command to NETCP. Sometime later, the statistics result will arrive and will be passed to 
 *  the caller via the asynchronus callback @ref NETCP_CFG_STATS_CB that is registered in this call.
 *  Note: to receive the stats callback, the @ref netapi_netcpPoll funcition must be called
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  cb   the callback function to invoke with the resulting statistics block, @ref NETCP_CFG_STATS_CB
 *  @param[in]  doClear     clear the stats in NETCP after the report (0=no, 1=yes) 
 *  @param[out] err     pointer to error return
 *  @retval     none 
 *  @pre       @ref netapi_init 
 */
void netapi_netcpCfgReqStats(NETAPI_T               h,
                             NETCP_CFG_STATS_CB     cb,
                             int                    doClear,
                             int*                   err);


/**
 *  @ingroup cfg_functions
 *  @brief netapi_netcpCfgExceptions    API to configure NETCP with global rules for exception packet handling
 *
 *  @details This api is used to configure NETCP with global rules of how to handle exception packets specified by exception_id.
 *  @param[in]  h    NETAPI instance handle, @ref NETAPI_T
 *  @param[in]  exception_id id of the exception packet, refer to pa.h,.pa_EROUTE_XXX for list of exception packet id's
 *  @param[in]  p_route handle to NETCP route.
 *  @param[in]  action, action for NETCP to take upon classifying packet as type exception, refer to nwal. nwal_matchAction_t
 *  @retval returns app_id, @ref NETCP_CFG_EXCEPTION_PKT_T
 *  @pre       @ref netapi_init 
 */
NETCP_CFG_EXCEPTION_PKT_T netapi_netcpCfgExceptions(NETAPI_T                    h,
                                                    int                         exception_id ,
                                                    nwal_matchAction_t          action,
                                                    NETCP_CFG_ROUTE_HANDLE_T    p_route);


#ifdef __cplusplus
}
#endif
#endif

