/*************************************************
* FILE:  shmtest.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application (not used, kept as refenence)
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **********************************************************************/


#include <stdlib.h>
#include <sys/shm.h>
#include <errno.h>

 char * p= NULL;
main(int argc, char * argv[])
{
int fd;
int op=0;
int name = 0xfeed;
int err;
if (argc<3) {printf("shmtest  (c[reate]| d[delete]| o[open])  key\n"); exit(1);}


switch (argv[1][0])
{
case 'D':
case 'd':
	op =2; //delete
	break;
case 'c':
case 'C':
	op =0; //create
	break;
case 'o':
case 'O':
	op =1; //open
        break;
default:
printf(" unknown op code %c.  Need d, o, or c\n", argv[1][0]);
exit(1);
break;
}

name = atoi(argv[2]); printf("key = %d op=%d\n", name,op);
switch (op)
{
case(1): /* open */
default:
	fd = shmget( (key_t) name, 100000, 0666 );
	if (fd <0) {
          perror(" shget open failed\n"); 
          exit( 1);
        }
        break;

case(0): /* create */
  fd = shmget(name, 100000, IPC_CREAT | 0666 );
  if (fd<0) {perror(" shget create failed , exiting "); exit(1);}
  break;

case(2):
   fd = shmget( (key_t) name, 100000, 0666 );
  if (fd <0) {
      perror(" delete: shget open failed\n");
      exit( 1);
  }
  err=shmctl(fd, IPC_RMID, 0);
  if(err<0) {perror("ctl failed: ");}
  exit( 0);  //all we do 
  break;
}

/* map into us */  
p = shmat(fd, 0, 0);
if (p == -1) {
perror("shmat failed\n"); exit(1);
}
 else {printf("mapped to %x\n",p);}

if (op==1) { printf("got something: %s\n", p);}
else if(op==0) 
{
sprintf(p,"created some shared memory, key=%d...\n",name); 
printf("creating shm ok: %s",p);
}

}
