/*******************************************************************
* FILE:  qmsim.c
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  desktop simulator for queues.
 *              (test code, not operational) 
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ***********************************************************************/


#include <stdlib.h>
#include "qmsim.h"

/*-------------------Simulator part ---------------*/

void dumpq(Q* q)
{
  printf("QDUMP: %d %d %d %d\n",q->size, q->ne, q->head,q->tail);
}
void qsim_close(Q * q)
{
  if (!q) return;
  if (q->q) free(q->q);
  free(q);
  printf("QSIM: freeing queue\n");
}

Q * qsim_create(nelem)
{
void *p;
Q * q;
if (!nelem) return NULL;
q = (Q*) calloc(1,sizeof(Q));
if (!q) return NULL;

p = (void **) calloc(nelem,sizeof(void *));
if (!p) { free(q); return NULL; }

q->size=nelem;
q->q = p ;
 printf("QSIM CREATE .. "); dumpq(q);
return q;
}

int  qsim_push(Q *q, void *p)
{
 if (!q) return -1;
 if (q->ne >= q->size) return-1;
 q->q[q->tail] = p;
 q->tail +=1;
 q->ne+=1;
 if (q->tail >= q->size) q->tail = 0;
 //dumpq(q);
 return 1;
}
void * qsim_pop(Q *q )
{
   void * val;
   if (!q) return NULL;
   if (q->ne ==0) return NULL;
   val = q->q[q->head];
   q->head+=1;
   if(q->head>= q->size) q->head=0;
   q->ne -=1;
 //dumpq(q);
   return val;
} 

//#define TEST_QSIM
#ifdef TEST_QSIM
main()
{
 Q * q;
 int i;
 q=  qsim_create(10);
 for(i=1;i<11;i++)  qsim_push(q, (void *) i);
 
 for(;i<16;i++) { void * val; val = qsim_pop(q); printf("%d\n", (int) val);}
 for(;i<21;i++) qsim_push(q, (void *) i);
 for(;i<31;i++) { void * val; val = qsim_pop(q); printf("%d\n", (int) val);}
}
#endif
