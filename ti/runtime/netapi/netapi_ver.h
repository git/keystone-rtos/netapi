/*******************************
 * file: netapi_ver.h
 * purpose: netapi version information
 **************************************************************
 * @file netapi.h
 * 
 * @brief DESCRIPTION:  netapi version information for user space transport
 *               library
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013-2016
 * 
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************/

#ifndef __NETAPI_ERR__
#define __NETAPI_ERR__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @def NETAPI_VERSION_ID
 *      This is the NETAPI Version. Versions numbers are encoded in the following 
 *      format:
 *              0xAABBCCDD -> Arch (AA); API Changes (BB); Major (CC); Minor (DD)
 */
#define NETAPI_VERSION_ID                   (0x01010008)

/**
 * @def NETAPI_VERSION_STR
 *      This is the version string which describes the NETAPI along with the
 *      date and build information.
 */
#define NETAPI_VERSION_STR      "NETAPI Revision: 01.01.00.08"

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_mGetVersion  API used to get version of NETAPI 
 *
 *  @details This  API used to get version of netapi
 *  @retval     int version of netapi
 *  @pre        @ref netapi_init
 */
static inline int netapi_getVersion(void)
{
return NETAPI_VERSION_ID;
}

/**
 *  @ingroup netapi_gen_functions
 *  @brief netapi_mGetVersionString  API used to get version string of NETAPI 
 *
 *  @details This  API used to get version string of netapi
 *  @retval     char version string of netapi
 *  @pre        @ref netapi_init
 */
static inline char * netapi_getVersionString(void) { return NETAPI_VERSION_STR;}



#ifdef __cplusplus
}
#endif
#endif
