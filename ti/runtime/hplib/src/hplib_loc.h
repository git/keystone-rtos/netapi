/**
 *   FILENAME  hplib_loc.h
 *
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
 
#ifndef __HPLIB_LOC__H
#define __HPLIB_LOC__H

#ifdef __cplusplus
extern "C" {
#endif


#include "ti/csl/cslr_device.h"

#define hplib_Log printf


#ifndef CSL_PA_SS_CFG_REGS
#define CSL_PA_SS_CFG_REGS  CSL_NETCP_CFG_REGS
#endif
#ifndef CSL_PA_SS_CFG_CP_ACE_CFG_REGS
#define CSL_PA_SS_CFG_CP_ACE_CFG_REGS (CSL_NETCP_CFG_REGS + 0xC0000)
#endif

/* Physical address map & size for various subsystems */
#ifdef CSL_QM_SS_CFG_QUE_PEEK_REGS
/* PDK Version 2 */
#define QMSS_CFG_BASE_ADDR  CSL_QM_SS_CFG_QUE_PEEK_REGS
#define QMSS_CFG_BLK_SZ (1*1024*1024)
#define QMSS_DATA_BASE_ADDR 0x44020000
#define QMSS_DATA_ARM_PROXY_QUEUE_DEQUEUE_REGS (0x44040000)
#define QMSS_DATA_BLK_SZ (0x60000)
#define SRIO_CFG_BASE_ADDR  CSL_SRIO_CONFIG_REGS
#define PASS_CFG_BASE_ADDR  CSL_PA_SS_CFG_REGS
#define MSMC_SRAM_BASE_ADDR CSL_MSMC_SRAM_REGS
#define NETCP_CFG_BASE_ADDR  CSL_PA_SS_CFG_REGS
#define PASS_CFG_BLK_SZ (1*1024*1024)
#define SRIO_CFG_BLK_SZ (132*1024)
#else
#define QMSS_CFG_BLK_SZ (0x00200000)
#define QMSS_DATA_BLK_SZ (0x00100000)

#if defined(DEVICE_K2E) || defined(DEVICE_K2L)
#define PASS_CFG_BLK_SZ (16*1024*1024)
#else
#define PASS_CFG_BLK_SZ (1*1024*1024)
#endif

#endif


typedef uint16_t HPLIB_BOOL_T;

#define  HPLIB_TRUE  1
#define  HPLIB_FALSE  0

#define TUNE_HPLIB_DESC_SIZE  128 //don't change!!


typedef struct hplib_VirtMemPoolHeader_Tag
{
    uint32_t ref_count;
    uint32_t totalAllocated;
    uint32_t bmap[0];
} hplib_VirtMemPoolheader_T;



/*  The structure contains the HPLIB Virtual Memory Address Information for
 *  Memory pools. */
typedef struct hplib_virtualMemPoolAddr_Tag
{
    uint8_t *memStartPhy;
    uint8_t *memStart;
    uint8_t *memEndPhy;
    uint8_t *memEnd;
    hplib_VirtMemPoolheader_T * memPoolHdr;
    uint8_t *memAllocPtr;
    uint32_t memSize;
    uint32_t hplib_VM_virt_to_phy_mapping;
    uint32_t hplib_VM_phy_to_virt_mapping;
    uint8_t *virtMapInfo;
} hplib_virtualMemPoolAddr_T;



/************** NON API utility functions *******************/


/**************************************************************************
 *  FUNCTION PURPOSE:   Opens the hplib kernel module
 **************************************************************************/
int  hplib_utilModOpen(void);

/**************************************************************************
 *  FUNCTION PURPOSE:   Closes the hplib kernel module
 **************************************************************************/
void hplib_utilModClose(void);

/******************************************************************************
 *  FUNCTION PURPOSE:   Return the physical adrress of DDR memory region allocated via CMA
 ******************************************************************************/
unsigned long hplib_utilGetPhysOfBufferArea(void);

/**************************************************************************
 *  FUNCTION PURPOSE:   Return the size of DDR memory region allocated via CMA
 **************************************************************************/
 unsigned long hplib_utilGetSizeOfBufferArea(void);

 /******************************************************************************
 *  FUNCTION PURPOSE:   mmap the block into our user space process memory map
 ******************************************************************************/
unsigned long hplib_utilGetVaOfBufferArea(unsigned int offset, unsigned int size);

#ifdef __cplusplus
}
#endif
void * bmAllocInit(uint8_t * base, uint32_t max_size, uint32_t *bitmap, uint32_t psize);
uint8_t* bmAlloc(void* blob, uint32_t size);
void * bmAllocClose(void * blob);
void bmFree(void * blob, uint8_t *p, uint32_t size);



#endif
