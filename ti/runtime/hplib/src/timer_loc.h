/********************************************************
  * File: timer_loc.h
  * Purpose: local definitions for timer module
  ******************************************************/

#ifndef __TIMER_LOC__H
#define __TIMER_LOC__H

#ifdef __cplusplus
extern "C" {
#endif



#include <stdint.h>

#ifndef NULL
#define NULL 0
#endif


/**********************************
 * A timer object
 *  active timrs have non zero cookie
 **********************************/
//struct TIM_tag;
typedef struct TIM_tag
{
struct TIM_tag * next;
void * cookie;
unsigned long long t;
} TIM_T;

//a list of timers 
typedef struct TIM_LIST_tAG
{
   TIM_T * head;
   TIM_T * tail;
} TIM_LIST_T;

typedef void * HPLIB_TIMER_GROUP_HANDLE_T;

typedef void * HPLIB_TIMER_LIST_T;

typedef void (*HPLIB_TIMER_CB_T) (
    HPLIB_TIMER_GROUP_HANDLE_T th,
    int n_fired,
    HPLIB_TIMER_LIST_T fired_list,
    uint64_t currentTime);

//the timer group context
typedef struct TIMER_GROUP_Tag
{
  void * h;       //back pointer
  int n_cells;       //#of cells (hash entries)
  int n_timers;      //# of timer objects
  int cell_width;    //size of each cell in ticks
  TIM_LIST_T free;   //free list of timer objects
  TIM_LIST_T * cells;//active timers hash table
  unsigned long long last_polled;  //in ticks
  HPLIB_TIMER_CB_T cb; //timer callback
  int local;   //1 => local timer, 0 =>global timer
  int exp2cancel; //1=> expect to cancel, 0=> expect to fire
  int tol;     //in ticks  [FUTURE]
} TIMER_GROUP_T;

/**********************INTERNAL API*******************/
//create a timer group
int tim_group_create(TIMER_GROUP_T *g, int n_cells, int n_timers);

//return a list of timer objects to free list
void tim_return_free(TIM_LIST_T *ftl,  TIM_LIST_T *p, int n);  

//get a free timer oblject
TIM_T * tim_get_free(TIM_LIST_T *ftl, void *cookie, unsigned long long t);

//return a list of timers that have fired
void tim_return_fired_list(TIM_LIST_T *tl, TIM_LIST_T * pr, TIM_LIST_T * ftl, unsigned long long t,  int * p_nf);


//cancel a timer
void tim_cancel(TIM_T * p, int *pErr);

//insert an active timer into hash cell
TIM_T *tim_set(TIM_LIST_T *tl, TIM_LIST_T *free_tl, unsigned long long t, void *cookie, int *pErr);

/********************************************************/
/********************internal control of hw*************/
/******************************************************/
//memmap t64 registers. fd is open(/dev/mem)
int t64_memmap(int fd);

//start the timer 
int t64_start(void);

#ifdef __cplusplus
}
#endif

#endif
