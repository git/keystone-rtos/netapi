#include <stdlib.h>
#include <stdint.h>
/***********************************************************************
 * FILE: bm_alloc.c
 * Purpose:  Memory allocator which uses a bit-map for book keeping purposes
 * to keep track of used and un-used memory locations
 ***********************************************************************
 * FILE: bm_alloc.c
 * 
 * DESCRIPTION:  Memory allocator which uses a bit-map for book keeping purposes
 * to keep track of used and un-used memory locations
 * 
 * REVISION HISTORY:
 *
 *  Copyright (c) Texas Instruments Incorporated 2013
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  *****************************/

#define PSZ_DEF 256  /* defatult Page Size */

/* Internal structure used to manage the alloc/free memory region */
typedef struct BM_MALLOC_STATE_Tag
{
    /* this must point to a multi-proc shararable area, typically at the start
       of the memory area to be managed. the pointer is in this process addr space.
       Must be at least of size maxsize/(NPAGES*8*4) */
    uint32_t *bmap;  
    /* this is the pointer to the memory area to be managed, in this processes addr space */
    uint8_t *pBase;
    /* size of the memory area */
    int maxsize;
    /* number of pages in the memory area */
    int NPAGES;
    
    /* the page size to use  should be power of 2*/
    int PSZ;
} BM_MALLOC_STATE_T;


static inline int bmOff( BM_MALLOC_STATE_T *p, int wordpos, int bitpos)
{
   return  (wordpos*8*sizeof(uint32_t))*p->PSZ +  (bitpos&0x1f)*p->PSZ;

}
static inline int bmToWordBit( BM_MALLOC_STATE_T *p, int off,  int * pbitpos)
{
    int temp=  off/p->PSZ;
    int wp = temp/(8*sizeof(uint32_t));
    *pbitpos = temp - wp*(8*sizeof(uint32_t));
    return wp;    
}
static inline void bmSetBits(BM_MALLOC_STATE_T *p,int sw,int sb, int ew, int eb, int val)
{
int i;
int j;

    if( ew>sw)
    {
        //1st word
        if (val)
            for(j=sb;j<32;j++) p->bmap[sw] |= 1<< (31-j);
        else
            for(j=sb;j<32;j++) p->bmap[sw] &= ~(1<<(31-j));
     
        for(i=sw+1; i< ew; i++)
        {
            if(val)
                p->bmap[i]= 0xffffffff;
            else
                p->bmap[i]= 0x00000000;
        }
        //last word
        if (val)
        {
            for(j=0;j<=eb;j++) 
                p->bmap[ew] |= 1<< (31-j);
        }
        else
        {
            for(j=0;j<=eb;j++)
                p->bmap[ew] &= ~(1<<(31-j));
        }
    }
    else //special case of allocation/free just in one word
    {
        if (val)
        {
            for(j=sb;j<=eb;j++) 
                p->bmap[sw] |= 1<< (31-j);
        }
        else
        {
            for(j=sb;j<=eb;j++)
                p->bmap[sw] &= ~(1<<(31-j));
        }
    }
}

/* find sz worth of consecutive free slots using first-fit */
static int inline bmFindSlots(BM_MALLOC_STATE_T *p, uint32_t size)
{
    int i,j, flag,c;
    int szinpages= size/p->PSZ;
    uint32_t * pW = &p->bmap[0];
    int wordpos=0;
    int bitpos=0;
    int ewordpos=0;
    int ebitpos=0;
    flag=0;
    if ((szinpages * p->PSZ) < size) szinpages+=1;
    for(i=0,c=0;i<p->NPAGES/(8*sizeof(uint32_t)) && (c< szinpages) ;)
    {
        if ((!flag) && (*pW == 0xffffffff)) {pW+=1;i+=1;continue;}
        for(j=0;j<(8*sizeof(uint32_t));j++)
        {
            if (!flag)
            { 
                if (*pW & ( 1<<(31-j)) ) continue;
                c=1;
                flag= 1;
                wordpos = ewordpos=i;
                bitpos= ebitpos=j;
            }
            else
            {
                if (c==szinpages) break; //skipping rest of inner loop
                if(*pW & (1<<(31-j)) )
                {
                    //not large enough
                    c=0;
                    flag=0;
                    continue;
                }
                c +=1;
                ewordpos=i;
                ebitpos=j;
            }
        }
        pW+=1;
        i+=1;
    }
    if (c!=szinpages)
    {
        return -1;
    }
    //got it
    bmSetBits(p,wordpos,bitpos, ewordpos, ebitpos, 1);
    return bmOff(p,wordpos,bitpos);
}




/********************************************************************
* FUNCTION PURPOSE: Malloc a block of memory from memory area
********************************************************************
* DESCRIPTION:  Malloc a block from the region whose state is in blob
* Note:in multi proc/multi thread, caller needs to do a critical section
 ********************************************************************/
uint8_t* bmAlloc(void* blob, uint32_t size)
{
    BM_MALLOC_STATE_T *pCtx= (BM_MALLOC_STATE_T *) blob;
    int off;
    
    if (size> pCtx->maxsize) return NULL;

    off= bmFindSlots(pCtx,size);
    if (off<0)
        return NULL;
     return &pCtx->pBase[off];
}
/********************************************************************
* FUNCTION PURPOSE: Free back  a block of memory from allocated memory 
*                   retion
********************************************************************
* DESCRIPTION:  Free a block of memory previously allocated from
* the region whose state is in the blob
* note: in multi thread/process, the caller needs to do a critical section
 ********************************************************************/
void bmFree(void * blob, uint8_t *p, uint32_t size)
{
    BM_MALLOC_STATE_T *pCtx= (BM_MALLOC_STATE_T *) blob;
    unsigned int off = p-pCtx->pBase;
    int wpos, bpos;
    int ewpos, ebpos;
    int szinpages = size/pCtx->PSZ;
    if (off > pCtx->maxsize) return;
    if (szinpages*pCtx->PSZ != size) size = (szinpages+1)*pCtx->PSZ;
    wpos= bmToWordBit(pCtx,  off, &bpos);
    ewpos= bmToWordBit(pCtx, off+size, &ebpos);
    //adjust by 1
    if (ebpos==0) { ebpos=31; ewpos-=1;} else {ebpos-=1;}
    bmSetBits(pCtx, wpos,bpos,ewpos,ebpos,0);
}


/********************************************************************
* FUNCTION PURPOSE: Open a bit map alloc instance on a memory region
********************************************************************
* DESCRIPTION:  Open a bit map alloc instance on a memory area
*     base:      ptr to start of memory area to be managed 
*     max_size:  size of memory area in bytes
*     bitmap:    pointer to bitmap area (typically just in front of pbase)
*  (note: Master process needs to make sure bitmap is zeroed)
*     psize:     page size to use. If user, PSZ_DEF is assumed
* Return: a ptr to a blob holding the state of the region
* ********************************************************************/
void * bmAllocInit(uint8_t * base, uint32_t max_size, uint32_t *bitmap, uint32_t psize)
{
    int l;  
    BM_MALLOC_STATE_T *pCtx= (BM_MALLOC_STATE_T *) malloc(sizeof(BM_MALLOC_STATE_T));
    if (!pCtx) return (void*) NULL;
    pCtx->maxsize = max_size;
    pCtx->bmap = bitmap;
    pCtx->pBase=base;
    /* todo other checks for PSZ: ^2, >= 64, .. */
    pCtx->PSZ = psize? psize: PSZ_DEF;
    pCtx->NPAGES= pCtx->maxsize/pCtx->PSZ;
    //make sure NPAGES is multiple of 32
    l= pCtx->NPAGES/(8*sizeof(uint32_t)); 
    if ((l * 8 * sizeof(uint32_t)) < pCtx->NPAGES)
        pCtx->NPAGES= l*8*sizeof(uint32_t); 
    if (pCtx->maxsize > pCtx->NPAGES*pCtx->PSZ)
    {
        pCtx->maxsize=pCtx->NPAGES*pCtx->PSZ;
    }
    return (void *) pCtx;
}

/********************************************************************
* FUNCTION PURPOSE: Close bit map alloc instance instance context
********************************************************************
* DESCRIPTION:  Close bit map alloc instance instance context
 ********************************************************************/
void bmAllocClose(void * blob)
{
    free(blob);
}


