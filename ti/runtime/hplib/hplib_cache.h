/**
 *   FILE NAME  hplib_cache.h
 *
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *   @file hplib_cache.h
 *
 *   @brief
*      Header file for the High Performance Cache  Primitives. 
 *      The file exposes the data structures
 *      and exported API which are available for use by applications.
 *   NOTE:
 *      These cache primitives are required for non-cache coherent memory architectures,
 *      for coherent coherent memory architectures, these cache primitives are not required
 *      as cache coherency functionalityis handled by the hardware.
 *
 */




#ifndef __HPLIB_CACHE_H__
#define __HPLIB_CACHE_H__
#ifdef __cplusplus
extern "C" {
#endif


#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include "hplibmod.h"
#include "hplib_sync.h"
#include "hplib_shm.h"

extern int hplib_mod_fd;
/* Variables used by inline functions below */
/* virtual address of the [only] memory pool */
extern uint8_t *hplib_VM_mem_start;
extern uint8_t *hplib_VM_mem_end;
/* Physical address of the [only] memory pool */
extern uint8_t *hplib_VM_mem_start_phy;
extern uint8_t *hplib_VM_mem_end_phy;
extern uint32_t hplib_VM_virt_to_phy_mapping;
extern uint32_t hplib_VM_phy_to_virt_mapping;
extern hplib_virtualMemPoolAddr_T memPoolAddr[];

/**
@defgroup HPLIB_CACHE_API High Performance Library Cache API's
@ingroup HPLIB_API
*/


/** @addtogroup HPLIB_CACHE_API
 @{ */

/**
 *  @b Description
 *  @n  
 *      The function API is used to perform a cache writeback operation 
 *      for a block of cached memory.
 *  @param[in] ptr
 *      Pointer to cache memory location to writeback to ddr.
 *  @param[in] size
 *      size of cached memory to writeback to ddr
 *  @retval;
 *      @ref hplib_OK on success, @ref hplib_FAILURE on failure
 */
static inline hplib_RetValue hplib_cacheWb(void *ptr, size_t size)
{
#ifdef  CORTEX_A8
    struct hplibmod_block block;

    if (((uint8_t*)ptr <hplib_VM_mem_start)||( (uint8_t*)ptr>hplib_VM_mem_end)) return hplib_FAILURE;
    block.addr = (unsigned long)ptr;
    block.size = size;


    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCCACHEWB | HPLIBMOD_IOCMAGIC, &block) == -1)
    {
        return hplib_FAILURE;
    }
#else
    (void)ptr;
    (void)size;
#endif
    return hplib_OK;
}


/**
 *  @b Description
 *  @n  
 *      The function API is used to perform a cache writeback operation and invalidate
 *      for a block of cached memory.
 *  @param[in] ptr
 *      Pointer to cache memory location to writeback to ddr.
 *  @param[in] size
 *      size of cached memory to writeback to ddr
 *  @retval;
 *      @ref hplib_OK on success, @ref hplib_FAILURE on failure
 */
static inline hplib_RetValue hplib_cacheWbInv(void *ptr, size_t size)
{
#ifdef  CORTEX_A8
    struct hplibmod_block block;

     if (((uint8_t*)ptr <hplib_VM_mem_start)||( (uint8_t*)ptr>hplib_VM_mem_end)) return hplib_FAILURE;
    block.addr = (unsigned long)ptr;
    block.size = size;
   
    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCCACHEWBINV | HPLIBMOD_IOCMAGIC, &block) == -1) {
        return hplib_FAILURE;
    }
#else
    (void)ptr;
    (void)size;
#endif
    return hplib_OK;
}

/**
 *  @b Description
 *  @n  
 *      The function API is used to perform a cache invalidate operation
 *      for a block of cached memory.
 *  @param[in] ptr
 *      Pointer to cache memory location to invalidate
 *  @param[in] size
 *      size of cached memory to invalidate
 *  @retval;
 *      @ref hplib_OK on success, @ref hplib_FAILURE on failure
 */
static inline hplib_RetValue hplib_cacheInv(void *ptr, size_t size)
{
#ifdef  CORTEX_A8
    struct hplibmod_block block;

    if (((uint8_t*)ptr <hplib_VM_mem_start)||( (uint8_t*)ptr>hplib_VM_mem_end)) return hplib_FAILURE;
    block.addr = (unsigned long)ptr;
    block.size = size;
   
    if (ioctl(hplib_mod_fd, HPLIBMOD_IOCCACHEINV | HPLIBMOD_IOCMAGIC, &block) == -1) {
        return hplib_FAILURE;
    }
#else
    (void)ptr;
    (void)size;
#endif
    return hplib_OK;
}

/**
 *  @b Description
 *  @n  
 *      The function API is used to  perform a preload cache operation.
 *  @param[in] ptr
 *      Address of the memory location to prefetch into cache
 *  @retval      none
 */
static inline void hplib_cachePrefetch(void *ptr)
{
    __builtin_prefetch(ptr);
}

/**
@}
*/

#ifdef __cplusplus
}
#endif
#endif

