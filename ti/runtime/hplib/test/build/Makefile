empty =
space =$(empty) $(empty)

export ARMV7OBJDIR ?= ./obj
export ARMV7BINDIR ?= ./bin
export ARMV7LIBDIR ?= ./lib
export ARMV7LIBDIRLOCAL ?= ../../lib
WORKDIR ?= $(TRANS_SDK_INSTALL_PATH)

ifeq ($(CPUFLAGS), cortex-a8)
        CFLAGS += -DCORTEX_A8
endif


# INCLUDE Directories
QMSS_INC_DIR = $(PDK_INSTALL_PATH)/ti/drv/qmss
CPPI_INC_DIR = $(PDK_INSTALL_PATH)/ti/drv/cppi
#
HPLIB_SRC_DIR ?= $(TRANS_SDK_INSTALL_PATH)/ti/runtime/hplib

# Support Libraries used by HPLIB 
HP_LIB = -lhplib_$(DEVICE)

ifeq ($(USEDYNAMIC_LIB), yes)
#presuming ARM executable would depend on dynamic library dependency
EXE_EXTN = _so
LIBS     = $(HP_LIB)
else
#forcing ARM executable to depend on static LLD libraries
LIBS = -Wl,-Bstatic $(HP_LIB) -Wl,-Bdynamic
EXE_EXTN =
endif


ifdef CROSS_TOOL_INSTALL_PATH
## Support backwards compatibility with KeyStone1 approach
 CC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
 AC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)as
 AR = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)ar
 LD = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
endif


CFLAGS+= $(DEBUG_FLAG) -I../ -I. -I$(PDK_INSTALL_PATH) -I$(TRANS_SDK_INSTALL_PATH) -I$(QMSS_INC_DIR) -I$(CPPI_INC_DIR) $(CSL_DEVICE) -D__ARMv7 -D_VIRTUAL_ADDR_SUPPORT -D__LINUX_USER_SPACE -D_LITTLE_ENDIAN=1 -DMAKEFILE_BUILD -pthread -D _GNU_SOURCE

# Linker options, commenting out INTERNALLINKDEFS
INTERNALLINKDEFS = -Wl,--start-group -L$(ARMV7LIBDIR) -L$(PDK_ARMV7LIBDIR) -L$(ARMV7LIBDIRLOCAL) $(LIBS) -Wl,--end-group -lrt

all: tests 

tests: $(ARMV7BINDIR)/hplib/test/.created $(ARMV7BINDIR)/hplib/test/synchtest $(ARMV7BINDIR)/hplib/test/synchtest2 $(ARMV7BINDIR)/hplib/test/pmutest $(ARMV7BINDIR)/hplib/test/shmtest $(ARMV7BINDIR)/hplib/test/bmAllocTest $(ARMV7BINDIR)/hplib/test/hplibmod_test

api_clean:
	rm -f  $(ARMV7LIBDIR)/libhp.a
	rm -rf $(ARMV7OBJDIR)/hplib/lib/*.o

clean:	
	rm -rf $(ARMV7OBJDIR)/hplib/test
	rm -rf $(ARMV7OBJDIR)/hplib/device
	rm -f $(ARMV7BINDIR)/hplib/test/synchtest $(ARMV7BINDIR)/hplib/test/synchtest2 $(ARMV7BINDIR)/hplib/test/pmutest $(ARMV7BINDIR)/hplib/test/hplibmod_test 


$(ARMV7OBJDIR)/hplib/test/%.o: $(HPLIB_SRC_DIR)/test/%.c $(ARMV7OBJDIR)/hplib/test/.created
	@echo compiling $< ... 
	$(CC) -c  $(CFLAGS)  $<  -o $@

$(ARMV7OBJDIR)/hplib/test/.created:
	@mkdir -p $(ARMV7OBJDIR)/hplib/test/

$(ARMV7BINDIR)/hplib/test/.created:
	@mkdir -p $(ARMV7BINDIR)/hplib/test/

$(ARMV7BINDIR)/hplib/test/synchtest: $(ARMV7OBJDIR)/hplib/test/synchtest.o
	$(CC) $(LDFLAGS) $(ARMV7OBJDIR)/hplib/test/synchtest.o $(INTERNALLINKDEFS) -o $(ARMV7BINDIR)/hplib/test/synchtest$(EXE_EXTN) -lpthread

$(ARMV7BINDIR)/hplib/test/synchtest2: $(ARMV7OBJDIR)/hplib/test/synchtest2.o
	$(CC) $(LDFLAGS) $(ARMV7OBJDIR)/hplib/test/synchtest2.o $(INTERNALLINKDEFS) -o $(ARMV7BINDIR)/hplib/test/synchtest2$(EXE_EXTN) -lpthread
	
$(ARMV7BINDIR)/hplib/test/hplibmod_test: $(ARMV7OBJDIR)/hplib/test/hplibmod_test.o
	$(CC) $(LDFLAGS) $(ARMV7OBJDIR)/hplib/test/hplibmod_test.o $(INTERNALLINKDEFS) -o $(ARMV7BINDIR)/hplib/test/hplibmod_test$(EXE_EXTN)

$(ARMV7BINDIR)/hplib/test/pmutest: $(ARMV7OBJDIR)/hplib/test/pmutest.o
	$(CC) $(LDFLAGS) $(ARMV7OBJDIR)/hplib/test/pmutest.o $(INTERNALLINKDEFS) -o $(ARMV7BINDIR)/hplib/test/pmutest$(EXE_EXTN) -lpthread


$(ARMV7BINDIR)/hplib/test/shmtest: $(ARMV7OBJDIR)/hplib/test/shmtest.o
	$(CC) $(LDFLAGS) $(ARMV7OBJDIR)/hplib/test/shmtest.o $(INTERNALLINKDEFS) -o $(ARMV7BINDIR)/hplib/test/shmtest$(EXE_EXTN)

$(ARMV7BINDIR)/hplib/test/bmAllocTest: $(ARMV7OBJDIR)/hplib/test/bmAllocTest.o
	$(CC) $(LDFLAGS) $(ARMV7OBJDIR)/hplib/test/bmAllocTest.o $(INTERNALLINKDEFS) -o $(ARMV7BINDIR)/hplib/test/bmAllocTest$(EXE_EXTN) -lpthread
