/********************************
 * file: pmutest.c
 * sync primitives unit test
 ************************************************
* FILE:  pmutest.c 
 * 
 * DESCRIPTION:  
 *               library  test application -> pmu primitives
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2012
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include "hplib.h"

//usefull events
#define PMU_IFR 0x1  //instruction fetch causing refill of I cache
#define PMU_IFT 0x2 //instruction fetch causing refill of TLB
#define PMU_DR 0x3 //data read/write causing cache refill
#define PMU_DT 0x5 //data read/wrtie causing TLB refill
#define PMU_PF 0x10  //pipeline flush due to branch no predicted. 
#define PMU_WBF 0x40 //write buffer full
#define PMU_L2 0x43 //access to L2
#define PMU_L2_MISS 0x44 //miss in L2
#define PMU_NOI  0x56 //count stall cycles?




//program slot:slot to count evnet: event
void program_pmu_event(int slot, int event)
{
//write_pmu_select_cntr(slot);
hplib_mUtilWritePmuSelectCntr(slot);
hplib_mUtilWritePmuEventToCount(event);
//write_pmu_event_to_count(event);
}

//read event counter in slot: slot
long read_pmu_event(int slot)
{
hplib_mUtilWritePmuSelectCntr(slot);
//write_pmu_select_cntr(slot);
//return read_pmu_counter();
return hplib_mUtilReadPmuCounter();
}

#define MAXJ 5
int x[MAXJ]={1,2,3,4,5};
int run_bench(int n)
{
int sum=0;
int i,j=0;
for(i=0;i<n;i++)
{
 sum+=x[j];
 j++;
 if (j>=MAXJ) j=0; 
}
return sum;
}
main()
{
long v1,v2,v3,v4;
long v1_a,v2_a,v3_a,v4_a;
int res;
long t1,t2;
cpu_set_t set;


CPU_ZERO( &set);
CPU_SET( 0, &set);
hplib_utilSetupThread(0, &set, hplib_spinLock_Type_LOL);

program_pmu_event(0,  PMU_DR);
program_pmu_event(1,  PMU_DT);
program_pmu_event(2,  PMU_IFR);
program_pmu_event(3,  PMU_L2_MISS);
hplib_mUtilWritePmuEnableAllCounters();
//write_pmu_enable_all_counters();
sleep(2);
v1=read_pmu_event(0);
v2=read_pmu_event(1);
v3=read_pmu_event(2);
v4=read_pmu_event(3);
asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(t1));
res=run_bench(100000);
asm volatile("mrc p15, 0, %0, c9, c13, 0" :  "=r"(t2));
v1_a=read_pmu_event(0);
v2_a=read_pmu_event(1);
v3_a=read_pmu_event(2);
v4_a=read_pmu_event(3);
printf("%d miss=%d , noi=%d e0x57=%d dt=%d cycl=%d\n", res,v1_a-v1, v2_a-v2, v3_a-v3, v4_a-v4,t2-t1);

}

