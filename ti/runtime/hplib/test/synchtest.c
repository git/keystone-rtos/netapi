/********************************
 * file: synchtest.c
 * sync primitives unit test
 ************************************************
* FILE:  synchtest.c 
 * 
 * DESCRIPTION:  netapi user space transport
 *               library  test application -> synchronization primitives
 * 
 * REVISION HISTORY:  rev 0.0.1 
 *
 *  Copyright (c) Texas Instruments Incorporated 2010-2011
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include "hplib.h"

#define timing_start hplib_mUtilGetPmuCCNT
#define timing_stop hplib_mUtilGetPmuCCNT

int spot=0;

hplib_rwLock_T  rwLock;

hplib_spinLock_T val;

void *test1( void *arg )
{
    cpu_set_t set;
    CPU_ZERO( &set);
    int proc_num = (int)(long)arg;
    int v1, v2;


#ifdef CORTEX_A8
    CPU_SET( 0, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#else
    CPU_SET( proc_num, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#endif

   printf("cpu[%d]:     thread %d, trying to acquire rwLock for writing\n", 
            sched_getcpu(), proc_num);
    v1 = timing_start();
    hplib_mRWLockWriteLock(&rwLock);
    v2 = timing_stop();
    printf("cpu[%d]:     thread %d, rwLock for writing is aquired, cycles= %d, going to sleep\n", 
            sched_getcpu(), proc_num, v2-v1);
    
    sleep(2);
    printf("cpu[%d]:     thread %d, awake now, going to unlock rwLock\n", 
            sched_getcpu(), proc_num);

    v1 = timing_start();
    hplib_mRWLockWriteUnlock(&rwLock);
    v2 = timing_start();
    printf("cpu[%d]:     thread %d, rwLock write un locked, cycles= %d \n", sched_getcpu(), proc_num, v2-v1);

    return NULL;
}

void *test2( void *arg )
{
    cpu_set_t set;
    int proc_num = (int)(long)arg;
    int v1, v2;
    CPU_ZERO( &set);
#ifdef CORTEX_A8
    CPU_SET( 0, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#else
    CPU_SET( proc_num, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#endif

    printf("cpu[%d]:     thread %d, trying to acquire rwLock for writing\n", 
            sched_getcpu(), proc_num);
    v1 = timing_start();
    hplib_mRWLockWriteLock(&rwLock);
    v2 = timing_stop();
    printf("cpu[%d]:     thread %d, rwLock for writing is aquired, cycles= %d, going to sleep\n", 
            sched_getcpu(), proc_num, v2-v1);
    
    sleep(2);
    printf("cpu[%d]:     thread %d, awake now, going to unlock rwLock\n", 
            sched_getcpu(), proc_num);

    v1 = timing_start();
    hplib_mRWLockWriteUnlock(&rwLock);
    v2 = timing_start();
    printf("cpu[%d]:     thread %d, rwLock for writing un locked, cycles= %d \n", sched_getcpu(), proc_num, v2-v1);
    return NULL;
}
void *test3( void *arg )
{
    cpu_set_t set;
    int proc_num = (int)(long)arg;
    int v1, v2;
    CPU_ZERO( &set);
#ifdef CORTEX_A8
    CPU_SET( 0, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#else
    CPU_SET( proc_num, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#endif

    printf("cpu[%d]:     thread %d, trying to acquire rwLock for reading\n", 
            sched_getcpu(), proc_num);
    v1 = timing_start();
    hplib_mRWLockReadLock(&rwLock);
    v2 = timing_stop();
    printf("cpu[%d]:     thread %d, rwLock for reading is aquired, cycles= %d, going to sleep\n", 
            sched_getcpu(), proc_num, v2-v1);
    
    sleep(2);
    printf("cpu[%d]:     thread %d, awake now, going to unlock rwLock\n", 
            sched_getcpu(), proc_num);

    v1 = timing_start();
    hplib_mRWLockReadUnlock(&rwLock);
    v2 = timing_start();
    printf("cpu[%d]:     thread %d, rwLock for reading un locked, cycles= %d \n", sched_getcpu(),
           proc_num, v2-v1);
    return NULL;
}
void *test4( void *arg )
{
    cpu_set_t set;
    int proc_num = (int)(long)arg;
    int v1, v2;
    CPU_ZERO( &set);
#ifdef CORTEX_A8
    CPU_SET( 0, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#else
    CPU_SET( proc_num, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#endif

    printf("cpu[%d]:     thread %d, trying to acquire rwLock for writing\n", 
            sched_getcpu(), proc_num);
    v1 = timing_start();
    hplib_mRWLockReadLock(&rwLock);
    v2 = timing_stop();
    printf("cpu[%d]:     thread %d, rwLock for reading is aquired, cycles= %d, going to sleep\n", 
            sched_getcpu(), proc_num, v2-v1);
    
    sleep(5);
    printf("cpu[%d]:     thread %d, awake now, going to unlock rwLock\n", 
            sched_getcpu(), proc_num);

    v1 = timing_start();
    hplib_mRWLockReadUnlock(&rwLock);
    v2 = timing_start();
    printf("cpu[%d]:     thread %d, rwLock write un locked, cycles= %d \n", sched_getcpu(), proc_num, v2-v1);
    return NULL;
}

typedef void (*spinlockfuncptr)(hplib_spinLock_T*);


void my_lock(hplib_spinLock_T *value)
{
    hplib_mSpinLockLockMP(value);
}

void my_unlock(hplib_spinLock_T *value)
{
    hplib_mSpinLockUnlockMP(value);
}

void *thread_routine( void *arg )
{
    int proc_num = (int)(long)arg;
    int i;
    cpu_set_t set;
    unsigned long long t1, t2;
    //hplib_spinLock_T val;
    unsigned long v1,v2;
    int val2;
    int count=10;

    spinlockfuncptr lock = & my_lock;
    spinlockfuncptr unlock = &my_unlock;
    
    CPU_ZERO( &set);
#ifdef CORTEX_A8
    CPU_SET( 0, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#else
    CPU_SET( proc_num, &set);
    hplib_utilSetupThread(proc_num, &set, hplib_spinLock_Type_LOL);
#endif
        //now we try the synch_lock_and_test
    hplib_mSpinLockInit(&val);
    v1 = timing_start();
    for(i=0;i<count;i++) 
    {
        
        //val=__sync_fetch_and_add(&spot, 1);
        /*printf("cpu[%d]:     thread %d, val2 =%d %d\n", sched_getcpu(), 
            proc_num, val2, spot); */
        //printf(" val = %d %d\n",val,spot);

        //now we try the synch_lock_and_test
        //hplib_mSpinLockInit(&val);
        //printf("cpu[%d]:     thread %d, after spinLocInit\n", sched_getcpu(), proc_num);
        //printf(" after spinLockinit\n");

        //v1 = timing_start();
        lock(&val);
        //hplib_mSpinLockLockMP(&val);
        //v2 = timing_stop();
        /*printf("mylock: cpu[%d]:     thread %d, lock time, val = %d cycles = %d\n",
            sched_getcpu(), proc_num, val, v2-v1); */

#if 0
        //try the lock
        v1 = timing_start();
        val2=hplib_mSpinLockTryLock(&val);
        v2 = timing_stop();
        printf("cpu[%d]:     thread %d, try lock has returned %d, cycles = %d\n", 
            sched_getcpu(), proc_num, val2, v2-v1);
        //printf("try lock has returns %d, cycles=%d\n", val2, v2-v1);


        //poll the lock 
        v1 = timing_start();
        val2=hplib_mSpinLockIsLocked(&val);
        v2 = timing_stop();
        printf("cpu[%d]:     thread %d, is lock has returned %d, cycles = %d\n", 
            sched_getcpu(), proc_num, val2, v2-v1);
        
        //printf("is_locked has returns %d, cycles=%d\n", val2, v2-v1);
#endif

        //unlock
        //v1 = timing_start();
        unlock(&val);
        //hplib_mSpinLockUnlockMP(&val);
        //v2 = timing_stop();
        /*printf("my_lock: cpu[%d]:     thread %d, unlock time, val =  %d, cycles = %d\n", 
            sched_getcpu(), proc_num, val, v2-v1); */
        
        //printf("unlocked, val= %d cycles=%d\n", val,v2-v1);

#if 0
        /*-------now try rwLock--------*/
        test1(proc_num);
        test2(proc_num);
        test3(proc_num);
        test3(proc_num);
        test4(proc_num);
        test4(proc_num);
#endif
    }
    v2 = timing_stop();
    printf("v1 %d, v2: %d\n",v1, v2);
    
    printf("my_lock: cpu[%d]:     thread %d, lock time, val = %d cycles = %d\n",
            sched_getcpu(), proc_num, val, (v2-v1)/count); 
}


main()
{
    int procs = 0;
    int procs_conf;
    int procs_avail;
    int i;
    pthread_t *thrs;
    cpu_set_t set;

    printf("at start up\n");


    procs_avail= get_nprocs();
    procs_conf = get_nprocs_conf();


    printf("procs_avail: %d, procs_conf: %d\n", procs_avail, procs_conf);

    // Getting number of CPUs
    procs = (int)sysconf( _SC_NPROCESSORS_ONLN );
    if (procs < 0)
    {
        perror( "sysconf" );
        return -1;
    }
    printf(" num cpus = %d\n",procs);

    if (procs==1)
    {
        procs+=1;
        printf("adding 2nd 'core' \n");
    }
    thrs = malloc( sizeof( pthread_t ) * procs);
    if (thrs == NULL)
    {
        perror( "malloc" );
        return -1;
    }
    /* init the RW lock */
    hplib_mRWLockInit(&rwLock);

    for (i = 0; i < 1; i++)
    {
        if (pthread_create( &thrs[0], NULL, thread_routine,
        (void *)(long)0 ))
        {
            perror( "pthread_create" );
            //procs = i;
            break;
        }
#if 0
        if (pthread_create( &thrs[1], NULL, test2,
        (void *)(long)1 ))
        {
            perror( "pthread_create" );
            //procs = i;
            break;
        }
        if (pthread_create( &thrs[2], NULL, test3,
        (void *)(long)2 ))
        {
            perror( "pthread_create" );
            //procs = i;
            break;
        }
        if (pthread_create( &thrs[3], NULL, test4,
        (void *)(long)3 ))
        {
            perror( "pthread_create" );
            //procs = i;
            break;
        }
#endif
    }
    
    for (i = 0; i < 1; i++)
        pthread_join( thrs[i], NULL );

    //free( thrs );

    return 0;
}



