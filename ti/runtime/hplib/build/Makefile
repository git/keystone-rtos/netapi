# Macro definitions referenced below
#
empty =
space =$(empty) $(empty)

export PDK_VER ?= v2

ifeq ($(PDK_VER),v2)
PDK_VERSION = 2
else
PDK_VERSION = 3
endif

# INCLUDE Directories
CPPI_INC_DIR = $(PDK_INSTALL_PATH)/ti/drv/cppi


ifeq ($(strip $(SA_INSTALL_PATH)),)
INCDIR := ../../..;$(PDK_INSTALL_PATH);$(ROOTDIR)
else
INCDIR := ../../..;$(PDK_INSTALL_PATH);$(ROOTDIR);$(SA_INSTALL_PATH)
endif



# Output for prebuilt generated libraries
ARMV7LIBDIR ?= ./lib
ARMV7OBJDIR ?= ./obj
ARMV7OBJDIR_SO := $(ARMV7OBJDIR)/hplib/lib_so
ARMV7OBJDIR_NO_OSAL_SO := $(ARMV7OBJDIR)/hplib/lib_no_osal_so
ARMV7OBJDIR_NO_OSAL := $(ARMV7OBJDIR)/hplib/lib_no_osal
ARMV7OBJDIR := $(ARMV7OBJDIR)/hplib/lib
ARMV7BINDIR ?= ./bin

INCS = -I. -I$(strip $(subst ;, -I,$(subst $(space),\$(space),$(INCDIR))))

INTERNALDEFS = -D__ARMv7 -D_LITTLE_ENDIAN=1 -D_VIRTUAL_ADDR_SUPPORT -DMAKEFILE_BUILD 



ifeq ($(CPU),  cortex-a8)
INTERNALDEFS += -DCORTEX_A8
endif

OBJEXT = o 
INTERNALLINKDEFS =
SRCDIR = ./src

VPATH=$(SRCDIR)

ifdef CROSS_TOOL_INSTALL_PATH
## Support backwards compatibility with KeyStone1 approach
 CC = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
 AR = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)ar
 LD = $(CROSS_TOOL_INSTALL_PATH)/$(CROSS_TOOL_PRFX)gcc
endif
#Cross tools


#List the COMMONSRC Files
COMMONSRCC = \
    hplib_vm.c \
    hplib_shm.c \
    hplib_util.c \
    osal.c \
    tim64.c  \
    timlist.c \
    bm_alloc.c

#ifneq ($(DISABLE_OSAL), yes)
#COMMONSRCC +=  osal.c
#endif

CFLAGS+= $(DEBUG_FLAG) $(CSL_DEVICE) -I. -I $(SRCDIR) -I$(PDK_INSTALL_PATH) -I$(TRANS_SDK_INSTALL_PATH) -I$(CPPI_INC_DIR) -D__LINUX_USER_SPACE -pthread -D _GNU_SOURCE -DPDK_VERSION=$(PDK_VERSION)


# Make Rule for the COMMONSRC Files
COMMONSRCCOBJS = $(patsubst %.c, $(ARMV7OBJDIR)/%.$(OBJEXT), $(COMMONSRCC))
COMMONSRCCOBJS_SO = $(patsubst %.c, $(ARMV7OBJDIR_SO)/%.$(OBJEXT), $(COMMONSRCC))

$(COMMONSRCCOBJS): $(ARMV7OBJDIR)/%.$(OBJEXT): %.c $(ARMV7OBJDIR)/.created
	-@echo compiling $< ... $(CC)
	$(CC) -c $(CFLAGS) $(INTERNALDEFS) $(INCS) $< -o $@

$(COMMONSRCCOBJS_SO): $(ARMV7OBJDIR_SO)/%.$(OBJEXT): %.c $(ARMV7OBJDIR_SO)/.created
	-@echo compiling $< ...
	$(CC) -c $(CFLAGS) $(INTERNALDEFS) -fPIC $(INCS) $< -o $@

$(ARMV7LIBDIR)/libhplib_$(DEVICE).a: $(COMMONSRCCOBJS) $(ARMV7LIBDIR)/.created
	@mkdir -p $(ARMV7LIBDIR)
	@echo archiving $? into $@ ...
	@$(AR) -r $@ $?

libhplib_$(DEVICE).so: $(COMMONSRCCOBJS_SO)
	@echo archiving $? into $(ARMV7LIBDIR)/$@.1 ...
	@$(CC) $(DEBUG_FLAG) -ggdb2 -Wl,-soname=$@.1 -shared -fPIC ${LDFLAGS} -o $@.1.0.0 $^
	@ln -s $@.1.0.0 $@.1
	@ln -s $@.1 $@
	mkdir -p $(ARMV7LIBDIR)
	@mv -f $@.1.0.0 $(ARMV7LIBDIR)/$@.1.0.0
	@mv -f $@.1 $(ARMV7LIBDIR)/$@.1
	@mv -f $@   $(ARMV7LIBDIR)/$@

$(ARMV7OBJDIR)/.created:
	@mkdir -p $(ARMV7OBJDIR)
	@touch $(ARMV7OBJDIR)/.created

$(ARMV7OBJDIR_SO)/.created:
	@mkdir -p $(ARMV7OBJDIR_SO)
	@touch $(ARMV7OBJDIR_SO)/.created

COMMONSRCC1 = \
    hplib_vm.c \
    hplib_shm.c \
    hplib_util.c \
    tim64.c  \
    timlist.c \
    bm_alloc.c

COMMONSRCCOBJS1 = $(patsubst %.c, $(ARMV7OBJDIR_NO_OSAL)/%.$(OBJEXT), $(COMMONSRCC1))
COMMONSRCCOBJS1_SO = $(patsubst %.c, $(ARMV7OBJDIR_NO_OSAL_SO)/%.$(OBJEXT), $(COMMONSRCC1))

$(COMMONSRCCOBJS1): $(ARMV7OBJDIR_NO_OSAL)/%.$(OBJEXT): %.c $(ARMV7OBJDIR_NO_OSAL)/.created
	@echo compiling $< ... $(CC)
	$(CC) -c $(CFLAGS) $(INTERNALDEFS) $(INCS) $< -o $@

$(COMMONSRCCOBJS1_SO): $(ARMV7OBJDIR_NO_OSAL_SO)/%.$(OBJEXT): %.c $(ARMV7OBJDIR_NO_OSAL_SO)/.created
	@echo compiling $< ...
	$(CC) -c $(CFLAGS) $(INTERNALDEFS) -fPIC $(INCS) $< -o $@



$(ARMV7LIBDIR)/libhplib-no-osal_$(DEVICE).a: $(COMMONSRCCOBJS1) $(ARMV7LIBDIR)/.created
	@mkdir -p $(ARMV7LIBDIR)
	@echo archiving $? into $@ ...
	@$(AR) -r $@ $?

libhplib-no-osal_$(DEVICE).so: $(COMMONSRCCOBJS1_SO)
	@echo archiving $? into $(ARMV7LIBDIR)/$@.1 ...
	@$(CC) $(DEBUG_FLAG) -ggdb2 -Wl,-soname=$@.1 -shared -fPIC ${LDFLAGS} -o $@.1.0.0 $^
	@ln -s $@.1.0.0 $@.1
	@ln -s $@.1 $@
	mkdir -p $(ARMV7LIBDIR)
	@mv -f $@.1.0.0 $(ARMV7LIBDIR)/$@.1.0.0
	@mv -f $@.1 $(ARMV7LIBDIR)/$@.1
	@mv -f $@   $(ARMV7LIBDIR)/$@

$(ARMV7OBJDIR_NO_OSAL)/.created:
	@mkdir -p $(ARMV7OBJDIR_NO_OSAL)
	@touch $(ARMV7OBJDIR_NO_OSAL)/.created

$(ARMV7OBJDIR_NO_OSAL_SO)/.created:
	@mkdir -p $(ARMV7OBJDIR_NO_OSAL_SO)
	@touch $(ARMV7OBJDIR_NO_OSAL_SO)/.created

clean:
	rm -rf $(ARMV7OBJDIR)
	rm -rf $(ARMV7OBJDIR_SO)
	rm -rf $(ARMV7OBJDIR_NO_OSAL)
	rm -rf $(ARMV7OBJDIR_NO_OSAL_SO)
	rm -f $(ARMV7LIBDIR)/libhplib_$(DEVICE).*
	rm -f $(ARMV7LIBDIR)/libhplib-no-osal_$(DEVICE).*

module:
	-@echo compiling $< ...
	make -f ../module/Makefile

doxygen:
	doxygen hplib_doxygen.cfg


$(ARMV7LIBDIR)/.created:
	@mkdir -p $(ARMV7LIBDIR)
	@touch $(ARMV7LIBDIR)/.created

