/*
 * File name: hplibmod.h
 *
 * Description: HPLIBMOD utility module header file.
 *
 * Copyright (C) 2012 Texas Instruments, Incorporated
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation version 2.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef __TI_HPLIBMOD_H__
#define __TI_HPLIBMOD_H__

#ifdef __cplusplus
extern "C" {
#endif


#define HPLIBMOD_IOCMAGIC 0x0000fe00

/* Supported "base" ioctl cmds for the driver. */
#define HPLIBMOD_IOCGETPHYS   1
#define HPLIBMOD_IOCGETSIZE   2
#define HPLIBMOD_IOCCACHE     3
#define HPLIBMOD_IOINIT       4

/* ioctl cmd "flavors" */
#define HPLIBMOD_WB   0x00010000
#define HPLIBMOD_INV  0x00020000

/* Supported "flavors" to "base" ioctl cmds for the driver. */
#define HPLIBMOD_IOCCACHEWBINV    HPLIBMOD_IOCCACHE | HPLIBMOD_WB | HPLIBMOD_INV
#define HPLIBMOD_IOCCACHEWB       HPLIBMOD_IOCCACHE | HPLIBMOD_WB
#define HPLIBMOD_IOCCACHEINV      HPLIBMOD_IOCCACHE | HPLIBMOD_INV

#define HPLIBMOD_IOCCMDMASK   0x000000ff

/* Default size of DMA coherent memory to be allocated */
#define HPLIBMOD_MEMSZ 0xf00000

/* MMAP offsets */
/* Offset to map CMA allocated memory */
#define HPLIBMOD_MMAP_DMA_MEM_OFFSET    0

/* Offset to map QMSS Data register region */

#ifdef CORTEX_A8
#define HPLIBMOD_MMAP_QM_DATA_REG_MEM_OFFSET    0x44020000
#else
//#define HPLIBMOD_MMAP_QM_DATA_REG_MEM_OFFSET CSL_QMSS_DATA_QM1_QUEUE_MANAGEMENT_REGS
#define HPLIBMOD_MMAP_QM_DATA_REG_MEM_OFFSET (0x23A00000)

#endif



struct hplibmod_block {
    unsigned long addr;
    size_t size;
};

#ifdef __cplusplus
}
#endif

#endif /*__TI_HPLIBMOD_H__ */

