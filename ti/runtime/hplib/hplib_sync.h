/**
 *   FILENAME  hplib_sync.h
 *
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *   @file  hplib_sync.h
 *
 *   @brief
 *      Header file for the High Performance Synchronization API's. 
 *      The file exposes the data structures
 *      and exported API which are available for use by applications.
 *
 */


#ifndef HPLIB_SYNCH_H
#define HPLIB_SYNCH_H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief
 *  HPLIB spinlock type
 *
 * @details
 *  This enumeration provides the application ability to chose which set of HPLIB
 *  spinlocks to use.
 *  The value is encoded as follows:
 *  0 = low overhead linux use case, make use of WFE/SEV)
 *  1 = multi process environment use case, makes use of sched_yield when lock is not
 *  available.
 *
 */
typedef enum
{
    /*
    * @brief 0 = Low overhead linux use case.
    */
    hplib_spinLock_Type_LOL = 0,

    /*
    * @brief 1 = Multi Process environment use case.
    */
    hplib_spinLock_Type_MP
}hplib_spinLock_Type;


/**
 * @brief   This defines the HPLIB spinLock type.
 */
//typedef int hplib_spinLock_T;
typedef int  hplib_spinLock_T __attribute__((aligned (16)));

/**
 * @brief This defines the HPLIB spinLock lock value. This define should 
 *        never be modified.
 */
#define hplib_spinLock_LOCKVAL 1

/**
 * @brief This defines the HPLIB spinLock unlock value. This define should 
 *        never be modified.
 */
#define hplib_spinLock_UNLOCKVAL 0


/**
 * @brief This defines the HPLIB spinLock unlock value at time of lock initialization.
 */
#define hplib_spinLock_UNLOCKED_INITIALIZER (hplib_spinLock_UNLOCKVAL)


/** 
 * @brief 
 *  This defines the HPLIB read write(rw) lock structure.  
 *
 * @details
 * Pointer to this structure is passed into the HPLIB rw lock function API's to
 * achieve locking synchronization for reading and writing TBD
 */
typedef struct hplib_rwLock_Tag
{
    /**
    * @brief Outer lock, very short duration lock.
    */
    hplib_spinLock_T lock_outer;
    /**
    * @brief Writer lock
    */
    hplib_spinLock_T lock_w;
    /**
    * @brief   Number of active readers of this lock.
     */
    unsigned long n_readers; /* # readers active */
} hplib_rwLock_T;


/** 
 * @brief 
 *  The structure contains the HPLIB 32 Bit Atomic variable definition.
 *
 * @details
 * This structure is passed into the HPLIB 32 BIT specific function API's to
 * peform atomic operations on 64 Bit variables
 */
typedef struct hplib_atomic32_Tag
{
    /**
    * @brief  32 bit atomic variable being operated upon
    */
    long val;
} hplib_atomic32_T;


/** 
 * @brief 
 *  The structure contains the HPLIB 64 Bit Atomic variable definition.
 *
 * @details
 * This structure is passed into the HPLIB 64 BIT specific function API's to
 * peform atomic operations on 64 Bit variables.
 */
typedef struct hplib_atomic64_Tag
{
    /**
    * @brief  HPLIB spinLock used to achieve atomic operation.
    */
    hplib_spinLock_T lock;
    /**
    * @brief  64 bit atomic variable being operated upon
    */
    long long val;
} hplib_atomic64_T;


typedef struct HPLIB_SPINLOCK_IF_Tag
{
    void (*init_lock)(hplib_spinLock_T *);
    int (*try_lock)(hplib_spinLock_T *);
    int (*is_locked)(hplib_spinLock_T *);
    void (*lock)(hplib_spinLock_T *);
    void (*unlock)(hplib_spinLock_T *);
    void (*init_rwLock)(hplib_rwLock_T *);
    void (*rw_lockWriteLock)(hplib_rwLock_T *);
    void (*rw_lockWriteUnLock)(hplib_rwLock_T *);
    void (*rw_lockReadLock)(hplib_rwLock_T *);
    void (*rw_lockReadUnLock)(hplib_rwLock_T *);
} HPLIB_SPINLOCK_IF_T;

/** @addtogroup HPLIB_SYNC_API
 @{ */

/**
 *  @b Description
 *  @n  
 *      Initialize a spinLock in the unlocked state
 *  @param[in] val
 *       a pointer to the spinLock variable
 *  @retval
 *      none
 */
static inline void hplib_mSpinLockInit (hplib_spinLock_T * val)
{
    *val=hplib_spinLock_UNLOCKVAL;
}

/**
 *  @b Description
 *  @n
 *      Acquire a spinLock. Will actively loop waiting for the spinLock to
 *      become available.
 *  @param[in] val
 *       a pointer to the spinLock variable
 *  @retval
 *      none
 */
static inline void hplib_mSpinLockLock(hplib_spinLock_T * val)
{
    while(__sync_lock_test_and_set(val, hplib_spinLock_LOCKVAL))
    {
        asm volatile("wfe" ::  );
    }
}


/**
 *  @b Description
 *  @n
 *      Acquire a spinLock. Will actively loop waiting for the spinLock to
 *      become available, Multi-Process friendly
 *  @param[in] val
 *       a pointer to the spinLock variable
 *  @retval
 *      none
 */
static inline void hplib_mSpinLockLockMP(hplib_spinLock_T * val)
{
    while(__sync_lock_test_and_set(val, hplib_spinLock_LOCKVAL))
    {
        sched_yield();
    }
}


/**
 *  @b Description
 *  @n
 *      Try to acquire a spinLock. Atomically test if the spinLock is available
 *      and acquires it in this case, otherwise do nothing.
 *  @param[in] val
 *       a pointer to the spinLock variable 
 *  @retval
 *      int @ref hplib_OK on sucess, @ref hplib_FAILURE on failure
 *  @pre
 *      @ref hplib_mSpinLockInit
 */
static inline int hplib_mSpinLockTryLock( hplib_spinLock_T* val)
{
    if (__sync_lock_test_and_set(val, hplib_spinLock_LOCKVAL)) 
        return hplib_OK;
    return hplib_FAILURE;
}

/**
 *  @b Description
 *  @n
 *      Release a spinLock.
 *  @param[in] val
 *       a pointer to the spinLock variable
 *  @retval    
 *      none
 *  @pre        
 *      @ref hplib_mSpinLockInit
 */
static inline void hplib_mSpinLockUnlock(hplib_spinLock_T * val)
{
    __sync_lock_release(val);
        asm volatile("sev" ::  );
}

/**
 *  @b Description
 *  @n
 *      Release a spinLock, Multi-Process friendly
 *  @param[in] val
 *       a pointer to the spinLock variable
 *  @retval    
 *      none
 *  @pre        
 *      @ref hplib_mSpinLockInit
 */
static inline void hplib_mSpinLockUnlockMP(hplib_spinLock_T * val)
{
    __sync_lock_release(val);
    asm volatile("sev" ::  );
}

/**
 *  @b Description
 *  @n
 *      Test if a spinLock is locked.
 *  @param[in] val
 *       a pointer to the spinLock variable
 *  @retval
 *      1 if locked, 0 if unlocked
 *  @pre        
 *      @ref hplib_mSpinLockInit
 */
static inline int hplib_mSpinLockIsLocked(hplib_spinLock_T * val)
{
    return *val;
}


/**
 *  @b Description
 *  @n
 *      Initialize a read/write lock in the unlocked state.
 *  @param[in] p_lock
 *      a pointer to read write lock variable  being intialized
 *  @retval
 *      none
 */
static inline void hplib_mRWLockInit(hplib_rwLock_T * p_lock)
{
    hplib_mSpinLockInit(&p_lock->lock_outer);
    hplib_mSpinLockInit(&p_lock->lock_w);
    p_lock->n_readers=0;
}

/**
 *  @b Description
 *  @n
 *     Acquire a read/write lock for writing.
 *     Must actively loop waiting for the read/write lock to become available for
 *     a writer, if the lock is currently read-locked [by other core(s)] or is
 *     write-locked by another core.
 *  @param[in] p_lock
 *      a pointer to read write lock variable  being intialized
 *  @retval    
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */ 
static inline void hplib_mRWLockWriteLock(hplib_rwLock_T * p_lock)
{
    int ret;
    while(1)
    {
        /*get outer lock - now nothing can change */
        hplib_mSpinLockLock(&p_lock->lock_outer);
        /* check for 0 readers */
        if(p_lock->n_readers)
        {
            /*give up outer & spin */
            hplib_mSpinLockUnlock(&p_lock->lock_outer);
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
           continue;
        }

        /*ok, no readers.  see if we can get writer lock */
        ret=hplib_mSpinLockTryLock(&p_lock->lock_w); /*try get writer lock 1 time */
        if(!ret)      
        {
            /*give up outer & spin */
            hplib_mSpinLockUnlock(&p_lock->lock_outer); 
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            continue;  /* try again */
        }
        /* got write lock=> no other writer, no readers,
         keep the writelock but unlock the outer.*/
        hplib_mSpinLockUnlock(&p_lock->lock_outer);
        return;
    }
}

/**
 *  @b Description
 *  @n
 *     Acquire a read/write lock for writing, Multi-Process friendly
 *     Must actively loop waiting for the read/write lock to become available for
 *     a writer, if the lock is currently read-locked [by other core(s)] or is
 *     write-locked by another core.
 *  @param[in] p_lock
 *      a pointer to read write lock variable  being intialized
 *  @retval    
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */ 
static inline void hplib_mRWLockWriteLockMP(hplib_rwLock_T * p_lock)
{
    int ret;
    while(1)
    {
        /* get outer lock - now nothing can change */
        hplib_mSpinLockLockMP(&p_lock->lock_outer);
        /* check for 0 readers */
        if(p_lock->n_readers)
        {
            /* give up outer and try again after yield */
            hplib_mSpinLockUnlockMP(&p_lock->lock_outer);
            sched_yield();
            continue;
        }

        //ok, no readers.  see if we can get writer lock
        ret=hplib_mSpinLockTryLock(&p_lock->lock_w); /*try get writer lock 1 time */
        if(!ret)      
        {
            hplib_mSpinLockUnlockMP(&p_lock->lock_outer); /*give up outer & spin */
            sched_yield();
            continue;
        }
        /* got write lock=> no other writer, no readers,
           keep the writelock but unlock the outer. */
        hplib_mSpinLockUnlockMP(&p_lock->lock_outer);
        return;
    }
}


/**
 *  @b Description
 *  @n
 *      The function API is used to unlock the writer part of a read write lock.
 *  @param[in] p_lock
 *      a pointer to read write lock variable  being intialized
 *  @retval
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */
static inline void hplib_mRWLockWriteUnlock(hplib_rwLock_T * p_lock)
{
    hplib_mSpinLockUnlock(&p_lock->lock_w);
}

/**
 *  @b Description
 *  @n
 *      The function API is used to unlock the writer part of a read write lock.
 *      Multi-Process friendly.
 *  @param[in] p_lock
 *      a pointer to read write lock variable  being intialized
 *  @retval
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */
static inline void hplib_mRWLockWriteUnlockMP(hplib_rwLock_T * p_lock)
{
    hplib_mSpinLockUnlockMP(&p_lock->lock_w);
}


/**
 *  @b Description Acquire a read/write lock for reading.
 *  @n
 *      Acquire a read/write lock for reading. Must actively loop waiting for the 
 *      read/write lock to become available for readers, if the lock is currently 
 *      write-locked [by another core].
 *      -Support for recursivity is not required/expected.
 *      -Support for checking that the read/write lock is not write-locked by
 *        the current executing core is not required/expected.
 *  @param[in] p_lock
 *   A pointer to the read/write lock.
 *  @retval
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */ 
static inline void hplib_mRWLockReadLock(hplib_rwLock_T * p_lock)
{
    int ret;

    while(1)
    {
        /*1st grab outer lock. once we have it, nothing can change */
        hplib_mSpinLockLock(&p_lock->lock_outer);

        /* see if there is a writer */
        ret= hplib_mSpinLockIsLocked(&p_lock->lock_w);

        //there is a writer
        if (ret)
        {
            /*give up outer and spin */
            hplib_mSpinLockUnlock(&p_lock->lock_outer);  
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            asm volatile("nop" ::  );
            continue; 
        }

        /* there is no writer so we can read!*/
        p_lock->n_readers+=1;

        /* make sure every core sees that n_readers has changed */
        __sync_synchronize();  

        /* now give back the outer lock */
        hplib_mSpinLockUnlock(&p_lock->lock_outer);
        return;
    }
}

/**
 *  @b Description Acquire a read/write lock for reading. Multi-Process friendly.
 *  @n
 *      Acquire a read/write lock for reading. Must actively loop waiting for the
 *      read/write lock to become available for readers, if the lock is currently
 *      write-locked [by another core].
 *      -Support for recursivity is not required/expected.
 *      -Support for checking that the read/write lock is not write-locked by
 *        the current executing core is not required/expected.
 *  @param[in] p_lock
 *   A pointer to the read/write lock.
 *  @retval
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */ 
static inline void hplib_mRWLockReadLockMP(hplib_rwLock_T * p_lock)
{
    int ret;

    while(1)
    {
        /*1st grab outer lock. once we have it, nothing can change */
        hplib_mSpinLockLockMP(&p_lock->lock_outer);

        /* see if there is a writer */
        ret= hplib_mSpinLockIsLocked(&p_lock->lock_w);

        //there is a writer
        if (ret)
        {
            /* give up outer and try again after yield */
            hplib_mSpinLockUnlockMP(&p_lock->lock_outer);
            sched_yield();
            continue; 
        }

        /* there is no writer so we can read!*/
        p_lock->n_readers+=1;

        /*make sure every core sees that n_readers has changed */
        __sync_synchronize();

        /* now give back the outer lock */
        hplib_mSpinLockUnlockMP(&p_lock->lock_outer);
        return;
    }
}


/**
 *  @b Description
 *  @n
 *      Release a read/write lock previously acquired for reading.
 *      Multi-Process friendly.
 *  @param[in] p_lock
*   A pointer to the read/write lock.
 *  @retval    
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */
static inline void hplib_mRWLockReadUnlock(hplib_rwLock_T * p_lock)
{
    //grab outer
    hplib_mSpinLockLock(&p_lock->lock_outer);

    //decrement # of readers.  Make sure all cores see update
    p_lock->n_readers--;
    __sync_synchronize();
    
#ifdef DEBUG_NEG_READERS
    if (p_lock->n_readers <0)
        p_lock->n_readers=0;
#endif


    /* give up the outer */
    hplib_mSpinLockUnlock(&p_lock->lock_outer);
}

/**
 *  @b Description
 *  @n
 *      Release a read/write lock previously acquired for reading.
 *      Multi-Process friendly.
 *  @param[in] p_lock
*   A pointer to the read/write lock.
 *  @retval    
 *      none
 *  @pre        @ref hplib_mRWLockInit
 */
static inline void hplib_mRWLockReadUnlockMP(hplib_rwLock_T * p_lock)
{
    //grab outer
    hplib_mSpinLockLockMP(&p_lock->lock_outer);

    //decrement # of readers.  Make sure all cores see update
    p_lock->n_readers--;
    __sync_synchronize();
    
#ifdef DEBUG_NEG_READERS
    if (p_lock->n_readers <0)
        p_lock->n_readers=0;
#endif


    /* give up the outer */
    hplib_mSpinLockUnlockMP(&p_lock->lock_outer);
}


/**
 * @brief  Define used to initialize a @ref hplib_atomic32_T variable and set
 *         the state to unlock
 */
#define hplib_mAtomic32Init(x) {x}


/**
 *  @b Description
 *  @n
 *      Atomically read a 32-bit integer
 *  @param[in] p
 *      a pointer to the 32-bit atomic integer
 *  @retval    
 *      Variable being pointed to by p
 */
static inline int hplib_mAtomic32Read(hplib_atomic32_T *p) {return p->val;}


/**
 *  @b Description
 *  @n  
 *      Atomically set a 32-bit integer
 *  @param[in] p
 *      a pointer to the 32-bit atomic integer
 *  @param[in] val
 *      The variable whose value is being set to the variable that p points to.
 *  @retval    
 *      none
 */
static inline void hplib_mAtomic32Set(hplib_atomic32_T *p, int val) 
{p->val = val; }

/**
 *  @b Description
 *  @n
 *      Atomically add a value to a 32-bit integer
 *  @param[in] p
  *      a pointer to the 32-bit atomic integer
 *  @param[in] val
 *      The value to add to the 32-bit atomic integer.
 *  @retval    
 *      none
 */ 
static inline void hplib_mAtomic32Add(hplib_atomic32_T *p, int val)
{
    __sync_fetch_and_add(&p->val,val);
}



/**
 * @brief  Atomically increment by 1 a 32-bit integer
 */
#define hplib_mAtomic32Inc(p) hplib_mAtomic32Add(p,1);


/**
 *  @b Description
 *  @n
 *      Atomically subtract a value from a 32-bit integer
 *  @param[in] p
  *      a pointer to the 32-bit atomic integer
 *  @param[in] val
 *     The value to subtract from the 32-bit atomic integer.
 *  @retval    
 *      none
 */
static inline void hplib_mAtomic32Sub(hplib_atomic32_T *p, int val)
{__sync_fetch_and_sub(&p->val,val);}


/**
 * @brief  Atomically decrement by 1 a 32-bit integer.
 */
#define hplib_mAtomic32Dec(p) hplib_mAtomic32Sub(p,1);


/**
 *  @b Description
 *  @n
 *     Atomically add a value to a 32-bit integer, and return the new value
 *    of the 32-bit integer after the addition.
 *  @param[in] p
  *      a pointer to the 32-bit atomic integer
 *  @param[in] val
 *      The value to add to the 32-bit atomic integer.
 *  @retval    
 *      result of atomic add operation
 */ 
static inline int hplib_mAtomic32AddReturn(hplib_atomic32_T *p, int val)
{return __sync_add_and_fetch(&p->val,val);}



/**
 *  @b Description
 *  @n
 *      Atomically subtract a value from a 32-bit integer, and return the new
 *      value of the 32-bit integer after the subtraction.
  *  @param[in] p
 *      a pointer to the 32-bit atomic integer
 *  @param[in] val
 *      The value to subtract from the 32-bit atomic integer.
 *  @retval    
 *      result of atomic subtract operation
 */ 
static inline int hplib_mAtomic32SubReturn(hplib_atomic32_T *p, int val)
{return __sync_sub_and_fetch(&p->val,val);}


/**
 *  @b Description
 *  @n
 *      Atomically increment by 1 a 32-bit integer, and return a positive value if
 *      the new value of the 32-bit integer is zero, or zero in all other cases.
 *  @param[in] p
 *      a pointer to the 32-bit atomic integer
 *  @retval    
 *      > 0 if the new value of the 32-bit integer is zero
 *      0 otherwise
 */ 
static inline int hplib_mAtomic32IncAndTest(hplib_atomic32_T *p)
{return !__sync_add_and_fetch(&p->val,1);}



/**
 *  @b Description
 *  @n
 *      Atomically decrement by 1 a 32-bit integer, and return a positive value if
 *      the new value of the 32-bit integer is zero, or zero in all other cases.
 *  @param[in] p
 *      a pointer to the 32-bit atomic integer
 *  @retval    
 *      > 0 if the new value of the 32-bit integer is zero
 *      0 otherwise
 */
static inline int hplib_mAtomic32DecAndTest(hplib_atomic32_T *p)
{return !__sync_sub_and_fetch(&p->val,1);}



/**** the following API is TBD */
/**
 *  @b Description
 *  @n
 *      Atomically test and set to 1 a 32-bit integer.
 *  @param[in] p
 *      a pointer to the 32-bit atomic integer
 *  @retval    
 *     0 if 32-bit integer is already set (operation failed)
 *    1 if 32-bit integer was not set, and has been set (operation succeeded)
 */
static inline int hplib_mAtomic32TestSetReturn(hplib_atomic32_T *p)
{return (! __sync_lock_test_and_set(&p->val, 1));}

/**
 * @brief   Atomically set to zero a 32-bit  integer.
 */
#define hplib_mAtomic32Clear(p) hplib_mAtomic32Set(p,0);

/**
 * @brief  Define used to initialize a @ref hplib_atomic64_T variable and set the
 *         state to unlock
 */
#define hplib_mAtomic64Init(x) {hplib_spinLock_UNLOCKVAL,x}


#ifdef CORTEX_A8
/**
 *  @b Description
 *  @n
 *      Atomically read a 64-bit integer.
 *  @param[in] p
 *      A pointer to the 64-bit atomic integer.
 *  @retval    
 *      64-bit integer being pointed to by p
 */
static inline long long  hplib_mAtomic64Read(hplib_atomic64_T *p)
{
    long long latch_val;
    hplib_mSpinLockLock(&p->lock);  //acquire lock
    latch_val = p->val;
    hplib_mSpinLockUnlock(&p->lock);  //free lock
    return latch_val;
}


/**
 *  @b Description
 *  @n
 *      Atomically set a 64-bit integer
 *  @param[in] p
 *     A pointer to the 64-bit atomic integer.
 *  @param[in] val
 *     The value to set.
 *  @retval    
 *      none
 */
static inline void hplib_mAtomic64Set(hplib_atomic64_T *p,long long val)
{
    hplib_mSpinLockLock(&p->lock);
    p->val = val;
    hplib_mSpinLockUnlock(&p->lock);
}


/**
 *  @b Description
 *  @n
 *      Atomically add a value to a 64-bit integer
 *  @param[in] p
 *     A pointer to the 64-bit atomic integer.
 *  @param[in] val
 *      The value to add to the 64-bit atomic integer.
 *  @retval    
 *      none
 */ 
static inline void hplib_mAtomic64Add(hplib_atomic64_T *p, long long val) 
{
    hplib_mSpinLockLock(&p->lock);
    p->val += val;
    hplib_mSpinLockUnlock(&p->lock);
}
#else
/**
 *  @b Description
 *  @n
 *      Atomically read a 64-bit integer.
 *  @param[in] p
 *      A pointer to the 64-bit atomic integer.
 *  @retval    
 *      64-bit integer being pointed to by p
 */
static inline long long  hplib_mAtomic64Read(hplib_atomic64_T *p)
{
    return __sync_fetch_and_add( &p->val,0);
}

/**
 *  @b Description
 *  @n
 *      Atomically set a 64-bit integer
 *  @param[in] p
 *     A pointer to the 64-bit atomic integer.
 *  @param[in] val
 *     The value to set.
 *  @retval    
 *      none
 */
static inline void hplib_mAtomic64Set(hplib_atomic64_T *p,long long val)
{
    p->val = val;
}

/**
 *  @b Description
 *  @n  
 *      Atomically add a value to a 64-bit integer
 *  @param[in] p
 *     A pointer to the 64-bit atomic integer.
 *  @param[in] val
 *      The value to add to the 64-bit atomic integer.
 *  @retval    
 *      none
 */
static inline void hplib_mAtomic64Add(hplib_atomic64_T *p, long long val) 
{
    __sync_fetch_and_add(&p->val,val);
}
#endif

/**
 *  @b Description
 *  @n
 *     General Memory Barrier guarantees that all LOAD and STORE operations that
 *     were issued before the barrier occur before the LOAD and STORE operations
 *     issued after the barrier.
 *
 */
static inline void hplib_mMemBarrier(void) {__sync_synchronize();}


/**
 *  @b Description
 *  @n
 * Read memory barrier guarantees that all LOAD operations that were issued before
 * the barrier occur before the LOAD operations that are issued after.
 */
static inline void hplib_mReadMemBarrier(void) {__sync_synchronize();}




/**
 *  @b Description
 *  @n
 * Write memory barrier guarantees that all STORE operations that were issued 
 * before the barrier occur before the STORE operations that are issued after.
 */
static inline void hplib_mWriteMemBarrier(void) {__sync_synchronize();}

/**
@}
*/

#ifdef __cplusplus
}
#endif
#endif
